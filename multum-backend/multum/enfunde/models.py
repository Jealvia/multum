from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE
from django.utils import timezone
from maestros.models import Finca
# Create your models here.
class Cinta(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    fecha_desde = models.DateTimeField()
    fecha_hasta = models.DateTimeField()
    anio=models.IntegerField()
    semana=models.IntegerField()
    color=models.CharField(max_length=15,blank=True)
    colorhex=models.CharField(max_length=15,blank=True,null=True)
    activo = models.BooleanField(default=True)   
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "Cinta"
        verbose_name_plural = "Cintas"

    def __str__(self):
        return str(self.id)+"-"+self.color

    def get_absolute_url(self):
        return reverse("Cinta_detail", kwargs={"pk": self.pk})

class TipoEnfunde(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    nombre = models.CharField(max_length=50,blank=True)
    codigo = models.CharField(max_length=50,blank=True)
    activo = models.BooleanField(default=True)   
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "TipoEnfunde"
        verbose_name_plural = "TipoEnfundes"

    def __str__(self):
        return str(self.id)+"-"+self.nombre

    def get_absolute_url(self):
        return reverse("TipoEnfunde_detail", kwargs={"pk": self.pk})

class Enfunde(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    finca_id=models.ForeignKey(Finca,
    related_name='finca_id',on_delete=False)
    tipo_enfunde_id=models.ForeignKey(TipoEnfunde,
    related_name='tipo_enfunde_id',on_delete=False)
    cinta_id=models.ForeignKey(Cinta,
    related_name='cinta_id',on_delete=False)
    fecha_enfunde = models.DateTimeField()
    anio=models.IntegerField()
    semana=models.IntegerField()
    cantidad=models.IntegerField()
    activo = models.BooleanField(default=True)   
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "TipoEnfunde"
        verbose_name_plural = "TipoEnfundes"

    def __str__(self):
        return str(self.id)+"-"+str(self.cantidad)

    def get_absolute_url(self):
        return reverse("TipoEnfunde_detail", kwargs={"pk": self.pk})

class TipoCalibraje(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    nombre=models.CharField(max_length=50,blank=True)
    codigo = models.CharField(max_length=50,blank=True)
    activo = models.BooleanField(default=True)   
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "TipoCalibraje"
        verbose_name_plural = "TipoCalibrajes"

    def __str__(self):
        return str(self.id)+"-"+self.nombre

    def get_absolute_url(self):
        return reverse("TipoCalibraje_detail", kwargs={"pk": self.pk})

class Calibraje(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    finca_id=models.ForeignKey(Finca,
    related_name='cb_finca_id',on_delete=False)
    tipo_calibraje_id=models.ForeignKey(TipoCalibraje,
    related_name='cb_tipo_calibraje_id',on_delete=False)
    cinta_id=models.ForeignKey(Cinta,
    related_name='cb_cinta_id',on_delete=False,null=True)
    fecha_calibraje = models.DateTimeField()
    anio=models.IntegerField()
    semana=models.IntegerField()
    cantidad=models.IntegerField()
    activo = models.BooleanField(default=True)   
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "Calibraje"
        verbose_name_plural = "Calibrajes"

    def __str__(self):
        return str(self.id)+"-"+str(self.cantidad)

    def get_absolute_url(self):
        return reverse("Calibraje_detail", kwargs={"pk": self.pk})