from rest_framework import serializers
from .models import *
from datetime import datetime,date
from maestros.serializers import *


class EnfundeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Enfunde
        fields = ['finca_id','tipo_enfunde_id','cinta_id','fecha_enfunde',
        'anio','semana','cantidad']
    def create(self, validated_data):
            """
            Create and return a new `Enfunde` instance, given the validated data.
            """
            return Enfunde.objects.create(**validated_data)

    def update(self, instance, validated_data):
            """
            Update and return an existing `Enfunde` instance, given the validated data.
            """
            instance.finca_id = validated_data.get('finca_id', instance.finca_id)
            instance.tipo_enfunde_id = validated_data.get('tipo_enfunde_id', instance.tipo_enfunde_id)
            instance.cinta_id = validated_data.get('cinta_id', instance.cinta_id)
            instance.fecha_enfunde = validated_data.get('fecha_enfunde', instance.fecha_enfunde)
            instance.anio = validated_data.get('anio', instance.anio)
            instance.semana = validated_data.get('semana', instance.semana)
            instance.cantidad = validated_data.get('cantidad', instance.cantidad)
            instance.save()
            return instance

class CalibrajeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Calibraje
        fields = ['finca_id','tipo_calibraje_id','fecha_calibraje',
        'anio','semana','cantidad','cinta_id']
    def create(self, validated_data):
            """
            Create and return a new `Enfunde` instance, given the validated data.
            """
            return Calibraje.objects.create(**validated_data)

    def update(self, instance, validated_data):
            """
            Update and return an existing `Enfunde` instance, given the validated data.
            """
            instance.finca_id = validated_data.get('finca_id', instance.finca_id)
            instance.tipo_calibraje_id = validated_data.get('tipo_calibraje_id', instance.tipo_calibraje_id)
            instance.fecha_calibraje = validated_data.get('fecha_calibraje', instance.fecha_calibraje)
            instance.anio = validated_data.get('anio', instance.anio)
            instance.semana = validated_data.get('semana', instance.semana)
            instance.cantidad = validated_data.get('cantidad', instance.cantidad)
            instance.cinta_id = validated_data.get('cinta_id', instance.cinta_id)
            instance.save()
            return instance

class CintaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cinta
        fields = ['id','anio','semana','color','colorhex']

class TipoEnfundeSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoEnfunde
        fields = ['id','nombre']

class TipoCalibrajeSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoCalibraje
        fields = ['id','nombre']

class EnfundeListSerializer(serializers.ModelSerializer):
    cinta_id=serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='color'
     )
    finca_id=FincaListSerializer(many=False, read_only=True)
    tipo_enfunde_id=serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='nombre'
    )
    """ productor=serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='nombre'
     ) """
    class Meta:
        model = Enfunde
        fields = ['finca_id','cinta_id','semana','cantidad','tipo_enfunde_id']

class CalibrajeListSerializer(serializers.ModelSerializer):
    cinta_id=serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='color'
     )
    finca_id=FincaListSerializer(many=False, read_only=True)
    tipo_calibraje_id=serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='nombre'
    )
    """ productor=serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='nombre'
     ) """
    class Meta:
        model = Calibraje
        fields = ['finca_id','cinta_id','semana','cantidad','tipo_calibraje_id']

class EnfundeListMovilSerializer(serializers.ModelSerializer):
    cinta_id=CintaSerializer(many=False, read_only=True)
    tipo_enfunde_id=TipoEnfundeSerializer(many=False, read_only=True)
    finca_id=FincaIdNombre(many=False, read_only=True)
    class Meta:
        model = Enfunde
        fields = ['id','tipo_enfunde_id','cinta_id','semana','anio','cantidad','finca_id','fecha_enfunde']

class CalibrajeListMovilSerializer(serializers.ModelSerializer):
    cinta_id=CintaSerializer(many=False, read_only=True)
    tipo_calibraje_id=TipoCalibrajeSerializer(many=False, read_only=True)
    finca_id=FincaIdNombre(many=False, read_only=True)
    class Meta:
        model = Calibraje
        fields = ['id','tipo_calibraje_id','cinta_id','semana','anio','cantidad','finca_id','fecha_calibraje']

