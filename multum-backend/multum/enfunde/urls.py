from django.conf.urls import url
from .views import *

app_name='enfunde'

urlpatterns = [
    #Movil
    url(r'^crear-enfunde/$', AgregarEnfunde.as_view()),
    url(r'^buscar-enfunde/$', BuscarEnfundeCinta.as_view()),
    url(r'^lista-tipo-enfunde/$', TipoEnfundeList.as_view()),
    url(r'^lista-tipo-calibraje/$', TipoCalibrajeList.as_view()),
    url(r'^registrar-calibraje/$', RegistrarCalibraje.as_view()),
    url(r'^lista-enfunde-movil/anio=(?P<anio>\d+)/sem_desde=(?P<sem_desde>\d+)/sem_hasta=(?P<sem_hasta>\d+)/finca=(?P<finca>\w+)/todos=(?P<todos>\w+)/$', EnfundeListMovil.as_view()),
    url(r'^lista-calibraje-movil/anio=(?P<anio>\d+)/sem_desde=(?P<sem_desde>\d+)/sem_hasta=(?P<sem_hasta>\d+)/finca=(?P<finca>\w+)/todos=(?P<todos>\w+)/$', CalibrajeListMovil.as_view()),
    #Web
    url(r'^lista-enfunde/pagina=(?P<pagina>\d+)/elementos=(?P<elementos>\d+)/anio=(?P<anio>\w+)/sem_desde=(?P<sem_desde>\w+)/sem_hasta=(?P<sem_hasta>\w+)/todos=(?P<todos>\w+)/$', EnfundeListPaging.as_view()),
    url(r'^lista-calibraje/pagina=(?P<pagina>\d+)/elementos=(?P<elementos>\d+)/anio=(?P<anio>\w+)/sem_desde=(?P<sem_desde>\w+)/sem_hasta=(?P<sem_hasta>\w+)/todos=(?P<todos>\w+)/$', CalibrajeListPaging.as_view()),
]