from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE
from django.utils import timezone
from django.contrib.auth.models import User
import os
#Maestros
class TipoIdentificacion(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    nombre = models.CharField(max_length=50,blank=True)
    codigo = models.CharField(max_length=50,blank=True)
    activo = models.BooleanField(default=True)   
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "TipoIdentificacion"
        verbose_name_plural = "TipoIdentificaciones"

    def __str__(self):
        return str(self.id)+"-"+self.nombre

    def get_absolute_url(self):
        return reverse("TipoIdentificacion_detail", kwargs={"pk": self.pk})

class TipoRol(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    nombre = models.CharField(max_length=50,blank=True)
    codigo = models.CharField(max_length=50,blank=True)
    activo = models.BooleanField(default=True)
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "TipoRol"
        verbose_name_plural = "TipoRoles"

    def __str__(self):
        return str(self.id)+"-"+self.nombre

    def get_absolute_url(self):
        return reverse("TipoRol_detail", kwargs={"pk": self.pk})

class AgroArtesanal(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    nombre = models.CharField(max_length=255,blank=True)
    activo = models.BooleanField(default=True)
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "AgroArtesanal"
        verbose_name_plural = "AgroArtesanales"

    def __str__(self):
        return str(self.id)+"-"+self.nombre

    def get_absolute_url(self):
        return reverse("AgroArtesanal_detail", kwargs={"pk": self.pk})

class TipoFinca(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    nombre = models.CharField(max_length=255,blank=True)
    codigo = models.CharField(max_length=50,blank=True)
    activo = models.BooleanField(default=True)
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "TipoFinca"
        verbose_name_plural = "TipoFincas"

    def __str__(self):
        return str(self.id)+"-"+self.nombre

    def get_absolute_url(self):
        return reverse("TipoFinca_detail", kwargs={"pk": self.pk})

class Sector(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    nombre = models.CharField(max_length=255,blank=True)
    activo = models.BooleanField(default=True)
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "Sector"
        verbose_name_plural = "Sectores"

    def __str__(self):
        return str(self.id)+"-"+self.nombre

    def get_absolute_url(self):
        return reverse("Sector_detail", kwargs={"pk": self.pk})

class Vapor(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    nombre = models.CharField(max_length=255,blank=True)
    activo = models.BooleanField(default=True)
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "Vapor"
        verbose_name_plural = "Vapores"

    def __str__(self):
        return str(self.id)+"-"+self.nombre

    def get_absolute_url(self):
        return reverse("Vapor_detail", kwargs={"pk": self.pk})

class Puerto(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    nombre = models.CharField(max_length=255,blank=True)
    activo = models.BooleanField(default=True)
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "Puerto"
        verbose_name_plural = "Puertos"

    def __str__(self):
        return str(self.id)+"-"+self.nombre

    def get_absolute_url(self):
        return reverse("Puerto_detail", kwargs={"pk": self.pk})

class Persona(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    tipo_identificacion_id=models.ForeignKey(TipoIdentificacion,
    related_name='tipo_identificacion_id',on_delete=False)

    user = models.ForeignKey(User,on_delete=False)
    identificacion = models.CharField(max_length=25,blank=True)
    primer_apellido = models.CharField(max_length=50,blank=True,null=True)
    segundo_apellido = models.CharField(max_length=50,blank=True,null=True)
    primer_nombre = models.CharField(max_length=50,blank=True,null=True)
    segundo_nombre = models.CharField(max_length=50,blank=True,null=True)
    nombre=models.CharField(max_length=300,blank=True,null=True)
    fecha_nacimiento = models.DateTimeField(default=timezone.now,null=True)
    activo = models.BooleanField(default=True)
    telefono=models.CharField(max_length=25,blank=True,null=True)
    direccion=models.CharField(max_length=100,blank=True,null=True)
    
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "Persona"
        verbose_name_plural = "Personas"

    def __str__(self):
        return str(self.id)+"-"+self.nombre

    def get_absolute_url(self):
        return reverse("Persona_detail", kwargs={"pk": self.pk})

class Finca(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    area_total = models.CharField(max_length=20,blank=True)
    area_cultivada = models.CharField(max_length=50,blank=True)
    cajas_por_semana = models.CharField(max_length=50,blank=True)
    codigo_magap = models.CharField(max_length=50,blank=True)
    tipo_finca_id=models.ForeignKey(TipoFinca,
    related_name='tipo_finca_id',on_delete=False)
    sector_id=models.ForeignKey(Sector,
    related_name='sector_id',on_delete=False)
    productor_id=models.ForeignKey(Persona,
    related_name='fn_productor_id',on_delete=False,null=True)
    tecnico_id=models.ForeignKey(Persona,
    related_name='fn_tecnico_id',on_delete=False,null=True)
    calificador_id=models.ForeignKey(Persona,
    related_name='fn_calificador_id',on_delete=False,null=True)
    nombre = models.CharField(max_length=100,blank=True)

    activo = models.BooleanField(default=True)
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "Finca"
        verbose_name_plural = "Fincas"

    def __str__(self):
        return str(self.id)+"-"+self.nombre

    def get_absolute_url(self):
        return reverse("Finca_detail", kwargs={"pk": self.pk})

class TipoNotificacion(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    descripcion = models.CharField(max_length=255,blank=True)
    codigo = models.CharField(max_length=25,blank=True)

    activo = models.BooleanField(default=True)
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "TipoNotificacion"
        verbose_name_plural = "TipoNotificaciones"

    def __str__(self):
        return str(self.id)+"-"+self.descripcion

    def get_absolute_url(self):
        return reverse("TipoNotificacion_detail", kwargs={"pk": self.pk})

class Notificacion(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    titulo = models.CharField(max_length=120,blank=True)
    cuerpo = models.CharField(max_length=500,blank=True)
    fecha_leido = models.DateTimeField()
    tipo_notificacion_id=models.ForeignKey(TipoNotificacion,related_name='tipo_notificacion_id',
        on_delete=False,null=True)
    server_movil = models.BooleanField(default=True)
    productor_id=models.ForeignKey(Persona,
        related_name='not_productor_id',on_delete=False,null=True)
    empleado_id=models.ForeignKey(Persona,
        related_name='not_empleado_id',on_delete=False,null=True)

    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "Notificacion"
        verbose_name_plural = "Notificaciones"

    def __str__(self):
        return str(self.id)+"-"+self.titulo

    def get_absolute_url(self):
        return reverse("Notificacion_detail", kwargs={"pk": self.pk})

class TipoCaja(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    nombre = models.CharField(max_length=255,blank=True)
    codigo = models.CharField(max_length=20,blank=True)
    peso=models.FloatField(blank=True,null=True)
    segunda = models.BooleanField(default=True)
    activo = models.BooleanField(default=True)
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "TipoCaja"
        verbose_name_plural = "TipoCajas"

    def __str__(self):
        return str(self.id)+"-"+self.nombre

    def get_absolute_url(self):
        return reverse("TipoCaja_detail", kwargs={"pk": self.pk})