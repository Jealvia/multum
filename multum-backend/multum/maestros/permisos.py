from rest_framework import permissions

class PruebaPermission(permissions.BasePermission):
    """
    Global permission check for blacklisted IPs.
    """

    def has_permission(self, request, view):
        print(request)
        return True