from drf_extra_fields.fields import Base64ImageField
from rest_framework import serializers
from .models import *
from datetime import datetime,date
from registro.serializers import PersonaRolSerializer

class PersonaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Persona
        fields = ['tipo_identificacion_id','user','primer_apellido','identificacion',
        'segundo_apellido','primer_nombre','segundo_nombre','nombre','fecha_nacimiento',
        'telefono','direccion']
    def create(self, validated_data):
            """
            Create and return a new `Persona` instance, given the validated data.
            """
            return Persona.objects.create(**validated_data)

    def update(self, instance, validated_data):
            """
            Update and return an existing `Persona` instance, given the validated data.
            """
            instance.tipo_identificacion_id = validated_data.get('tipo_identificacion_id', instance.tipo_identificacion_id)
            instance.user = validated_data.get('user', instance.user)
            instance.primer_apellido = validated_data.get('primer_apellido', instance.primer_apellido)
            instance.segundo_apellido = validated_data.get('segundo_apellido', instance.segundo_apellido)
            instance.primer_nombre = validated_data.get('primer_nombre', instance.primer_nombre)
            instance.segundo_nombre = validated_data.get('segundo_nombre', instance.segundo_nombre)
            instance.nombre = validated_data.get('nombre', instance.nombre)
            instance.fecha_nacimiento = validated_data.get('fecha_nacimiento', instance.fecha_nacimiento)
            instance.telefono = validated_data.get('telefono', instance.telefono)
            instance.direccion = validated_data.get('direccion', instance.direccion)
            instance.save()
            return instance

class FincaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Finca
        fields = ['area_total','area_cultivada','cajas_por_semana','codigo_magap',
        'tipo_finca_id','sector_id','productor_id','nombre']
    def create(self, validated_data):
            """
            Create and return a new `Finca` instance, given the validated data.
            """
            return Finca.objects.create(**validated_data)

    def update(self, instance, validated_data):
            """
            Update and return an existing `Finca` instance, given the validated data.
            """
            instance.area_total = validated_data.get('area_total', instance.area_total)
            instance.area_cultivada = validated_data.get('area_cultivada', instance.area_cultivada)
            instance.cajas_por_semana = validated_data.get('cajas_por_semana', instance.cajas_por_semana)
            instance.codigo_magap = validated_data.get('codigo_magap', instance.codigo_magap)
            instance.tipo_finca_id = validated_data.get('tipo_finca_id', instance.tipo_finca_id)
            instance.sector_id = validated_data.get('sector_id', instance.sector_id)
            instance.productor_id = validated_data.get('productor_id', instance.productor_id)
            instance.nombre = validated_data.get('nombre', instance.nombre)
            instance.save()
            return instance

class VaporSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vapor
        fields = ['nombre']
    def create(self, validated_data):
            """
            Create and return a new `Vapor` instance, given the validated data.
            """
            return Vapor.objects.create(**validated_data)

    def update(self, instance, validated_data):
            """
            Update and return an existing `Vapor` instance, given the validated data.
            """
            instance.nombre = validated_data.get('nombre', instance.nombre)
            instance.save()
            return instance

class PuertoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Puerto
        fields = ['nombre']
    def create(self, validated_data):
            """
            Create and return a new `Puerto` instance, given the validated data.
            """
            return Puerto.objects.create(**validated_data)

    def update(self, instance, validated_data):
            """
            Update and return an existing `Puerto` instance, given the validated data.
            """
            instance.nombre = validated_data.get('nombre', instance.nombre)
            instance.save()
            return instance


#Serializers para las listas
class FincaListSerializer(serializers.ModelSerializer):
    productor_id=serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='nombre'
     )
    class Meta:
        model = Finca
        fields = ['id','nombre','productor_id']

class FincaIdNombre(serializers.ModelSerializer):
    class Meta:
        model = Finca
        fields = ['id','nombre']

class FincaListMovilSerializer(serializers.ModelSerializer):
    tipo_finca_id=FincaIdNombre(many=False, read_only=True)
    class Meta:
        model = Finca
        fields = ['id','area_total','area_cultivada','cajas_por_semana','codigo_magap',
        'tipo_finca_id','sector_id','productor_id','nombre']

class TipoFincaListSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoFinca
        fields = ['id','nombre']

class TipoCajaListSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoCaja
        fields = ['id','nombre','peso']

class SectorListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sector
        fields = ['id','nombre']

class VaporListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vapor
        fields = ['id','nombre']

class PuertoListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Puerto
        fields = ['id','nombre']

class PersonaListSerializer(serializers.ModelSerializer):
    user=serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='email'
     )
    persona_id=serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='aprobado'
     )
    fn_productor_id=FincaSerializer(many=True, read_only=True)
    class Meta:
        model = Persona
        fields = ['id','user','nombre','identificacion','telefono','direccion','persona_id','fn_productor_id']
