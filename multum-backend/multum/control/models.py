from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE
from django.utils import timezone
from envio.models import OrdenCorte
from maestros.models import TipoCaja

class ControlColor(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE
    
    color = models.CharField(max_length=12,blank=True)
    orden_enfunde=models.IntegerField()
    orden_edad=models.IntegerField()
    activo = models.BooleanField(default=True)   
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "ControlColor"
        verbose_name_plural = "ControlColores"

    def __str__(self):
        return str(self.id)+"-"+self.color

    def get_absolute_url(self):
        return reverse("ControlColor_detail", kwargs={"pk": self.pk})

class RacimoControl(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    orden_corte_id=models.ForeignKey(OrdenCorte,
    related_name='rc_orden_corte_id',on_delete=False,blank=True,null=True)
    controlcolor_id=models.ForeignKey(ControlColor,
    related_name='rc_controlcolor_id',on_delete=False,blank=True,null=True)
    edad=models.IntegerField(null=True)
    rechazo = models.BooleanField(default=False)
    cantidad=models.IntegerField()
    activo = models.BooleanField(default=True)   
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "RacimoControl"
        verbose_name_plural = "RacimoControles"

    def __str__(self):
        return str(self.id)+"-"+str(self.cantidad)

    def get_absolute_url(self):
        return reverse("RacimoControl_detail", kwargs={"pk": self.pk})

class CajaControl(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    orden_corte_id=models.ForeignKey(OrdenCorte,
    related_name='cc_orden_corte_id',on_delete=False,blank=True,null=True)
    tipo_caja_id=models.ForeignKey(TipoCaja,
    related_name='cc_tipo_caja_id',on_delete=False,blank=True,null=True)
    segunda = models.BooleanField(default=False)
    cantidad=models.IntegerField()
    activo = models.BooleanField(default=True)   
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "CajaControl"
        verbose_name_plural = "CajaControles"

    def __str__(self):
        return str(self.id)+"-"+str(self.cantidad)

    def get_absolute_url(self):
        return reverse("CajaControl_detail", kwargs={"pk": self.pk})

class ControlEdad(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE
    
    edad_minima=models.IntegerField()
    edad_maxima=models.IntegerField()
    edad_referencia=models.IntegerField()
    orden_maximo=models.IntegerField()
    activo = models.BooleanField(default=True)   
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "ControlEdad"
        verbose_name_plural = "ControlEdades"

    def __str__(self):
        return str(self.id)+"-"+str(self.edad_minima)

    def get_absolute_url(self):
        return reverse("ControlEdad_detail", kwargs={"pk": self.pk})



