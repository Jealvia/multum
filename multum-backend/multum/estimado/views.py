from rest_framework.parsers import JSONParser,FormParser
from rest_framework import generics,status,viewsets
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
#from rest_framework_jwt.settings import api_settings
from rest_framework.authtoken.models import Token
#import jwt
from django.contrib.auth.models import User

from django.shortcuts import get_object_or_404,render,redirect
from django.http import Http404,JsonResponse,HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .serializers import *
import random as rd
import os
import time
import json
from django.db.models import Subquery,F,Func
from django.db.models.functions import *
import math

from datetime import datetime,date
from rest_framework.authentication import SessionAuthentication, BasicAuthentication,TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.db.models import Q

from fcm_django.models import FCMDevice
from rest_framework.parsers import FileUploadParser,MultiPartParser,FormParser
from .models import EstimadoCaja
from enfunde.models import Cinta

#Manejo de inicio y registro de usuarios
class RegistrarEstimadoCaja(APIView):
    authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated] 
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        #Data recibida y parseo de datos
        fecha_enfunde = body['fecha_estimado']
        cinta_id = body['cinta_id']
        finca_id= body['finca_id']
        estimado= body['estimado']
        semana= body['semana']
        anio= body['anio']
        id=body['id']
        datos={
            'finca_id':finca_id,
            'fecha_estimado':fecha_enfunde,
            'anio':anio,
            'semana':semana,
            'cantidad':estimado
        }
        if id!=0:
            estimado_tmp=EstimadoCaja.objects.all().filter(id=id).first()
            serializer = EstimadoCajaSerializer(estimado_tmp,data=datos)
            print(serializer.is_valid())
            print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
                return Response(
                {'mensaje': 'Guardado satisfactoriamente',
                'status': 'success'},)
            else:
                return Response(
                {'mensaje': 'Ocurrio un error en el servicio',
                'status': 'failed'},)
        else:
            serializer = EstimadoCajaSerializer(data=datos)
            print(serializer.is_valid())
            print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
                return Response(
                {'mensaje': 'Guardado satisfactoriamente',
                'status': 'success'},)
            else:
                return Response(
                {'mensaje': 'Ocurrio un error en el servicio',
                'status': 'failed'},)

class MEstimadoList(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated] 
    def get(self, request,finca,anio,sem_desde,sem_hasta,todos):
        if todos=="true":
            queryset = EstimadoCaja.objects.all().filter(
                finca_id__productor_id__user__id=request.user.id,
                anio=anio,
                semana__gte=sem_desde,
                semana__lte=sem_hasta
            )
            serializer = MEstimadoCajaListSerializer(queryset, many = True)
            datos={
                'data':serializer.data,
                'length':queryset.count()
            }
            return JsonResponse(datos, safe = False)
        elif todos=="false":
            queryset = EstimadoCaja.objects.all().filter(
                finca_id=finca,
                anio=anio,
                semana__gte=sem_desde,
                semana__lte=sem_hasta
            )
            serializer = MEstimadoCajaListSerializer(queryset, many = True)
            datos={
                'data':serializer.data,
                'length':queryset.count()
            }
            return JsonResponse(datos, safe = False)
        else:
            queryset = EstimadoCaja.objects.all().filter(
                finca_id__productor_id__user__id=request.user.id
            ).order_by('-created_date')
            serializer = MEstimadoCajaListSerializer(queryset, many = True)
            datos={
                'data':serializer.data,
                'length':queryset.count()
            }
            return JsonResponse(datos, safe = False)



#Apis de web
class EstimadoListPaging(APIView):
    """ authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]  """
    def get(self, request,pagina,elementos,anio,sem_desde,sem_hasta,todos):
        inicio=0
        if pagina!="1":
            inicio=(int(pagina)-1)*int(elementos)
        
        fin=int(elementos)*int(pagina)
        if anio!="null":
            if todos=="true":
                queryset = EstimadoCaja.objects.all().filter(
                    anio=anio,
                    semana__gte=sem_desde,
                    semana__lte=sem_hasta
                )
                serializer = EstimadoCajaListSerializer(queryset, many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)
            else:
                queryset = EstimadoCaja.objects.all().filter(
                    anio=anio,
                    semana__gte=sem_desde,
                    semana__lte=sem_hasta
                )
                serializer = EstimadoCajaListSerializer(queryset[inicio:fin], many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)
        else:
            if todos=="true":
                queryset = EstimadoCaja.objects.all()
                serializer = EstimadoCajaListSerializer(queryset, many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)
            else:
                queryset = EstimadoCaja.objects.all()
                serializer = EstimadoCajaListSerializer(queryset[inicio:fin], many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)

class EstimadoSeleccionListPaging(APIView):
    """ authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]  """
    def get(self, request,pagina,elementos,anio,sem_desde,sem_hasta,todos):
        inicio=0
        if pagina!="1":
            inicio=(int(pagina)-1)*int(elementos)
        
        fin=int(elementos)*int(pagina)
        if anio!="null":
            if todos=="true":
                queryset = EstimadoCaja.objects.all().filter(
                    anio=anio,
                    semana__gte=sem_desde,
                    semana__lte=sem_hasta,
                    bloqueado=False
                )
                serializer = EstimadoCajaListSerializer(queryset, many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)
            else:
                queryset = EstimadoCaja.objects.all().filter(
                    anio=anio,
                    semana__gte=sem_desde,
                    semana__lte=sem_hasta,
                    bloqueado=False
                )
                serializer = EstimadoCajaListSerializer(queryset[inicio:fin], many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)
        else:
            if todos=="true":
                queryset = EstimadoCaja.objects.all().filter(bloqueado=False)
                serializer = EstimadoCajaListSerializer(queryset, many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)
            else:
                queryset = EstimadoCaja.objects.all().filter(bloqueado=False)
                serializer = EstimadoCajaListSerializer(queryset[inicio:fin], many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)

