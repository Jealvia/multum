from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE
from django.utils import timezone
from maestros.models import Persona,Finca
# Create your models here.

class EstimadoCaja(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    finca_id = models.ForeignKey(Finca,
    related_name='ec_finca_id',on_delete=False)
    
    fecha_estimado = models.DateTimeField()
    anio=models.IntegerField()
    semana=models.IntegerField()
    cantidad=models.IntegerField()
    activo = models.BooleanField(default=True)
    bloqueado = models.BooleanField(default=False)
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "EstimadoCaja"
        verbose_name_plural = "EstimadoCajas"

    def __str__(self):
        return str(self.id)+"-"+str(self.cantidad)

    def get_absolute_url(self):
        return reverse("EstimadoCaja_detail", kwargs={"pk": self.pk})
