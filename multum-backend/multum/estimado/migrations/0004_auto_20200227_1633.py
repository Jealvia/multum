# Generated by Django 2.2.7 on 2020-02-27 16:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('estimado', '0003_estimadocaja_bloqueado'),
    ]

    operations = [
        migrations.RenameField(
            model_name='estimadocaja',
            old_name='estimado',
            new_name='cantidad',
        ),
    ]
