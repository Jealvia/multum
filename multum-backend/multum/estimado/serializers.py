from rest_framework import serializers
from .models import *
from datetime import datetime,date
from maestros.serializers import *

class EstimadoCajaSerializer(serializers.ModelSerializer):
    class Meta:
        model = EstimadoCaja
        fields = ['finca_id','fecha_estimado','anio','semana','cantidad','bloqueado']
    def create(self, validated_data):
            """
            Create and return a new `EstimadoCaja` instance, given the validated data.
            """
            return EstimadoCaja.objects.create(**validated_data)

    def update(self, instance, validated_data):
            """
            Update and return an existing `EstimadoCaja` instance, given the validated data.
            """
            instance.finca_id = validated_data.get('finca_id', instance.finca_id)
            instance.fecha_estimado = validated_data.get('fecha_estimado', instance.fecha_estimado)
            instance.anio = validated_data.get('anio', instance.anio)
            instance.semana = validated_data.get('semana', instance.semana)
            instance.cantidad = validated_data.get('cantidad', instance.cantidad)
            instance.bloqueado = validated_data.get('bloqueado', instance.bloqueado)
            instance.save()
            return instance

#Listas
class EstimadoCajaListSerializer(serializers.ModelSerializer):
    finca_id=FincaListSerializer(many=False, read_only=True)
    class Meta:
        model = EstimadoCaja
        fields = ['id','finca_id','semana','cantidad','anio']

class MEstimadoCajaListSerializer(serializers.ModelSerializer):
    finca_id=FincaIdNombre(many=False, read_only=True)
    class Meta:
        model = EstimadoCaja
        fields = ['id','finca_id','semana','cantidad','anio','fecha_estimado','bloqueado']
