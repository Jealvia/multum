from rest_framework.parsers import JSONParser,FormParser
from rest_framework import generics,status,viewsets
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
#from rest_framework_jwt.settings import api_settings
from rest_framework.authtoken.models import Token
#import jwt
from django.contrib.auth.models import User

from django.shortcuts import get_object_or_404,render,redirect
from django.http import Http404,JsonResponse,HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .serializers import *
from .models import Embarque
from control.models import CajaControl
from estimado.models import EstimadoCaja
from estimado.serializers import EstimadoCajaSerializer
from envio.serializers import OrdenCorteGuardadoSerializer
from enfunde.models import Enfunde
import random as rd
import os
import time
import json
from django.db.models import Subquery,F,Func
from django.db.models.functions import *
import math

from datetime import datetime,date
from rest_framework.authentication import SessionAuthentication, BasicAuthentication,TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.db.models import Q

from fcm_django.models import FCMDevice
from rest_framework.parsers import FileUploadParser,MultiPartParser,FormParser
from django.db.models import Sum


class OrdenCorteList(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated] 
    def get(self, request,anio,sem_desde,sem_hasta,finca,todos):
        if todos=="true":
            queryset = OrdenCorte.objects.all().filter(
                finca_id__productor_id__user__id=request.user.id,
                anio=anio,
                semana__gte=sem_desde,
                semana__lte=sem_hasta
            ).order_by('-fecha_corte')
            serializer = OrdenCorteSerializer(queryset, many = True)
            return JsonResponse(serializer.data, safe = False)
        elif todos=="false":
            queryset = OrdenCorte.objects.all().filter(
                finca_id=finca,
                anio=anio,
                semana__gte=sem_desde,
                semana__lte=sem_hasta
            ).order_by('-fecha_corte')
            serializer = OrdenCorteSerializer(queryset, many = True)
            return JsonResponse(serializer.data, safe = False)
        else:
            queryset = OrdenCorte.objects.all().filter(
                finca_id__productor_id__user__id=request.user.id
            ).order_by('-fecha_corte')
            serializer = OrdenCorteSerializer(queryset, many = True)
            return JsonResponse(serializer.data, safe = False)

class MarcaCajaList(APIView):
    """ authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]  """
    def get(self, request):
        queryset = MarcaCaja.objects.all().order_by('-nombre')
        serializer = MarcaCajaSerializer(queryset, many = True)
        return JsonResponse(serializer.data, safe = False)

class OrdenCorteControlList(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated] 
    def get(self, request):
        queryset = OrdenCorte.objects.all().filter(cajas_terminado=False,finca_id__productor_id__user__id=request.user.id).order_by('-fecha_corte')
        serializer = OrdenCorteSerializer(queryset, many = True)
        return JsonResponse(serializer.data, safe = False)

class OrdenCorteControRacimolList(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated] 
    def get(self, request):
        queryset = OrdenCorte.objects.all().filter(racimo_terminado=False,finca_id__productor_id__user__id=request.user.id).order_by('-fecha_corte')
        serializer = OrdenCorteSerializer(queryset, many = True)
        return JsonResponse(serializer.data, safe = False)

#Modelos para la web
class OrdenCorteListPaging(APIView):
    """ authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]  """
    def get(self, request,pagina,elementos,anio,sem_desde,sem_hasta,embarque_id,todos):
        inicio=0
        if pagina!="1":
            inicio=(int(pagina)-1)*int(elementos)
        fin=int(elementos)*int(pagina)
        #order_by('-pub_date', 'headline')
        if embarque_id!=0:
            queryset=OrdenCorte.objects.all().filter(
                embarque_id=embarque_id
            )
        if anio!="null":
            queryset=queryset.filter(
                anio=anio,
                semana__gte=sem_desde,
                semana__lte=sem_hasta
            )
            if todos:
                serializer = OrdenCorteListSerializer(queryset, many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)
            else:
                serializer = OrdenCorteListSerializer(queryset[inicio:fin], many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)
        else:
            if todos:
                serializer = OrdenCorteListSerializer(queryset, many = True)
                datos={
                    'data':serializer.data,
                    'length':OrdenCorte.objects.all().count()
                }
                return JsonResponse(datos, safe = False)
            else:
                serializer = OrdenCorteListSerializer(queryset[inicio:fin], many = True)
                datos={
                    'data':serializer.data,
                    'length':OrdenCorte.objects.all().count()
                }
                return JsonResponse(datos, safe = False)

class CrearEmbarque(APIView):
    """ authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated]  """
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        #Data recibida y parseo de datos
        
        id = body['id']
        codigo_alterno = body['codigo_alterno']
        fecha_embarque_desde = body['fecha_embarque_desde']
        fecha_embarque_hasta = body['fecha_embarque_hasta']
        anio = body['anio']
        semana = body['semana']
        cantidad = body['cantidad']
        peso = body['peso']
        largo = body['largo']
        calidad_minima = body['calidad_minima']
        calidad_maxima = body['calidad_maxima']
        precio = body['precio']
        puerto_salida_id = body['puerto_salida_id']
        puerto_destino_id = body['puerto_destino_id']
        vapor_id = body['vapor_id']
        marca_caja_id = body['marca_caja_id']
        datos={
            'codigo_alterno':codigo_alterno,
            'fecha_embarque_desde':fecha_embarque_desde,
            'fecha_embarque_hasta':fecha_embarque_hasta,
            'anio':anio,
            'semana':semana,
            'cantidad':cantidad,
            'peso':peso,
            'largo':largo,
            'calidad_minima':calidad_minima,
            'calidad_maxima':calidad_maxima,
            'precio':precio,
            'puerto_salida_id':puerto_salida_id,
            'puerto_destino_id':puerto_destino_id,
            'vapor_id':vapor_id,
            'marca_caja_id':marca_caja_id
        }
        
        if id==0:
            serializer = EnvioEmbarqueSerializer(data=datos)
            print(serializer.is_valid())
            print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
                return Response(
                {'mensaje': 'Guardado satisfactoriamente',
                'status': 'success'},)
            else:
                return Response(
                {'mensaje': 'Ocurrio un error en el servicio',
                'status': 'failed'},)
        else:
            embarque=Embarque.objects.all().filter(
                id=id
            ).first()
            serializer = EnvioEmbarqueSerializer(embarque,data=datos)
            print(serializer.is_valid())
            print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
                return Response(
                {'mensaje': 'Guardado satisfactoriamente',
                'status': 'success'},)
            else:
                return Response(
                {'mensaje': 'Ocurrio un error en el servicio',
                'status': 'failed'},)

class EmbarqueListPaging(APIView):
    """ authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]  """
    def get(self, request,pagina,elementos,anio,sem_desde,sem_hasta,todos):
        inicio=0
        if pagina!="1":
            inicio=(int(pagina)-1)*int(elementos)
        
        fin=int(elementos)*int(pagina)
        if anio!="null":
            if todos=="true":
                queryset = Embarque.objects.all().filter(
                        anio=anio,
                        semana__gte=sem_desde,
                        semana__lte=sem_hasta).order_by('-created_date')
                serializer = EmbarqueListPagingSerializer(queryset, many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)
            else:
                queryset = Embarque.objects.all().filter(
                        anio=anio,
                        semana__gte=sem_desde,
                        semana__lte=sem_hasta).order_by('-created_date')
                serializer = EmbarqueListPagingSerializer(queryset[inicio:fin], many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)
        else:
            if todos=="true":
                queryset = Embarque.objects.all().order_by('-created_date')
                serializer = EmbarqueListPagingSerializer(queryset, many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)
            else:
                queryset = Embarque.objects.all().order_by('-created_date')
                serializer = EmbarqueListPagingSerializer(queryset[inicio:fin], many = True)
                datos={
                    'data':serializer.data,
                    'length':queryset.count()
                }
                return JsonResponse(datos, safe = False)

class EmbarqueSeleccionEstimado(APIView):
    """ authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated]  """
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        #Data recibida y parseo de datos ponlo de color blanco

        id = body['embarque_id']
        estimados = body['estimados']
        for estimado in estimados:
            tmp_query=EstimadoCaja.objects.filter(id=estimado).first()
            datos={
                'codigo_bodega':"skd",
                'anio':tmp_query.anio,
                'semana':tmp_query.semana,
                'cantidad':tmp_query.cantidad,
                'facturado':False,
                'finca_id':tmp_query.finca_id.id,
                'embarque_id':id,
                'estimado_id':estimado
            }
            serializer = OrdenCorteGuardadoSerializer(data=datos)
            print(serializer.is_valid())
            print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
            datos={
                'finca_id':tmp_query.finca_id.id,
                'fecha_estimado':tmp_query.fecha_estimado,
                'anio':tmp_query.anio,
                'semana':tmp_query.semana,
                'cantidad':tmp_query.cantidad,
                'bloqueado':True
            }
            serializer = EstimadoCajaSerializer(tmp_query,data=datos)
            print(serializer.is_valid())
            print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
        return Response(
                {'mensaje': 'Guardado satisfactoriamente',
                'status': 'success'},)

class VistaEmbarqueOrdenCorte(APIView):
    """ authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated]  """
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        #Data recibida y parseo de datos
        id = body['id']
        embarque=Embarque.objects.filter(id=id).first()
        if(embarque):
            serializer=EmbarqueVistaSerializer(embarque)
            orden_corte=OrdenCorte.objects.filter(embarque_id=id)
            if(orden_corte):
                serializer_orden=OrdenCorteEmbarqueSerializer(orden_corte, many=True)
                return Response(
                    {'embarque':serializer.data ,
                    'ordencortelist':serializer_orden.data },)
            return Response(
                {'embarque':serializer.data ,
                'ordencortelist':[] },)
        else:
            return Response(
            {'mensaje': 'Ocurrio un error en el servicio',
            'status': 'failed'},)

class AplicarOrdenCorte(APIView):
    """ authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated]  """
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        #Data recibida y parseo de datos
        ordencortelist = body['ordencortelist']
        embarqueid = body['embarqueId']
        total = body['total']
        finalizado = body['finalizado']
        if(finalizado):#EmbarqueFinalizadoSerializer
            embarque=Embarque.objects.filter(id=embarqueid).first()
            datos={
                'finalizado':True,
                'cantidad_actual':total
            }
            serializer=EmbarqueFinalizadoSerializer(embarque,data=datos)
            print(serializer.is_valid())
            print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
            else:
                return Response(
                    {'mensaje': 'Ocurrio un error en el servicio',
                    'status': 'failed'},)
            for ordencorte in ordencortelist:
                orden=OrdenCorte.objects.filter(id=ordencorte['id']).first()
                if(ordencorte['valor']==0):
                    tmp_query=EstimadoCaja.objects.filter(id=orden.estimado_id.id).first()
                    datos={
                        'finca_id':tmp_query.finca_id.id,
                        'fecha_estimado':tmp_query.fecha_estimado,
                        'anio':tmp_query.anio,
                        'semana':tmp_query.semana,
                        'cantidad':tmp_query.cantidad,
                        'bloqueado':False
                    }
                    serializer = EstimadoCajaSerializer(tmp_query,data=datos)
                    print(serializer.is_valid())
                    print(serializer.errors)
                    if serializer.is_valid():
                        serializer.save()
                    else:
                        return Response(
                        {'mensaje': 'Ocurrio un error en el servicio',
                        'status': 'failed'},)
                    orden.delete()
                else:
                    tmp_query=EstimadoCaja.objects.filter(id=orden.estimado_id.id).first()
                    datos={
                        'codigo_bodega':"skd",
                        'anio':tmp_query.anio,
                        'semana':tmp_query.semana,
                        'cantidad':ordencorte['valor'],
                        'facturado':False,
                        'finca_id':tmp_query.finca_id.id,
                        'embarque_id':orden.embarque_id.id,
                        'estimado_id':tmp_query.id,
                        #'calificador_id':tmp_query.finca_id.calificador_id.id
                    }
                    serializer = OrdenCorteGuardadoSerializer(orden,data=datos)
                    print(serializer.is_valid())
                    print(serializer.errors)
                    if serializer.is_valid():
                        serializer.save()
                        dispositivo = FCMDevice.objects.all().filter(
                            user=tmp_query.finca_id.productor_id.user.id
                        ).first()
                        print("-----------*********ENVIO NOTIFICACION***********-------------")
                        print(tmp_query.finca_id.productor_id.id)
                        print(dispositivo)
                        if(dispositivo):
                            dispositivo.send_message(
                                title="Orden de corte",
                                body="Orden #"+str(serializer.data['id'])
                                +" / Finca: "+tmp_query.finca_id.nombre
                                +" / Cajas "+str(ordencorte['valor'])+".",
                                data={"landing_page": "Data devuelta de la notificacion"}
                            )
                    else:
                        return Response(
                        {'mensaje': 'Ocurrio un error en el servicio',
                        'status': 'failed'},)
            return Response(
                {'mensaje': 'Guardado satisfactoriamente',
                'status': 'success'},)
        else:
            embarque=Embarque.objects.filter(id=embarqueid).first()
            datos={
                'finalizado':False,
                'cantidad_actual':total
            }
            serializer=EmbarqueFinalizadoSerializer(embarque,data=datos)
            print(serializer.is_valid())
            print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
            for ordencorte in ordencortelist:
                orden=OrdenCorte.objects.filter(id=ordencorte['id']).first()
                tmp_query=EstimadoCaja.objects.filter(id=orden.estimado_id.id).first()
                datos={
                    'codigo_bodega':"skd",
                    'anio':tmp_query.anio,
                    'semana':tmp_query.semana,
                    'cantidad':ordencorte['valor'],
                    'facturado':False,
                    'finca_id':tmp_query.finca_id.id,
                    'embarque_id':orden.embarque_id.id,
                    'estimado_id':tmp_query.id
                }
                serializer = OrdenCorteGuardadoSerializer(orden,data=datos)
                print(serializer.is_valid())
                print(serializer.errors)
                if serializer.is_valid():
                    serializer.save()
                else:
                    return Response(
                    {'mensaje': 'Ocurrio un error en el servicio',
                    'status': 'failed'},)
            return Response(
                {'mensaje': 'Guardado satisfactoriamente',
                'status': 'success'},)

class EliminarOrdenCorte(APIView):
    """ authentication_classes = [TokenAuthentication,]
    permission_classes = [IsAuthenticated]  """
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        #Data recibida y parseo de datos
        id = body['id']
        orden=OrdenCorte.objects.filter(id=id).first()
        if orden:
            tmp_query=EstimadoCaja.objects.filter(id=orden.estimado_id.id).first()
            datos={
                'finca_id':tmp_query.finca_id.id,
                'fecha_estimado':tmp_query.fecha_estimado,
                'anio':tmp_query.anio,
                'semana':tmp_query.semana,
                'cantidad':tmp_query.cantidad,
                'bloqueado':False
            }
            serializer = EstimadoCajaSerializer(tmp_query,data=datos)
            print(serializer.is_valid())
            print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
                orden.delete()
                return Response(
                {'mensaje': 'Eliminado satisfactoriamente',
                'status': 'success'},)
        else:
            return Response(
                {'mensaje': 'Orden de corte no existe',
                'status': 'failed'},)
