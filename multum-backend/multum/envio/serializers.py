from drf_extra_fields.fields import Base64ImageField
from rest_framework import serializers
from .models import *
from datetime import datetime,date
from maestros.serializers import *

class EnvioEmbarqueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Embarque
        fields = ['codigo_alterno','fecha_embarque_desde','fecha_embarque_hasta','anio'
        ,'semana','cantidad','peso','largo','calidad_minima','calidad_maxima','precio',
        'puerto_salida_id','puerto_destino_id','vapor_id','marca_caja_id']
    def create(self, validated_data):
            """
            Create and return a new `Embarque` instance, given the validated data.
            """
            return Embarque.objects.create(**validated_data)

    def update(self, instance, validated_data):
            """
            Update and return an existing `Embarque` instance, given the validated data.
            """
            instance.codigo_alterno = validated_data.get('codigo_alterno', instance.codigo_alterno)
            instance.fecha_embarque_desde = validated_data.get('fecha_embarque_desde', instance.fecha_embarque_desde)
            instance.fecha_embarque_hasta = validated_data.get('fecha_embarque_hasta', instance.fecha_embarque_hasta)
            instance.anio = validated_data.get('anio', instance.anio)
            instance.semana = validated_data.get('semana', instance.semana)
            instance.cantidad = validated_data.get('cantidad', instance.cantidad)
            instance.peso = validated_data.get('peso', instance.peso)
            instance.largo = validated_data.get('largo', instance.largo)
            instance.calidad_minima = validated_data.get('calidad_minima', instance.calidad_minima)
            instance.calidad_maxima = validated_data.get('calidad_maxima', instance.calidad_maxima)
            instance.precio = validated_data.get('precio', instance.precio)
            instance.puerto_salida_id = validated_data.get('puerto_salida_id', instance.puerto_salida_id)
            instance.puerto_destino_id = validated_data.get('puerto_destino_id', instance.puerto_destino_id)
            instance.vapor_id = validated_data.get('vapor_id', instance.vapor_id)
            instance.marca_caja_id = validated_data.get('marca_caja_id', instance.marca_caja_id)
            instance.save()
            return instance

class EmbarqueVistaMarcaCajaSerializer(serializers.ModelSerializer):
    marca_caja_id=serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='nombre'
     )
    class Meta:
        model = Embarque
        fields = ['marca_caja_id']

class OrdenCorteSerializer(serializers.ModelSerializer):
    embarque_id=EmbarqueVistaMarcaCajaSerializer(many=False, read_only=True)
    finca_id=serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='nombre'
    )
    calificador_id=serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='primer_apellido'
    )
    class Meta:
        model = OrdenCorte
        fields = ['id','fecha_corte','semana','finca_id','embarque_id','cantidad','calificador_id']

class MarcaCajaSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarcaCaja
        fields = ['id','nombre']

class EmbarqueSerializer(serializers.ModelSerializer):
    vapor_id=serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='vapor'
     )
    class Meta:
        model = Embarque
        fields = ['id','vapor_id']

class OrdenCorteListSerializer(serializers.ModelSerializer):
    finca_id=FincaListSerializer(many=False, read_only=True)
    embarque_id=EmbarqueSerializer(many=False, read_only=True)

    class Meta:
        model = OrdenCorte
        fields = ['semana','finca_id','embarque_id','cantidad']

class EmbarqueListPagingSerializer(serializers.ModelSerializer):
    puerto_destino_id=PuertoListSerializer(
        many=False,
        read_only=True,
     )
    vapor_id=VaporListSerializer(
        many=False,
        read_only=True,
     )
    class Meta:
        model = Embarque
        fields = ['id','codigo_alterno','fecha_embarque_desde','fecha_embarque_hasta','anio'
        ,'semana','cantidad','cantidad_actual','peso','largo','calidad_minima','calidad_maxima','precio',
        'puerto_salida_id','puerto_destino_id','vapor_id','marca_caja_id','finalizado']

class OrdenCorteGuardadoSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrdenCorte
        fields = ['id','codigo_bodega','fecha_corte','anio','semana',
        'cantidad','facturado','finca_id','embarque_id','estimado_id','cajas_terminado','racimo_terminado']
    def create(self, validated_data):
            """
            Create and return a new `OrdenCorte` instance, given the validated data.
            """
            return OrdenCorte.objects.create(**validated_data)

    def update(self, instance, validated_data):
            """
            Update and return an existing `OrdenCorte` instance, given the validated data.
            """
            instance.codigo_bodega = validated_data.get('codigo_bodega', instance.codigo_bodega)
            instance.fecha_corte = validated_data.get('fecha_corte', instance.fecha_corte)
            instance.anio = validated_data.get('anio', instance.anio)
            instance.semana = validated_data.get('semana', instance.semana)
            instance.cantidad = validated_data.get('cantidad', instance.cantidad)
            instance.facturado = validated_data.get('facturado', instance.facturado)
            instance.finca_id = validated_data.get('finca_id', instance.finca_id)
            instance.embarque_id = validated_data.get('embarque_id', instance.embarque_id)
            instance.estimado_id = validated_data.get('estimado_id', instance.estimado_id)
            #instance.calificador_id = validated_data.get('calificador_id', instance.calificador_id)
            instance.cajas_terminado = validated_data.get('cajas_terminado', instance.cajas_terminado)
            instance.racimo_terminado = validated_data.get('racimo_terminado', instance.racimo_terminado)
            instance.save()
            return instance

class OrdenCorteEmbarqueSerializer(serializers.ModelSerializer):
    finca_id=FincaListSerializer(many=False, read_only=True)
    estimado_id=serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='cantidad'
     )
    class Meta:
        model = OrdenCorte
        fields = ['id','codigo_bodega','fecha_corte','anio','semana','finca_id',
        'embarque_id','cantidad','facturado','estimado_id']

class EmbarqueVistaSerializer(serializers.ModelSerializer):
    puerto_destino_id=PuertoListSerializer(
        many=False,
        read_only=True,
     )
    puerto_salida_id=PuertoListSerializer(
        many=False,
        read_only=True,
     )
    vapor_id=VaporListSerializer(
        many=False,
        read_only=True,
     )
    marca_caja_id=serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='nombre'
     )
    class Meta:
        model = Embarque
        fields = ['id','codigo_alterno','fecha_embarque_desde','fecha_embarque_hasta','anio'
        ,'semana','cantidad','cantidad_actual','peso','largo','calidad_minima','calidad_maxima','precio',
        'puerto_salida_id','puerto_destino_id','vapor_id','marca_caja_id','finalizado']

class EmbarqueFinalizadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Embarque
        fields = ['finalizado','cantidad_actual']
    def create(self, validated_data):
            """
            Create and return a new `Embarque` instance, given the validated data.
            """
            return Embarque.objects.create(**validated_data)

    def update(self, instance, validated_data):
            """
            Update and return an existing `Embarque` instance, given the validated data.
            """
            instance.finalizado = validated_data.get('finalizado', instance.finalizado)
            instance.cantidad_actual = validated_data.get('cantidad_actual', instance.cantidad_actual)
            instance.save()
            return instance

class OrdenCorteActualizarSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrdenCorte
        fields = ['cantidad']
    def create(self, validated_data):
            """
            Create and return a new `OrdenCorte` instance, given the validated data.
            """
            return OrdenCorte.objects.create(**validated_data)

    def update(self, instance, validated_data):
            """
            Update and return an existing `OrdenCorte` instance, given the validated data.
            """
            instance.cantidad = validated_data.get('cantidad', instance.cantidad)
            instance.save()
            return instance