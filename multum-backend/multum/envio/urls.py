from django.conf.urls import url

from .views import *

app_name='envio'

urlpatterns = [
    url(r'^orden-corte-list/anio=(?P<anio>\d+)/sem_desde=(?P<sem_desde>\d+)/sem_hasta=(?P<sem_hasta>\d+)/finca=(?P<finca>\d+)/todos=(?P<todos>\w+)/$', OrdenCorteList.as_view()),
    url(r'^marca-caja-list/$', MarcaCajaList.as_view()),
    url(r'^oren-corte-control-list/$', OrdenCorteControlList.as_view()),
    url(r'^oren-corte-control-racimo-list/$', OrdenCorteControRacimolList.as_view()),
    url(r'^lista-orden-corte/pagina=(?P<pagina>\d+)/elementos=(?P<elementos>\d+)/anio=(?P<anio>\w+)/sem_desde=(?P<sem_desde>\w+)/sem_hasta=(?P<sem_hasta>\w+)/embarque_id=(?P<embarque_id>\d+)/todos=(?P<todos>\w+)/$', OrdenCorteListPaging.as_view()),
    url(r'^crear-embarque/$', CrearEmbarque.as_view()),
    url(r'^lista-embarque/pagina=(?P<pagina>\d+)/elementos=(?P<elementos>\d+)/anio=(?P<anio>\w+)/sem_desde=(?P<sem_desde>\w+)/sem_hasta=(?P<sem_hasta>\w+)/todos=(?P<todos>\w+)/$', EmbarqueListPaging.as_view()),
    url(r'^crear-orden-seleccion/$', EmbarqueSeleccionEstimado.as_view()),
    url(r'^vista-embarque-orden/$', VistaEmbarqueOrdenCorte.as_view()),
    url(r'^finalizar-orden-corte/$', AplicarOrdenCorte.as_view()),
    url(r'^eliminar-orden-corte/$', EliminarOrdenCorte.as_view()),
]