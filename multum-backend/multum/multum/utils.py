from rest_framework.views import exception_handler
from rest_framework.exceptions import APIException
from rest_framework.response import Response

def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first, 
    # to get the standard error response.
    response = exception_handler(exc, context)
    """ print(exc.detail)
    print(exc.get_codes())
    print(exc.get_full_details())
    print("-------------------")
    print(context)
    print("-------------------")
    print(context['view'])
    print("-------------------")
    print(response) """
    # Now add the HTTP status code to the response.
    if response is not None:
        response.data['status_code'] = response.status_code
        response.data['code']=exc.get_codes()

    return response

class ErrorServicio(APIException):
    status_code = 503
    default_detail = 'Service temporarily unavailable, try again later.'
    default_code = 'service_unavailable'
    lalaal='askdas'

def error_servicio():
    return Response(
        {'mensaje': 'Ha ocurrido un error en el servicio',
        'status': 'failed'},)