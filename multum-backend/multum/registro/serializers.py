from drf_extra_fields.fields import Base64ImageField
from rest_framework import serializers
from .models import *
from datetime import datetime,date

class PersonaRolSerializer(serializers.ModelSerializer):
    class Meta:
        model = PersonaRol
        fields = ['persona_id','tipo_rol_id','agro_artesanal_id','aprobado']
    def create(self, validated_data):
            """
            Create and return a new `PersonaRol` instance, given the validated data.
            """
            return PersonaRol.objects.create(**validated_data)

    def update(self, instance, validated_data):
            """
            Update and return an existing `PersonaRol` instance, given the validated data.
            """
            instance.persona_id = validated_data.get('persona_id', instance.persona_id)
            instance.tipo_rol_id = validated_data.get('tipo_rol_id', instance.tipo_rol_id)
            instance.agro_artesanal_id = validated_data.get('agro_artesanal_id', instance.agro_artesanal_id)
            instance.aprobado = validated_data.get('aprobado', instance.aprobado)
            instance.save()
            return instance