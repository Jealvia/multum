from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE
from django.utils import timezone
from maestros.models import Vapor,Puerto,Finca
# Create your models here.

class MarcaCaja(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    marca_caja = models.CharField(max_length=255,blank=True)
    nombre_alterno = models.CharField(max_length=255,blank=True)
    activo = models.BooleanField(default=True)   
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "MarcaCaja"
        verbose_name_plural = "MarcaCajas"

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse("MarcaCaja_detail", kwargs={"pk": self.pk})

class Embarque(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    codigo_alterno = models.CharField(max_length=120,blank=True)
    fecha_embarque_desde = models.DateTimeField()
    fecha_embarque_hasta = models.DateTimeField()
    anio=models.IntegerField()
    semana=models.IntegerField()
    cupo=models.IntegerField()
    peso=models.IntegerField()
    largo=models.IntegerField()
    calidad_minima=models.IntegerField()
    calidad_maxima=models.IntegerField()
    
    puerto_salida_id=models.ForeignKey(Puerto,
    related_name='puerto_salida_id',on_delete=False)
    puerto_destino_id=models.ForeignKey(Puerto,
    related_name='puerto_destino_id',on_delete=False)
    vapor_id=models.ForeignKey(Vapor,
    related_name='vapor_id',on_delete=False)
    marca_caja_id=models.ForeignKey(MarcaCaja,
    related_name='eb_marca_caja_id',on_delete=False)
    
    activo = models.BooleanField(default=True)
    created_date = models.DateTimeField(default=timezone.now)

    class Meta:
        verbose_name = "Embarque"
        verbose_name_plural = "Embarques"

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse("Embarque_detail", kwargs={"pk": self.pk})

class OrdenCorte(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    codigo_bodega = models.CharField(max_length=50,blank=True)
    fecha_corte = models.DateTimeField()
    anio=models.IntegerField()
    semana=models.IntegerField()
    cupo=models.IntegerField()
    
    finca_id = models.ForeignKey(Finca,
    related_name='oc_finca_id',on_delete=False)
    embarque_id=models.ForeignKey(Embarque,
    related_name='oc_embarque_id',on_delete=False)
    
    activo = models.BooleanField(default=True)
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "OrdenCorte"
        verbose_name_plural = "OrdenCortes"

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse("OrdenCorte_detail", kwargs={"pk": self.pk})