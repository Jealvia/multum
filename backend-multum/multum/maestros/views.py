from rest_framework.parsers import JSONParser,FormParser
from rest_framework import generics,status,viewsets
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
#from rest_framework_jwt.settings import api_settings
from rest_framework.authtoken.models import Token
#import jwt
from django.contrib.auth.models import User

from django.shortcuts import get_object_or_404,render,redirect
from django.http import Http404,JsonResponse,HttpResponse
from django.views.decorators.csrf import csrf_exempt


import random as rd
import os
import time
import json
from django.db.models import Subquery,F,Func
from django.db.models.functions import *
import math

from datetime import datetime,date
from rest_framework.authentication import SessionAuthentication, BasicAuthentication,TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.db.models import Q

from fcm_django.models import FCMDevice
from rest_framework.parsers import FileUploadParser,MultiPartParser,FormParser

class Register(APIView):
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        uid = body['usuario']
        contasena = body['contasena']
        nombre = body['nombre']
        user = User(email="mail@gmail.com", username=uid)
        usuario=User.objects.all().filter(username=uid).first()
        if usuario:
            return Response(
              {'error': 'Usuario existente',
              'status': 'failed'},)
        else:
            user.set_password(contasena)
            user.save()
            return Response(
              {'error': 'Usuario guardado satisfactoriamente',
              'status': 'success'},)

class InicioSesion(APIView):
    def post(self,request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        uid = body['usuario']
        contasena = body['contasena']
        user = authenticate(request, username=uid, password=contasena)
        if user:
            login(request, user)
            token, created = Token.objects.get_or_create(user=user)
            datos = {
                'user':request.user.username,
                'session':request.session.session_key,
                'token': token,
                'status': 'success'
            }
            return Response(datos)
        return Response(
              {'error': 'Datos incorrectos',
              'status': 'failed'},)

class HolaMundo(APIView):
    def get(self,request):
        return Response(
              {'error': 'Usuario existente',
              'status': 'failed'},)