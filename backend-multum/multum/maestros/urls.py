from django.conf.urls import url

from .views import *

app_name='maestros'

urlpatterns = [
    url(r'^registro/$', Register.as_view()),
    url(r'^holamundo/$', HolaMundo.as_view()),
    url(r'^iniciar-sesion/$', InicioSesion.as_view()),
]