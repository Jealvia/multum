from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE
from django.utils import timezone
#Maestros
class TipoIdentificacion(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    tipo_identificacion = models.CharField(max_length=50,blank=True)
    codigo = models.CharField(max_length=50,blank=True)
    activo = models.BooleanField(default=True)   
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "TipoIdentificacion"
        verbose_name_plural = "TipoIdentificaciones"

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse("TipoIdentificacion_detail", kwargs={"pk": self.pk})

class TipoRol(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    tipo_rol = models.CharField(max_length=50,blank=True)
    codigo = models.CharField(max_length=50,blank=True)
    activo = models.BooleanField(default=True)
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "TipoRol"
        verbose_name_plural = "TipoRoles"

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse("TipoRol_detail", kwargs={"pk": self.pk})

class AgroArtesanal(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    agro_artesanal = models.CharField(max_length=255,blank=True)
    activo = models.BooleanField(default=True)
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "AgroArtesanal"
        verbose_name_plural = "AgroArtesanales"

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse("AgroArtesanal_detail", kwargs={"pk": self.pk})

class TipoFinca(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    tipo_finca = models.CharField(max_length=255,blank=True)
    activo = models.BooleanField(default=True)
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "TipoFinca"
        verbose_name_plural = "TipoFincas"

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse("TipoFinca_detail", kwargs={"pk": self.pk})

class Sector(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    sector = models.CharField(max_length=255,blank=True)
    activo = models.BooleanField(default=True)
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "Sector"
        verbose_name_plural = "Sectores"

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse("Sector_detail", kwargs={"pk": self.pk})

class Vapor(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    vapor = models.CharField(max_length=255,blank=True)
    activo = models.BooleanField(default=True)
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "Vapor"
        verbose_name_plural = "Vapores"

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse("Vapor_detail", kwargs={"pk": self.pk})

class Puerto(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    puerto = models.CharField(max_length=255,blank=True)
    activo = models.BooleanField(default=True)
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "Puerto"
        verbose_name_plural = "Puertos"

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse("Puerto_detail", kwargs={"pk": self.pk})

class Finca(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    area_total = models.CharField(max_length=20,blank=True)
    area_cultivada = models.CharField(max_length=50,blank=True)
    cajas_por_semana = models.CharField(max_length=50,blank=True)
    codigo_magap = models.CharField(max_length=50,blank=True)
    tipo_finca_id=models.ForeignKey(TipoFinca,
    related_name='tipo_finca_id',on_delete=False)
    sector_id=models.ForeignKey(Sector,
    related_name='sector_id',on_delete=False)
    activo = models.BooleanField(default=True)
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "Finca"
        verbose_name_plural = "Fincas"

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse("Finca_detail", kwargs={"pk": self.pk})

class Persona(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    tipo_identificacion_id=models.ForeignKey(TipoIdentificacion,
    related_name='tipo_identificacion_id',on_delete=False)

    identificacion = models.CharField(max_length=25,blank=True)
    primer_apellido = models.CharField(max_length=50,blank=True)
    segundo_apellido = models.CharField(max_length=50,blank=True)
    primer_nombre = models.CharField(max_length=50,blank=True)
    segundo_nombre = models.CharField(max_length=50,blank=True)
    nombre=models.CharField(max_length=300,blank=True)
    fecha_nacimiento = models.DateTimeField()
    activo = models.BooleanField(default=True)
    
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "Persona"
        verbose_name_plural = "Personas"

    def __str__(self):
        return self.identificacion

    def get_absolute_url(self):
        return reverse("Persona_detail", kwargs={"pk": self.pk})