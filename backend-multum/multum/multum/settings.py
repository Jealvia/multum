"""
Django settings for multum project.

Generated by 'django-admin startproject' using Django 2.2.7.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '0t55-)elqa_e8yn^n4w0j$y@m-35mt%fquqsk!-05l6q5v@l1v'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']


# Application definition

SHARED_APPS = [
    'tenant_schemas',  # mandatory
    'tenant', # you must list the app where your tenant model resides in
    'maestros',
    'safedelete',
    # everything below here is optional
    'django.contrib.contenttypes',
    
    'corsheaders',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.sessions',
    'django.contrib.messages',
    'rest_framework',
    'rest_framework.authtoken',
    'django.contrib.staticfiles',
    'fcm_django',
    'django_filters',
    #'reset_migrations',

]
""" 'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.sessions',
    'django.contrib.messages', """
TENANT_APPS = [
    # The following Django contrib apps must be in TENANT_APPS
    'django.contrib.contenttypes',
    
    'registro',
    'enfunde',
    'envio',
    'estimado',
    'safedelete',
    'facturacion',
    'corsheaders',
]

DEFAULT_FILE_STORAGE = "tenant_schemas.storage.TenantFileSystemStorage"


TENANT_MODEL = "tenant.Client" # app.Model

#TENANT_DOMAIN_MODEL = "tenant.Domain"  # app.Model

INSTALLED_APPS = list(set(SHARED_APPS+TENANT_APPS ))

#INSTALLED_APPS = list(SHARED_APPS) + [app for app in TENANT_APPS if app not in SHARED_APPS]


""" INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'rest_framework',
    'django.contrib.staticfiles',
    'fcm_django',
    'django_filters',
] """

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    #'django_tenants.middleware.main.TenantMainMiddleware',
    'tenant_schemas.middleware.TenantMiddleware',
    'corsheaders.middleware.CorsMiddleware',

]

FCM_DJANGO_SETTINGS = {
        "FCM_SERVER_KEY": ""
}

ROOT_URLCONF = 'multum.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'multum.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

""" DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
} """
""" DATABASES = {
	'default':{
        'ENGINE': 'django_tenants.postgresql_backend',
		#'ENGINE':'django.db.backends.postgresql_psycopg2',
		'NAME':'multum',
		'USER':'sosnegsa',
		'PASSWORD':'#D3sBas3',
		'HOST':'127.0.0.1',
		'PORT':'5432',
	}
} """

DATABASES = {
	'default':{
        'ENGINE': 'tenant_schemas.postgresql_backend',
		'NAME':'multum',
		'USER': 'jea2',
        'PASSWORD': '123456',
        'HOST': 'localhost',
        'PORT': '5432',
	}
}

DATABASE_ROUTERS = (
    'tenant_schemas.routers.TenantSyncRouter',
)
# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

""" REST_FRAMEWORK={
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE':10,
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
    ),
    'DEFAULT_PARSER_CLASSES': [
        'rest_framework.parsers.JSONParser',
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser'
    ]
    #'DEFAULT_PERMISSION_CLASSES': (
    #    'rest_framework.permissions.IsAuthenticated',
    #),
} """

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

CORS_ORIGIN_ALLOW_ALL = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'

#dominios: export_1.sosnegsa.com