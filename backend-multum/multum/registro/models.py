from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE
from django.utils import timezone
from maestros.models import AgroArtesanal,Persona,TipoRol
# Create your models here.
class PersonaRol(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    persona_id=models.ForeignKey(Persona,
    related_name='persona_id',on_delete=False)
    tipo_rol_id=models.ForeignKey(TipoRol,
    related_name='tipo_rol_id',on_delete=False)
    agro_artesanal_id=models.ForeignKey(AgroArtesanal,
    related_name='agro_artesanal_id',on_delete=False)
    activo = models.BooleanField(default=True)
    
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "PersonaRol"
        verbose_name_plural = "PersonaRoles"

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse("Persona_Rol_detail", kwargs={"pk": self.pk})