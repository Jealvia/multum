from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE
from django.utils import timezone
from maestros.models import Persona,Finca
from envio.models import MarcaCaja
# Create your models here.

class EstimadoCaja(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    finca_id = models.ForeignKey(Finca,
    related_name='ec_finca_id',on_delete=False)
    calificador_id = models.ForeignKey(Persona,
    related_name='calificador_id',on_delete=False)
    marca_caja_id = models.ForeignKey(MarcaCaja,
    related_name='ec_marca_caja_id',on_delete=False)
    fecha_estimado = models.DateTimeField()
    anio=models.IntegerField()
    semana=models.IntegerField()
    estimado=models.IntegerField()
    activo = models.BooleanField(default=True)   
    created_date = models.DateTimeField(
            default=timezone.now)

    class Meta:
        verbose_name = "EstimadoCaja"
        verbose_name_plural = "EstimadoCajas"

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse("EstimadoCaja_detail", kwargs={"pk": self.pk})
