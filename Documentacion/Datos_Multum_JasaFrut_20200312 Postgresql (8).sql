DELETE FROM facturacion_facturacabecera;
DELETE FROM facturacion_facturadetalle;
DELETE FROM control_cajacontrol;
DELETE FROM control_racimocontrol;
DELETE FROM envio_ordencorte;
DELETE FROM envio_embarque;
DELETE FROM enfunde_calibraje;
DELETE FROM enfunde_enfunde;
DELETE FROM estimado_estimadocaja;
DELETE FROM maestros_notificacion;
DELETE FROM maestros_finca;
DELETE FROM registro_personarol;
DELETE FROM maestros_persona;
DELETE FROM enfunde_cinta;
DELETE FROM maestros_tiponotificacion;
DELETE FROM maestros_tipoidentificacion;
DELETE FROM maestros_tipofinca;
DELETE FROM maestros_agroartesanal;
DELETE FROM maestros_puerto;
DELETE FROM maestros_vapor;
DELETE FROM maestros_sector;
DELETE FROM enfunde_tipoenfunde;
DELETE FROM enfunde_tipocalibraje;
DELETE FROM maestros_tiporol;
DELETE FROM envio_marcacaja;
DELETE FROM maestros_tipocaja;
DELETE FROM control_controlcolor;
DELETE FROM control_controledad;
DELETE FROM auth_user;

--- En postgres
ALTER SEQUENCE facturacion_facturadetalle_id_seq RESTART WITH 1;
ALTER SEQUENCE facturacion_facturacabecera_id_seq RESTART WITH 1;
ALTER SEQUENCE control_cajacontrol_id_seq RESTART WITH 1;
ALTER SEQUENCE control_racimocontrol_id_seq RESTART WITH 1;
ALTER SEQUENCE envio_ordencorte_id_seq RESTART WITH 1;
ALTER SEQUENCE envio_embarque_id_seq RESTART WITH 1;
ALTER SEQUENCE enfunde_calibraje_id_seq RESTART WITH 1;
ALTER SEQUENCE enfunde_enfunde_id_seq RESTART WITH 1;
ALTER SEQUENCE estimado_estimadocaja_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_notificacion_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_finca_id_seq RESTART WITH 1;
ALTER SEQUENCE registro_personarol_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_persona_id_seq RESTART WITH 1;
ALTER SEQUENCE enfunde_cinta_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_tiponotificacion_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_tipoidentificacion_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_tipofinca_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_agroartesanal_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_puerto_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_vapor_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_sector_id_seq RESTART WITH 1;
ALTER SEQUENCE enfunde_tipoenfunde_id_seq RESTART WITH 1;
ALTER SEQUENCE enfunde_tipocalibraje_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_tiporol_id_seq RESTART WITH 1;
ALTER SEQUENCE envio_marcacaja_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_tipocaja_id_seq RESTART WITH 1;
ALTER SEQUENCE control_controlcolor_id_seq RESTART WITH 1;
ALTER SEQUENCE control_controledad_id_seq RESTART WITH 1;
ALTER SEQUENCE auth_user_id_seq RESTART WITH 1;

--------------------------------------------------------

INSERT INTO control_controledad(deleted, edad_minima, edad_maxima, edad_referencia, orden_maximo, activo, created_date) VALUES (NULL, 8, 14, 12, 5, TRUE,'2020-01-09 01:01:01');
/*
INSERT INTO control_controlcolor(deleted, color, orden_enfunde, orden_edad, edad, activo, created_date) VALUES (NULL, 'verde', 1, 6, 14, TRUE,'2020-01-09 01:01:01');
INSERT INTO control_controlcolor(deleted, color, orden_enfunde, orden_edad, edad, activo, created_date) VALUES (NULL, 'azul', 2, 5, 13, TRUE,'2020-01-09 01:01:01');
INSERT INTO control_controlcolor(deleted, color, orden_enfunde, orden_edad, edad, activo, created_date) VALUES (NULL, 'rojo', 3, 4, 12, TRUE,'2020-01-09 01:01:01');
INSERT INTO control_controlcolor(deleted, color, orden_enfunde, orden_edad, edad, activo, created_date) VALUES (NULL, 'negro', 4, 3, 11, TRUE,'2020-01-09 01:01:01');
INSERT INTO control_controlcolor(deleted, color, orden_enfunde, orden_edad, edad, activo, created_date) VALUES (NULL, 'amarillo', 5, 2, 10, TRUE,'2020-01-09 01:01:01');
INSERT INTO control_controlcolor(deleted, color, orden_enfunde, orden_edad, edad, activo, created_date) VALUES (NULL, 'blanco', 6, 1, 9, TRUE,'2020-01-09 01:01:01');
*/
INSERT INTO control_controlcolor(deleted, color, orden_enfunde, orden_edad, activo, created_date) VALUES (NULL, 'verde', 1, 6,  TRUE,'2020-01-09 01:01:01');
INSERT INTO control_controlcolor(deleted, color, orden_enfunde, orden_edad, activo, created_date) VALUES (NULL, 'azul', 2, 5, TRUE,'2020-01-09 01:01:01');
INSERT INTO control_controlcolor(deleted, color, orden_enfunde, orden_edad, activo, created_date) VALUES (NULL, 'rojo', 3, 4, TRUE,'2020-01-09 01:01:01');
INSERT INTO control_controlcolor(deleted, color, orden_enfunde, orden_edad, activo, created_date) VALUES (NULL, 'negro', 4, 3, TRUE,'2020-01-09 01:01:01');
INSERT INTO control_controlcolor(deleted, color, orden_enfunde, orden_edad, activo, created_date) VALUES (NULL, 'amarillo', 5, 2, TRUE,'2020-01-09 01:01:01');
INSERT INTO control_controlcolor(deleted, color, orden_enfunde, orden_edad, activo, created_date) VALUES (NULL, 'blanco', 6, 1, TRUE,'2020-01-09 01:01:01');

INSERT INTO maestros_tipocaja(deleted, nombre, codigo, peso, segunda, activo, created_date) VALUES (NULL, 'CAJAS DE SEGUNDA', 'SEG', 50.00, TRUE, TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_tipocaja(deleted, nombre, codigo, peso, segunda, activo, created_date) VALUES (NULL, 'CAJAS DE 31.00', 'C31', 31.00, FALSE, TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_tipocaja(deleted, nombre, codigo, peso, segunda, activo, created_date) VALUES (NULL, 'CAJAS DE 37.48', 'C37', 37.48, FALSE, TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_tipocaja(deleted, nombre, codigo, peso, segunda, activo, created_date) VALUES (NULL, 'CAJAS DE 40.50', 'C40', 40.50, FALSE, TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_tipocaja(deleted, nombre, codigo, peso, segunda, activo, created_date) VALUES (NULL, 'CAJAS DE 41.50', 'C41', 41.50, FALSE, TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_tipocaja(deleted, nombre, codigo, peso, segunda, activo, created_date) VALUES (NULL, 'CAJAS DE 42.00', 'C42', 42.00, FALSE, TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_tipocaja(deleted, nombre, codigo, peso, segunda, activo, created_date) VALUES (NULL, 'CAJAS DE 43.00', 'C43', 43.00, FALSE, TRUE,'2020-01-09 01:01:01');



	
--------------------------------------------------------
---- EXPORTADORA: JASAFRUT  2019   38 A 52
--------------------------------------------------------
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-09-14 01:01:01','2019-09-20 01:01:01',2019,38,'blanco','FFFFFF',TRUE,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-09-21 01:01:01','2019-09-27 01:01:01',2019,39,'verde','31A833',TRUE,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-09-28 01:01:01','2019-10-04 01:01:01',2019,40,'azul','F60D0D',TRUE,'2019-01-09 01:01:01');

INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-10-05 01:01:01','2019-10-11 01:01:01',2019,41,'rojo','F60D0D',TRUE,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-10-12 01:01:01','2019-10-18 01:01:01',2019,42,'negro','000000',TRUE,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-10-19 01:01:01','2019-10-25 01:01:01',2019,43,'amarillo','F8F32B',TRUE,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-10-26 01:01:01','2019-11-01 01:01:01',2019,44,'blanco','FFFFFF',TRUE,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-11-02 01:01:01','2019-11-08 01:01:01',2019,45,'verde','31A833',TRUE,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-11-09 01:01:01','2019-11-15 01:01:01',2019,46,'azul','F60D0D',TRUE,'2019-01-09 01:01:01');

INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-11-16 01:01:01','2019-11-22 01:01:01',2019,47,'rojo','F60D0D',TRUE,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-11-23 01:01:01','2019-11-29 01:01:01',2019,48,'negro','000000',TRUE,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-11-30 01:01:01','2019-12-06 01:01:01',2019,49,'amarillo','F8F32B',TRUE,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-12-07 01:01:01','2019-12-13 01:01:01',2019,50,'blanco','FFFFFF',TRUE,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-12-14 01:01:01','2019-12-20 01:01:01',2019,51,'verde','31A833',TRUE,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-12-21 01:01:01','2019-12-27 01:01:01',2019,52,'azul','F60D0D',TRUE,'2019-01-09 01:01:01');


---- EXPORTADORA: JASAFRUT  2020  (16)
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-12-30 01:01:01','2020-01-05 01:01:01',2020,1,'rojo','F60D0D',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-01-06 01:01:01','2020-01-12 01:01:01',2020,2,'negro','000000',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-01-13 01:01:01','2020-01-19 01:01:01',2020,3,'amarillo','F8F32B',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-01-20 01:01:01','2020-01-26 01:01:01',2020,4,'blanco','FFFFFF',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-01-27 01:01:01','2020-02-02 01:01:01',2020,5,'verde','31A833',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-02-03 01:01:01','2020-02-09 01:01:01',2020,6,'azul','F60D0D',TRUE,'2020-01-09 01:01:01');

INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-02-10 01:01:01','2020-02-16 01:01:01',2020,7,'rojo','F60D0D',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-02-17 01:01:01','2020-02-23 01:01:01',2020,8,'negro','000000',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-02-24 01:01:01','2020-03-01 01:01:01',2020,9,'amarillo','F8F32B',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-03-02 01:01:01','2020-03-08 01:01:01',2020,10,'blanco','FFFFFF',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-03-09 01:01:01','2020-03-15 01:01:01',2020,11,'verde','31A833',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-03-16 01:01:01','2020-03-22 01:01:01',2020,12,'azul','F60D0D',TRUE,'2020-01-09 01:01:01');

INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-03-23 01:01:01','2020-03-29 01:01:01',2020,13,'rojo','F60D0D',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-03-30 01:01:01','2020-04-05 01:01:01',2020,14,'negro','000000',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-04-06 01:01:01','2020-04-12 01:01:01',2020,15,'amarillo','F8F32B',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-04-13 01:01:01','2020-04-19 01:01:01',2020,16,'blanco','FFFFFF',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-04-20 01:01:01','2020-04-26 01:01:01',2020,17,'verde','31A833',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-04-27 01:01:01','2020-05-03 01:01:01',2020,18,'azul','F60D0D',TRUE,'2020-01-09 01:01:01');

INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-05-04 01:01:01','2020-05-10 01:01:01',2020,19,'rojo','F60D0D',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-05-11 01:01:01','2020-05-17 01:01:01',2020,20,'negro','000000',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-05-18 01:01:01','2020-05-24 01:01:01',2020,21,'amarillo','F8F32B',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-05-25 01:01:01','2020-05-31 01:01:01',2020,22,'blanco','FFFFFF',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-06-01 01:01:01','2020-06-07 01:01:01',2020,23,'verde','31A833',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-06-08 01:01:01','2020-06-14 01:01:01',2020,24,'azul','F60D0D',TRUE,'2020-01-09 01:01:01');

INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-06-15 01:01:01','2020-06-21 01:01:01',2020,25,'rojo','F60D0D',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-06-22 01:01:01','2020-06-28 01:01:01',2020,26,'negro','000000',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-06-29 01:01:01','2020-07-05 01:01:01',2020,27,'amarillo','F8F32B',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-07-06 01:01:01','2020-07-12 01:01:01',2020,28,'blanco','FFFFFF',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-07-13 01:01:01','2020-07-19 01:01:01',2020,29,'verde','31A833',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-07-20 01:01:01','2020-07-26 01:01:01',2020,30,'azul','F60D0D',TRUE,'2020-01-09 01:01:01');

INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-07-27 01:01:01','2020-08-02 01:01:01',2020,31,'rojo','F60D0D',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-08-03 01:01:01','2020-08-09 01:01:01',2020,32,'negro','000000',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-08-10 01:01:01','2020-08-16 01:01:01',2020,33,'amarillo','F8F32B',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-08-17 01:01:01','2020-08-23 01:01:01',2020,34,'blanco','FFFFFF',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-08-24 01:01:01','2020-08-30 01:01:01',2020,35,'verde','31A833',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-08-31 01:01:01','2020-09-06 01:01:01',2020,36,'azul','F60D0D',TRUE,'2020-01-09 01:01:01');

INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-09-07 01:01:01','2020-09-13 01:01:01',2020,37,'rojo','F60D0D',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-09-14 01:01:01','2020-09-20 01:01:01',2020,38,'negro','000000',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-09-21 01:01:01','2020-09-27 01:01:01',2020,39,'amarillo','F8F32B',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-09-28 01:01:01','2020-10-04 01:01:01',2020,40,'blanco','FFFFFF',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-10-05 01:01:01','2020-10-11 01:01:01',2020,41,'verde','31A833',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-10-12 01:01:01','2020-10-18 01:01:01',2020,42,'azul','F60D0D',TRUE,'2020-01-09 01:01:01');

INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-10-19 01:01:01','2020-10-25 01:01:01',2020,43,'rojo','F60D0D',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-10-26 01:01:01','2020-11-01 01:01:01',2020,44,'negro','000000',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-11-02 01:01:01','2020-11-08 01:01:01',2020,45,'amarillo','F8F32B',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-11-09 01:01:01','2020-11-15 01:01:01',2020,46,'blanco','FFFFFF',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-11-16 01:01:01','2020-11-22 01:01:01',2020,47,'verde','31A833',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-11-23 01:01:01','2020-11-29 01:01:01',2020,48,'azul','F60D0D',TRUE,'2020-01-09 01:01:01');

INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-11-30 01:01:01','2020-12-06 01:01:01',2020,49,'rojo','F60D0D',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-12-07 01:01:01','2020-12-13 01:01:01',2020,50,'negro','000000',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-12-14 01:01:01','2020-12-20 01:01:01',2020,51,'amarillo','F8F32B',TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-12-21 01:01:01','2020-12-27 01:01:01',2020,52,'blanco','FFFFFF',TRUE,'2020-01-09 01:01:01');


INSERT INTO maestros_tipoidentificacion( deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'RUC', 'R', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_tipoidentificacion( deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'CEDULA', 'C', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_tipoidentificacion( deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'PASAPORTE', 'P', TRUE,'2020-01-09 01:01:01');

INSERT INTO maestros_tipofinca(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'CONVENCIONAL', 'CONV', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_tipofinca(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'ORGANICA', 'ORGA', TRUE,'2020-01-09 01:01:01');

INSERT INTO maestros_agroartesanal(deleted, nombre, activo, created_date) VALUES (NULL, 'EL GUABO', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_agroartesanal(deleted, nombre, activo, created_date) VALUES (NULL, 'SAN CARLOS', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_agroartesanal(deleted, nombre, activo, created_date) VALUES (NULL, 'SANTA ROSA', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_agroartesanal(deleted, nombre, activo, created_date) VALUES (NULL, 'PASAJE', TRUE,'2020-01-09 01:01:01');

INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO BOLÍVAR', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO MANTA', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO GYE', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO DE OAKLAND', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO DE SEATTLE-TACOMA', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO DE VALENCIA', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO DE EL HAVRE', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO DE FLENSBURG', TRUE,'2020-01-09 01:01:01');

INSERT INTO maestros_vapor(deleted, nombre, activo, created_date) VALUES (NULL, 'SUNRISE', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_vapor(deleted, nombre, activo, created_date) VALUES (NULL, 'LIMARI', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_vapor(deleted, nombre, activo, created_date) VALUES (NULL, 'NORTHERN DEFENDER', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_vapor(deleted, nombre, activo, created_date) VALUES (NULL, 'ANTILEN', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_vapor(deleted, nombre, activo, created_date) VALUES (NULL, 'AQUAVIT', TRUE,'2020-01-09 01:01:01');

INSERT INTO maestros_sector(deleted, nombre, activo, created_date) VALUES (NULL, 'TILLALES', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_sector(deleted, nombre, activo, created_date) VALUES (NULL, 'BARBONES', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_sector(deleted, nombre, activo, created_date) VALUES (NULL, 'SAN CARLOS', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_sector(deleted, nombre, activo, created_date) VALUES (NULL, 'SAUCES', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_sector(deleted, nombre, activo, created_date) VALUES (NULL, 'PROSPERINA', TRUE,'2020-01-09 01:01:01');

INSERT INTO enfunde_tipoenfunde(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'ENFUNDE', 'E', TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_tipoenfunde(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'RECUPERACION', 'R', TRUE,'2020-01-09 01:01:01');

INSERT INTO enfunde_tipocalibraje(deleted, nombre, codigo, activo, created_date) VALUES (NULL, '1ER CALIBRAJE', '1ER', TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_tipocalibraje(deleted, nombre, codigo, activo, created_date) VALUES (NULL, '2DO CALIBRAJE', '2DO', TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_tipocalibraje(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'BARRIDA', 'BAR', TRUE,'2020-01-09 01:01:01');
INSERT INTO enfunde_tipocalibraje(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'ADELANTADA', 'ADE', TRUE,'2020-01-09 01:01:01');

INSERT INTO maestros_tiporol(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'EMPLEADO', 'E', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_tiporol(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'PRODUCTOR', 'P', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_tiporol(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'TECNICO', 'T', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_tiporol(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'CALIFICADOR', 'C', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_tiporol(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'EXPORTADOR', 'C', TRUE,'2020-01-09 01:01:01');

INSERT INTO envio_marcacaja(deleted, nombre, nombre_alterno, activo, created_date) VALUES (NULL, 'EQUAPAK 22XU', '22XU', TRUE,'2020-01-09 01:01:01');
INSERT INTO envio_marcacaja(deleted, nombre, nombre_alterno, activo, created_date) VALUES (NULL, 'EQUAPAK 209', '209', TRUE,'2020-01-09 01:01:01');
INSERT INTO envio_marcacaja(deleted, nombre, nombre_alterno, activo, created_date) VALUES (NULL, 'EQUAPAK SF101', 'SF101', TRUE,'2020-01-09 01:01:01');

INSERT INTO maestros_tiponotificacion(deleted, descripcion, codigo, activo, created_date) VALUES (NULL, 'Mensaje', 'M', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_tiponotificacion(deleted, descripcion, codigo, activo, created_date) VALUES (NULL, 'Alertas', 'A', TRUE,'2020-01-09 01:01:01');
INSERT INTO maestros_tiponotificacion(deleted, descripcion, codigo, activo, created_date) VALUES (NULL, 'Eventos', 'E', TRUE,'2020-01-09 01:01:01');


-------------------------------
--- EXPORTADORES
-------------------------------
/*
DELETE FROM auth_user;
ALTER SEQUENCE auth_user_id_seq RESTART WITH 1;
*/
/*
UPDATE auth_user SET password='pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', last_login='2020-01-09 01:01:01', is_superuser=FALSE, username='EQUAPAK', first_name='EQUAPAK', last_name='', email='equapak@gmail.com', is_staff=FALSE, is_active=TRUE, date_joined='2020-01-09 01:01:01' WHERE id=1;
UPDATE auth_user SET password='pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', last_login='2020-01-09 01:01:01', is_superuser=FALSE, username='IREN', first_name='IREN', last_name='', email='iren@gmail.com', is_staff=FALSE, is_active=TRUE, date_joined='2020-01-09 01:01:01' WHERE id=2;
UPDATE auth_user SET password='pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', last_login='2020-01-09 01:01:01', is_superuser=FALSE, username='UBESA', first_name='UBESA', last_name='', email='ubesa@gmail.com', is_staff=FALSE, is_active=TRUE, date_joined='2020-01-09 01:01:01' WHERE id=3;
UPDATE auth_user SET password='pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', last_login='2020-01-09 01:01:01', is_superuser=FALSE, username='TRUISFRUIT', first_name='TRUISFRUIT', last_name='', email='truisfruit@gmail.com', is_staff=FALSE, is_active=TRUE, date_joined='2020-01-09 01:01:01' WHERE id=4;
*/
/*
INSERT INTO authtoken_token(key, created, user_id) VALUES ('475fb7eae4b22cf0c773da155f58dd8cfdd35f39', '2020-01-09 01:01:01', 1);
INSERT INTO authtoken_token(key, created, user_id) VALUES ('5583f63bc6dc0748a12151d2bae828e33369bc6e', '2020-01-09 01:01:01', 2);
INSERT INTO authtoken_token(key, created, user_id) VALUES ('b7faf9bd7aec0a87edf826ff4d88fd5b01736c43', '2020-01-09 01:01:01', 3);
INSERT INTO authtoken_token(key, created, user_id) VALUES ('00abc5b1697e80e061d68117a6061b3a17e93359', '2020-01-09 01:01:01', 4);
*/


INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', FALSE, 'EQUAPAK', 'EQUAPAK', '', 'equapak@gmail.com', FALSE, TRUE,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', FALSE, 'IREN', 'IREN', '', 'iren@gmail.com', FALSE, TRUE,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', FALSE, 'UBESA', 'UBESA', '', 'ubesa@gmail.com', FALSE, TRUE,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', FALSE, 'ASOAGRIBAL', 'ASOAGRIBAL', '', 'asoagribal@gmail.com', FALSE, TRUE,'2020-01-09 01:01:01');
	
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', FALSE, 'TRUISFRUIT', 'TRUISFRUIT', '', 'truisfruit@gmail.com', FALSE, TRUE,'2020-01-09 01:01:01');

--- TECNICOS
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', FALSE, 'JARMIJOS', 'JOSE', 'ARMIJOS', 'jarmijos@gmail.com', FALSE, TRUE,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', FALSE, 'LGOMEZ', 'LUIS', 'GOMEZ', 'lgomez@gmail.com', FALSE, TRUE,'2020-01-09 01:01:01');

--- PRODUCTOR
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', FALSE, 'FNAULA', 'FREDDY', 'NAULA', 'fnaula@gmail.com', FALSE, TRUE,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', FALSE, 'JSERRANO', 'JORGE ALEX', 'SERRANO', 'jserrano@gmail.com', FALSE, TRUE,'2020-01-09 01:01:01');

--- Productores  desde id 10
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', FALSE, 'FGUERRERO', 'GUERRERO RÍOS', 'FERNANDO MAURICIO', 'fguerrero@gmail.com', FALSE, TRUE,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', FALSE, 'PGUERRON', 'GUERRERO VALENCIA', 'PABLO RAUL', 'pguerrero@gmail.com', FALSE, TRUE,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', FALSE, 'AGUEVARA', 'GUERRÓN ALMEIDA', 'ALEXEY GIOVANNY', 'aguerron@gmail.com', FALSE, TRUE,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', FALSE, 'MGUEVARA', 'GUEVARA BACULIMA', 'MARTHA DEL ROCÍO', 'mguevara@gmail.com', FALSE, TRUE,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', FALSE, 'JGUILLEN', 'GUEVARA CARRILLO', 'JORGE LUIS', 'jguevara@gmail.com', FALSE, TRUE,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', FALSE, 'MGUILLERMO', 'GUILLÉN OSCAR', 'MEDARDO ', 'mguillen@gmail.com', FALSE, TRUE,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', FALSE, 'JGUZMAN', 'GUILLERMO DELGADO', 'JORGE ERNESTO', 'jguillermo@gmail.com', FALSE, TRUE,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', FALSE, 'HGUZMAN', 'GUZMÁN CASTAÑEDA', 'HIMMLER ROBERTO', 'hguzman@gmail.com', FALSE, TRUE,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', FALSE, 'LHARO', 'GUZMÁN ROCHINA', 'LUIS GABRIEL', 'lguzman@gmail.com', FALSE, TRUE,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', FALSE, 'THEREDIA', 'HARO FIGUEROA', 'TANIA MARÍA', 'tharo@gmail.com', FALSE, TRUE,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', FALSE, 'EHEREDIA', 'HEREDIA CABRERA', 'EDWIN WALDEMAR', 'eheredia@gmail.com', FALSE, TRUE,'2020-01-09 01:01:01');

--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
--- EXPORTADORES
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0791777011001', NULL, NULL, NULL, NULL, 'EQUAPAK', NULL, TRUE,'2020-01-09 01:01:01',1, 1, 'El Guabo', '2950469');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0993221287001', NULL, NULL, NULL, NULL, 'IRENECUADOR', NULL, TRUE,'2020-01-09 01:01:01',1, 2, 'El Guabo', '2988199');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0990011419001', NULL, NULL, NULL, NULL, 'UBESA', NULL, TRUE,'2020-01-09 01:01:01',1, 3, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0791741254001', NULL, NULL, NULL, NULL, 'ASOAGRIBAL', NULL, TRUE,'2020-01-09 01:01:01',1, 4, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0992601523001', NULL, NULL, NULL, NULL, 'TRUISFRUIT', NULL, TRUE,'2020-01-09 01:01:01',1, 5, 'El Guabo', '21233434');
--- TECNICO 6
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1232025339', 'JOSE', 'LUIS', 'ARMIJOS', 'ARMIJOS', 'JOSE LUIS ARMIJOS ARMIJOS', NULL, TRUE,'2020-01-09 01:01:01',2, 6, 'El Guabo', '21233434');
--- CALIFICADOR 7
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1562025339', 'LUIS', 'ENRIQUE', 'GOMEZ', 'GOMEZ', 'LUIS ENRIQUE GOMEZ GOMEZ', NULL, TRUE,'2020-01-09 01:01:01',2, 7, 'El Guabo', '21233434');
--- PRODUCTOR 8
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0702025339001', 'FREDDY', 'SEGUNDO', 'NAULA', 'MORA', 'FREDDY SEGUNDO NAULA MORA', NULL, TRUE,'2020-01-09 01:01:01',1, 8, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0760015020001', 'JORGE', 'ALEX', 'SERRANO', 'AGUILAR', 'JORGE ALEX SERRANO AGUILAR', NULL, TRUE,'2020-01-09 01:01:01',1, 9, 'El Guabo', '21233434');
--- desde el 10
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1102618673', 'GUERRERO', 'RÍOS', 'FERNANDO', 'MAURICIO', 'GUERRERO RÍOS FERNANDO MAURICIO', NULL, TRUE,'2020-01-09 01:01:01',1, 10, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0800599821', 'GUERRERO', 'VALENCIA', 'PABLO', 'RAUL', 'GUERRERO VALENCIA PABLO RAUL', NULL, TRUE,'2020-01-09 01:01:01',1, 11, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0400783759', 'GUERRÓN', 'ALMEIDA', 'ALEXEY', 'GIOVANNY', 'GUERRÓN ALMEIDA ALEXEY GIOVANNY', NULL, TRUE,'2020-01-09 01:01:01',1, 12, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0102775624', 'GUEVARA', 'BACULIMA', 'MARTHA', 'DEL ROCÍO', 'GUEVARA BACULIMA MARTHA DEL ROCÍO', NULL, TRUE,'2020-01-09 01:01:01',1, 13, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1801910678', 'GUEVARA', 'CARRILLO', 'JORGE', 'LUIS', 'GUEVARA CARRILLO JORGE LUIS', NULL, TRUE,'2020-01-09 01:01:01',1, 14, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0101348795', 'GUILLÉN', 'OSCAR', 'MEDARDO', '', 'GUILLÉN OSCAR MEDARDO ', NULL, TRUE,'2020-01-09 01:01:01',1, 15, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0101342707', 'GUILLERMO', 'DELGADO', 'JORGE', 'ERNESTO', 'GUILLERMO DELGADO JORGE ERNESTO', NULL, TRUE,'2020-01-09 01:01:01',1, 16, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1706381975', 'GUZMÁN', 'CASTAÑEDA', 'HIMMLER', 'ROBERTO', 'GUZMÁN CASTAÑEDA HIMMLER ROBERTO', NULL, TRUE,'2020-01-09 01:01:01',1, 17, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0201628237', 'GUZMÁN', 'ROCHINA', 'LUIS', 'GABRIEL', 'GUZMÁN ROCHINA LUIS GABRIEL', NULL, TRUE,'2020-01-09 01:01:01',1, 18, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1802577708', 'HARO', 'FIGUEROA', 'TANIA', 'MARÍA', 'HARO FIGUEROA TANIA MARÍA', NULL, TRUE,'2020-01-09 01:01:01',1, 19, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0601554694', 'HEREDIA', 'CABRERA', 'EDWIN', 'WALDEMAR', 'HEREDIA CABRERA EDWIN WALDEMAR', NULL, TRUE,'2020-01-09 01:01:01',1, 20, 'El Guabo', '21233434');

--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
--- EXPORTADORES
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE,'2020-01-09 01:01:01', 1, 1, 5, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE,'2020-01-09 01:01:01', 1, 2, 5, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE,'2020-01-09 01:01:01', 1, 3, 5, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE,'2020-01-09 01:01:01', 1, 4, 5, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE,'2020-01-09 01:01:01', 1, 5, 5, TRUE);

--- TECNICO 6
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE,'2020-01-09 01:01:01', 1, 6, 3, TRUE);
--- CALIFICADOR 7
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE,'2020-01-09 01:01:01', 1, 7, 4, TRUE);

--- PRODUCTOR  8 al 53
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE,'2020-01-09 01:01:01', 1, 8, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE,'2020-01-09 01:01:01', 1, 9, 2, TRUE);

INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE,'2020-01-09 01:01:01', 1, 10, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE,'2020-01-09 01:01:01', 1, 11, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE,'2020-01-09 01:01:01', 1, 12, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE,'2020-01-09 01:01:01', 1, 13, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE,'2020-01-09 01:01:01', 1, 14, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE,'2020-01-09 01:01:01', 1, 15, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE,'2020-01-09 01:01:01', 1, 16, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE,'2020-01-09 01:01:01', 1, 17, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE,'2020-01-09 01:01:01', 1, 18, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE,'2020-01-09 01:01:01', 1, 19, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE,'2020-01-09 01:01:01', 1, 20, 2, TRUE);

-------------------------------
--- FINCAS 
-------------------------------
--- TECNICO 6 / 8 FREDDY NAULA / 9 JORGE ALEX SERRANO
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE,'2020-01-09 01:01:01', 1, 1, 8, 6, 7, 'LA PATRICIA');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE,'2020-01-09 01:01:01', 1, 1, 8, 6, 7, 'LA GUARICO');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE,'2020-01-09 01:01:01', 1, 1, 8, 6, 7, 'FIDECADE');

INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE,'2020-01-09 01:01:01', 1, 1, 9, 6, 7, 'BASTIDAS');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE,'2020-01-09 01:01:01', 1, 1, 9, 6, 7, 'DEL VALLE');

INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE,'2020-01-09 01:01:01', 1, 1, 10, 6, 7, 'AVICOLAS FINCAVIC S.A.');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE,'2020-01-09 01:01:01', 1, 1, 11, 6, 7, 'URBANAS S A');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE,'2020-01-09 01:01:01', 1, 1, 12, 6, 7, 'PACIFICO');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE,'2020-01-09 01:01:01', 1, 1, 13, 6, 7, 'AZAN S.A.');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE,'2020-01-09 01:01:01', 1, 1, 14, 6, 7, 'MARINAS CORONEL CHALEN');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE,'2020-01-09 01:01:01', 1, 1, 15, 6, 7, 'SAN MARCOS');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE,'2020-01-09 01:01:01', 1, 1, 16, 6, 7, 'AGROACUICOLA FUZU');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE,'2020-01-09 01:01:01', 1, 1, 17, 6, 7, 'UNIDAS');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE,'2020-01-09 01:01:01', 1, 1, 18, 6, 7, 'DEL MAR');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE,'2020-01-09 01:01:01', 1, 1, 19, 6, 7, 'MARINAS');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE,'2020-01-09 01:01:01', 1, 1, 20, 6, 7, 'AVAMER');

---------------------------------------------------------------------------------
---- ENFUNDE
---------------------------------------------------------------------------------
--- fincas de freddy naula
--- fincas de jorge alex  2019   38 A 52
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-09-16 01:01:01', 2019, 38, 1565, TRUE,'2019-09-16 01:01:01', 1, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-09-23 01:01:01', 2019, 39, 1375, TRUE,'2019-09-23 01:01:01', 2, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-09-30 01:01:01', 2019, 40, 1381, TRUE,'2019-09-30 01:01:01', 3, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-10-07 01:01:01', 2019, 41, 1532, TRUE,'2019-10-07 01:01:01', 4, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-10-14 01:01:01', 2019, 42, 1413, TRUE,'2019-10-14 01:01:01', 5, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-10-21 01:01:01', 2019, 43, 1442, TRUE,'2019-10-21 01:01:01', 6, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-10-28 01:01:01', 2019, 44, 1386, TRUE,'2019-10-28 01:01:01', 7, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-11-04 01:01:01', 2019, 45, 1643, TRUE,'2019-11-04 01:01:01', 8, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-11-11 01:01:01', 2019, 46, 1275, TRUE,'2019-11-11 01:01:01', 9, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-11-18 01:01:01', 2019, 47, 1198, TRUE,'2019-11-18 01:01:01', 10, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-11-25 01:01:01', 2019, 48, 1370, TRUE,'2019-11-25 01:01:01', 11, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-12-02 01:01:01', 2019, 49, 1424, TRUE,'2019-12-02 01:01:01', 12, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-12-09 01:01:01', 2019, 50, 1299, TRUE,'2019-12-09 01:01:01', 13, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-12-16 01:01:01', 2019, 51, 1476, TRUE,'2019-12-16 01:01:01', 14, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-12-23 01:01:01', 2019, 52, 1562, TRUE,'2019-12-23 01:01:01', 15, 4, 1);

--- fincas de jorge alex  2020
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 1635, TRUE,'2020-01-03 01:01:01', 16, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 1633, TRUE,'2020-01-10 01:01:01', 17, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 1759, TRUE,'2020-01-17 01:01:01', 18, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 1679, TRUE,'2020-01-24 01:01:01', 19, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-31 01:01:01', 2020, 5, 1476, TRUE,'2020-01-31 01:01:01', 20, 1, 1);


--- fincas de jorge alex  2019   38 A 52
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-09-16 01:01:01', 2019, 38, 1565, TRUE,'2019-09-16 01:01:01', 1, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-09-23 01:01:01', 2019, 39, 1375, TRUE,'2019-09-23 01:01:01', 2, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-09-30 01:01:01', 2019, 40, 1381, TRUE,'2019-09-30 01:01:01', 3, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-10-07 01:01:01', 2019, 41, 1532, TRUE,'2019-10-07 01:01:01', 4, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-10-14 01:01:01', 2019, 42, 1413, TRUE,'2019-10-14 01:01:01', 5, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-10-21 01:01:01', 2019, 43, 1442, TRUE,'2019-10-21 01:01:01', 6, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-10-28 01:01:01', 2019, 44, 1386, TRUE,'2019-10-28 01:01:01', 7, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-11-04 01:01:01', 2019, 45, 1643, TRUE,'2019-11-04 01:01:01', 8, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-11-11 01:01:01', 2019, 46, 1275, TRUE,'2019-11-11 01:01:01', 9, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-11-18 01:01:01', 2019, 47, 1198, TRUE,'2019-11-18 01:01:01', 10, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-11-25 01:01:01', 2019, 48, 1370, TRUE,'2019-11-25 01:01:01', 11, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-12-02 01:01:01', 2019, 49, 1424, TRUE,'2019-12-02 01:01:01', 12, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-12-09 01:01:01', 2019, 50, 1299, TRUE,'2019-12-09 01:01:01', 13, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-12-16 01:01:01', 2019, 51, 1476, TRUE,'2019-12-16 01:01:01', 14, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-12-23 01:01:01', 2019, 52, 1562, TRUE,'2019-12-23 01:01:01', 15, 4, 1);

--- fincas de jorge alex  2020
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 1635, TRUE,'2020-01-03 01:01:01', 16, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 1633, TRUE,'2020-01-10 01:01:01', 17, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 1759, TRUE,'2020-01-17 01:01:01', 18, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 1679, TRUE,'2020-01-24 01:01:01', 19, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-31 01:01:01', 2020, 5, 1476, TRUE,'2020-01-31 01:01:01', 20, 4, 1);

--- otros productores copie de finca 5
--- INSERT INTO enfunde_enfunde (deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) SELECT enfunde_enfunde.deleted, enfunde_enfunde.fecha_enfunde, enfunde_enfunde.anio, enfunde_enfunde.semana, enfunde_enfunde.cantidad, enfunde_enfunde.activo, enfunde_enfunde.created_date, enfunde_enfunde.cinta_id_id, maestros_finca.id, enfunde_enfunde.tipo_enfunde_id_id FROM enfunde_enfunde, maestros_finca WHERE maestros_finca.id>=6 AND enfunde_enfunde.id >= 36 AND enfunde_enfunde.id <=39;

---------------------------------------------------------------------------------
---- CALIBRAJE
---------------------------------------------------------------------------------
---- Estimados  7 semanas   1 al 48
/*
1, 4 ,  16
*/
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 842, TRUE,'2020-01-03 01:01:01', 1, 1, 16);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 842, TRUE,'2020-01-03 01:01:01', 1, 2, 16);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 594, TRUE,'2020-01-03 01:01:01', 1, 3, 16);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 403, TRUE,'2020-01-03 01:01:01', 1, 4, 16);

INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 787, TRUE,'2020-01-10 01:01:01', 1, 1, 17);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 787, TRUE,'2020-01-10 01:01:01', 1, 2, 17);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 663, TRUE,'2020-01-10 01:01:01', 1, 3, 17);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 769, TRUE,'2020-01-10 01:01:01', 1, 4, 17);

INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 618, TRUE,'2020-01-17 01:01:01', 1, 1, 18);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 618, TRUE,'2020-01-17 01:01:01', 1, 2, 18);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 749, TRUE,'2020-01-17 01:01:01', 1, 3, 18);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 831, TRUE,'2020-01-17 01:01:01', 1, 4, 18);

INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 400, TRUE,'2020-01-24 01:01:01', 1, 1, 19);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 400, TRUE,'2020-01-24 01:01:01', 1, 2, 19);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 696, TRUE,'2020-01-24 01:01:01', 1, 3, 19);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 861, TRUE,'2020-01-24 01:01:01', 1, 4, 19);

--- Calibraje Jorge Alex
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 842, TRUE,'2020-01-03 01:01:01', 4, 1, 16);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 842, TRUE,'2020-01-03 01:01:01', 4, 2, 16);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 594, TRUE,'2020-01-03 01:01:01', 4, 3, 16);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 403, TRUE,'2020-01-03 01:01:01', 4, 4, 16);

INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 787, TRUE,'2020-01-10 01:01:01', 4, 1, 17);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 787, TRUE,'2020-01-10 01:01:01', 4, 2, 17);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 663, TRUE,'2020-01-10 01:01:01', 4, 3, 17);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 769, TRUE,'2020-01-10 01:01:01', 4, 4, 17);

INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 618, TRUE,'2020-01-17 01:01:01', 4, 1, 18);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 618, TRUE,'2020-01-17 01:01:01', 4, 2, 18);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 749, TRUE,'2020-01-17 01:01:01', 4, 3, 18);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 831, TRUE,'2020-01-17 01:01:01', 4, 4, 18);

INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 400, TRUE,'2020-01-24 01:01:01', 1, 1, 19);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 400, TRUE,'2020-01-24 01:01:01', 1, 2, 19);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 696, TRUE,'2020-01-24 01:01:01', 1, 3, 19);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 861, TRUE,'2020-01-24 01:01:01', 1, 4, 19);


---------------------------------------------------------------------------------
---- ESTIMADO
---------------------------------------------------------------------------------
---- Estimados  7 semanas
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 1635, TRUE,'2020-01-03 01:01:01', 1, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 1633, TRUE,'2020-01-10 01:01:01', 1, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 1759, TRUE,'2020-01-17 01:01:01', 1, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 1679, TRUE,'2020-01-24 01:01:01', 1, FALSE);

INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 1635, TRUE,'2020-01-03 01:01:01', 4, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 1633, TRUE,'2020-01-10 01:01:01', 4, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 1759, TRUE,'2020-01-17 01:01:01', 4, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 1679, TRUE,'2020-01-24 01:01:01', 4, FALSE);


---------------------------------------------------------------------------------
---- EMBARQUE
---------------------------------------------------------------------------------
-------------------
--- 2019
-------------------
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'EM201909SEM38', '2019-09-14 01:01:01', '2019-09-14 01:01:01', 2019, 38, 2376, 6.50, 22, 20, 10, 12, TRUE, '2019-09-14 01:01:01', 1, 1, 1, 1, FALSE);
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'EM201909SEM39', '2019-09-21 01:01:01', '2019-09-21 01:01:01', 2019, 39, 2280, 6.50, 22, 20, 10, 12, TRUE, '2019-09-21 01:01:01', 1, 1, 1, 1, FALSE);
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'EM201909SEM40', '2019-09-28 01:01:01', '2019-09-28 01:01:01', 2019, 40, 1656, 6.50, 22, 20, 10, 12, TRUE, '2019-09-28 01:01:01', 1, 1, 1, 1, FALSE);
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'EM201910SEM41', '2019-10-05 01:01:01', '2019-10-05 01:01:01', 2019, 41, 1656, 6.50, 22, 20, 10, 12, TRUE, '2019-10-05 01:01:01', 1, 1, 1, 1, FALSE);
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'EM201910SEM42', '2019-10-12 01:01:01', '2019-10-12 01:01:01', 2019, 42, 1944, 6.50, 22, 20, 10, 12, TRUE, '2019-10-12 01:01:01', 1, 1, 1, 1, FALSE);
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'EM201910SEM43', '2019-10-19 01:01:01', '2019-10-19 01:01:01', 2019, 43, 1656, 6.50, 22, 20, 10, 12, TRUE, '2019-10-19 01:01:01', 1, 1, 1, 1, FALSE);
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'EM201910SEM44', '2019-10-26 01:01:01', '2019-10-26 01:01:01', 2019, 44, 2064, 6.50, 22, 20, 10, 12, TRUE, '2019-10-26 01:01:01', 1, 1, 1, 1, FALSE);
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'EM201911SEM45', '2019-11-02 01:01:01', '2019-11-02 01:01:01', 2019, 45, 1632, 6.50, 22, 20, 10, 12, TRUE, '2019-11-02 01:01:01', 1, 1, 1, 1, FALSE);
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'EM201911SEM46', '2019-11-09 01:01:01', '2019-11-09 01:01:01', 2019, 46, 1056, 6.50, 22, 20, 10, 12, TRUE, '2019-11-09 01:01:01', 1, 1, 1, 1, FALSE);
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'EM201911SEM47', '2019-11-16 01:01:01', '2019-11-16 01:01:01', 2019, 47, 1558, 6.50, 22, 20, 10, 12, TRUE, '2019-11-16 01:01:01', 1, 1, 1, 1, FALSE);
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'EM201911SEM48', '2019-11-23 01:01:01', '2019-11-23 01:01:01', 2019, 48, 2096, 6.50, 22, 20, 10, 12, TRUE, '2019-11-23 01:01:01', 1, 1, 1, 1, FALSE);
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'EM201911SEM49', '2019-11-30 01:01:01', '2019-11-30 01:01:01', 2019, 49, 2304, 6.50, 22, 20, 10, 12, TRUE, '2019-11-30 01:01:01', 1, 1, 1, 1, FALSE);
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'EM201912SEM50', '2019-12-07 01:01:01', '2019-12-07 01:01:01', 2019, 50, 2786, 6.50, 22, 20, 10, 12, TRUE, '2019-12-07 01:01:01', 1, 1, 1, 1, FALSE);
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'EM201912SEM51', '2019-12-14 01:01:01', '2019-12-14 01:01:01', 2019, 51, 2040, 6.50, 22, 20, 10, 12, TRUE, '2019-12-14 01:01:01', 1, 1, 1, 1, FALSE);
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'EM201912SEM52', '2019-12-21 01:01:01', '2019-12-21 01:01:01', 2019, 52, 2136, 6.50, 22, 20, 10, 12, TRUE, '2019-12-21 01:01:01', 1, 1, 1, 1, FALSE);

-------------------
--- 2020
-------------------
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'EM202001SEM01', '2020-01-03 01:01:01', '2020-01-03 01:01:01', 2020, 1, 2496, 6.50, 22, 20, 10, 12, TRUE, '2020-01-03 01:01:01', 1, 1, 1, 1, FALSE);
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'EM202001SEM02', '2020-01-10 01:01:01', '2020-01-10 01:01:01', 2020, 2, 2903, 6.50, 22, 20, 10, 12, TRUE, '2020-01-10 01:01:01', 1, 1, 1, 1, FALSE);
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'EM202001SEM03', '2020-01-17 01:01:01', '2020-01-17 01:01:01', 2020, 3, 2736, 6.50, 22, 20, 10, 12, TRUE, '2020-01-17 01:01:01', 1, 1, 1, 1, FALSE);
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'EM202001SEM04', '2020-01-24 01:01:01', '2020-01-24 01:01:01', 2020, 4, 2732, 6.50, 22, 20, 10, 12, TRUE, '2020-01-24 01:01:01', 1, 1, 1, 1, FALSE);

---------------------------------------------------------------------------------
---- ORDEN DE CORTE
---------------------------------------------------------------------------------
-------------------
--- 2019
-------------------
INSERT INTO envio_ordencorte(deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) VALUES (NULL , 'BOD123', '2019-09-14 01:01:01', 2019, 38, 2376, TRUE, '2019-09-14 01:01:01', 1, 1, FALSE);
INSERT INTO envio_ordencorte(deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) VALUES (NULL , 'BOD123', '2019-09-21 01:01:01', 2019, 39, 2280, TRUE, '2019-09-21 01:01:01', 2, 1, FALSE);
INSERT INTO envio_ordencorte(deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) VALUES (NULL , 'BOD123', '2019-09-28 01:01:01', 2019, 40, 1656, TRUE, '2019-09-28 01:01:01', 3, 1, FALSE);
INSERT INTO envio_ordencorte(deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) VALUES (NULL , 'BOD123', '2019-10-05 01:01:01', 2019, 41, 1656, TRUE, '2019-10-05 01:01:01', 4, 1, FALSE);
INSERT INTO envio_ordencorte(deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) VALUES (NULL , 'BOD123', '2019-10-12 01:01:01', 2019, 42, 1944, TRUE, '2019-10-12 01:01:01', 5, 1, FALSE);
INSERT INTO envio_ordencorte(deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) VALUES (NULL , 'BOD123', '2019-10-19 01:01:01', 2019, 43, 1656, TRUE, '2019-10-19 01:01:01', 6, 1, FALSE);
INSERT INTO envio_ordencorte(deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) VALUES (NULL , 'BOD123', '2019-10-26 01:01:01', 2019, 44, 2064, TRUE, '2019-10-26 01:01:01', 7, 1, FALSE);
INSERT INTO envio_ordencorte(deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) VALUES (NULL , 'BOD123', '2019-11-02 01:01:01', 2019, 45, 1632, TRUE, '2019-11-02 01:01:01', 8, 1, FALSE);
INSERT INTO envio_ordencorte(deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) VALUES (NULL , 'BOD123', '2019-11-09 01:01:01', 2019, 46, 1056, TRUE, '2019-11-09 01:01:01', 9, 1, FALSE);
INSERT INTO envio_ordencorte(deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) VALUES (NULL , 'BOD123', '2019-11-16 01:01:01', 2019, 47, 1558, TRUE, '2019-11-16 01:01:01', 10, 1, FALSE);
INSERT INTO envio_ordencorte(deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) VALUES (NULL , 'BOD123', '2019-11-23 01:01:01', 2019, 48, 2096, TRUE, '2019-11-23 01:01:01', 11, 1, FALSE);
INSERT INTO envio_ordencorte(deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) VALUES (NULL , 'BOD123', '2019-12-07 01:01:01', 2019, 49, 2304, TRUE, '2019-12-07 01:01:01', 12, 1, FALSE);
INSERT INTO envio_ordencorte(deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) VALUES (NULL , 'BOD123', '2019-12-14 01:01:01', 2019, 50, 2786, TRUE, '2019-12-14 01:01:01', 13, 1, FALSE);
INSERT INTO envio_ordencorte(deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) VALUES (NULL , 'BOD123', '2019-12-21 01:01:01', 2019, 51, 2040, TRUE, '2019-12-21 01:01:01', 14, 1, FALSE);
INSERT INTO envio_ordencorte(deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) VALUES (NULL , 'BOD123', '2019-12-28 01:01:01', 2019, 52, 2136, TRUE, '2019-12-28 01:01:01', 15, 1, FALSE);
-------------------
--- 2020
-------------------
INSERT INTO envio_ordencorte(deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) VALUES (NULL , 'BOD123', '2020-01-03 01:01:01', 2020, 1, 2496, TRUE, '2020-01-03 01:01:01', 16, 1, FALSE);
INSERT INTO envio_ordencorte(deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) VALUES (NULL , 'BOD123', '2020-01-10 01:01:01', 2020, 2, 2903, TRUE, '2020-01-10 01:01:01', 17, 1, FALSE);
INSERT INTO envio_ordencorte(deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) VALUES (NULL , 'BOD123', '2020-01-17 01:01:01', 2020, 3, 2736, TRUE, '2020-01-17 01:01:01', 18, 1, FALSE);
INSERT INTO envio_ordencorte(deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) VALUES (NULL , 'BOD123', '2020-01-24 01:01:01', 2020, 4, 2732, TRUE, '2020-01-24 01:01:01', 19, 1, FALSE);


---------------------------------------------------------------------------------
---- RACIMO CONTROL
---------------------------------------------------------------------------------
----------------------------------------
--- ANIO 2019
----------------------------------------
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-09-14 01:01:01', 1, 1, 8);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-09-14 01:01:01', 1, 6, 9);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 1, TRUE, '2019-09-14 01:01:01', 1, 5, 10);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 12, TRUE, '2019-09-14 01:01:01', 1, 4, 11);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 328, TRUE, '2019-09-14 01:01:01', 1, 3, 12);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 1679, TRUE, '2019-09-14 01:01:01', 1, 2, 13);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 37, TRUE, '2019-09-14 01:01:01', 1, 1, 14);
---
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, TRUE, 39, TRUE, '2019-09-14 01:01:01', 1, 1, 0);
----------------------------------------
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-09-21 01:01:01', 2, 1, 8);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-09-21 01:01:01', 2, 6, 9);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-09-21 01:01:01', 2, 5, 10);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 7, TRUE, '2019-09-21 01:01:01', 2, 4, 11);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 384, TRUE, '2019-09-21 01:01:01', 2, 3, 12);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 1459, TRUE, '2019-09-21 01:01:01', 2, 2, 13);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 118, TRUE, '2019-09-21 01:01:01', 2, 1, 14);
---
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, TRUE, 44, TRUE, '2019-09-21 01:01:01', 2, 1, 0);
----------------------------------------
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-09-28 01:01:01', 3, 1, 8);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-09-28 01:01:01', 3, 6, 9);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-09-28 01:01:01', 3, 5, 10);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 6, TRUE, '2019-09-28 01:01:01', 3, 4, 11);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 121, TRUE, '2019-09-28 01:01:01', 3, 3, 12);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 1230, TRUE, '2019-09-28 01:01:01', 3, 2, 13);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 54, TRUE, '2019-09-28 01:01:01', 3, 1, 14);
---
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, TRUE, 51, TRUE, '2019-09-28 01:01:01', 3, 1, 0);
----------------------------------------
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-10-05 01:01:01', 4, 1, 8);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-10-05 01:01:01', 4, 6, 9);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-10-05 01:01:01', 4, 5, 10);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-10-05 01:01:01', 4, 4, 11);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 2, TRUE, '2019-10-05 01:01:01', 4, 3, 12);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 1399, TRUE, '2019-10-05 01:01:01', 4, 2, 13);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 49, TRUE, '2019-10-05 01:01:01', 4, 1, 14);
---
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, TRUE, 66, TRUE, '2019-10-05 01:01:01', 4, 1, 0);
----------------------------------------
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-10-12 01:01:01', 5, 1, 8);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-10-12 01:01:01', 5, 6, 9);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-10-12 01:01:01', 5, 5, 10);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 1, TRUE, '2019-10-12 01:01:01', 5, 4, 11);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 34, TRUE, '2019-10-12 01:01:01', 5, 3, 12);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 1583, TRUE, '2019-10-12 01:01:01', 5, 2, 13);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 104, TRUE, '2019-10-12 01:01:01', 5, 1, 14);
---
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, TRUE, 75, TRUE, '2019-10-12 01:01:01', 5, 1, 0);
----------------------------------------
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-10-19 01:01:01', 6, 1, 8);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-10-19 01:01:01', 6, 6, 9);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-10-19 01:01:01', 6, 5, 10);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-10-19 01:01:01', 6, 4, 11);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 3, TRUE, '2019-10-19 01:01:01', 6, 3, 12);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 1406, TRUE, '2019-10-19 01:01:01', 6, 2, 13);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 68, TRUE, '2019-10-19 01:01:01', 6, 1, 14);
---
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, TRUE, 44, TRUE, '2019-10-19 01:01:01', 6, 1, 0);
----------------------------------------
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-10-26 01:01:01', 7, 1, 8);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 1, TRUE, '2019-10-26 01:01:01', 7, 6, 9);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 1, TRUE, '2019-10-26 01:01:01', 7, 5, 10);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 2, TRUE, '2019-10-26 01:01:01', 7, 4, 11);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 59, TRUE, '2019-10-26 01:01:01', 7, 3, 12);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 1558, TRUE, '2019-10-26 01:01:01', 7, 2, 13);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 51, TRUE, '2019-10-26 01:01:01', 7, 1, 14);
---
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, TRUE, 89, TRUE, '2019-10-26 01:01:01', 7, 1, 0);
----------------------------------------
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-11-02 01:01:01', 8, 1, 8);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 3, TRUE, '2019-11-02 01:01:01', 8, 6, 9);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-11-02 01:01:01', 8, 5, 10);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 5, TRUE, '2019-11-02 01:01:01', 8, 4, 11);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 140, TRUE, '2019-11-02 01:01:01', 8, 3, 12);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 1085, TRUE, '2019-11-02 01:01:01', 8, 2, 13);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 100, TRUE, '2019-11-02 01:01:01', 8, 1, 14);
---
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, TRUE, 56, TRUE, '2019-11-02 01:01:01', 8, 1, 0);
----------------------------------------
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-11-09 01:01:01', 9, 1, 8);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 8, TRUE, '2019-11-09 01:01:01', 9, 6, 9);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-11-09 01:01:01', 9, 5, 10);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-11-09 01:01:01', 9, 4, 11);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 14, TRUE, '2019-11-09 01:01:01', 9, 3, 12);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 440, TRUE, '2019-11-09 01:01:01', 9, 2, 13);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 378, TRUE, '2019-11-09 01:01:01', 9, 1, 14);
---
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, TRUE, 32, TRUE, '2019-11-09 01:01:01', 9, 1, 0);
----------------------------------------
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-11-16 01:01:01', 10, 1, 8);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 12, TRUE, '2019-11-16 01:01:01', 10, 6, 9);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-11-16 01:01:01', 10, 5, 10);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-11-16 01:01:01', 10, 4, 11);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 4, TRUE, '2019-11-16 01:01:01', 10, 3, 12);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 331, TRUE, '2019-11-16 01:01:01', 10, 2, 13);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 906, TRUE, '2019-11-16 01:01:01', 10, 1, 14);
---
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, TRUE, 43, TRUE, '2019-11-16 01:01:01', 10, 1, 0);
----------------------------------------
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-11-23 01:01:01', 11, 1, 8);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 51, TRUE, '2019-11-23 01:01:01', 11, 6, 9);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 4, TRUE, '2019-11-23 01:01:01', 11, 5, 10);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 6, TRUE, '2019-11-23 01:01:01', 11, 4, 11);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 62, TRUE, '2019-11-23 01:01:01', 11, 3, 12);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 586, TRUE, '2019-11-23 01:01:01', 11, 2, 13);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 1071, TRUE, '2019-11-23 01:01:01', 11, 1, 14);
---
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, TRUE, 54, TRUE, '2019-11-23 01:01:01', 11, 1, 0);
----------------------------------------
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-11-30 01:01:01', 12, 1, 8);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 63, TRUE, '2019-11-30 01:01:01', 12, 6, 9);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 1, TRUE, '2019-11-30 01:01:01', 12, 5, 10);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 53, TRUE, '2019-11-30 01:01:01', 12, 4, 11);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 135, TRUE, '2019-11-30 01:01:01', 12, 3, 12);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 864, TRUE, '2019-11-30 01:01:01', 12, 2, 13);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 791, TRUE, '2019-11-30 01:01:01', 12, 1, 14);
---
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, TRUE, 100, TRUE, '2019-11-30 01:01:01', 12, 1, 0);
----------------------------------------
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-12-07 01:01:01', 13, 1, 8);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 16, TRUE, '2019-12-07 01:01:01', 13, 6, 9);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 2, TRUE, '2019-12-07 01:01:01', 13, 5, 10);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 16, TRUE, '2019-12-07 01:01:01', 13, 4, 11);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 621, TRUE, '2019-12-07 01:01:01', 13, 3, 12);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 1270, TRUE, '2019-12-07 01:01:01', 13, 2, 13);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 409, TRUE, '2019-12-07 01:01:01', 13, 1, 14);
---
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, TRUE, 137, TRUE, '2019-12-07 01:01:01', 13, 1, 0);
----------------------------------------
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-12-14 01:01:01', 14, 1, 8);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 3, TRUE, '2019-12-14 01:01:01', 14, 6, 9);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 9, TRUE, '2019-12-14 01:01:01', 14, 5, 10);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 135, TRUE, '2019-12-14 01:01:01', 14, 4, 11);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 607, TRUE, '2019-12-14 01:01:01', 14, 3, 12);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 872, TRUE, '2019-12-14 01:01:01', 14, 2, 13);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 37, TRUE, '2019-12-14 01:01:01', 14, 1, 14);
---
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, TRUE, 78, TRUE, '2019-12-14 01:01:01', 14, 1, 0);
----------------------------------------
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-12-21 01:01:01', 15, 1, 8);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2019-12-21 01:01:01', 15, 6, 9);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 7, TRUE, '2019-12-21 01:01:01', 15, 5, 10);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 164, TRUE, '2019-12-21 01:01:01', 15, 4, 11);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 695, TRUE, '2019-12-21 01:01:01', 15, 3, 12);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 781, TRUE, '2019-12-21 01:01:01', 15, 2, 13);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 16, TRUE, '2019-12-21 01:01:01', 15, 1, 14);
---
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, TRUE, 72, TRUE, '2019-12-21 01:01:01', 15, 1, 0);
----------------------------------------




----------------------------------------
--- ANIO 2020
----------------------------------------
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2020-01-03 01:01:01', 16, 1, 8);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 1, TRUE, '2020-01-03 01:01:01', 16, 6, 9);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 12, TRUE, '2020-01-03 01:01:01', 16, 5, 10);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 403, TRUE, '2020-01-03 01:01:01', 16, 4, 11);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 842, TRUE, '2020-01-03 01:01:01', 16, 3, 12);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 582, TRUE, '2020-01-03 01:01:01', 16, 2, 13);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 11, TRUE, '2020-01-03 01:01:01', 16, 1, 14);
-------------
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2020-01-10 01:01:01', 17, 2, 8);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 14, TRUE, '2020-01-10 01:01:01', 17, 1, 9);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 192, TRUE, '2020-01-10 01:01:01', 17, 6, 10);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 769, TRUE, '2020-01-10 01:01:01', 17, 5, 11);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 787, TRUE, '2020-01-10 01:01:01', 17, 4, 12);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 441, TRUE, '2020-01-10 01:01:01', 17, 3, 13);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 16, TRUE, '2020-01-10 01:01:01', 17, 2, 14);
-------------
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2020-01-17 01:01:01', 18, 3, 8);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 49, TRUE, '2020-01-17 01:01:01', 18, 2, 9);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 749, TRUE, '2020-01-17 01:01:01', 18, 1, 10);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 831, TRUE, '2020-01-17 01:01:01', 18, 6, 11);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 618, TRUE, '2020-01-17 01:01:01', 18, 5, 12);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 149, TRUE, '2020-01-17 01:01:01', 18, 4, 13);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 42, TRUE, '2020-01-17 01:01:01', 18, 3, 14);
-------------
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2020-01-24 01:01:01', 19, 4, 8);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 78, TRUE, '2020-01-24 01:01:01', 19, 3, 9);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 553, TRUE, '2020-01-24 01:01:01', 19, 2, 10);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 861, TRUE, '2020-01-24 01:01:01', 19, 1, 11);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 400, TRUE, '2020-01-24 01:01:01', 19, 6, 12);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 65, TRUE, '2020-01-24 01:01:01', 19, 5, 13);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, FALSE, 0, TRUE, '2020-01-24 01:01:01', 19, 4, 14);
---
--- Rechazo, controlcolor_id_id=1, NO se debe usar en codigo, rechazo no tiene color.
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, TRUE, 84, TRUE, '2020-01-03 01:01:01', 16, 1, 0);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, TRUE, 191, TRUE, '2020-01-10 01:01:01', 17, 1, 0);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, TRUE, 191, TRUE, '2020-01-17 01:01:01', 18, 1, 0);
INSERT INTO control_racimocontrol(deleted, rechazo, cantidad, activo, created_date, orden_corte_id_id, controlcolor_id_id, edad) VALUES (NULL, TRUE, 134, TRUE, '2020-01-24 01:01:01', 19, 1, 0);

---------------------------------------------------------------------------------
---- CAJA CONTROL
---------------------------------------------------------------------------------
INSERT INTO control_cajacontrol(deleted, segunda, cantidad, activo, created_date, orden_corte_id_id, tipo_caja_id_id) VALUES (NULL, FALSE, 2496, TRUE, '2020-01-03 01:01:01', 1, 5);
INSERT INTO control_cajacontrol(deleted, segunda, cantidad, activo, created_date, orden_corte_id_id, tipo_caja_id_id) VALUES (NULL, FALSE, 1700, TRUE, '2020-01-10 01:01:01', 2, 3);
INSERT INTO control_cajacontrol(deleted, segunda, cantidad, activo, created_date, orden_corte_id_id, tipo_caja_id_id) VALUES (NULL, FALSE, 1203, TRUE, '2020-01-10 01:01:01', 2, 7);
INSERT INTO control_cajacontrol(deleted, segunda, cantidad, activo, created_date, orden_corte_id_id, tipo_caja_id_id) VALUES (NULL, FALSE, 1200, TRUE, '2020-01-17 01:01:01', 3, 3);
INSERT INTO control_cajacontrol(deleted, segunda, cantidad, activo, created_date, orden_corte_id_id, tipo_caja_id_id) VALUES (NULL, FALSE, 576, TRUE, '2020-01-17 01:01:01', 3, 5);
INSERT INTO control_cajacontrol(deleted, segunda, cantidad, activo, created_date, orden_corte_id_id, tipo_caja_id_id) VALUES (NULL, FALSE, 960, TRUE, '2020-01-17 01:01:01', 3, 7);
INSERT INTO control_cajacontrol(deleted, segunda, cantidad, activo, created_date, orden_corte_id_id, tipo_caja_id_id) VALUES (NULL, FALSE, 1200, TRUE, '2020-01-24 01:01:01', 4, 3);
INSERT INTO control_cajacontrol(deleted, segunda, cantidad, activo, created_date, orden_corte_id_id, tipo_caja_id_id) VALUES (NULL, FALSE, 1008, TRUE, '2020-01-24 01:01:01', 4, 5);
INSERT INTO control_cajacontrol(deleted, segunda, cantidad, activo, created_date, orden_corte_id_id, tipo_caja_id_id) VALUES (NULL, FALSE, 375, TRUE, '2020-01-24 01:01:01', 4, 7);
--- CAJAS DE SEGUNDA
INSERT INTO control_cajacontrol(deleted, segunda, cantidad, activo, created_date, orden_corte_id_id, tipo_caja_id_id) VALUES (NULL, TRUE, 149, TRUE, '2020-01-24 01:01:01', 4, 1);


