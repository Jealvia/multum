/*
pythonanywhere.com/user/sosnegsa/consoles/14602004

sosnegsa@gmail.com
#D3sAdm1n

cd multum-backend/multum/
---- python manage.py migrate
sqlite3 db.sqlite3

https://hackintosher.com/guides/virtual-macos-use-macos-sierra-virtual-machine-vmware/
https://www.youtube.com/watch?v=iUhF07XaF1w&t=205s

https://www.brsoftech.com/blog/iot-application-in-agriculture/


SELECT * FROM maestros_tipofinca;

PRAGMA table_info(enfunde_tipocalibraje);

.tables
.schema
.headers on
.mode column

Explicacion: la fecha la maneja como un string siempre en YYYY-MM-DD ejemplo '2020-08-03'.   El insert debe tener ese orden y los meses y dias deben ser completados con cero.
semana 3  = '2020-01-13','2020-01-19'

ALTER TABLE enfunde_cinta ALTER COLUMN color TYPE VARCHAR(12);

ALTER TABLE maestros_persona ALTER COLUMN primer_apellido DROP NOT NULL;
ALTER TABLE maestros_persona ALTER COLUMN segundo_apellido DROP NOT NULL;
ALTER TABLE maestros_persona ALTER COLUMN primer_nombre DROP NOT NULL;
ALTER TABLE maestros_persona ALTER COLUMN segundo_nombre DROP NOT NULL;
ALTER TABLE maestros_persona ALTER COLUMN fecha_nacimiento DROP NOT NULL;
ALTER TABLE registro_personarol ALTER COLUMN agro_artesanal_id_id DROP NOT NULL;

ALTER TABLE maestros_finca ADD productor_id_id integer NOT NULL;
ALTER TABLE maestros_finca ADD tecnico_id_id integer NOT NULL;
ALTER TABLE maestros_finca ADD calificador_id_id integer NOT NULL;


PRAGMA table_info(facturacion_facturacabecera);
PRAGMA table_info(facturacion_facturadetalle);
PRAGMA table_info(envio_embarque);
PRAGMA table_info(envio_ordencorte);
PRAGMA table_info(enfunde_calibraje);
PRAGMA table_info(enfunde_enfunde);
PRAGMA table_info(estimado_estimadocaja);
PRAGMA table_info(maestros_notificacion);
PRAGMA table_info(maestros_finca);
PRAGMA table_info(registro_personarol);
PRAGMA table_info(maestros_persona);
PRAGMA table_info(enfunde_cinta);
PRAGMA table_info(maestros_tiponotificacion);
PRAGMA table_info(maestros_tipoidentificacion);
PRAGMA table_info(maestros_tipofinca);
PRAGMA table_info(maestros_agroartesanal);
PRAGMA table_info(maestros_puerto);
PRAGMA table_info(maestros_vapor);
PRAGMA table_info(maestros_sector);
PRAGMA table_info(enfunde_tipoenfunde);
PRAGMA table_info(enfunde_tipocalibraje);
PRAGMA table_info(maestros_tiporol);
PRAGMA table_info(envio_marcacaja);
PRAGMA table_info(auth_user);


SELECT * FROM facturacion_facturacabecera;
SELECT * FROM facturacion_facturadetalle;
SELECT * FROM envio_embarque;
SELECT * FROM envio_ordencorte;
SELECT * FROM enfunde_calibraje;
SELECT * FROM enfunde_enfunde;
SELECT * FROM estimado_estimadocaja;
SELECT * FROM maestros_notificacion;
SELECT * FROM maestros_finca;
SELECT * FROM registro_personarol;
SELECT * FROM maestros_persona;
SELECT * FROM enfunde_cinta;
SELECT * FROM maestros_tiponotificacion;
SELECT * FROM maestros_tipoidentificacion;
SELECT * FROM maestros_tipofinca;
SELECT * FROM maestros_agroartesanal;
SELECT * FROM maestros_puerto;
SELECT * FROM maestros_vapor;
SELECT * FROM maestros_sector;
SELECT * FROM enfunde_tipoenfunde;
SELECT * FROM enfunde_tipocalibraje;
SELECT * FROM maestros_tiporol;
SELECT * FROM envio_marcacaja;
SELECT * FROM auth_user;
*/

DELETE FROM facturacion_facturacabecera;
DELETE FROM facturacion_facturadetalle;
DELETE FROM envio_embarque;
DELETE FROM envio_ordencorte;
DELETE FROM enfunde_calibraje;
DELETE FROM enfunde_enfunde;
DELETE FROM estimado_estimadocaja;
DELETE FROM maestros_notificacion;
DELETE FROM maestros_finca;
DELETE FROM registro_personarol;
DELETE FROM maestros_persona;
DELETE FROM enfunde_cinta;
DELETE FROM maestros_tiponotificacion;
DELETE FROM maestros_tipoidentificacion;
DELETE FROM maestros_tipofinca;
DELETE FROM maestros_agroartesanal;
DELETE FROM maestros_puerto;
DELETE FROM maestros_vapor;
DELETE FROM maestros_sector;
DELETE FROM enfunde_tipoenfunde;
DELETE FROM enfunde_tipocalibraje;
DELETE FROM maestros_tiporol;
DELETE FROM envio_marcacaja;
DELETE FROM auth_user;


UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='facturacion_facturacabecera';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='facturacion_facturadetalle';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='envio_embarque';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='envio_ordencorte';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='enfunde_calibraje';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='enfunde_enfunde';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='estimado_estimadocaja';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_notificacion';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_finca';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='registro_personarol';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_persona';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='enfunde_cinta';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_tiponotificacion';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_tipoidentificacion';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_tipofinca';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_agroartesanal';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_puerto';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_vapor';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_sector';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='enfunde_tipoenfunde';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='enfunde_tipocalibraje';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_tiporol';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='envio_marcacaja';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='auth_user';

/*
--- En postgres
ALTER SEQUENCE facturacion_facturadetalle_id_seq RESTART WITH 1;
ALTER SEQUENCE facturacion_facturacabecera_id_seq RESTART WITH 1;
ALTER SEQUENCE envio_embarque_id_seq RESTART WITH 1;
ALTER SEQUENCE envio_ordencorte_id_seq RESTART WITH 1;
ALTER SEQUENCE enfunde_calibraje_id_seq RESTART WITH 1;
ALTER SEQUENCE enfunde_enfunde_id_seq RESTART WITH 1;
ALTER SEQUENCE estimado_estimadocaja_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_notificacion_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_finca_id_seq RESTART WITH 1;
ALTER SEQUENCE registro_personarol_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_persona_id_seq RESTART WITH 1;
ALTER SEQUENCE enfunde_cinta_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_tiponotificacion_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_tipoidentificacion_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_tipofinca_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_agroartesanal_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_puerto_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_vapor_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_sector_id_seq RESTART WITH 1;
ALTER SEQUENCE enfunde_tipoenfunde_id_seq RESTART WITH 1;
ALTER SEQUENCE enfunde_tipocalibraje_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_tiporol_id_seq RESTART WITH 1;
ALTER SEQUENCE envio_marcacaja_id_seq RESTART WITH 1;
ALTER SEQUENCE auth_user_id_seq RESTART WITH 1;
*/

/*
UPDATE auth_user SET username='IREN', first_name='IREN', email='iren@gmail.com' WHERE id=2;
UPDATE maestros_persona SET nombre='IREN' WHERE id=2;
UPDATE maestros_persona SET primer_apellido='NAULA', segundo_apellido='MORA', primer_nombre='FREDDY', segundo_nombre='SEGUNDO' WHERE id=5;
UPDATE maestros_persona SET primer_apellido='SERRANO', segundo_apellido='AGUILAR', primer_nombre='JORGE', segundo_nombre='ALEX', nombre='JORGE ALEX SERRANO AGUILAR' WHERE id=6;
SELECT * FROM maestros_persona;
*/

INSERT INTO maestros_controledad(deleted, edadminima, edadmaxima, edadreferencia, ordenmaximo, activo, created_date) VALUES (NULL, 8, 14, 12, 5, 1,'2020-01-09 01:01:01');

INSERT INTO maestros_controlcolor(deleted, color, ordenenfunde, ordenedad, activo, created_date) VALUES (NULL, 'negro', 1, 1, 1,'2020-01-09 01:01:01');
INSERT INTO maestros_controlcolor(deleted, color, ordenenfunde, ordenedad, activo, created_date) VALUES (NULL, 'amarillo', 2, 6, 1,'2020-01-09 01:01:01');
INSERT INTO maestros_controlcolor(deleted, color, ordenenfunde, ordenedad, activo, created_date) VALUES (NULL, 'blanco', 3, 5, 1,'2020-01-09 01:01:01');
INSERT INTO maestros_controlcolor(deleted, color, ordenenfunde, ordenedad, activo, created_date) VALUES (NULL, 'verde', 4, 4, 1,'2020-01-09 01:01:01');
INSERT INTO maestros_controlcolor(deleted, color, ordenenfunde, ordenedad, activo, created_date) VALUES (NULL, 'azul', 5, 3, 1,'2020-01-09 01:01:01');
INSERT INTO maestros_controlcolor(deleted, color, ordenenfunde, ordenedad, activo, created_date) VALUES (NULL, 'rojo', 6, 2, 1,'2020-01-09 01:01:01');

---- EXPORTADORA: JASAFRUT  2019   38 A 52
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-09-14 01:01:01','2019-09-20 01:01:01',2019,38,'blanco','FFFFFF',1,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-09-21 01:01:01','2019-09-27 01:01:01',2019,39,'verde','31A833',1,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-09-28 01:01:01','2019-10-04 01:01:01',2019,40,'azul','F60D0D',1,'2019-01-09 01:01:01');

INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-10-05 01:01:01','2019-10-11 01:01:01',2019,41,'rojo','F60D0D',1,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-10-12 01:01:01','2019-10-18 01:01:01',2019,42,'negro','000000',1,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-10-19 01:01:01','2019-10-25 01:01:01',2019,43,'amarillo','F8F32B',1,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-10-26 01:01:01','2019-11-01 01:01:01',2019,44,'blanco','FFFFFF',1,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-11-02 01:01:01','2019-11-08 01:01:01',2019,45,'verde','31A833',1,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-11-09 01:01:01','2019-11-15 01:01:01',2019,46,'azul','F60D0D',1,'2019-01-09 01:01:01');

INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-11-16 01:01:01','2019-11-22 01:01:01',2019,47,'rojo','F60D0D',1,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-11-23 01:01:01','2019-11-29 01:01:01',2019,48,'negro','000000',1,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-11-30 01:01:01','2019-12-06 01:01:01',2019,49,'amarillo','F8F32B',1,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-12-07 01:01:01','2019-12-13 01:01:01',2019,50,'blanco','FFFFFF',1,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-12-14 01:01:01','2019-12-20 01:01:01',2019,51,'verde','31A833',1,'2019-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-12-21 01:01:01','2019-12-27 01:01:01',2019,52,'azul','F60D0D',1,'2019-01-09 01:01:01');


---- EXPORTADORA: JASAFRUT  2020  (16)
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-12-30 01:01:01','2020-01-05 01:01:01',2020,1,'rojo','F60D0D',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-01-06 01:01:01','2020-01-12 01:01:01',2020,2,'negro','000000',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-01-13 01:01:01','2020-01-19 01:01:01',2020,3,'amarillo','F8F32B',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-01-20 01:01:01','2020-01-26 01:01:01',2020,4,'blanco','FFFFFF',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-01-27 01:01:01','2020-02-02 01:01:01',2020,5,'verde','31A833',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-02-03 01:01:01','2020-02-09 01:01:01',2020,6,'azul','F60D0D',1,'2020-01-09 01:01:01');

INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-02-10 01:01:01','2020-02-16 01:01:01',2020,7,'rojo','F60D0D',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-02-17 01:01:01','2020-02-23 01:01:01',2020,8,'negro','000000',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-02-24 01:01:01','2020-03-01 01:01:01',2020,9,'amarillo','F8F32B',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-03-02 01:01:01','2020-03-08 01:01:01',2020,10,'blanco','FFFFFF',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-03-09 01:01:01','2020-03-15 01:01:01',2020,11,'verde','31A833',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-03-16 01:01:01','2020-03-22 01:01:01',2020,12,'azul','F60D0D',1,'2020-01-09 01:01:01');

INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-03-23 01:01:01','2020-03-29 01:01:01',2020,13,'rojo','F60D0D',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-03-30 01:01:01','2020-04-05 01:01:01',2020,14,'negro','000000',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-04-06 01:01:01','2020-04-12 01:01:01',2020,15,'amarillo','F8F32B',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-04-13 01:01:01','2020-04-19 01:01:01',2020,16,'blanco','FFFFFF',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-04-20 01:01:01','2020-04-26 01:01:01',2020,17,'verde','31A833',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-04-27 01:01:01','2020-05-03 01:01:01',2020,18,'azul','F60D0D',1,'2020-01-09 01:01:01');

INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-05-04 01:01:01','2020-05-10 01:01:01',2020,19,'rojo','F60D0D',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-05-11 01:01:01','2020-05-17 01:01:01',2020,20,'negro','000000',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-05-18 01:01:01','2020-05-24 01:01:01',2020,21,'amarillo','F8F32B',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-05-25 01:01:01','2020-05-31 01:01:01',2020,22,'blanco','FFFFFF',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-06-01 01:01:01','2020-06-07 01:01:01',2020,23,'verde','31A833',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-06-08 01:01:01','2020-06-14 01:01:01',2020,24,'azul','F60D0D',1,'2020-01-09 01:01:01');

INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-06-15 01:01:01','2020-06-21 01:01:01',2020,25,'rojo','F60D0D',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-06-22 01:01:01','2020-06-28 01:01:01',2020,26,'negro','000000',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-06-29 01:01:01','2020-07-05 01:01:01',2020,27,'amarillo','F8F32B',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-07-06 01:01:01','2020-07-12 01:01:01',2020,28,'blanco','FFFFFF',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-07-13 01:01:01','2020-07-19 01:01:01',2020,29,'verde','31A833',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-07-20 01:01:01','2020-07-26 01:01:01',2020,30,'azul','F60D0D',1,'2020-01-09 01:01:01');

INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-07-27 01:01:01','2020-08-02 01:01:01',2020,31,'rojo','F60D0D',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-08-03 01:01:01','2020-08-09 01:01:01',2020,32,'negro','000000',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-08-10 01:01:01','2020-08-16 01:01:01',2020,33,'amarillo','F8F32B',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-08-17 01:01:01','2020-08-23 01:01:01',2020,34,'blanco','FFFFFF',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-08-24 01:01:01','2020-08-30 01:01:01',2020,35,'verde','31A833',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-08-31 01:01:01','2020-09-06 01:01:01',2020,36,'azul','F60D0D',1,'2020-01-09 01:01:01');

INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-09-07 01:01:01','2020-09-13 01:01:01',2020,37,'rojo','F60D0D',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-09-14 01:01:01','2020-09-20 01:01:01',2020,38,'negro','000000',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-09-21 01:01:01','2020-09-27 01:01:01',2020,39,'amarillo','F8F32B',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-09-28 01:01:01','2020-10-04 01:01:01',2020,40,'blanco','FFFFFF',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-10-05 01:01:01','2020-10-11 01:01:01',2020,41,'verde','31A833',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-10-12 01:01:01','2020-10-18 01:01:01',2020,42,'azul','F60D0D',1,'2020-01-09 01:01:01');

INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-10-19 01:01:01','2020-10-25 01:01:01',2020,43,'rojo','F60D0D',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-10-26 01:01:01','2020-11-01 01:01:01',2020,44,'negro','000000',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-11-02 01:01:01','2020-11-08 01:01:01',2020,45,'amarillo','F8F32B',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-11-09 01:01:01','2020-11-15 01:01:01',2020,46,'blanco','FFFFFF',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-11-16 01:01:01','2020-11-22 01:01:01',2020,47,'verde','31A833',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-11-23 01:01:01','2020-11-29 01:01:01',2020,48,'azul','F60D0D',1,'2020-01-09 01:01:01');

INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-11-30 01:01:01','2020-12-06 01:01:01',2020,49,'rojo','F60D0D',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-12-07 01:01:01','2020-12-13 01:01:01',2020,50,'negro','000000',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-12-14 01:01:01','2020-12-20 01:01:01',2020,51,'amarillo','F8F32B',1,'2020-01-09 01:01:01');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-12-21 01:01:01','2020-12-27 01:01:01',2020,52,'blanco','FFFFFF',1,'2020-01-09 01:01:01');


INSERT INTO maestros_tipoidentificacion( deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'RUC', 'R', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_tipoidentificacion( deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'CEDULA', 'C', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_tipoidentificacion( deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'PASAPORTE', 'P', 1,'2020-01-09 01:01:01');

INSERT INTO maestros_tipofinca(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'CONVENCIONAL', 'CONV', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_tipofinca(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'ORGANICA', 'ORGA', 1,'2020-01-09 01:01:01');

INSERT INTO maestros_agroartesanal(deleted, nombre, activo, created_date) VALUES (NULL, 'EL GUABO', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_agroartesanal(deleted, nombre, activo, created_date) VALUES (NULL, 'SAN CARLOS', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_agroartesanal(deleted, nombre, activo, created_date) VALUES (NULL, 'SANTA ROSA', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_agroartesanal(deleted, nombre, activo, created_date) VALUES (NULL, 'PASAJE', 1,'2020-01-09 01:01:01');

INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO BOLÍVAR', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO MANTA', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO GYE', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO DE OAKLAND', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO DE SEATTLE-TACOMA', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO DE VALENCIA', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO DE EL HAVRE', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO DE FLENSBURG', 1,'2020-01-09 01:01:01');

INSERT INTO maestros_vapor(deleted, nombre, activo, created_date) VALUES (NULL, 'SUNRISE', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_vapor(deleted, nombre, activo, created_date) VALUES (NULL, 'LIMARI', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_vapor(deleted, nombre, activo, created_date) VALUES (NULL, 'NORTHERN DEFENDER', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_vapor(deleted, nombre, activo, created_date) VALUES (NULL, 'ANTILEN', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_vapor(deleted, nombre, activo, created_date) VALUES (NULL, 'AQUAVIT', 1,'2020-01-09 01:01:01');

INSERT INTO maestros_sector(deleted, nombre, activo, created_date) VALUES (NULL, 'TILLALES', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_sector(deleted, nombre, activo, created_date) VALUES (NULL, 'BARBONES', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_sector(deleted, nombre, activo, created_date) VALUES (NULL, 'SAN CARLOS', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_sector(deleted, nombre, activo, created_date) VALUES (NULL, 'SAUCES', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_sector(deleted, nombre, activo, created_date) VALUES (NULL, 'PROSPERINA', 1,'2020-01-09 01:01:01');

INSERT INTO enfunde_tipoenfunde(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'ENFUNDE', 'E', 1,'2020-01-09 01:01:01');
INSERT INTO enfunde_tipoenfunde(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'RECUPERACION', 'R', 1,'2020-01-09 01:01:01');

INSERT INTO enfunde_tipocalibraje(deleted, nombre, codigo, activo, created_date) VALUES (NULL, '1ER CALIBRAJE', '1ER', 1,'2020-01-09 01:01:01');
INSERT INTO enfunde_tipocalibraje(deleted, nombre, codigo, activo, created_date) VALUES (NULL, '2DO CALIBRAJE', '2DO', 1,'2020-01-09 01:01:01');
INSERT INTO enfunde_tipocalibraje(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'BARRIDA', 'BAR', 1,'2020-01-09 01:01:01');
INSERT INTO enfunde_tipocalibraje(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'ADELANTADA', 'ADE', 1,'2020-01-09 01:01:01');

INSERT INTO maestros_tiporol(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'EMPLEADO', 'E', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_tiporol(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'PRODUCTOR', 'P', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_tiporol(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'TECNICO', 'T', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_tiporol(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'CALIFICADOR', 'C', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_tiporol(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'EXPORTADOR', 'C', 1,'2020-01-09 01:01:01');

INSERT INTO envio_marcacaja(deleted, nombre, nombre_alterno, activo, created_date) VALUES (NULL, 'EQUAPAK 22XU', '22XU', 1,'2020-01-09 01:01:01');
INSERT INTO envio_marcacaja(deleted, nombre, nombre_alterno, activo, created_date) VALUES (NULL, 'EQUAPAK 209', '209', 1,'2020-01-09 01:01:01');
INSERT INTO envio_marcacaja(deleted, nombre, nombre_alterno, activo, created_date) VALUES (NULL, 'EQUAPAK SF101', 'SF101', 1,'2020-01-09 01:01:01');

INSERT INTO maestros_tiponotificacion(deleted, descripcion, codigo, activo, created_date) VALUES (NULL, 'Mensaje', 'M', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_tiponotificacion(deleted, descripcion, codigo, activo, created_date) VALUES (NULL, 'Alertas', 'A', 1,'2020-01-09 01:01:01');
INSERT INTO maestros_tiponotificacion(deleted, descripcion, codigo, activo, created_date) VALUES (NULL, 'Eventos', 'E', 1,'2020-01-09 01:01:01');


-------------------------------
--- EXPORTADORES
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', 0, 'EQUAPAK', 'EQUAPAK', '', 'equapak@gmail.com', 0, 1,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', 0, 'IREN', 'IREN', '', 'iren@gmail.com', 0, 1,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', 0, 'UBESA', 'UBESA', '', 'ubesa@gmail.com', 0, 1,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', 0, 'ASOAGRIBAL', 'ASOAGRIBAL', '', 'asoagribal@gmail.com', 0, 1,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', 0, 'TRUISFRUIT', 'TRUISFRUIT', '', 'truisfruit@gmail.com', 0, 1,'2020-01-09 01:01:01');

--- TECNICOS
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', 0, 'JARMIJOS', 'JOSE', 'ARMIJOS', 'jarmijos@gmail.com', 0, 1,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', 0, 'LGOMEZ', 'LUIS', 'GOMEZ', 'lgomez@gmail.com', 0, 1,'2020-01-09 01:01:01');

--- PRODUCTOR
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', 0, 'FNAULA', 'FREDDY', 'NAULA', 'fnaula@gmail.com', 0, 1,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', 0, 'JSERRANO', 'JORGE ALEX', 'SERRANO', 'jserrano@gmail.com', 0, 1,'2020-01-09 01:01:01');

--- Productores  desde id 10
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', 0, 'FGUERRERO', 'GUERRERO RÍOS', 'FERNANDO MAURICIO', 'fguerrero@gmail.com', 0, 1,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', 0, 'PGUERRON', 'GUERRERO VALENCIA', 'PABLO RAUL', 'pguerrero@gmail.com', 0, 1,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', 0, 'AGUEVARA', 'GUERRÓN ALMEIDA', 'ALEXEY GIOVANNY', 'aguerron@gmail.com', 0, 1,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', 0, 'MGUEVARA', 'GUEVARA BACULIMA', 'MARTHA DEL ROCÍO', 'mguevara@gmail.com', 0, 1,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', 0, 'JGUILLEN', 'GUEVARA CARRILLO', 'JORGE LUIS', 'jguevara@gmail.com', 0, 1,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', 0, 'MGUILLERMO', 'GUILLÉN OSCAR', 'MEDARDO ', 'mguillen@gmail.com', 0, 1,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', 0, 'JGUZMAN', 'GUILLERMO DELGADO', 'JORGE ERNESTO', 'jguillermo@gmail.com', 0, 1,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', 0, 'HGUZMAN', 'GUZMÁN CASTAÑEDA', 'HIMMLER ROBERTO', 'hguzman@gmail.com', 0, 1,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', 0, 'LHARO', 'GUZMÁN ROCHINA', 'LUIS GABRIEL', 'lguzman@gmail.com', 0, 1,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', 0, 'THEREDIA', 'HARO FIGUEROA', 'TANIA MARÍA', 'tharo@gmail.com', 0, 1,'2020-01-09 01:01:01');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09 01:01:01', 0, 'EHEREDIA', 'HEREDIA CABRERA', 'EDWIN WALDEMAR', 'eheredia@gmail.com', 0, 1,'2020-01-09 01:01:01');

--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
--- EXPORTADORES
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0791777011001', NULL, NULL, NULL, NULL, 'EQUAPAK', NULL, 1,'2020-01-09 01:01:01',1, 1, 'El Guabo', '2950469');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0993221287001', NULL, NULL, NULL, NULL, 'IRENECUADOR', NULL, 1,'2020-01-09 01:01:01',1, 2, 'El Guabo', '2988199');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0990011419001', NULL, NULL, NULL, NULL, 'UBESA', NULL, 1,'2020-01-09 01:01:01',1, 3, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0791741254001', NULL, NULL, NULL, NULL, 'ASOAGRIBAL', NULL, 1,'2020-01-09 01:01:01',1, 4, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0992601523001', NULL, NULL, NULL, NULL, 'TRUISFRUIT', NULL, 1,'2020-01-09 01:01:01',1, 5, 'El Guabo', '21233434');
--- TECNICO 6
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1232025339', 'JOSE', 'LUIS', 'ARMIJOS', 'ARMIJOS', 'JOSE LUIS ARMIJOS ARMIJOS', NULL, 1,'2020-01-09 01:01:01',2, 6, 'El Guabo', '21233434');
--- CALIFICADOR 7
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1562025339', 'LUIS', 'ENRIQUE', 'GOMEZ', 'GOMEZ', 'LUIS ENRIQUE GOMEZ GOMEZ', NULL, 1,'2020-01-09 01:01:01',2, 7, 'El Guabo', '21233434');
--- PRODUCTOR 8
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0702025339001', 'FREDDY', 'SEGUNDO', 'NAULA', 'MORA', 'FREDDY SEGUNDO NAULA MORA', NULL, 1,'2020-01-09 01:01:01',1, 8, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0760015020001', 'JORGE', 'ALEX', 'SERRANO', 'AGUILAR', 'JORGE ALEX SERRANO AGUILAR', NULL, 1,'2020-01-09 01:01:01',1, 9, 'El Guabo', '21233434');
--- desde el 10
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1102618673', 'GUERRERO', 'RÍOS', 'FERNANDO', 'MAURICIO', 'GUERRERO RÍOS FERNANDO MAURICIO', NULL, 1,'2020-01-09 01:01:01',1, 10, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0800599821', 'GUERRERO', 'VALENCIA', 'PABLO', 'RAUL', 'GUERRERO VALENCIA PABLO RAUL', NULL, 1,'2020-01-09 01:01:01',1, 11, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0400783759', 'GUERRÓN', 'ALMEIDA', 'ALEXEY', 'GIOVANNY', 'GUERRÓN ALMEIDA ALEXEY GIOVANNY', NULL, 1,'2020-01-09 01:01:01',1, 12, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0102775624', 'GUEVARA', 'BACULIMA', 'MARTHA', 'DEL ROCÍO', 'GUEVARA BACULIMA MARTHA DEL ROCÍO', NULL, 1,'2020-01-09 01:01:01',1, 13, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1801910678', 'GUEVARA', 'CARRILLO', 'JORGE', 'LUIS', 'GUEVARA CARRILLO JORGE LUIS', NULL, 1,'2020-01-09 01:01:01',1, 14, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0101348795', 'GUILLÉN', 'OSCAR', 'MEDARDO', '', 'GUILLÉN OSCAR MEDARDO ', NULL, 1,'2020-01-09 01:01:01',1, 15, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0101342707', 'GUILLERMO', 'DELGADO', 'JORGE', 'ERNESTO', 'GUILLERMO DELGADO JORGE ERNESTO', NULL, 1,'2020-01-09 01:01:01',1, 16, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1706381975', 'GUZMÁN', 'CASTAÑEDA', 'HIMMLER', 'ROBERTO', 'GUZMÁN CASTAÑEDA HIMMLER ROBERTO', NULL, 1,'2020-01-09 01:01:01',1, 17, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0201628237', 'GUZMÁN', 'ROCHINA', 'LUIS', 'GABRIEL', 'GUZMÁN ROCHINA LUIS GABRIEL', NULL, 1,'2020-01-09 01:01:01',1, 18, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1802577708', 'HARO', 'FIGUEROA', 'TANIA', 'MARÍA', 'HARO FIGUEROA TANIA MARÍA', NULL, 1,'2020-01-09 01:01:01',1, 19, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0601554694', 'HEREDIA', 'CABRERA', 'EDWIN', 'WALDEMAR', 'HEREDIA CABRERA EDWIN WALDEMAR', NULL, 1,'2020-01-09 01:01:01',1, 20, 'El Guabo', '21233434');

--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
--- EXPORTADORES
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, 1,'2020-01-09 01:01:01', 1, 1, 5, 1);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, 1,'2020-01-09 01:01:01', 1, 2, 5, 1);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, 1,'2020-01-09 01:01:01', 1, 3, 5, 1);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, 1,'2020-01-09 01:01:01', 1, 4, 5, 1);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, 1,'2020-01-09 01:01:01', 1, 5, 5, 1);

--- TECNICO 6
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, 1,'2020-01-09 01:01:01', 1, 6, 3, 1);
--- CALIFICADOR 7
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, 1,'2020-01-09 01:01:01', 1, 7, 4, 1);

--- PRODUCTOR  8 al 53
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, 1,'2020-01-09 01:01:01', 1, 8, 2, 1);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, 1,'2020-01-09 01:01:01', 1, 9, 2, 1);

INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, 1,'2020-01-09 01:01:01', 1, 10, 2, 1);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, 1,'2020-01-09 01:01:01', 1, 11, 2, 1);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, 1,'2020-01-09 01:01:01', 1, 12, 2, 1);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, 1,'2020-01-09 01:01:01', 1, 13, 2, 1);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, 1,'2020-01-09 01:01:01', 1, 14, 2, 1);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, 1,'2020-01-09 01:01:01', 1, 15, 2, 1);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, 1,'2020-01-09 01:01:01', 1, 16, 2, 1);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, 1,'2020-01-09 01:01:01', 1, 17, 2, 1);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, 1,'2020-01-09 01:01:01', 1, 18, 2, 1);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, 1,'2020-01-09 01:01:01', 1, 19, 2, 1);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, 1,'2020-01-09 01:01:01', 1, 20, 2, 1);

-------------------------------
--- FINCAS 
-------------------------------
--- TECNICO 6 / 8 FREDDY NAULA / 9 JORGE ALEX SERRANO
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', 1,'2020-01-09 01:01:01', 1, 1, 8, 6, 7, 'LA PATRICIA');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', 1,'2020-01-09 01:01:01', 1, 1, 8, 6, 7, 'LA GUARICO');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', 1,'2020-01-09 01:01:01', 1, 1, 8, 6, 7, 'FIDECADE');

INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', 1,'2020-01-09 01:01:01', 1, 1, 9, 6, 7, 'BASTIDAS');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', 1,'2020-01-09 01:01:01', 1, 1, 9, 6, 7, 'DEL VALLE');

INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', 1,'2020-01-09 01:01:01', 1, 1, 10, 6, 7, 'AVICOLAS FINCAVIC S.A.');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', 1,'2020-01-09 01:01:01', 1, 1, 11, 6, 7, 'URBANAS S A');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', 1,'2020-01-09 01:01:01', 1, 1, 12, 6, 7, 'PACIFICO');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', 1,'2020-01-09 01:01:01', 1, 1, 13, 6, 7, 'AZAN S.A.');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', 1,'2020-01-09 01:01:01', 1, 1, 14, 6, 7, 'MARINAS CORONEL CHALEN');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', 1,'2020-01-09 01:01:01', 1, 1, 15, 6, 7, 'SAN MARCOS');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', 1,'2020-01-09 01:01:01', 1, 1, 16, 6, 7, 'AGROACUICOLA FUZU');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', 1,'2020-01-09 01:01:01', 1, 1, 17, 6, 7, 'UNIDAS');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', 1,'2020-01-09 01:01:01', 1, 1, 18, 6, 7, 'DEL MAR');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', 1,'2020-01-09 01:01:01', 1, 1, 19, 6, 7, 'MARINAS');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', 1,'2020-01-09 01:01:01', 1, 1, 20, 6, 7, 'AVAMER');

---------------------------------------------------------------------------------
---- ENFUNDE
---------------------------------------------------------------------------------
--- fincas de freddy naula
--- fincas de jorge alex  2019   38 A 52
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-09-16 01:01:01', 2019, 38, 1565, 1,'2019-09-16 01:01:01', 1, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-09-23 01:01:01', 2019, 39, 1375, 1,'2019-09-23 01:01:01', 2, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-09-30 01:01:01', 2019, 40, 1381, 1,'2019-09-30 01:01:01', 3, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-10-07 01:01:01', 2019, 41, 1532, 1,'2019-10-07 01:01:01', 4, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-10-14 01:01:01', 2019, 42, 1413, 1,'2019-10-14 01:01:01', 5, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-10-21 01:01:01', 2019, 43, 1442, 1,'2019-10-21 01:01:01', 6, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-10-28 01:01:01', 2019, 44, 1386, 1,'2019-10-28 01:01:01', 7, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-11-04 01:01:01', 2019, 45, 1643, 1,'2019-11-04 01:01:01', 8, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-11-11 01:01:01', 2019, 46, 1275, 1,'2019-11-11 01:01:01', 9, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-11-18 01:01:01', 2019, 47, 1198, 1,'2019-11-18 01:01:01', 10, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-11-25 01:01:01', 2019, 48, 1370, 1,'2019-11-25 01:01:01', 11, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-12-02 01:01:01', 2019, 49, 1424, 1,'2019-12-02 01:01:01', 12, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-12-09 01:01:01', 2019, 50, 1299, 1,'2019-12-09 01:01:01', 13, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-12-16 01:01:01', 2019, 51, 1476, 1,'2019-12-16 01:01:01', 14, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-12-23 01:01:01', 2019, 52, 1562, 1,'2019-12-23 01:01:01', 15, 4, 1);

--- fincas de jorge alex  2020
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 1635, 1,'2020-01-03 01:01:01', 16, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 1633, 1,'2020-01-10 01:01:01', 17, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 1759, 1,'2020-01-17 01:01:01', 18, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 1679, 1,'2020-01-24 01:01:01', 19, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-31 01:01:01', 2020, 5, 1476, 1,'2020-01-31 01:01:01', 20, 1, 1);


--- fincas de jorge alex  2019   38 A 52
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-09-16 01:01:01', 2019, 38, 1565, 1,'2019-09-16 01:01:01', 1, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-09-23 01:01:01', 2019, 39, 1375, 1,'2019-09-23 01:01:01', 2, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-09-30 01:01:01', 2019, 40, 1381, 1,'2019-09-30 01:01:01', 3, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-10-07 01:01:01', 2019, 41, 1532, 1,'2019-10-07 01:01:01', 4, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-10-14 01:01:01', 2019, 42, 1413, 1,'2019-10-14 01:01:01', 5, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-10-21 01:01:01', 2019, 43, 1442, 1,'2019-10-21 01:01:01', 6, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-10-28 01:01:01', 2019, 44, 1386, 1,'2019-10-28 01:01:01', 7, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-11-04 01:01:01', 2019, 45, 1643, 1,'2019-11-04 01:01:01', 8, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-11-11 01:01:01', 2019, 46, 1275, 1,'2019-11-11 01:01:01', 9, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-11-18 01:01:01', 2019, 47, 1198, 1,'2019-11-18 01:01:01', 10, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-11-25 01:01:01', 2019, 48, 1370, 1,'2019-11-25 01:01:01', 11, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-12-02 01:01:01', 2019, 49, 1424, 1,'2019-12-02 01:01:01', 12, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-12-09 01:01:01', 2019, 50, 1299, 1,'2019-12-09 01:01:01', 13, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-12-16 01:01:01', 2019, 51, 1476, 1,'2019-12-16 01:01:01', 14, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2019-12-23 01:01:01', 2019, 52, 1562, 1,'2019-12-23 01:01:01', 15, 4, 1);

--- fincas de jorge alex  2020
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 1635, 1,'2020-01-03 01:01:01', 16, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 1633, 1,'2020-01-10 01:01:01', 17, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 1759, 1,'2020-01-17 01:01:01', 18, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 1679, 1,'2020-01-24 01:01:01', 19, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-31 01:01:01', 2020, 5, 1476, 1,'2020-01-31 01:01:01', 20, 4, 1);

--- otros productores copie de finca 5
--- INSERT INTO enfunde_enfunde (deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) SELECT enfunde_enfunde.deleted, enfunde_enfunde.fecha_enfunde, enfunde_enfunde.anio, enfunde_enfunde.semana, enfunde_enfunde.cantidad, enfunde_enfunde.activo, enfunde_enfunde.created_date, enfunde_enfunde.cinta_id_id, maestros_finca.id, enfunde_enfunde.tipo_enfunde_id_id FROM enfunde_enfunde, maestros_finca WHERE maestros_finca.id>=6 AND enfunde_enfunde.id >= 36 AND enfunde_enfunde.id <=39;

---------------------------------------------------------------------------------
---- CALIBRAJE
---------------------------------------------------------------------------------
---- Estimados  7 semanas   1 al 48
/*
1, 4 ,  16
*/
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 842, 1,'2020-01-03 01:01:01', 1, 1, 16);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 842, 1,'2020-01-03 01:01:01', 1, 2, 16);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 594, 1,'2020-01-03 01:01:01', 1, 3, 16);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 403, 1,'2020-01-03 01:01:01', 1, 4, 16);

INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 787, 1,'2020-01-10 01:01:01', 1, 1, 17);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 787, 1,'2020-01-10 01:01:01', 1, 2, 17);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 663, 1,'2020-01-10 01:01:01', 1, 3, 17);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 769, 1,'2020-01-10 01:01:01', 1, 4, 17);

INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 618, 1,'2020-01-17 01:01:01', 1, 1, 18);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 618, 1,'2020-01-17 01:01:01', 1, 2, 18);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 749, 1,'2020-01-17 01:01:01', 1, 3, 18);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 831, 1,'2020-01-17 01:01:01', 1, 4, 18);

INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 400, 1,'2020-01-24 01:01:01', 1, 1, 19);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 400, 1,'2020-01-24 01:01:01', 1, 2, 19);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 696, 1,'2020-01-24 01:01:01', 1, 3, 19);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 861, 1,'2020-01-24 01:01:01', 1, 4, 19);

--- Calibraje Jorge Alex
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 842, 1,'2020-01-03 01:01:01', 4, 1, 16);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 842, 1,'2020-01-03 01:01:01', 4, 2, 16);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 594, 1,'2020-01-03 01:01:01', 4, 3, 16);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 403, 1,'2020-01-03 01:01:01', 4, 4, 16);

INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 787, 1,'2020-01-10 01:01:01', 4, 1, 17);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 787, 1,'2020-01-10 01:01:01', 4, 2, 17);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 663, 1,'2020-01-10 01:01:01', 4, 3, 17);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 769, 1,'2020-01-10 01:01:01', 4, 4, 17);

INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 618, 1,'2020-01-17 01:01:01', 4, 1, 18);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 618, 1,'2020-01-17 01:01:01', 4, 2, 18);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 749, 1,'2020-01-17 01:01:01', 4, 3, 18);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 831, 1,'2020-01-17 01:01:01', 4, 4, 18);

INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 400, 1,'2020-01-24 01:01:01', 1, 1, 19);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 400, 1,'2020-01-24 01:01:01', 1, 2, 19);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 696, 1,'2020-01-24 01:01:01', 1, 3, 19);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 861, 1,'2020-01-24 01:01:01', 1, 4, 19);


--- otros productores copie de finca 5
----INSERT INTO enfunde_calibraje (deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id, cinta_id_id) SELECT enfunde_calibraje.deleted, enfunde_calibraje.fecha_calibraje, enfunde_calibraje.anio, enfunde_calibraje.semana, enfunde_calibraje.cantidad, enfunde_calibraje.activo, enfunde_calibraje.created_date, maestros_finca.id, enfunde_calibraje.tipo_calibraje_id_id, enfunde_calibraje.cinta_id_id FROM enfunde_calibraje, maestros_finca WHERE maestros_finca.id>=6 AND enfunde_calibraje.id >= 45 AND enfunde_calibraje.id <= 50;

Orden 8415 / Finca LA PATRICIA / Cajas 100  
---------------------------------------------------------------------------------
---- ESTIMADO
---------------------------------------------------------------------------------
---- Estimados  7 semanas
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 1635, 1,'2020-01-03 01:01:01', 1, 0);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 1633, 1,'2020-01-10 01:01:01', 1, 0);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 1759, 1,'2020-01-17 01:01:01', 1, 0);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 1679, 1,'2020-01-24 01:01:01', 1, 0);

INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-03 01:01:01', 2020, 1, 1635, 1,'2020-01-03 01:01:01', 4, 0);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-10 01:01:01', 2020, 2, 1633, 1,'2020-01-10 01:01:01', 4, 0);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-17 01:01:01', 2020, 3, 1759, 1,'2020-01-17 01:01:01', 4, 0);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-24 01:01:01', 2020, 4, 1679, 1,'2020-01-24 01:01:01', 4, 0);

--- otros productores copie de finca 5
---INSERT INTO estimado_estimadocaja (deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) SELECT estimado_estimadocaja.deleted, estimado_estimadocaja.fecha_estimado, estimado_estimadocaja.anio, estimado_estimadocaja.semana, estimado_estimadocaja.cantidad, estimado_estimadocaja.activo, estimado_estimadocaja.created_date, maestros_finca.id, estimado_estimadocaja.bloqueado FROM estimado_estimadocaja, maestros_finca WHERE maestros_finca.id>=6 AND estimado_estimadocaja.id >= 33 AND estimado_estimadocaja.id <= 35;
---SELECT * FROM estimado_estimadocaja;
---------------------------------------------------------------------------------
---- EMBARQUE
---------------------------------------------------------------------------------
/*
--- Limpia la data de embarque
DELETE FROM envio_embarque;
DELETE FROM envio_ordencorte;
ALTER SEQUENCE envio_embarque_id_seq RESTART WITH 1;
ALTER SEQUENCE envio_ordencorte_id_seq RESTART WITH 1;
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='envio_embarque';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='envio_ordencorte';


--- Crea embarques basado marca de caja
INSERT INTO envio_embarque (deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) SELECT NULL, 'Embarque de'||' '||envio_marcacaja.nombre, enfunde_cinta.fecha_desde, enfunde_cinta.fecha_desde, enfunde_cinta.anio, enfunde_cinta.semana, 200, 6.50, 22, 20, 10, 12, 1, enfunde_cinta.created_date, envio_marcacaja.id, 1, 5, 1, 0 FROM enfunde_cinta, envio_marcacaja WHERE enfunde_cinta.semana IN (1,3,5,7) AND envio_marcacaja.id = 1;

INSERT INTO envio_embarque (deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) SELECT NULL, 'Embarque de'||' '||envio_marcacaja.nombre, enfunde_cinta.fecha_desde, enfunde_cinta.fecha_desde, enfunde_cinta.anio, enfunde_cinta.semana, 200, 6.50, 22, 20, 10, 12, 1, enfunde_cinta.created_date, envio_marcacaja.id, 1, 5, 1, 0 FROM enfunde_cinta, envio_marcacaja WHERE enfunde_cinta.semana IN (2,4,6) AND envio_marcacaja.id = 2;

--- Copiar los estimados como Orden de Corte
INSERT INTO envio_ordencorte (deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) SELECT estimado_estimadocaja.deleted, estimado_estimadocaja.id, envio_embarque.fecha_embarque_desde, estimado_estimadocaja.anio, estimado_estimadocaja.semana, estimado_estimadocaja.cantidad, estimado_estimadocaja.activo, estimado_estimadocaja.created_date, envio_embarque.id, estimado_estimadocaja.finca_id_id, 0 FROM estimado_estimadocaja, envio_embarque WHERE estimado_estimadocaja.deleted IS NULL ORDER BY estimado_estimadocaja.anio, estimado_estimadocaja.semana, envio_embarque.id, finca_id_id;

*/

SELECT * FROM envio_ordencorte;
/*


--- https://www.marinetraffic.com/es/ais/details/ports/1489/Ecuador_port:PUERTO%20BOLIVAR


select * from Web_Sector
select * from Web_MarcaCaja
select * from Web_TipoCalibraje




UBESA
REYBANPAC
ASOAGRIBAL
TRUISFRUIT
FRUTADELI
COMERSUR
ASISBANE
SABROSTAR FRUIT
ECUAGREENPRODEX
TROPICAL FRUIT EXPORT
FRUTICAL
AGZULASA
EXP. SOPRISA
EXPORTSWEET
EXBAORO
GINAFRUIT
CHIQUITA BANANA ECUADOR
*/
