/*
pythonanywhere.com/user/sosnegsa/consoles/14602004
sosnegsa@gmail.com
#D3sAdm1n
---- python manage.py migrate
sqlite3 db.sqlite3

https://hackintosher.com/guides/virtual-macos-use-macos-sierra-virtual-machine-vmware/
https://www.youtube.com/watch?v=iUhF07XaF1w&t=205s

https://www.brsoftech.com/blog/iot-application-in-agriculture/

cd multum-backend/multum/
SELECT * FROM maestros_tipofinca;
PRAGMA table_info(maestros_tipofinca);

.tables
.schema
.headers on
.mode column

Explicacion: la fecha la maneja como un string siempre en YYYY-MM-DD ejemplo '2020-08-03'.   El insert debe tener ese orden y los meses y dias deben ser completados con cero.
semana 3  = '2020-01-13','2020-01-19'

ALTER TABLE enfunde_cinta ALTER COLUMN color TYPE VARCHAR(12);

ALTER TABLE maestros_persona ALTER COLUMN primer_apellido DROP NOT NULL;
ALTER TABLE maestros_persona ALTER COLUMN segundo_apellido DROP NOT NULL;
ALTER TABLE maestros_persona ALTER COLUMN primer_nombre DROP NOT NULL;
ALTER TABLE maestros_persona ALTER COLUMN segundo_nombre DROP NOT NULL;
ALTER TABLE maestros_persona ALTER COLUMN fecha_nacimiento DROP NOT NULL;
ALTER TABLE registro_personarol ALTER COLUMN agro_artesanal_id_id DROP NOT NULL;

ALTER TABLE maestros_finca ADD productor_id_id integer NOT NULL;
ALTER TABLE maestros_finca ADD tecnico_id_id integer NOT NULL;
ALTER TABLE maestros_finca ADD calificador_id_id integer NOT NULL;

SELECT * FROM facturacion_facturacabecera;
SELECT * FROM facturacion_facturadetalle;
SELECT * FROM envio_embarque;
SELECT * FROM envio_ordencorte;
SELECT * FROM enfunde_calibraje;
SELECT * FROM enfunde_enfunde;
SELECT * FROM estimado_estimadocaja;
SELECT * FROM maestros_notificacion;
SELECT * FROM maestros_finca;
SELECT * FROM registro_personarol;
SELECT * FROM maestros_persona;
SELECT * FROM enfunde_cinta;
SELECT * FROM maestros_tiponotificacion;
SELECT * FROM maestros_tipoidentificacion;
SELECT * FROM maestros_tipofinca;
SELECT * FROM maestros_agroartesanal;
SELECT * FROM maestros_puerto;
SELECT * FROM maestros_vapor;
SELECT * FROM maestros_sector;
SELECT * FROM enfunde_tipoenfunde;
SELECT * FROM enfunde_tipocalibraje;
SELECT * FROM maestros_tiporol;
SELECT * FROM envio_marcacaja;
SELECT * FROM auth_user;
*/

DELETE FROM facturacion_facturacabecera;
DELETE FROM facturacion_facturadetalle;
DELETE FROM envio_embarque;
DELETE FROM envio_ordencorte;
DELETE FROM enfunde_calibraje;
DELETE FROM enfunde_enfunde;
DELETE FROM estimado_estimadocaja;
DELETE FROM maestros_notificacion;
DELETE FROM maestros_finca;
DELETE FROM registro_personarol;
DELETE FROM maestros_persona;
DELETE FROM enfunde_cinta;
DELETE FROM maestros_tiponotificacion;
DELETE FROM maestros_tipoidentificacion;
DELETE FROM maestros_tipofinca;
DELETE FROM maestros_agroartesanal;
DELETE FROM maestros_puerto;
DELETE FROM maestros_vapor;
DELETE FROM maestros_sector;
DELETE FROM enfunde_tipoenfunde;
DELETE FROM enfunde_tipocalibraje;
DELETE FROM maestros_tiporol;
DELETE FROM envio_marcacaja;
DELETE FROM auth_user;

/*
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='facturacion_facturacabecera';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='facturacion_facturadetalle';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='envio_embarque';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='envio_ordencorte';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='enfunde_calibraje';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='enfunde_enfunde';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='estimado_estimadocaja';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_notificacion';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_finca';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='registro_personarol';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_persona';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='enfunde_cinta';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_tiponotificacion';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_tipoidentificacion';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_tipofinca';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_agroartesanal';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_puerto';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_vapor';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_sector';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='enfunde_tipoenfunde';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='enfunde_tipocalibraje';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='maestros_tiporol';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='envio_marcacaja';
UPDATE SQLITE_SEQUENCE SET SEQ= 0 WHERE NAME='auth_user';
*/

--- En postgres
ALTER SEQUENCE facturacion_facturadetalle_id_seq RESTART WITH 1;
ALTER SEQUENCE facturacion_facturacabecera_id_seq RESTART WITH 1;
ALTER SEQUENCE envio_embarque_id_seq RESTART WITH 1;
ALTER SEQUENCE envio_ordencorte_id_seq RESTART WITH 1;
ALTER SEQUENCE enfunde_calibraje_id_seq RESTART WITH 1;
ALTER SEQUENCE enfunde_enfunde_id_seq RESTART WITH 1;
ALTER SEQUENCE estimado_estimadocaja_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_notificacion_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_finca_id_seq RESTART WITH 1;
ALTER SEQUENCE registro_personarol_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_persona_id_seq RESTART WITH 1;
ALTER SEQUENCE enfunde_cinta_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_tiponotificacion_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_tipoidentificacion_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_tipofinca_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_agroartesanal_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_puerto_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_vapor_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_sector_id_seq RESTART WITH 1;
ALTER SEQUENCE enfunde_tipoenfunde_id_seq RESTART WITH 1;
ALTER SEQUENCE enfunde_tipocalibraje_id_seq RESTART WITH 1;
ALTER SEQUENCE maestros_tiporol_id_seq RESTART WITH 1;
ALTER SEQUENCE envio_marcacaja_id_seq RESTART WITH 1;
ALTER SEQUENCE auth_user_id_seq RESTART WITH 1;

/*
UPDATE auth_user SET username='IREN', first_name='IREN', email='iren@gmail.com' WHERE id=2;
UPDATE maestros_persona SET nombre='IREN' WHERE id=2;
UPDATE maestros_persona SET primer_apellido='NAULA', segundo_apellido='MORA', primer_nombre='FREDDY', segundo_nombre='SEGUNDO' WHERE id=5;
UPDATE maestros_persona SET primer_apellido='SERRANO', segundo_apellido='AGUILAR', primer_nombre='JORGE', segundo_nombre='ALEX', nombre='JORGE ALEX SERRANO AGUILAR' WHERE id=6;
SELECT * FROM maestros_persona;
*/

INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2019-12-30','2020-01-05',2020,1,'naranja','FF8000',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-01-06','2020-01-12',2020,2,'cafe','804000',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-01-13','2020-01-19',2020,3,'plomo','9C9C9C',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-01-20','2020-01-26',2020,4,'verde','31A833',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-01-27','2020-02-02',2020,5,'lila','A82DDD',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-02-03','2020-02-09',2020,6,'rojo','F60D0D',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-02-10','2020-02-16',2020,7,'blanco','FFFFFF',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-02-17','2020-02-23',2020,8,'negro','000000',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-02-24','2020-03-01',2020,9,'amarillo','F8F32B',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-03-02','2020-03-08',2020,10,'azul','0000FF',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-03-09','2020-03-15',2020,11,'naranja','FF8000',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-03-16','2020-03-22',2020,12,'cafe','804000',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-03-23','2020-03-29',2020,13,'plomo','9C9C9C',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-03-30','2020-04-05',2020,14,'verde','31A833',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-04-06','2020-04-12',2020,15,'lila','A82DDD',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-04-13','2020-04-19',2020,16,'rojo','F60D0D',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-04-20','2020-04-26',2020,17,'blanco','FFFFFF',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-04-27','2020-05-03',2020,18,'negro','000000',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-05-04','2020-05-10',2020,19,'amarillo','F8F32B',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-05-11','2020-05-17',2020,20,'azul','0000FF',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-05-18','2020-05-24',2020,21,'naranja','FF8000',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-05-25','2020-05-31',2020,22,'cafe','804000',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-06-01','2020-06-07',2020,23,'plomo','9C9C9C',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-06-08','2020-06-14',2020,24,'verde','31A833',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-06-15','2020-06-21',2020,25,'lila','A82DDD',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-06-22','2020-06-28',2020,26,'rojo','F60D0D',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-06-29','2020-07-05',2020,27,'blanco','FFFFFF',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-07-06','2020-07-12',2020,28,'negro','000000',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-07-13','2020-07-19',2020,29,'amarillo','F8F32B',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-07-20','2020-07-26',2020,30,'azul','0000FF',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-07-27','2020-08-02',2020,31,'naranja','FF8000',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-08-03','2020-08-09',2020,32,'cafe','804000',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-08-10','2020-08-16',2020,33,'plomo','9C9C9C',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-08-17','2020-08-23',2020,34,'verde','31A833',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-08-24','2020-08-30',2020,35,'lila','A82DDD',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-08-31','2020-09-06',2020,36,'rojo','F60D0D',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-09-07','2020-09-13',2020,37,'blanco','FFFFFF',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-09-14','2020-09-20',2020,38,'negro','000000',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-09-21','2020-09-27',2020,39,'amarillo','F8F32B',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-09-28','2020-10-04',2020,40,'azul','0000FF',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-10-05','2020-10-11',2020,41,'naranja','FF8000',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-10-12','2020-10-18',2020,42,'cafe','804000',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-10-19','2020-10-25',2020,43,'plomo','9C9C9C',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-10-26','2020-11-01',2020,44,'verde','31A833',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-11-02','2020-11-08',2020,45,'lila','A82DDD',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-11-09','2020-11-15',2020,46,'rojo','F60D0D',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-11-16','2020-11-22',2020,47,'blanco','FFFFFF',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-11-23','2020-11-29',2020,48,'negro','000000',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-11-30','2020-12-06',2020,49,'amarillo','F8F32B',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-12-07','2020-12-13',2020,50,'azul','0000FF',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-12-14','2020-12-20',2020,51,'naranja','FF8000',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-12-21','2020-12-27',2020,52,'cafe','804000',TRUE,'2020-01-09');
INSERT INTO enfunde_cinta(deleted,fecha_desde,fecha_hasta,anio,semana,color,colorhex,activo,created_date) VALUES (NULL,'2020-12-28','2021-01-03',2020,53,'plomo','9C9C9C',TRUE,'2020-01-09');

INSERT INTO maestros_tipoidentificacion( deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'RUC', 'R', TRUE, '2020-01-09');
INSERT INTO maestros_tipoidentificacion( deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'CEDULA', 'C', TRUE, '2020-01-09');
INSERT INTO maestros_tipoidentificacion( deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'PASAPORTE', 'P', TRUE, '2020-01-09');

INSERT INTO maestros_tipofinca(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'CONVENCIONAL', 'CONV', TRUE, '2020-01-09');
INSERT INTO maestros_tipofinca(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'ORGANICA', 'ORGA', TRUE, '2020-01-09');

INSERT INTO maestros_agroartesanal(deleted, nombre, activo, created_date) VALUES (NULL, 'EL GUABO', TRUE, '2020-01-09');
INSERT INTO maestros_agroartesanal(deleted, nombre, activo, created_date) VALUES (NULL, 'SAN CARLOS', TRUE, '2020-01-09');
INSERT INTO maestros_agroartesanal(deleted, nombre, activo, created_date) VALUES (NULL, 'SANTA ROSA', TRUE, '2020-01-09');
INSERT INTO maestros_agroartesanal(deleted, nombre, activo, created_date) VALUES (NULL, 'PASAJE', TRUE, '2020-01-09');

INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO BOLÍVAR', TRUE, '2020-01-09');
INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO MANTA', TRUE, '2020-01-09');
INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO GYE', TRUE, '2020-01-09');
INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO DE OAKLAND', TRUE, '2020-01-09');
INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO DE SEATTLE-TACOMA', TRUE, '2020-01-09');
INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO DE VALENCIA', TRUE, '2020-01-09');
INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO DE EL HAVRE', TRUE, '2020-01-09');
INSERT INTO maestros_puerto(deleted, nombre, activo, created_date) VALUES (NULL, 'PUERTO DE FLENSBURG', TRUE, '2020-01-09');

INSERT INTO maestros_vapor(deleted, nombre, activo, created_date) VALUES (NULL, 'SUNRISE', TRUE, '2020-01-09');
INSERT INTO maestros_vapor(deleted, nombre, activo, created_date) VALUES (NULL, 'LIMARI', TRUE, '2020-01-09');
INSERT INTO maestros_vapor(deleted, nombre, activo, created_date) VALUES (NULL, 'NORTHERN DEFENDER', TRUE, '2020-01-09');
INSERT INTO maestros_vapor(deleted, nombre, activo, created_date) VALUES (NULL, 'ANTILEN', TRUE, '2020-01-09');
INSERT INTO maestros_vapor(deleted, nombre, activo, created_date) VALUES (NULL, 'AQUAVIT', TRUE, '2020-01-09');

INSERT INTO maestros_sector(deleted, nombre, activo, created_date) VALUES (NULL, 'TILLALES', TRUE, '2020-01-09');
INSERT INTO maestros_sector(deleted, nombre, activo, created_date) VALUES (NULL, 'BARBONES', TRUE, '2020-01-09');
INSERT INTO maestros_sector(deleted, nombre, activo, created_date) VALUES (NULL, 'SAN CARLOS', TRUE, '2020-01-09');
INSERT INTO maestros_sector(deleted, nombre, activo, created_date) VALUES (NULL, 'SAUCES', TRUE, '2020-01-09');
INSERT INTO maestros_sector(deleted, nombre, activo, created_date) VALUES (NULL, 'PROSPERINA', TRUE, '2020-01-09');

INSERT INTO enfunde_tipoenfunde(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'ENFUNDE', 'E', TRUE, '2020-01-09');
INSERT INTO enfunde_tipoenfunde(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'RECUPERACION', 'R', TRUE, '2020-01-09');

INSERT INTO enfunde_tipocalibraje(deleted, nombre, codigo, activo, created_date) VALUES (NULL, '1ER CALIBRAJE', '1ER', TRUE, '2020-01-09');
INSERT INTO enfunde_tipocalibraje(deleted, nombre, codigo, activo, created_date) VALUES (NULL, '2DO CALIBRAJE', '2DO', TRUE, '2020-01-09');
INSERT INTO enfunde_tipocalibraje(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'BARRIDA', 'BAR', TRUE, '2020-01-09');
INSERT INTO enfunde_tipocalibraje(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'ADELANTADA', 'ADE', TRUE, '2020-01-09');

INSERT INTO maestros_tiporol(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'EMPLEADO', 'E', TRUE, '2020-01-09');
INSERT INTO maestros_tiporol(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'PRODUCTOR', 'P', TRUE, '2020-01-09');
INSERT INTO maestros_tiporol(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'TECNICO', 'T', TRUE, '2020-01-09');
INSERT INTO maestros_tiporol(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'CALIFICADOR', 'C', TRUE, '2020-01-09');
INSERT INTO maestros_tiporol(deleted, nombre, codigo, activo, created_date) VALUES (NULL, 'EXPORTADOR', 'C', TRUE, '2020-01-09');

INSERT INTO envio_marcacaja(deleted, nombre, nombre_alterno, activo, created_date) VALUES (NULL, 'EQUAPAK 22XU', '22XU', TRUE, '2020-01-09');
INSERT INTO envio_marcacaja(deleted, nombre, nombre_alterno, activo, created_date) VALUES (NULL, 'EQUAPAK 209', '209', TRUE, '2020-01-09');
INSERT INTO envio_marcacaja(deleted, nombre, nombre_alterno, activo, created_date) VALUES (NULL, 'EQUAPAK SF101', 'SF101', TRUE, '2020-01-09');

INSERT INTO maestros_tiponotificacion(deleted, descripcion, codigo, activo, created_date) VALUES (NULL, 'Mensaje', 'M', TRUE, '2020-01-09');
INSERT INTO maestros_tiponotificacion(deleted, descripcion, codigo, activo, created_date) VALUES (NULL, 'Alertas', 'A', TRUE, '2020-01-09');
INSERT INTO maestros_tiponotificacion(deleted, descripcion, codigo, activo, created_date) VALUES (NULL, 'Eventos', 'E', TRUE, '2020-01-09');


-------------------------------
--- EXPORTADORES
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'EQUAPAK', 'EQUAPAK', '', 'equapak@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'IREN', 'IREN', '', 'iren@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'UBESA', 'UBESA', '', 'ubesa@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'ASOAGRIBAL', 'ASOAGRIBAL', '', 'asoagribal@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'TRUISFRUIT', 'TRUISFRUIT', '', 'truisfruit@gmail.com', FALSE, TRUE, '2020-01-09');

--- TECNICOS
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'JARMIJOS', 'JOSE', 'ARMIJOS', 'jarmijos@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'LGOMEZ', 'LUIS', 'GOMEZ', 'lgomez@gmail.com', FALSE, TRUE, '2020-01-09');

--- PRODUCTOR
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
	VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'FNAULA', 'FREDDY', 'NAULA', 'fnaula@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'JSERRANO', 'JORGE ALEX', 'SERRANO', 'jserrano@gmail.com', FALSE, TRUE, '2020-01-09');

--- Productores  desde id 10
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'FGUERRERO', 'GUERRERO RÍOS', 'FERNANDO MAURICIO', 'fguerrero@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'PGUERRON', 'GUERRERO VALENCIA', 'PABLO RAUL', 'pguerrero@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'AGUEVARA', 'GUERRÓN ALMEIDA', 'ALEXEY GIOVANNY', 'aguerron@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'MGUEVARA', 'GUEVARA BACULIMA', 'MARTHA DEL ROCÍO', 'mguevara@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'JGUILLEN', 'GUEVARA CARRILLO', 'JORGE LUIS', 'jguevara@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'MGUILLERMO', 'GUILLÉN OSCAR', 'MEDARDO ', 'mguillen@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'JGUZMAN', 'GUILLERMO DELGADO', 'JORGE ERNESTO', 'jguillermo@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'HGUZMAN', 'GUZMÁN CASTAÑEDA', 'HIMMLER ROBERTO', 'hguzman@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'LHARO', 'GUZMÁN ROCHINA', 'LUIS GABRIEL', 'lguzman@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'THEREDIA', 'HARO FIGUEROA', 'TANIA MARÍA', 'tharo@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'EHEREDIA', 'HEREDIA CABRERA', 'EDWIN WALDEMAR', 'eheredia@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'MHERKT', 'HEREDIA PROAÑO', 'MÓNICA ALEXANDRA', 'mheredia@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'EHERNANDEZ', 'HERKT PLAZA', 'ERIKA GLADYS', 'eherkt@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'MHERNANDEZ', 'HERNÁNDEZ ANDINO', 'MILTON GUSTAVO', 'mhernandez@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'GHERNANDEZ', 'HERNÁNDEZ ANDRADE', 'GLADYS ELINA', 'ghernandez@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'OHERNANDEZ', 'HERNÁNDEZ HIDROBO', 'OLAVO MARCIAL', 'ohernandez@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'CHERNANDEZ', 'HERNÁNDEZ PAZMIÑO', 'CESAR ERNESTO', 'chernandez@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'AHERNANDEZ', 'HERNÁNDEZ PAZMIÑO', 'MERCEDES AMIRA', 'mhernandez@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'VHERRERA', 'HERNÁNDEZ RODRÍGUEZ', 'VENUS ADRIANA', 'vhernandez@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'EHIDALGO', 'HERRERA ESPINOZA', 'EDDY IVONE', 'eherrera@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'YHIDALGO', 'HIDALGO ORDÓÑEZ', 'YESENIA DEL ROCÍO', 'yhidalgo@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'AHIDROVO', 'HIDALGO SANTAMARÍA', 'ANA MARÍA', 'ahidalgo@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'NHUGO', 'HIDROVO CONFORME', 'NANCY MARÍA', 'nhidrovo@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'LHURTADO', 'HUGO CORONEL', 'LUIGI SALVATORE', 'lhugo@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'JHURTADO', 'HURTADO DEL CASTILLO', 'JAIME RAMIRO', 'jhurtado@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'SINFANTES', 'HURTADO TORAL', 'SAYNE XIOMARA', 'shurtado@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'MINGA', 'INFANTES MANTILLA', 'MARÍA AUXILIADORA', 'minfantes@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'JINSUASTI', 'INGA YANZA', 'JULIO CÉSAR', 'jinga@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'KINTRIAGO', 'INSUASTI DELGADO', 'KAROOL SUSANA', 'kinsuasti@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'FINTRIAGO', 'INTRIAGO LOOR', 'FELIX ENRIQUE', 'fintriago@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'MISAZA', 'INTRIAGO MEJÍA', 'MAGNO GABRIEL', 'mintriago@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'JITURRALDE', 'ISAZA PIEDRAHITA', 'JUAN CARLOS', 'jisaza@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'JIZQUIERDO', 'ITURRALDE HIDALGO', 'JUAN CARLOS', 'jiturralde@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'AIZURIETA', 'IZQUIERDO TAPIA', 'ANITA DEL PILAR', 'aizquierdo@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'KJACOME', 'IZURIETA DÁVILA', 'KARINA ', 'kizurieta@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'IJACOME', 'JACOME NOGUERA', 'IVÁNA JAJAIRA', 'ijacome@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'MJACOME', 'JÁCOME ORDÓÑEZ', 'MARÍA DEL CARMEN', 'mjacome@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'EJARAMILLO', 'JACOME PAZMIÑO', 'ÉDISON POMERIO', 'ejacome@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'CJARAMILLO', 'JARAMILLO CEVALLOS', 'CARMEN INÉS', 'cjaramillo@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'GJARAMILLO', 'JARAMILLO GONZÁLEZ', 'GALO ARTURO', 'gjaramillo@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'MJARAMILLO', 'JARAMILLO JUMBO', 'MARTHA ELIZABETH', 'mjaramillo@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'TJARAMILLO', 'JARAMILLO LUZURIAGA', 'TANIA CATALINA', 'tjaramillo@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'LJARAMILLO', 'JARAMILLO REYES', 'LUIS PATRICIO', 'ljaramillo@gmail.com', FALSE, TRUE, '2020-01-09');
INSERT INTO auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES ('pbkdf2_sha256$150000$HqPAhXKeainN$Z4IXm6ApD2T0g4RxpqOzyv3+B15RSWqU4lqrECNbxh4=', '2020-01-09', FALSE, 'JJARAMILLO', 'JARAMILLO SALINAS', 'JUAN AGUSTÍN', 'jjaramillo@gmail.com', FALSE, TRUE, '2020-01-09');


--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
/*
SELECT * FROM maestros_persona;
SELECT id, first_name, last_name FROM auth_user;
*/

--- EXPORTADORES
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0791777011001', NULL, NULL, NULL, NULL, 'EQUAPAK', NULL, TRUE, '2020-01-09',1, 1, 'El Guabo', '2950469');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0993221287001', NULL, NULL, NULL, NULL, 'IRENECUADOR', NULL, TRUE, '2020-01-09',1, 2, 'El Guabo', '2988199');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0990011419001', NULL, NULL, NULL, NULL, 'UBESA', NULL, TRUE, '2020-01-09',1, 3, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0791741254001', NULL, NULL, NULL, NULL, 'ASOAGRIBAL', NULL, TRUE, '2020-01-09',1, 4, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0992601523001', NULL, NULL, NULL, NULL, 'TRUISFRUIT', NULL, TRUE, '2020-01-09',1, 5, 'El Guabo', '21233434');
--- TECNICO 6
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1232025339', 'JOSE', 'LUIS', 'ARMIJOS', 'ARMIJOS', 'JOSE LUIS ARMIJOS ARMIJOS', NULL, TRUE, '2020-01-09',2, 6, 'El Guabo', '21233434');
--- CALIFICADOR 7
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1562025339', 'LUIS', 'ENRIQUE', 'GOMEZ', 'GOMEZ', 'LUIS ENRIQUE GOMEZ GOMEZ', NULL, TRUE, '2020-01-09',2, 7, 'El Guabo', '21233434');
--- PRODUCTOR 8
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0702025339001', 'FREDDY', 'SEGUNDO', 'NAULA', 'MORA', 'FREDDY SEGUNDO NAULA MORA', NULL, TRUE, '2020-01-09',1, 8, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0760015020001', 'JORGE', 'ALEX', 'SERRANO', 'AGUILAR', 'JORGE ALEX SERRANO AGUILAR', NULL, TRUE, '2020-01-09',1, 9, 'El Guabo', '21233434');
--- desde el 10
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1102618673', 'GUERRERO', 'RÍOS', 'FERNANDO', 'MAURICIO', 'GUERRERO RÍOS FERNANDO MAURICIO', NULL, TRUE, '2020-01-09',1, 10, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0800599821', 'GUERRERO', 'VALENCIA', 'PABLO', 'RAUL', 'GUERRERO VALENCIA PABLO RAUL', NULL, TRUE, '2020-01-09',1, 11, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0400783759', 'GUERRÓN', 'ALMEIDA', 'ALEXEY', 'GIOVANNY', 'GUERRÓN ALMEIDA ALEXEY GIOVANNY', NULL, TRUE, '2020-01-09',1, 12, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0102775624', 'GUEVARA', 'BACULIMA', 'MARTHA', 'DEL ROCÍO', 'GUEVARA BACULIMA MARTHA DEL ROCÍO', NULL, TRUE, '2020-01-09',1, 13, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1801910678', 'GUEVARA', 'CARRILLO', 'JORGE', 'LUIS', 'GUEVARA CARRILLO JORGE LUIS', NULL, TRUE, '2020-01-09',1, 14, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0101348795', 'GUILLÉN', 'OSCAR', 'MEDARDO', '', 'GUILLÉN OSCAR MEDARDO ', NULL, TRUE, '2020-01-09',1, 15, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0101342707', 'GUILLERMO', 'DELGADO', 'JORGE', 'ERNESTO', 'GUILLERMO DELGADO JORGE ERNESTO', NULL, TRUE, '2020-01-09',1, 16, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1706381975', 'GUZMÁN', 'CASTAÑEDA', 'HIMMLER', 'ROBERTO', 'GUZMÁN CASTAÑEDA HIMMLER ROBERTO', NULL, TRUE, '2020-01-09',1, 17, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0201628237', 'GUZMÁN', 'ROCHINA', 'LUIS', 'GABRIEL', 'GUZMÁN ROCHINA LUIS GABRIEL', NULL, TRUE, '2020-01-09',1, 18, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1802577708', 'HARO', 'FIGUEROA', 'TANIA', 'MARÍA', 'HARO FIGUEROA TANIA MARÍA', NULL, TRUE, '2020-01-09',1, 19, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0601554694', 'HEREDIA', 'CABRERA', 'EDWIN', 'WALDEMAR', 'HEREDIA CABRERA EDWIN WALDEMAR', NULL, TRUE, '2020-01-09',1, 20, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1707744130', 'HEREDIA', 'PROAÑO', 'MÓNICA', 'ALEXANDRA', 'HEREDIA PROAÑO MÓNICA ALEXANDRA', NULL, TRUE, '2020-01-09',1, 21, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0800565715', 'HERKT', 'PLAZA', 'ERIKA', 'GLADYS', 'HERKT PLAZA ERIKA GLADYS', NULL, TRUE, '2020-01-09',1, 22, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1709527178', 'HERNÁNDEZ', 'ANDINO', 'MILTON', 'GUSTAVO', 'HERNÁNDEZ ANDINO MILTON GUSTAVO', NULL, TRUE, '2020-01-09',1, 23, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0919450379', 'HERNÁNDEZ', 'ANDRADE', 'GLADYS', 'ELINA', 'HERNÁNDEZ ANDRADE GLADYS ELINA', NULL, TRUE, '2020-01-09',1, 24, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1001281250', 'HERNÁNDEZ', 'HIDROBO', 'OLAVO', 'MARCIAL', 'HERNÁNDEZ HIDROBO OLAVO MARCIAL', NULL, TRUE, '2020-01-09',1, 25, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0801328733', 'HERNÁNDEZ', 'PAZMIÑO', 'CESAR', 'ERNESTO', 'HERNÁNDEZ PAZMIÑO CESAR ERNESTO', NULL, TRUE, '2020-01-09',1, 26, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0916407851', 'HERNÁNDEZ', 'RODRÍGUEZ', 'VENUS', 'ADRIANA', 'HERNÁNDEZ RODRÍGUEZ VENUS ADRIANA', NULL, TRUE, '2020-01-09',1, 27, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1710717891', 'HERRERA', 'ESPINOZA', 'EDDY', 'IVONE', 'HERRERA ESPINOZA EDDY IVONE', NULL, TRUE, '2020-01-09',1, 28, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0915185201', 'HIDALGO', 'ORDÓÑEZ', 'YESENIA', 'DEL ROCÍO', 'HIDALGO ORDÓÑEZ YESENIA DEL ROCÍO', NULL, TRUE, '2020-01-09',1, 29, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1802385144', 'HIDALGO', 'SANTAMARÍA', 'ANA', 'MARÍA', 'HIDALGO SANTAMARÍA ANA MARÍA', NULL, TRUE, '2020-01-09',1, 30, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1304950353', 'HIDROVO', 'CONFORME', 'NANCY', 'MARÍA', 'HIDROVO CONFORME NANCY MARÍA', NULL, TRUE, '2020-01-09',1, 31, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0301264768', 'HUGO', 'CORONEL', 'LUIGI', 'SALVATORE', 'HUGO CORONEL LUIGI SALVATORE', NULL, TRUE, '2020-01-09',1, 32, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0908187974', 'HURTADO', 'DEL CASTILLO', 'JAIME', 'RAMIRO', 'HURTADO DEL CASTILLO JAIME RAMIRO', NULL, TRUE, '2020-01-09',1, 33, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0801635038', 'HURTADO', 'TORAL', 'SAYNE', 'XIOMARA', 'HURTADO TORAL SAYNE XIOMARA', NULL, TRUE, '2020-01-09',1, 34, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0918878190', 'INFANTES', 'MANTILLA', 'MARÍA', 'AUXILIADORA', 'INFANTES MANTILLA MARÍA AUXILIADORA', NULL, TRUE, '2020-01-09',1, 35, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0101594976', 'INGA', 'YANZA', 'JULIO', 'CÉSAR', 'INGA YANZA JULIO CÉSAR', NULL, TRUE, '2020-01-09',1, 36, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1714479605', 'INSUASTI', 'DELGADO', 'KAROOL', 'SUSANA', 'INSUASTI DELGADO KAROOL SUSANA', NULL, TRUE, '2020-01-09',1, 37, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1301892392', 'INTRIAGO', 'LOOR', 'FELIX', 'ENRIQUE', 'INTRIAGO LOOR FELIX ENRIQUE', NULL, TRUE, '2020-01-09',1, 38, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1308657905', 'INTRIAGO', 'MEJÍA', 'MAGNO', 'GABRIEL', 'INTRIAGO MEJÍA MAGNO GABRIEL', NULL, TRUE, '2020-01-09',1, 39, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0914990643', 'ISAZA', 'PIEDRAHITA', 'JUAN', 'CARLOS', 'ISAZA PIEDRAHITA JUAN CARLOS', NULL, TRUE, '2020-01-09',1, 40, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0916615305', 'ITURRALDE', 'HIDALGO', 'JUAN', 'CARLOS', 'ITURRALDE HIDALGO JUAN CARLOS', NULL, TRUE, '2020-01-09',1, 41, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0300917481', 'IZQUIERDO', 'TAPIA', 'ANITA', 'DEL PILAR', 'IZQUIERDO TAPIA ANITA DEL PILAR', NULL, TRUE, '2020-01-09',1, 42, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0703075812', 'IZURIETA', 'DÁVILA', 'KARINA', '', 'IZURIETA DÁVILA KARINA ', NULL, TRUE, '2020-01-09',1, 43, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1400432124', 'JACOME', 'NOGUERA', 'IVÁNA', 'JAJAIRA', 'JACOME NOGUERA IVÁNA JAJAIRA', NULL, TRUE, '2020-01-09',1, 44, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1801602259', 'JÁCOME', 'ORDÓÑEZ', 'MARÍA', 'DEL CARMEN', 'JÁCOME ORDÓÑEZ MARÍA DEL CARMEN', NULL, TRUE, '2020-01-09',1, 45, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '0200889962', 'JACOME', 'PAZMIÑO', 'ÉDISON', 'POMERIO', 'JACOME PAZMIÑO ÉDISON POMERIO', NULL, TRUE, '2020-01-09',1, 46, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1002047742', 'JARAMILLO', 'CEVALLOS', 'CARMEN', 'INÉS', 'JARAMILLO CEVALLOS CARMEN INÉS', NULL, TRUE, '2020-01-09',1, 47, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1102422290', 'JARAMILLO', 'GONZÁLEZ', 'GALO', 'ARTURO', 'JARAMILLO GONZÁLEZ GALO ARTURO', NULL, TRUE, '2020-01-09',1, 48, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1103393912', 'JARAMILLO', 'JUMBO', 'MARTHA', 'ELIZABETH', 'JARAMILLO JUMBO MARTHA ELIZABETH', NULL, TRUE, '2020-01-09',1, 49, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1102750211', 'JARAMILLO', 'LUZURIAGA', 'TANIA', 'CATALINA', 'JARAMILLO LUZURIAGA TANIA CATALINA', NULL, TRUE, '2020-01-09',1, 50, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1102324686', 'JARAMILLO', 'REYES', 'LUIS', 'PATRICIO', 'JARAMILLO REYES LUIS PATRICIO', NULL, TRUE, '2020-01-09',1, 51, 'El Guabo', '21233434');
INSERT INTO maestros_persona(deleted, identificacion, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, nombre, fecha_nacimiento, activo, created_date, tipo_identificacion_id_id, user_id, direccion, telefono) VALUES (NULL, '1707030514', 'JARAMILLO', 'SALINAS', 'JUAN', 'AGUSTÍN', 'JARAMILLO SALINAS JUAN AGUSTÍN', NULL, TRUE, '2020-01-09',1, 52, 'El Guabo', '21233434');



--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
--- EXPORTADORES
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 1, 5, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 2, 5, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 3, 5, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 4, 5, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 5, 5, TRUE);

--- TECNICO 6
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 6, 3, TRUE);
--- CALIFICADOR 7
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 7, 4, TRUE);

--- PRODUCTOR  8 al 53
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 8, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 9, 2, TRUE);

INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 10, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 11, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 12, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 13, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 14, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 15, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 16, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 17, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 18, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 19, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 20, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 21, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 22, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 23, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 24, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 25, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 26, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 27, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 28, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 29, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 30, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 31, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 32, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 33, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 34, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 35, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 36, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 37, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 38, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 39, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 40, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 41, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 42, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 43, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 44, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 45, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 46, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 47, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 48, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 49, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 50, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 51, 2, TRUE);
INSERT INTO registro_personarol(deleted, activo, created_date, agro_artesanal_id_id, persona_id_id, tipo_rol_id_id, aprobado) VALUES (NULL, TRUE, '2020-01-09', 1, 52, 2, TRUE);


-------------------------------
--- TECNICO 6
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 8, 6, 7, 'FINCA LA PATRICIA');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 8, 6, 7, 'FINCA LA GUARICO');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 8, 6, 7, 'FINCA FIDECADE');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 9, 6, 7, 'FINCA BASTIDAS');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 9, 6, 7, 'FINCA DEL VALLE');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 10, 6, 7, 'FINCA AVICOLAS FINCAVIC S.A.');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 11, 6, 7, 'FINCA URBANAS S A');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 12, 6, 7, 'FINCA PACIFICO');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 13, 6, 7, 'FINCA AZAN S.A.');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 14, 6, 7, 'FINCA MARINAS CORONEL CHALEN');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 15, 6, 7, 'FINCA SAN MARCOS');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 16, 6, 7, 'FINCA AGROACUICOLA FUZU');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 17, 6, 7, 'FINCA UNIDAS');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 18, 6, 7, 'FINCA DEL MAR');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 19, 6, 7, 'FINCA MARINAS');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 20, 6, 7, 'FINCA AVAMER');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 21, 6, 7, 'FINCA DELIA');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 22, 6, 7, 'FINCA MARINAS');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 23, 6, 7, 'FINCA DE EL ORO JR');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 24, 6, 7, 'FINCA HERMANOS TORAL CALLE');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 25, 6, 7, 'HACIENDA LA LOLITA');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 26, 6, 7, 'HACIENDA LA MARIA');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 27, 6, 7, 'HACIENDA LAS DELICIAS');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 28, 6, 7, 'HACIENDA LA SOFIA');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 29, 6, 7, 'HACIENDA LA COMPANIA');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 30, 6, 7, 'HACIENDA LA PIRAMIDE');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 31, 6, 7, 'HACIENDA LA MARTINA');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 32, 6, 7, 'HACIENDA LA GORDITA');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 33, 6, 7, 'HACIENDA LA HUELLA');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 34, 6, 7, 'HACIENDA LA JERONIMA');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 35, 6, 7, 'HACIENDA LA LAGUNA');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 36, 6, 7, 'HACIENDA LA PAPAYA');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 37, 6, 7, 'HACIENDA LA CALERA');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 38, 6, 7, 'HACIENDA LA SIRIA');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 39, 6, 7, 'HACIENDA LAS GEMELAS');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 40, 6, 7, 'HACIENDA LA VICTORIA');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 41, 6, 7, 'HACIENDA LAS ACACIAS');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 42, 6, 7, 'HACIENDA LAS MERCEDES');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 43, 6, 7, 'HACIENDA LA ESTANCIA');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 44, 6, 7, 'HACIENDA EL PALMAR');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 45, 6, 7, 'HACIENDA EL PRADO');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 46, 6, 7, 'HACIENDA EL MARQUEZ');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 47, 6, 7, 'HACIENDA EL CARMEN');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 48, 6, 7, 'HACIENDA EL PLACER');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 49, 6, 7, 'HACIENDA EL CHASQUI');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 50, 6, 7, 'HACIENDA EL PLACER');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 51, 6, 7, 'HACIENDA EL ROSARIO');
INSERT INTO maestros_finca(deleted, area_total, area_cultivada, cajas_por_semana, codigo_magap, activo, created_date, sector_id_id, tipo_finca_id_id, productor_id_id, tecnico_id_id, calificador_id_id, nombre) VALUES (NULL, 40, 40, 800, '1212', TRUE, '2020-01-09', 1, 1, 52, 6, 7, 'HACIENDA EL ZAMORANO');

---------------------------------------------------------------------------------
---- ENFUNDE
---------------------------------------------------------------------------------
--- fincas de freddy naula
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 200, TRUE, '2020-01-09', 1, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-10', 2020, 2, 180, TRUE, '2020-01-09', 2, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-17', 2020, 3, 200, TRUE, '2020-01-09', 3, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-24', 2020, 4, 180, TRUE, '2020-01-09', 4, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-31', 2020, 5, 200, TRUE, '2020-01-09', 5, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 190, TRUE, '2020-01-09', 6, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-02-14', 2020, 7, 200, TRUE, '2020-01-09', 7, 1, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 200, TRUE, '2020-01-09', 1, 2, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-10', 2020, 2, 140, TRUE, '2020-01-09', 2, 2, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-10', 2020, 2, 50, TRUE, '2020-01-09', 2, 2, 2);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-17', 2020, 3, 200, TRUE, '2020-01-09', 3, 2, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-24', 2020, 4, 180, TRUE, '2020-01-09', 4, 2, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-31', 2020, 5, 200, TRUE, '2020-01-09', 5, 2, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 140, TRUE, '2020-01-09', 6, 2, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 140, TRUE, '2020-01-09', 6, 2, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-02-14', 2020, 7, 200, TRUE, '2020-01-09', 7, 2, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 200, TRUE, '2020-01-09', 1, 3, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-10', 2020, 2, 180, TRUE, '2020-01-09', 2, 3, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-17', 2020, 3, 200, TRUE, '2020-01-09', 3, 3, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-24', 2020, 4, 180, TRUE, '2020-01-09', 4, 3, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-31', 2020, 5, 200, TRUE, '2020-01-09', 5, 3, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 190, TRUE, '2020-01-09', 6, 3, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-02-14', 2020, 7, 200, TRUE, '2020-01-09', 7, 3, 1);
--- fincas de jorge alex
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 200, TRUE, '2020-01-09', 1, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-10', 2020, 2, 180, TRUE, '2020-01-09', 2, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-17', 2020, 3, 200, TRUE, '2020-01-09', 3, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-24', 2020, 4, 180, TRUE, '2020-01-09', 4, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-31', 2020, 5, 200, TRUE, '2020-01-09', 5, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 190, TRUE, '2020-01-09', 6, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-02-14', 2020, 7, 200, TRUE, '2020-01-09', 7, 4, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 200, TRUE, '2020-01-09', 1, 5, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-10', 2020, 2, 140, TRUE, '2020-01-09', 2, 5, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-10', 2020, 2, 50, TRUE, '2020-01-09', 2, 5, 2);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-17', 2020, 3, 200, TRUE, '2020-01-09', 3, 5, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-24', 2020, 4, 180, TRUE, '2020-01-09', 4, 5, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-01-31', 2020, 5, 200, TRUE, '2020-01-09', 5, 5, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 140, TRUE, '2020-01-09', 6, 5, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 140, TRUE, '2020-01-09', 6, 5, 1);
INSERT INTO enfunde_enfunde(deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) VALUES (NULL, '2020-02-14', 2020, 7, 200, TRUE, '2020-01-09', 7, 5, 1);

--- otros productores copie de finca 5
INSERT INTO enfunde_enfunde (deleted, fecha_enfunde, anio, semana, cantidad, activo, created_date, cinta_id_id, finca_id_id, tipo_enfunde_id_id) SELECT enfunde_enfunde.deleted, enfunde_enfunde.fecha_enfunde, enfunde_enfunde.anio, enfunde_enfunde.semana, enfunde_enfunde.cantidad, enfunde_enfunde.activo, enfunde_enfunde.created_date, enfunde_enfunde.cinta_id_id, maestros_finca.id, enfunde_enfunde.tipo_enfunde_id_id FROM enfunde_enfunde, maestros_finca WHERE maestros_finca.id>=6 AND enfunde_enfunde.id >= 31 AND enfunde_enfunde.id <=39;

---------------------------------------------------------------------------------
---- CALIBRAJE
---------------------------------------------------------------------------------
---- Estimados  7 semanas   1 al 48

INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 190, TRUE, '2020-01-09', 1, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 170, TRUE, '2020-01-09', 1, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 10, TRUE, '2020-01-09', 1, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 20, TRUE, '2020-01-09', 1, 4);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-10', 2020, 2, 150, TRUE, '2020-01-09', 1, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 2, 160, TRUE, '2020-01-09', 1, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 2, 20, TRUE, '2020-01-09', 1, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 3, 190, TRUE, '2020-01-09', 1, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 3, 170, TRUE, '2020-01-09', 1, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 3, 10, TRUE, '2020-01-09', 1, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 3, 20, TRUE, '2020-01-09', 1, 4);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-24', 2020, 4, 160, TRUE, '2020-01-09', 1, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-24', 2020, 4, 170, TRUE, '2020-01-09', 1, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-24', 2020, 4, 10, TRUE, '2020-01-09', 1, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 5, 190, TRUE, '2020-01-09', 1, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 5, 180, TRUE, '2020-01-09', 1, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 5, 10, TRUE, '2020-01-09', 1, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 5, 10, TRUE, '2020-01-09', 1, 4);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 180, TRUE, '2020-01-09', 1, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 180, TRUE, '2020-01-09', 1, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 10, TRUE, '2020-01-09', 1, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-14', 2020, 7, 190, TRUE, '2020-01-09', 1, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-14', 2020, 7, 180, TRUE, '2020-01-09', 1, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-14', 2020, 7, 20, TRUE, '2020-01-09', 1, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 190, TRUE, '2020-01-09', 2, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 170, TRUE, '2020-01-09', 2, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 10, TRUE, '2020-01-09', 2, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 20, TRUE, '2020-01-09', 2, 4);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-10', 2020, 2, 150, TRUE, '2020-01-09', 2, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 2, 160, TRUE, '2020-01-09', 2, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 2, 20, TRUE, '2020-01-09', 2, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 3, 190, TRUE, '2020-01-09', 2, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 3, 170, TRUE, '2020-01-09', 2, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 3, 10, TRUE, '2020-01-09', 2, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 3, 20, TRUE, '2020-01-09', 2, 4);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-24', 2020, 4, 160, TRUE, '2020-01-09', 2, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-24', 2020, 4, 170, TRUE, '2020-01-09', 2, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-24', 2020, 4, 10, TRUE, '2020-01-09', 2, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 5, 190, TRUE, '2020-01-09', 2, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 5, 180, TRUE, '2020-01-09', 2, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 5, 10, TRUE, '2020-01-09', 2, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 5, 10, TRUE, '2020-01-09', 2, 4);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 180, TRUE, '2020-01-09', 2, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 180, TRUE, '2020-01-09', 2, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 10, TRUE, '2020-01-09', 2, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-14', 2020, 7, 190, TRUE, '2020-01-09', 2, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-14', 2020, 7, 180, TRUE, '2020-01-09', 2, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-14', 2020, 7, 20, TRUE, '2020-01-09', 2, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 190, TRUE, '2020-01-09', 3, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 170, TRUE, '2020-01-09', 3, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 10, TRUE, '2020-01-09', 3, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 20, TRUE, '2020-01-09', 3, 4);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-10', 2020, 2, 150, TRUE, '2020-01-09', 3, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 2, 160, TRUE, '2020-01-09', 3, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 2, 20, TRUE, '2020-01-09', 3, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 3, 190, TRUE, '2020-01-09', 3, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 3, 170, TRUE, '2020-01-09', 3, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 3, 10, TRUE, '2020-01-09', 3, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 3, 20, TRUE, '2020-01-09', 3, 4);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-24', 2020, 4, 160, TRUE, '2020-01-09', 3, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-24', 2020, 4, 170, TRUE, '2020-01-09', 3, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-24', 2020, 4, 10, TRUE, '2020-01-09', 3, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 5, 190, TRUE, '2020-01-09', 3, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 5, 180, TRUE, '2020-01-09', 3, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 5, 10, TRUE, '2020-01-09', 3, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 5, 10, TRUE, '2020-01-09', 3, 4);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 180, TRUE, '2020-01-09', 3, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 180, TRUE, '2020-01-09', 3, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 10, TRUE, '2020-01-09', 3, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-14', 2020, 7, 190, TRUE, '2020-01-09', 3, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-14', 2020, 7, 180, TRUE, '2020-01-09', 3, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-14', 2020, 7, 20, TRUE, '2020-01-09', 3, 3);
																
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 190, TRUE, '2020-01-09', 4, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 170, TRUE, '2020-01-09', 4, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 10, TRUE, '2020-01-09', 4, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 20, TRUE, '2020-01-09', 4, 4);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-10', 2020, 2, 150, TRUE, '2020-01-09', 4, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 2, 160, TRUE, '2020-01-09', 4, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 2, 20, TRUE, '2020-01-09', 4, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 3, 190, TRUE, '2020-01-09', 4, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 3, 170, TRUE, '2020-01-09', 4, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 3, 10, TRUE, '2020-01-09', 4, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 3, 20, TRUE, '2020-01-09', 4, 4);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-24', 2020, 4, 160, TRUE, '2020-01-09', 4, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-24', 2020, 4, 170, TRUE, '2020-01-09', 4, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-24', 2020, 4, 10, TRUE, '2020-01-09', 4, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 5, 190, TRUE, '2020-01-09', 4, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 5, 180, TRUE, '2020-01-09', 4, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 5, 10, TRUE, '2020-01-09', 4, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 5, 10, TRUE, '2020-01-09', 4, 4);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 180, TRUE, '2020-01-09', 4, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 180, TRUE, '2020-01-09', 4, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 10, TRUE, '2020-01-09', 4, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-14', 2020, 7, 190, TRUE, '2020-01-09', 4, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-14', 2020, 7, 180, TRUE, '2020-01-09', 4, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-14', 2020, 7, 20, TRUE, '2020-01-09', 4, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 190, TRUE, '2020-01-09', 5, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 170, TRUE, '2020-01-09', 5, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 10, TRUE, '2020-01-09', 5, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 1, 20, TRUE, '2020-01-09', 5, 4);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-10', 2020, 2, 150, TRUE, '2020-01-09', 5, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 2, 160, TRUE, '2020-01-09', 5, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 2, 20, TRUE, '2020-01-09', 5, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 3, 190, TRUE, '2020-01-09', 5, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 3, 170, TRUE, '2020-01-09', 5, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 3, 10, TRUE, '2020-01-09', 5, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 3, 20, TRUE, '2020-01-09', 5, 4);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-24', 2020, 4, 160, TRUE, '2020-01-09', 5, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-24', 2020, 4, 170, TRUE, '2020-01-09', 5, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-24', 2020, 4, 10, TRUE, '2020-01-09', 5, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 5, 190, TRUE, '2020-01-09', 5, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 5, 180, TRUE, '2020-01-09', 5, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 5, 10, TRUE, '2020-01-09', 5, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-01-03', 2020, 5, 10, TRUE, '2020-01-09', 5, 4);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 180, TRUE, '2020-01-09', 5, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 180, TRUE, '2020-01-09', 5, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-07', 2020, 6, 10, TRUE, '2020-01-09', 5, 3);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-14', 2020, 7, 190, TRUE, '2020-01-09', 5, 1);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-14', 2020, 7, 180, TRUE, '2020-01-09', 5, 2);
INSERT INTO enfunde_calibraje(deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) VALUES (NULL, '2020-02-14', 2020, 7, 20, TRUE, '2020-01-09', 5, 3);

--- otros productores copie de finca 5
INSERT INTO enfunde_calibraje (deleted, fecha_calibraje, anio, semana, cantidad, activo, created_date, finca_id_id, tipo_calibraje_id_id) SELECT enfunde_calibraje.deleted, enfunde_calibraje.fecha_calibraje, enfunde_calibraje.anio, enfunde_calibraje.semana, enfunde_calibraje.cantidad, enfunde_calibraje.activo, enfunde_calibraje.created_date, maestros_finca.id, enfunde_calibraje.tipo_calibraje_id_id FROM enfunde_calibraje, maestros_finca WHERE maestros_finca.id>=6 AND enfunde_calibraje.id >= 97 AND enfunde_calibraje.id <= 120;


---------------------------------------------------------------------------------
---- ESTIMADO
---------------------------------------------------------------------------------
---- Estimados  7 semanas
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-03', 2020, 1, 200, TRUE, '2020-01-09', 1, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-10', 2020, 2, 180, TRUE, '2020-01-09', 1, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-03', 2020, 3, 200, TRUE, '2020-01-09', 1, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-24', 2020, 4, 180, TRUE, '2020-01-09', 1, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-03', 2020, 5, 200, TRUE, '2020-01-09', 1, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-02-07', 2020, 6, 190, TRUE, '2020-01-09', 1, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-02-14', 2020, 7, 200, TRUE, '2020-01-09', 1, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-03', 2020, 1, 200, TRUE, '2020-01-09', 2, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-10', 2020, 2, 180, TRUE, '2020-01-09', 2, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-03', 2020, 3, 200, TRUE, '2020-01-09', 2, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-24', 2020, 4, 180, TRUE, '2020-01-09', 2, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-03', 2020, 5, 200, TRUE, '2020-01-09', 2, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-02-07', 2020, 6, 190, TRUE, '2020-01-09', 2, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-02-14', 2020, 7, 200, TRUE, '2020-01-09', 2, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-03', 2020, 1, 200, TRUE, '2020-01-09', 3, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-10', 2020, 2, 180, TRUE, '2020-01-09', 3, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-03', 2020, 3, 200, TRUE, '2020-01-09', 3, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-24', 2020, 4, 180, TRUE, '2020-01-09', 3, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-03', 2020, 5, 200, TRUE, '2020-01-09', 3, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-02-07', 2020, 6, 190, TRUE, '2020-01-09', 3, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-02-14', 2020, 7, 200, TRUE, '2020-01-09', 3, FALSE);

INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-03', 2020, 1, 200, TRUE, '2020-01-09', 4, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-10', 2020, 2, 180, TRUE, '2020-01-09', 4, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-03', 2020, 3, 200, TRUE, '2020-01-09', 4, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-24', 2020, 4, 180, TRUE, '2020-01-09', 4, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-03', 2020, 5, 200, TRUE, '2020-01-09', 4, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-02-07', 2020, 6, 190, TRUE, '2020-01-09', 4, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-02-14', 2020, 7, 200, TRUE, '2020-01-09', 4, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-03', 2020, 1, 200, TRUE, '2020-01-09', 5, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-10', 2020, 2, 180, TRUE, '2020-01-09', 5, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-03', 2020, 3, 200, TRUE, '2020-01-09', 5, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-24', 2020, 4, 180, TRUE, '2020-01-09', 5, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-01-03', 2020, 5, 200, TRUE, '2020-01-09', 5, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-02-07', 2020, 6, 190, TRUE, '2020-01-09', 5, FALSE);
INSERT INTO estimado_estimadocaja(deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) VALUES (NULL, '2020-02-14', 2020, 7, 200, TRUE, '2020-01-09', 5, FALSE);

--- otros productores copie de finca 5
INSERT INTO estimado_estimadocaja (deleted, fecha_estimado, anio, semana, cantidad, activo, created_date, finca_id_id, bloqueado) SELECT estimado_estimadocaja.deleted, estimado_estimadocaja.fecha_estimado, estimado_estimadocaja.anio, estimado_estimadocaja.semana, estimado_estimadocaja.cantidad, estimado_estimadocaja.activo, estimado_estimadocaja.created_date, maestros_finca.id, estimado_estimadocaja.bloqueado FROM estimado_estimadocaja, maestros_finca WHERE maestros_finca.id>=6 AND estimado_estimadocaja.id >= 29 AND estimado_estimadocaja.id <= 35;
SELECT * FROM estimado_estimadocaja;
---------------------------------------------------------------------------------
---- EMBARQUE
---------------------------------------------------------------------------------
/*
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'RUSIA 01', '1/1/2020', '5/1/2020', 2020, 1, 200, 6.50, 22, 20, 10, 12, TRUE, '2020-01-09', 1, 1, 1, 1, FALSE);
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'RUSIA 02', '6/1/2020', '12/1/2020', 2020, 2, 200, 6.50, 22, 20, 10, 12, TRUE, '2020-01-09', 1, 1, 1, 1, FALSE);
INSERT INTO envio_embarque(deleted, codigo_alterno, fecha_embarque_desde, fecha_embarque_hasta, anio, semana, cantidad, precio, peso, largo, calidad_minima, calidad_maxima, activo, created_date, marca_caja_id_id, puerto_destino_id_id, puerto_salida_id_id, vapor_id_id, finalizado) VALUES (NULL, 'RUSIA 03', '13/1/2020', '19/1/2020', 2020, 3, 200, 6.50, 22, 20, 10, 12, TRUE, '2020-01-09', 1, 1, 1, 1, FALSE);

INSERT INTO envio_ordencorte(deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) VALUES (NULL , 'BOD123', '14/1/2020', 2020, 3, 45, TRUE, '2020-01-09', 3, 1, FALSE);
INSERT INTO envio_ordencorte(deleted, codigo_bodega, fecha_corte, anio, semana, cantidad, activo, created_date, embarque_id_id, finca_id_id, facturado) VALUES (NULL , 'BOD123', '14/1/2020', 2020, 3, 45, TRUE, '2020-01-09', 3, 2, FALSE);
*/


/*


--- https://www.marinetraffic.com/es/ais/details/ports/1489/Ecuador_port:PUERTO%20BOLIVAR


select * from Web_Sector
select * from Web_MarcaCaja
select * from Web_TipoCalibraje




UBESA
REYBANPAC
ASOAGRIBAL
TRUISFRUIT
FRUTADELI
COMERSUR
ASISBANE
SABROSTAR FRUIT
ECUAGREENPRODEX
TROPICAL FRUIT EXPORT
FRUTICAL
AGZULASA
EXP. SOPRISA
EXPORTSWEET
EXBAORO
GINAFRUIT
CHIQUITA BANANA ECUADOR
*/
