# MULTUM

## Instalación backend
```bash
git clone https://gitlab.com/Jealvia/multum.git
En el directorio multum/multum-backend/multum/ se debe crear un archivo base.py con el siguiente contenido:

import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'multumdevelopment',
        'USER':'user',#Se debe agregar usuario de la base
		'PASSWORD':'123456',#Se debe agregar la contraseña del usuario
        'HOST': 'localhost',
        'PORT': '5432',
    }        
}

En la misma ruta esta el requirements donde estan las librerías necesarias para ejecutar el backend
Para crear el entorno, se debe crear en la raiz del proyecto esta agregado en el .gitignore: 
python3 -m venv multum
A continuación instalamos las librerias necesarias:
pip install -r requirements.txt

Ejecutamos las migraciones
python manage.py migrate
Ejecutamos el proyecto 
python manage.py runserver
```
Para más información 
[Django](https://tutorial.djangogirls.org/es/django_installation/)

## Instalación frontend
Ir a la ruta /multum/multum-web/
```bash
Ejecutar: npm install
Una vez completa la instalación nos dirigimos a /multum/multum-web/node_modules/@angular\material\_theming.scss
en la linea 602 agregamos el siguiente contenido
$sng-primary: (
  50: #e6f4e6,
  100: #c3e4c2,
  200: #9cd29a,
  300: #74c272,
  400: #54b553,
  500: #31a833,
  600: #27992a,
  700: #19881f,
  800: #057713,
  900: #005900,
  contrast: (
    50: $dark-primary-text,
    100: $dark-primary-text,
    200: $dark-primary-text,
    300: $dark-primary-text,
    400: $dark-primary-text,
    500: $light-primary-text,
    600: $light-primary-text,
    700: $light-primary-text,
    800: $light-primary-text,
    900: $light-primary-text,
  )
);
```
## Instalación app
Ir a la ruta /multum/multum-app/
```bash
Ejecutar: npm install
```