import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { RegistroService } from 'src/app/services/registro.service';
import { Storage } from '@ionic/storage';
import { registro } from 'src/app/services/estructura.modelo';
import { CommonService } from 'src/app/services/common.service';
@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  

  registroForm: FormGroup;
  mensaje: string = ''; b_mensaje: boolean = false;
  modelo: registro;
  carga=false;

  

  constructor(
    public alertControl: AlertController,
    private formBuilder: FormBuilder,
    private router:Router,
    private registroService:RegistroService,
    private storage: Storage,
    private commonService: CommonService,

  ) {
    
    this.prepareForm();
    
    
   }

   ngOnInit(): void {
    
  }

  onBlur(){
    let confirmar: string;
    if (this.registroForm.value.clave == this.registroForm.value.confClave) {
      console.log('confirmada clave');
    } else {
      //this.registroForm.setValue({'confClave': ''},{onlySelf: false});
      //this.registroForm.setValue.confClave 
      this.commonService.msjConfirm('Clave no coincide','Por Favor Ingrese la clave que coincida');
      
    }
  }

  prepareForm(){
    let regexPatterns = {
      // this can be improved
      numeros: "[0-2]?[0-9]?",
      minutes: "[0-5]?[0-9]?",
      ruc: "[0-9]{11,14}",
   };

    this.registroForm = this.formBuilder.group({
    pNombre: ['', Validators.required],
    sNombre: [''],
    identificacion: ['',[Validators.required,Validators.pattern(regexPatterns.ruc)]],
    pApellido: ['', Validators.required],
    sApellido: ['', Validators.required],

    telefono: ['', Validators.required],
    direccion: ['', Validators.required],
    correo: ['',  [Validators.required, Validators.email]],

    fechaNac: ['', Validators.required],
    usuario: ['',[Validators.required, Validators.minLength(4),Validators.maxLength(18)]],
    clave: ['',[Validators.required, Validators.minLength(6)]],
    confClave: ['', Validators.required],
    
    });
  }

  onSubmit(){
    this.carga=true;
    //this.parseForm();
    //let data: registro =  this.modelo;
    
    let data=
    {
      //'tipo_identificacion_id':this.registroForm.value.usuario,
      'primer_apellido': this.registroForm.value.pApellido,
      'identificacion': this.registroForm.value.identificacion,
      'segundo_apellido': this.registroForm.value.sApellido,
      'primer_nombre': this.registroForm.value.pNombre,
      'segundo_nombre': this.registroForm.value.sNombre,

      'telefono': this.registroForm.value.telefono,
      'direccion': this.registroForm.value.direccion,
      'correo':this.registroForm.value.correo,
      //'nombre': '',
      'fecha_nacimiento': this.registroForm.value.fechaNac,
      'usuario': this.registroForm.value.usuario,
      'contrasena': this.registroForm.value.clave,
      
    }
    
    this.registroService.regitrarUsuario(data).subscribe(
      res=>{
        console.log(res);
        if (res.status == 'success') {
          console.log("Guardado exitosamente");
          this.commonService.msjConfirm('Registro exitoso','Muchas gracias por su registro');
          this.prepareForm();
          this.carga=false;
          this.router.navigate(['/account/login']);
        } else {
          console.log("Some error occured");
          this.carga=false;
          if (res.mensaje == "Usuario existente") {
            this.commonService.msjConfirm('Usuario existente','Por Favor, intente con otro usuario');
          }
        }
      },
      error=>{
        //this.router.navigate(['/'])
        console.log('erroorr: ',error)
        this.carga=false;
        this.commonService.msjErrorServ();
      }
    )

  }

  tieneCuenta(){
    this.prepareForm();
    this.router.navigate(['/account/login']);

  }

  parseForm(){
    //this.modelo.tipo_identificacion_id = this.registroForm.value.usuario;
    
    console.log('form pApellido', this.registroForm.value.pApellido);
    console.log('form pApellido', this.registroForm);
    console.log('form pApellido', this.modelo);
    this.modelo.primer_apellido = this.registroForm.value.pApellido;
    this.modelo.identificacion = this.registroForm.value.identificacion;
    this.modelo.segundo_apellido = this.registroForm.value.sApellido;
    this.modelo.primer_nombre = this.registroForm.value.pNombre;
    this.modelo.segundo_nombre = this.registroForm.value.sNombre;
    this.modelo.nombre = '';
    this.modelo.fecha_nacimiento = this.registroForm.value.fechaNac;
    this.modelo.usuario = this.registroForm.value.usuario;
    this.modelo.contrasena = this.registroForm.value.clave;
  }


}
