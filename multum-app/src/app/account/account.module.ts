import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AccountRoutingModule } from './account-routing.module';

//componentes y pages
import { AccountComponent } from './account.component';


@NgModule({
  declarations: [
    AccountComponent,
  ],
  imports: [
    CommonModule,
    AccountRoutingModule,
    IonicModule.forRoot(),
    
  ],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AccountComponent]
})
export class AccountModule { }
