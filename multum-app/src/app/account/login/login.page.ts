import {
  Component,
  OnInit,
  Host
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import {
  AlertController
} from '@ionic/angular';
import {
  Router
} from '@angular/router';
import {
  RegistroService
} from 'src/app/services/registro.service';
import {
  Storage
} from '@ionic/storage';
import {
  CommonService
} from 'src/app/services/common.service';
import { HomePage } from 'src/app/pages/menu/home/home.page';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  mensaje: string = '';b_mensaje: boolean = false;
  carga = false;
  showPassword = false;
  mostrar = false;
  passwordIcon = 'eye-outline'
  constructor(
    public alertControl: AlertController,
    private formBuilder: FormBuilder,
    private router: Router,
    private registroService: RegistroService,
    private storage: Storage,
    private commonService: CommonService,
    //private homePage: HomePage
    //@Host() private homePage: HomePage
  ) {

    this.loginForm = this.formBuilder.group({
      usuario: ['', Validators.required],
      clave: ['', Validators.required],

    });

  }

  cambiarVisibilidad() {
    this.showPassword = !this.showPassword
    if (this.passwordIcon == 'eye-outline') {
      this.passwordIcon = 'eye-off-outline'
    } else {
      this.passwordIcon = 'eye-outline'
    }
  }

  ngOnInit() {
    this.storage.get('token').then(res => {
      if(res!=null){
        this.router.navigate(['/main/tabs/tab1'], {
          replaceUrl: true,
          skipLocationChange: false
        });
      }else{
        this.mostrar = true;  
      }
      
    }, error => {
      this.mostrar = true;
    })
  }

  onSubmit() {
    this.carga = true;
    this.b_mensaje = false;
    let data = {
      'usuario': this.loginForm.value.usuario,
      'contasena': this.loginForm.value.clave,
    }

    this.registroService.inicioSesion(data).subscribe(
      res => {
        console.log(res);
        if (res.status == 'success') {

          this.storage.set('token', res.token)
            .then(
              () => console.log('Stored item!'),
              error => console.error('Error storing item', error)
            );
          this.storage.set('session', res.session)
            .then(
              () => console.log('Stored item!'),
              error => console.error('Error storing item', error)
            );
          this.storage.set('user', res.user)
            .then(
              () => console.log('Stored item!'),
              error => console.error('Error storing item', error)
            );
          this.storage.set('nombres', res.nombres)
            .then(
              () => console.log('Stored item!'),
              error => console.error('Error storing item', error)
            );

          this.carga = false;
          //this.homePage.consultaReatardo()
          this.router.navigate(['/main']);
        } else {
          console.log("error datos invalidos")
          this.b_mensaje = true;
          //this.mensaje = '¡Datos Incorrectos!'
          this.mensaje = res.mensaje;
          this.carga = false;
        }
      },
      error => {
        console.log('erroorr: ', error)
        this.carga = false;
        this.commonService.msjErrorServ();
      }
    )

  }



}