import {
  NgModule
} from '@angular/core';
import {
  PreloadAllModules,
  RouterModule,
  Routes
} from '@angular/router';
import {
  AuthServiceService
} from '@services/auth-service.service';

const routes: Routes = [{
    path: '',
    redirectTo: 'account',
    pathMatch: 'full',
    canActivate: [AuthServiceService]
  },

  {
    path: 'account',
    loadChildren: () => import('./account/account.module').then(m => m.AccountModule),
    canActivate: [AuthServiceService]
  },
  {
    path: 'main',
    loadChildren: () => import('./tab/tabmain/tabmain.module').then(m => m.TabmainPageModule)
  },
  {
    path: 'totales',
    loadChildren: () => import('./pages/modales/totales/totales.module').then(m => m.TotalesPageModule)
  },
  {
    path: 'basica',
    loadChildren: () => import('./pages/modales/estadisticas/basica/Est-basica.module').then( m => m.BasicaPageModule)
  },

  /* {
    path: 'estadistica',
    loadChildren: () => import('./pages/modales/enfunde/estadistica/estadistica.module').then( m => m.EstadisticaPageModule)
  }, */
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}