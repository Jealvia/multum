import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TabmainPageRoutingModule } from './tabmain-routing.module';

import { TabmainPage } from './tabmain.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabmainPageRoutingModule,
    ComponentsModule
  ],
  declarations: [TabmainPage]
})
export class TabmainPageModule {}
