import { Component, OnInit, Host } from '@angular/core';
import { CommonService } from '@services/common.service';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { FCM } from '@ionic-native/fcm/ngx';
import { HomePage } from 'src/app/pages/menu/home/home.page';

@Component({
  selector: 'app-tabmain',
  templateUrl: './tabmain.page.html',
  styleUrls: ['./tabmain.page.scss'],
})
export class TabmainPage implements OnInit {
  notificaciones=0;
  home=true;
  labores=false;
  perfil=false;
  titulo="Inicio"
  constructor(
    private commonService:CommonService,
    
    private fcm: FCM,
  ) { }
  
  ngOnInit() {
    //this.functionNr1()
    this.fcm.getToken().then(token => {

      this.commonService.almacenarToken(token).subscribe(
        res => {
          console.log(res)
        }
      )
      //console.log("Obtencion token: "+token)
    });

    this.fcm.onTokenRefresh().subscribe(token => {

      this.commonService.almacenarToken(token).subscribe(
        res => {
          console.log(res)
        }
      )
      console.log(token);
    });
  }

  functionNr1() {
    setInterval(()=>{
      this.commonService.numeroNotificaciones().subscribe(
        res=>{
          console.log('respuesta listado', res)
          if(res.status=='success'){
            console.log(this.notificaciones)
            this.notificaciones = res.length;
          }
          
        }
      )
    },4000)
  }

  onClick(dir:string){
    if(dir=="home"){
      this.home=true;
      this.labores=false;
      this.perfil=false;
      this.titulo="Inicio"
    }else if(dir=="labores"){
      this.home=false;
      this.labores=true;
      this.perfil=false;
      this.titulo="Labores"
    }else{
      this.home=false;
      this.labores=false;
      this.perfil=true;
      this.titulo="Perfil"
    }
    
  }

}
