import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TabmainPage } from './tabmain.page';

describe('TabmainPage', () => {
  let component: TabmainPage;
  let fixture: ComponentFixture<TabmainPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabmainPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TabmainPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
