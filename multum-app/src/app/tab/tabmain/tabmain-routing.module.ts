import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabmainPage } from './tabmain.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabmainPage,
    children:
      [
        {
          path: 'tab1',
          children:
            [
              {
                path: '',
                loadChildren: () => import('../../pages/menu/home/home.module').then( m => m.HomePageModule)
              },
              
            ]
        },
        {
          path: 'tab2',
          children:
            [
              {
                path: '',
                loadChildren: () => import('../../pages/menu/labores/labores.module').then( m => m.LaboresPageModule)
              },
              {
                path: 'estimado',
                loadChildren: () => import('../../pages/menu/estimado/estimado.module').then( m => m.EstimadoPageModule)
              },
              {
                path: 'enfunde',
                loadChildren: () => import('../../pages/menu/enfunde/enfunde.module').then( m => m.EnfundePageModule)
              },
              {
                path: 'ordendecorte',
                loadChildren: () => import('../../pages/menu/ordendecorte/ordendecorte.module').then( m => m.OrdendecortePageModule)
              },
              {
                path: 'calibraje',
                loadChildren: () => import('../../pages/menu/calibraje/calibraje.module').then( m => m.CalibrajePageModule)
              },
              {
                path: 'salida-caja',
                loadChildren: () => import('../../pages/menu/salida-caja/salida-caja.module').then( m => m.SalidaCajaPageModule)
              },
              {
                path: 'produccion',
                loadChildren: () => import('../../pages/menu/produccion/produccion.module').then( m => m.ProduccionPageModule)
              },
              {
                path: 'racimo',
                loadChildren: () => import('../../pages/menu/racimo/racimo.module').then( m => m.RacimoPageModule)
              },
              {
                path: 'cajas',
                loadChildren: () => import('../../pages/menu/cajas/cajas.module').then( m => m.CajasPageModule)
              },
            ]
        },
        {
          path: 'tab3',
          children:
            [
              {
                path: '',
                loadChildren: () => import('../../pages/menu/perfil/perfil.module').then( m => m.PerfilPageModule)
              },
              {
                path: 'facturacion',
                loadChildren: () => import('../../pages/menu/facturacion/facturacion.module').then( m => m.FacturacionPageModule)
              },
              {
                path: 'notificaciones',
                loadChildren: () => import('../../pages/menu/notificaciones/notificaciones.module').then( m => m.NotificacionesPageModule)
              },
              {
                path: 'configuracion',
                loadChildren: () => import('../../pages/menu/configuracion/configuracion.module').then( m => m.ConfiguracionPageModule)
              },
              {
                path: 'calificacion',
                loadChildren: () => import('../../pages/menu/calificacion/calificacion.module').then( m => m.CalificacionPageModule)
              },
              {
                path: 'finca',
                loadChildren: () => import('../../pages/menu/finca/finca.module').then( m => m.FincaPageModule)
              },
              {
                path: 'finca/:nuevo',
                loadChildren: () => import('../../pages/menu/finca/finca.module').then( m => m.FincaPageModule)
              },
              {
                path: 'mis-datos',
                loadChildren: () => import('../../pages/menu/mis-datos/mis-datos.module').then( m => m.MisDatosPageModule)
              },
            ]
        },
        {
          path: '',
          redirectTo: 'tabs/tab1',
          pathMatch: 'full'
        }
      ]
  },
  {
    path: '',
    redirectTo: 'tabs/tab1',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabmainPageRoutingModule {}
