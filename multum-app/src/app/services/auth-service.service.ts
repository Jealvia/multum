import {
  Injectable
} from '@angular/core';
import {
  CanActivate,
  Router
} from '@angular/router';
import {
  CookieService
} from 'ngx-cookie-service';

import {
  Storage
} from '@ionic/storage';
@Injectable({
  providedIn: 'root'
})
export class AuthServiceService implements CanActivate {

  constructor(
    private cookieService: CookieService,
    private router: Router,
    private storage: Storage
  ) {}

  canActivate(route: import("@angular/router").ActivatedRouteSnapshot,
      state: import("@angular/router").RouterStateSnapshot):
    boolean | import("@angular/router").UrlTree | import("rxjs").Observable < boolean |
    import("@angular/router").UrlTree > | Promise < boolean | import("@angular/router").UrlTree > {
      let token = "";
      this.storage.get('token').then(
        data => {
          token = data;
        },
        error => {
        }
      )
      if (token != "") {
        this.router.navigate(['/main']);
        return false;
      } else {
        return true;
      }

    }
}