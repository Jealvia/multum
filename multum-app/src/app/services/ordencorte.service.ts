import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import * as Constants from './constants';
@Injectable({
  providedIn: 'root'
})
export class OrdenCorteService {

  constructor(
    private http: HttpClient,
    private cookieService: CookieService
  ) { }
  token=this.cookieService.get('token');

  listaOrdenCorte(anio:number, sem_desde:number, sem_hasta:number,finca:number,todos:boolean,token):any{
    let response; 
    //let s_elementos:string = elementos.toString();
    let s_anio:string = anio.toString();
    let s_sem_desde:string = sem_desde.toString();
    let s_sem_hasta:string = sem_hasta.toString();
    let s_finca:string = finca.toString();

    try {
      response = this.http.get < any > (`${Constants.API_ENDPOINT}` +
      "orden-corte-list/anio="+s_anio+'/sem_desde='+s_sem_desde+'/sem_hasta='+s_sem_hasta+'/finca='+s_finca+'/todos='+todos+"/", {
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + token,
        }
      }
     
      
      );
     
    } catch (error) {
      console.log("error:" + error)
    }
    
    return response;
  }

}
