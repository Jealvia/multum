import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as Constants from './constants';
import {
  HttpClient,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { finca } from './estructura.modelo';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class FincaService{

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private cookieService: CookieService
  ) { }
    valor;
    token=this.cookieService.get('token');

  registrar(data: any,token): any{
    let datos: any = data;
    let response;
    try {
      response = this.http.post < any > (`${Constants.API_ENDPOINT}` + "registro-finca/",
      datos, {
          headers: {
            'Content-Type': "application/x-www-form-urlencoded",
            'Authorization': "Token " + token,
          }
        }
      );
    } catch (error) {
      console.log("error:" + error)
    }

    return response;
  }

  async obtenerToken(){
    await this.storage.get('token').then(
      (val)=>{
        this.token=val;
        console.log(this.token)
        return true
      }
    )
    
  }

  listTipoFinca(token):any{
    let response;
     console.log('token',this.token);
    
    try {
      response = this.http.get < any > (`${Constants.API_ENDPOINT}` + "lista-tipo-finca/",{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + token,
        }
      }
     
      
      );
    } catch (error) {
      console.log("error:" + error)
    }

    return response;
  }

  listSector(token):any{
    let response;
    
    try {
      response = this.http.get < any > (`${Constants.API_ENDPOINT}` + "lista-sector/",{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + token,
        }
      }
      );
    } catch (error) {
      console.log("error:" + error)
    }
    
    return response;
  }

  listFincas(token):any{
    let response;
    try {
      response = this.http.get < any > (`${Constants.API_ENDPOINT}` + "lista-finca-general/",{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + token,
        }
      }
      );
    } catch (error) {
      console.log("error:" + error)
    }
    
    return response;
  }
  listFincasFiltro(data,token):any{
    let response;//fecha_desde,fecha_hasta
    let datos={
      'fecha_desde':data.fecha_desde,
      'fecha_hasta':data.fecha_hasta,
      'tipo':data.tipo
    }
    try {
      response = this.http.post < any > (`${Constants.API_ENDPOINT}` + 
      "lista-finca-filtro-general/",datos,{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + token,
        },
        /* params:params */
      }
      );
      console.log(response)
    } catch (error) {
      console.log("error:" + error)
    }
    
    return response;
  }
}
