import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as Constants from './constants';
import {
  HttpClient,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';
import {
  CookieService
} from 'ngx-cookie-service';
import { registro } from './estructura.modelo';
import { CommonService } from './common.service';


@Injectable({
  providedIn: 'root'
})
export class RegistroService {

  modelo: registro;
  constructor(
    private http: HttpClient,
    private cookieService: CookieService,
    private commonService: CommonService,
    
  ) { }

 regitrarUsuario(data: any): any {
   //regitrarUsuario(data: registro): any {
    /*
    let datos = {
      tipo_identificacion_id:String(data.tipo_identificacion_id),
      primer_apellido: String(data.primer_apellido),
      identificacion: String(data.identificacion),
      segundo_apellido: String(data.segundo_apellido),
      primer_nombre: String(data.primer_nombre),
      segundo_nombre: String(data.segundo_nombre),
      nombre: String(data.nombre),
      fecha_nacimiento: String(data.fecha_nacimiento),
      usuario: String(data.usuario),
      contrasena: String(data.contrasena),
    }
    */
   let datos: registro = data;
    let response;
    try {
      response = this.http.post < any > (`${Constants.API_ENDPOINT}` + "registro/",
      datos, {
          headers: {
            'Content-Type': "application/x-www-form-urlencoded"
          }
        }
      );
    } catch (error) {
      console.log("error 2:" + error);
      this.commonService.msjErrorServ();
    }

    return response;
  }

  inicioSesion(data: any): any {
    let datos = {
      usuario: String(data.usuario),
      contasena: String(data.contasena),
    }
    let response;
    try {
      response = this.http.post < any > (`${Constants.API_ENDPOINT}` + "iniciar-sesion/",
      datos, {
          headers: {
            'Content-Type': "application/x-www-form-urlencoded"
          }
        }
      );
    } catch (error) {
      console.log("error:" + error)
    }

    return response;
  }

  misDatos(token): any {
  
    let response;
    try {
      response = this.http.get < any > (`${Constants.API_ENDPOINT}` + "get-persona/",
       {
          headers: {
            'Content-Type': "application/x-www-form-urlencoded",
            'Authorization': "Token " + token,
          }
        }
      );
    } catch (error) {
      console.log("error:" + error)
    }

    return response;
  }

  updateData(data: any, token): any {
    let datos: registro = data;
    console.log('esto es lo que envio: ', data)
     let response;
     try {
       response = this.http.post < any > (`${Constants.API_ENDPOINT}` + "update-persona/",
       datos, {
           headers: {
             'Content-Type': "application/x-www-form-urlencoded",
             'Authorization': "Token " + token,
           }
         }
       );
     } catch (error) {
       console.log("error 2:" + error);
       this.commonService.msjErrorServ();
     }
 
     return response;
   }

  holaMundo(): any {
  
    let response;
    try {
      response = this.http.get < any > (`${Constants.API_ENDPOINT}` + "holamundo/",
       {
          headers: {
            'Content-Type': "application/x-www-form-urlencoded"
          }
        }
      );
    } catch (error) {
      console.log("error:" + error)
    }

    return response;
  }

  miResumen(token): any {
    let response;
    try {
      response = this.http.get < any > (`${Constants.API_ENDPOINT}` + "resumen-semana/",
       {
          headers: {
            'Content-Type': "application/x-www-form-urlencoded",
            'Authorization': "Token " + token,
          }
        }
      );
    } catch (error) {
      console.log("error:" + error)
    }
    return response;
  }

}
