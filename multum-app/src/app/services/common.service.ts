import { Injectable } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import * as Constants from './constants';
import { requeridos } from './estructura.modelo';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  token=this.cookieService.get('token');
  constructor(
    private http: HttpClient,
    private alertControl: AlertController,
    private cookieService: CookieService,
    private toast: ToastController,
    private router: Router,
  ) { }

  async msjConfirm(titulo: string, mensaje: string) {
    const alert = await this.alertControl.create({
      header: titulo,
      //subHeader: 'Subtitle',
      message: mensaje,
      buttons: ['ACEPTAR']
     
    });

    await alert.present();
  }
  
  async msjErrorServ() {
    const alert = await this.alertControl.create({
      header: 'Error en El Servicio',
      //subHeader: 'Subtitle',
      message: 'No tenemos respuesta del servidor, por favor intente en otro momento',
      buttons: ['ACEPTAR']
     
    });

    await alert.present();
  }

  async msjRequerid(titulo: string, mensaje: string, irA: string, url: string ) {
    const alert = await this.alertControl.create({
      header: titulo,
      message: mensaje,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ir a '+irA,
          handler: () => {
            console.log('Confirm Okay', irA);
            this.router.navigate(['/'+url])
          }
        }
      ]
    });

    await alert.present();
  }

  almacenarToken(token):any{
    console.log('1..el token telefono es 1::::::: ', token);
    let response;
    console.log('2....el token es 2::::::: ', this.cookieService.get('token'));
    let datos={
      'token':String(token),
      'dispositivo':"Android"
    }
    try {
      response = this.http.post < any > (`${Constants.API_ENDPOINT}` + "registrar-token/",
      datos,{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + this.cookieService.get('token'),
        }
      });
    } catch (error) {
      console.log("error:" + error)
    }
    return response;
  }

  notificacion(){
    let response;
    let datos={
      id:3
    }
    try {
      response = this.http.post < any > (`${Constants.API_ENDPOINT}` + "enviar-notificacion/",
      datos,{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + this.cookieService.get('token'),
        }
      });
    } catch (error) {
      console.log("error:" + error)
    }
    return response;
  }

  requerimientos( requiere: requeridos, token){
    let response;
    let datos=requiere
    
    try {
      response = this.http.post < any > (`${Constants.API_ENDPOINT}` + "requridos/",
      datos,{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + token,
        }
      });
    } catch (error) {
      console.log("error:" + error)
    }
    return response;
  }

  validarUsuario(token):any{
    let response;
    
    try {
      response = this.http.get < any > (`${Constants.API_ENDPOINT}` + "validar-usuario/",{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + token,
        }
      }
     
      
      );
     
    } catch (error) {
      console.log("error:" + error)
    }
    
    return response;
  }

  numeroNotificaciones():any{

    let response;
    if(this.cookieService.get('token')){
      try {
        response = this.http.get < any > (`${Constants.API_ENDPOINT}` + "lista-notificaciones/",{
           headers: {
          'Content-Type': "application/x-www-form-urlencoded",
          'Authorization': "Token " + this.cookieService.get('token'),
          }
        }
       
        
        );
       
      } catch (error) {
        console.log("error:" + error)
      }
    }
    
    
    
    return response;
  }

  async msjNotificacion(titulo: string, duracion: number){
    let toast = await this.toast.create({
      message: titulo,
      duration: duracion,
      color: 'primary'
    });
    
    toast.present(); 
  }

}
