import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as Constants from './constants';
@Injectable({
  providedIn: 'root'
})
export class EstadisticasService {

  constructor(
    private http: HttpClient,
  ) { }

  estNombre(token, nombre: string, anio:number, sem_desde:number, sem_hasta:number){
    let response;
    let s_anio:string = anio.toString();
    let s_sem_desde:string = sem_desde.toString();
    let s_sem_hasta:string = sem_hasta.toString();
    try {
      response = this.http.get < any > (`${Constants.API_ENDPOINT}` + "estadistica-"+nombre+"/anio="+s_anio+'/sem_desde='+s_sem_desde+'/sem_hasta='+s_sem_hasta+"/",{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + token,
        }
      }
      );
     
    } catch (error) {
      console.log("error:" + error)
    }
    
    return response;
  }

}
