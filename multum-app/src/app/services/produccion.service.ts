import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as Constants from './constants';
@Injectable({
  providedIn: 'root'
})
export class ProduccionService {

  constructor(
    private http: HttpClient,
  ) { }

  listaProduccion(anio:number, sem_desde:number, sem_hasta:number, finca:number,todos:boolean,token):any{
    let response;
    let s_anio:string = anio.toString();
    let s_sem_desde:string = sem_desde.toString();
    let s_sem_hasta:string = sem_hasta.toString();
    let s_finca:string = finca.toString();

    console.log('finca es: ', s_finca)       
    console.log(`${Constants.API_ENDPOINT}` + "produccion/anio="+s_anio+'/sem_desde='+s_sem_desde+'/sem_hasta='+s_sem_hasta+'/finca='+s_finca+'/todos='+todos+"/")
    try {                                                                         
      response = this.http.get < any > (`${Constants.API_ENDPOINT}` + "produccion/anio="+s_anio+'/sem_desde='+s_sem_desde+'/sem_hasta='+s_sem_hasta+'/finca='+s_finca+'/todos='+todos+"/",{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + token,
        }
      }
      );
     
    } catch (error) {
      console.log("error:" + error)
    }
    
    return response;
  }

  listaTipoCaja(token):any{
    let response;
    try {
      response = this.http.get < any > (`${Constants.API_ENDPOINT}` + "lista-tipo-caja/",{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + token,
        }
      }
      );
     
    } catch (error) {
      console.log("error:" + error)
    }
    
    return response;
  }

  guardarRacimoControl(token,data):any{
    let response;
    try {
      response = this.http.post < any > (`${Constants.API_ENDPOINT}` + "guardar-racimo-control/",
      data,
      {
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + token,
        }
      }
      );
     
    } catch (error) {
      console.log("error:" + error)
    }
    
    return response;
  }

  guardarCajaControl(token,data):any{
    let response;
    try {
      response = this.http.post < any > (`${Constants.API_ENDPOINT}` + "guardar-caja-control/",
      data,
      {
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + token,
        }
      }
      );
     
    } catch (error) {
      console.log("error:" + error)
    }
    
    return response;
  }

  listaOrdenCorte(token):any{
    let response;
    try {
      response = this.http.get < any > (`${Constants.API_ENDPOINT}` + "oren-corte-control-list/",{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + token,
        }
      }
      );
     
    } catch (error) {
      console.log("error:" + error)
    }
    
    return response;
  }

  listaOrdenCorteRacimo(token):any{
    let response;
    try {
      response = this.http.get < any > (`${Constants.API_ENDPOINT}` + "oren-corte-control-racimo-list/",{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + token,
        }
      }
      );
     
    } catch (error) {
      console.log("error:" + error)
    }
    
    return response;
  }
}
