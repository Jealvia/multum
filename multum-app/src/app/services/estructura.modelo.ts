export interface registro {
  //tipo_identificacion_id: number;
  primer_apellido: string;
  identificacion: string;
  segundo_apellido: string;
  primer_nombre: string;
  segundo_nombre: string;
  nombre: string;
  fecha_nacimiento: string;
  usuario: string;
  contrasena: string; 
  correo?: string; 
  direccion?: string; 
  telefono?: string; 
}
export interface requeridos{
  finca: boolean;
}

export interface resumenSemana{
  'sem_enfunde': string,
  'sem_calibraje': string,
  'sem_estimado': string,
  'sem_orden': string,
}


export interface estimado{
  fecha_estimado: string;
  cinta_id: number;
  finca_id: number;
  estimado: number;
  semana: number;
  anio: number;
  id:number;
}
export interface EstimadoList {
  id: number;
  finca_id: IdNombre;
  semana: number;
  cantidad: number;
  anio: number;
  fecha_estimado: Date;
  bloqueado:boolean;
}
export interface cintabyFecha{
  id: number;
  anio: number;
  semana: number;
  color: string;
  colorhex: string;
}

export interface enfunde{
  cinta_id: number;
  fecha_enfunde: string;
  anio: string;
  semana: string;
  enfunde: string;
}

export interface IdNombre{
  id:number;
  nombre:string
}


export interface listaEnfunde{
  id: number;
  tipo_enfunde_id: IdNombre;
  anio: string;
  semana: string;
  cinta_id: cintabyFecha;
  cantidad: string;
  finca_id:IdNombre;
  fecha_enfunde:string;
}
export interface facturaDetalles {
  id: number;
  linea: string; // OrdenCorteid>ordencortetabla: embarqueId> embarqueTabla: marcaCajaId>marcaCaja: MarcaCaja
  cantidad: number;
  precio: number;
  valor: number;
}
export interface facturaCabecera {
  Receptor: string;
  Subtotal: number;
  ValorIva: number;
  ValorIvaCero: number;
  Total: number;
}
export interface notificacion {
  origen: string;
  titulo: string;
  mensaje: string;
  estado: boolean;
  img: string;
}
export interface calificacion {
  ptsFuncion: number;
  ptsDiseno: number;
  comentario: string;
}
export interface finca{
  id:number;
  nombre: string;
  area_total: number;
  area_cultivada :number;
  cajas_por_semana :number;
  codigo_magap:string;
  tipo_finca_id :number;
  sector_id: number;
}
export interface fincalist{
  id:number;
  nombre: string;
  tipo_finca_id :IdNombre;
  area_total: number;
  area_cultivada :number;
  cajas_por_semana :number;
  codigo_magap:string;
  sector_id: number;
}
export interface parametros{
  finca: number,
  anio: number,
  semDesde: number,
  semHasta: number,
  id: number,
  todos: boolean,
  grafica?: string, // define cual grafica se mostrara
  showSelectGraf?: boolean // define si se muestra el select
  showfinca?: boolean
}
export interface ShowGrafico{
  inventarioFinal: boolean,
  cosecha: boolean,
  caja: boolean,
  ratio: boolean,
  tendencia: boolean,
  enfunde: boolean
}




/*
export interface enfunde {
  fechaEnfunde: string;
  anio: number;
  semana: number;
  color: string;
  enfunde: number; // Cantidad
}
*/
/*
export interface estimado {
  fincaId: number;
  fecha: string; //Fecha Ingreso
  semanaIngreso: number;
  semana: number; //Semana Estimada
  estimado: number;
}
*/

