import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import * as Constants from './constants';
@Injectable({
  providedIn: 'root'
})
export class EnfundeService {

  constructor(
    private http: HttpClient,
    private cookieService: CookieService
  ) { }
  token=this.cookieService.get('token');

  buscarEnfunde(fecha:any,token):any{
    let response;
    let datos={
      'fecha_enfunde':String(fecha)
    }
    try {
      response = this.http.post < any > (`${Constants.API_ENDPOINT}` + "buscar-enfunde/",
      datos,{
         headers: {
        //'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + token,
        }
      });
    } catch (error) {
      console.log("error:" + error)
    }
    return response;
  }

  listaFincas(token):any{
    let response;
    
    try {
      response = this.http.get < any > (`${Constants.API_ENDPOINT}` + "lista-finca/",{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + token,
        }
      }
     
      
      );
     
    } catch (error) {
      console.log("error:" + error)
    }
    
    return response;
  }

  listaTipoCalibraje(token):any{
    let response;
    
    try {
      response = this.http.get < any > (`${Constants.API_ENDPOINT}` + "lista-tipo-calibraje/",{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + token,
        }
      }
     
      
      );
     
    } catch (error) {
      console.log("error:" + error)
    }
    
    return response;
  }

  listaTipoEnfunde(token):any{
    let response;
    
    try {
      response = this.http.get < any > (`${Constants.API_ENDPOINT}` + "lista-tipo-enfunde/",{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + token,
        }
      }
     
      
      );
     
    } catch (error) {
      console.log("error:" + error)
    }
    
    return response;
  }

  enfundeEnviar(data:any,token):any{
    let datos: any = data;
    let response;
    try {
      response = this.http.post < any > (`${Constants.API_ENDPOINT}` + "crear-enfunde/",
      datos, {
          headers: {
            'Content-Type': "application/x-www-form-urlencoded",
            'Authorization': "Token " + token,
          }
        }
      );
    } catch (error) {
      console.log("error:" + error)
    }

    return response;
  }

  calibrajeEnviar(data:any,token):any{
    let datos: any = data;
    let response;
    try {
      response = this.http.post < any > (`${Constants.API_ENDPOINT}` + "registrar-calibraje/",
      datos, {
          headers: {
            'Content-Type': "application/x-www-form-urlencoded",
            'Authorization': "Token " + token,
          }
        }
      );
    } catch (error) {
      console.log("error:" + error)
    }

    return response;
  }

  listaEnfunde(anio:number, sem_desde:number, sem_hasta:number,finca:number,todos:boolean,token):any{
    let response;
    //let s_elementos:string = elementos.toString();
    let s_anio:string = anio.toString();
    let s_sem_desde:string = sem_desde.toString();
    let s_sem_hasta:string = sem_hasta.toString();
    let s_finca:string = finca.toString();

    console.log(`${Constants.API_ENDPOINT}` +
    "lista-enfunde-movil/anio="+s_anio+'/sem_desde='+s_sem_desde+'/sem_hasta='+s_sem_hasta+'/finca='+s_finca+'/todos='+todos+"/")
    try {
      response = this.http.get < any > (`${Constants.API_ENDPOINT}` +
      "lista-enfunde-movil/anio="+s_anio+'/sem_desde='+s_sem_desde+'/sem_hasta='+s_sem_hasta+'/finca='+s_finca+'/todos='+todos+"/", {
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + token,
        }
      }
     
      
      );
     
    } catch (error) {
      console.log("error:" + error)
    }
    
    return response;
  }

  listaCalibraje(anio:number, sem_desde:number, sem_hasta:number,finca:number,todos:boolean,token):any{
    let response;
    //let s_elementos:string = elementos.toString();
    let s_anio:string = anio.toString();
    let s_sem_desde:string = sem_desde.toString();
    let s_sem_hasta:string = sem_hasta.toString();
    let s_finca:string = finca.toString();
    console.log("a consultar >>>>",`${Constants.API_ENDPOINT}` +
    "lista-calibraje-movil/anio="+s_anio+'/sem_desde='+s_sem_desde+'/sem_hasta='+s_sem_hasta+'/finca='+s_finca+'/todos='+todos+"/")
    /* */
    try {
      response = this.http.get < any > (`${Constants.API_ENDPOINT}` +
      "lista-calibraje-movil/anio="+s_anio+'/sem_desde='+s_sem_desde+'/sem_hasta='+s_sem_hasta+'/finca='+s_finca+'/todos='+todos+"/", {
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + token,
        }
      }
     
      
      );
     
    } catch (error) {
      console.log("error:" + error)
    }
    
    return response;
  }

}
