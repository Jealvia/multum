import { TestBed } from '@angular/core/testing';

import { OrdenCorteService } from './orden-corte.service';

describe('OrdenCorteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OrdenCorteService = TestBed.get(OrdenCorteService);
    expect(service).toBeTruthy();
  });
});
