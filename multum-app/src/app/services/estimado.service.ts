import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import * as Constants from './constants';
@Injectable({
  providedIn: 'root'
})
export class EstimadoService {

  constructor(
    private http: HttpClient,
    private cookieService: CookieService
  ) { }
  token=this.cookieService.get('token');

  listaFincas():any{
    let response;
    
    try {
      response = this.http.get < any > (`${Constants.API_ENDPOINT}` + "lista-fincas/",{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + this.cookieService.get('token'),
        }
      }
      );
     
    } catch (error) {
      console.log("error:" + error)
    }
    
    return response;
  }
  estimadoEnviar(data:any,token):any{
    let datos: any = data;
    let response;
    try {
      response = this.http.post < any > (`${Constants.API_ENDPOINT}` + "crear-estimado/",
      datos, {
          headers: {
            'Content-Type': "application/x-www-form-urlencoded",
            'Authorization': "Token " + token,
          }
        }
      );
    } catch (error) {
      console.log("error:" + error)
    }

    return response;
  }

  listaEstimados(anio:number, sem_desde:number, sem_hasta:number,finca:number,todos:boolean,token):any{
    let response;
    console.log(`${Constants.API_ENDPOINT}` + "lista-movil-estimados/finca="+finca+"/anio="
    +anio+"/sem_desde="+sem_desde+"/sem_hasta="+sem_hasta+"/todos="+todos+"/")
    try {
      response = this.http.get < any > (`${Constants.API_ENDPOINT}` + "lista-movil-estimados/finca="+finca+"/anio="
      +anio+"/sem_desde="+sem_desde+"/sem_hasta="+sem_hasta+"/todos="+todos+"/",{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + token,
        }
      }
      );
     
    } catch (error) {
      console.log("error:" + error)
    }
    
    return response;
  }

}
