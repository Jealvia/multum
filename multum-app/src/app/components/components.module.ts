import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { IonicModule } from '@ionic/angular';
import {  RouterModule } from '@angular/router';
import { PopUserComponent } from "./popover/pop-user/pop-user.component";
import { PopNewProdComponent } from './popover/pop-new-prod/pop-new-prod.component';
import { FormsModule } from '@angular/forms';
import { ModulesModule } from '../modules/modules.module';
import { FacturarComponent } from './modal/facturar/facturar.component';
import { MenuComponent } from './menu/menu.component';
import { NewEnfundeComponent } from './modal/new-enfunde/new-enfunde.component';

@NgModule({
  declarations: [
    HeaderComponent,
    PopUserComponent,
    PopNewProdComponent,
    FacturarComponent,
    MenuComponent,
    NewEnfundeComponent,
  ],
  exports:[
    HeaderComponent,
    PopNewProdComponent,
    FacturarComponent,
    MenuComponent,
    NewEnfundeComponent,
  ],
  entryComponents:[
    PopUserComponent,
    PopNewProdComponent,
    FacturarComponent,
    NewEnfundeComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule,
    FormsModule,
    ModulesModule
  ]
})
export class ComponentsModule { }




