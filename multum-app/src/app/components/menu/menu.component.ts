import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  public appPages = [
    
    {
      title: 'Inicio',
      url: '/main/home',
      icon: 'home'
    },
    {
      title: 'Finca',
      url: '/main/finca',
      icon: 'leaf'
    },
    {
      title: 'Enfunde',
      url: '/main/enfunde',
      icon: 'cube'
    },
    {
      title: 'Estimado',
      url: '/main/estimado',
      icon: 'md-clock'
    },
    {
      title: 'Facturación',
      url: '/main/facturacion',
      icon: 'ios-paper'
    },
    {
      title: 'Notificación',
      url: '/main/notificaciones',
      icon: 'notifications'
    },
    {
      title: 'Configuración',
      url: '/main/configuracion',
      icon: 'md-settings'
    }
    

    /*
    {
      title: 'Calificacion',
      url: '/main/calificacion',
      icon: 'md-star'
    },
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'List',
      url: '/list',
      icon: 'list'
    }
    */
  ];

  constructor() { }

  ngOnInit() {}

}
