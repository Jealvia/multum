import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
import { PopoverController } from '@ionic/angular';
import { facturaDetalles } from 'src/app/services/estructura.modelo';

//enfunde 
import { Router } from '@angular/router';
import { enfunde, listaEnfunde } from 'src/app/services/estructura.modelo';
import { EnfundeService } from 'src/app/services/enfunde.service';
import { formatDate, DatePipe } from '@angular/common';
import { stringify } from 'querystring';
import { CommonService } from 'src/app/services/common.service';
import { anioS } from "@services/constants";

@Component({
  selector: 'app-pop-new-prod',
  templateUrl: './pop-new-prod.component.html',
  styleUrls: ['./pop-new-prod.component.scss'],
})
export class PopNewProdComponent implements OnInit {

  newProductoForm: FormGroup;
  @Input() id: number;
  @Input() b_mod: boolean = false;
  @Input() prod: facturaDetalles; // Modificar valores del padre
  prodNew: facturaDetalles = { id: 0, linea: '', cantidad: null, precio: null, valor: 0}; //valores para cuando es nuevo
  

  constructor(
    private formBuilder: FormBuilder,
    private popoverCtrl: PopoverController
  ) { 
    this.newProductoForm = this.formBuilder.group({
      lineaFactura: [''],
      cantidad: [''],
      precio: [''],
      valor: [''],
    })
    
   }

  ngOnInit() {
    console.log('bandera'+ this.b_mod);
    if (this.b_mod) {
      console.log('entro en condicion');
      this.prodNew = this.prod;
    }
    
  }

  guardar(){
    console.log('id ',this.b_mod );
    let b_guardado: string = 'si';
    if (!this.b_mod) {
      this.prodNew.id= this.id + 1;
    }    
    this.popoverCtrl.dismiss(this.prodNew);
  } 
  
}
    /* Metodo Manual de devolucion de valores
    this.popoverCtrl.dismiss({
      linea: this.prodNew.linea,
      cantidad: this.prodNew.cantidad,
      precio: this.prodNew.precio,
      valor: this.prodNew.valor,
    })
    */


  /*
  guardar(){
    console.log('valor de Linea Factura: '+ this.newProductoForm.value.lineaFactura);
    console.log('valor de Cantidad: '+ this.newProductoForm.value.cantidad);
    console.log('valor de precio: '+ this.newProductoForm.value.precio);
    console.log('valor de valor: '+ this.newProductoForm.value.valor);

    this.popoverCtrl.dismiss({
      linea: this.newProductoForm.value.lineaFactura,
      cantidad: this.newProductoForm.value.cantidad,
      precio: this.newProductoForm.value.precio,
      valor: this.newProductoForm.value.valor,
    })
  }
  */

