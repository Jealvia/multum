import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';
import { RegistroService } from 'src/app/services/registro.service';
import {
  Storage
} from '@ionic/storage';
@Component({
  selector: 'app-pop-user',
  templateUrl: './pop-user.component.html',
  styleUrls: ['./pop-user.component.scss'],
})
export class PopUserComponent implements OnInit {

  mensajeBakc: any;

  constructor(
    private popoverController: PopoverController,
    private router: Router,
    private registroService:RegistroService,
    private storage: Storage
  ) { }

  ngOnInit() {}

  exit(){
    this.storage.remove('token');
    this.storage.remove('session');
    this.storage.remove('user');
    this.storage.remove('nombres');
    this.storage.clear().then(res => {
      console.log('>>Borrado Todo: ', res)
    })
    this.router.navigate(['/']);
    this.popoverController.dismiss();
  }
  misDatos(){
    this.router.navigate(['/main/tabs/tab3/mis-datos']);
    this.popoverController.dismiss();
  }
  calificar(){
    this.router.navigate(['/main/calificacion']);
    this.popoverController.dismiss();
  }
  conexBack(){
    //this.mensajeBakc = this.registroService.holaMundo();
    //console.log('mensaje:', this.mensajeBakc);
    this.registroService.holaMundo().subscribe(
      res=>{
        console.log(res);
        if (res.status == 'success') {
          console.log("Guardado exitosamente")
          this.router.navigate(['/']);
        } else {
          console.log("Some error occured")
        }
      }
      )
  }
}
