import { Component, OnInit, Input, Host } from '@angular/core';
import { PopUserComponent } from "./../popover/pop-user/pop-user.component";
import { PopoverController } from '@ionic/angular';
import { CommonService } from "@services/common.service";
import { TabmainPage } from 'src/app/tab/tabmain/tabmain.page';
import { Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers:[TabmainPage]
})
export class HeaderComponent implements OnInit {

  @Input() titulo: string;
  //@Host() private _app: TabmainPage;
  notificaciones=0;
  constructor( 
    private popoverCtrl: PopoverController,
    private commonService:CommonService,
    private _app: TabmainPage,
    private router: Router,
    ) { }

  ngOnInit() {
    //this.functionNr1();
  }

  async popUser(evento){
    const popover = await this.popoverCtrl.create({
      component: PopUserComponent,
      event: evento,
      mode: 'ios',
      backdropDismiss: true
    });

    await popover.present();
  }

  functionNr1() {
    setInterval(()=>{
      this.commonService.numeroNotificaciones().subscribe(
        res=>{
          if(res.status=='success'){
            this.notificaciones = res.length;
          }
          
        }
      )
    },10000)
  }
  
  fnNotificaciones(){
    this.router.navigate(['/main/tabs/tab3/notificaciones']);
  }

}
