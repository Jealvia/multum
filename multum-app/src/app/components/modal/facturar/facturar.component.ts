import { Component, OnInit, Input } from '@angular/core';
import { facturaDetalles } from 'src/app/services/estructura.modelo';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-facturar',
  templateUrl: './facturar.component.html',
  styleUrls: ['./facturar.component.scss'],
})
export class FacturarComponent implements OnInit {

  @Input() facCuerpo: Array<facturaDetalles> ; 

  constructor(private modalController: ModalController) { }

  ngOnInit() {}

  salir(){
    this.modalController.dismiss();
  }
}
