import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, Validators } from '@angular/forms';
import { facturaDetalles, IdNombre } from 'src/app/services/estructura.modelo';
import { EnfundeService } from 'src/app/services/enfunde.service';
import { CommonService } from 'src/app/services/common.service';
import { ModalController } from '@ionic/angular';
//enfunde
import { enfunde } from 'src/app/services/estructura.modelo';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-enfunde',
  templateUrl: './new-enfunde.component.html',
  styleUrls: ['./new-enfunde.component.scss'],
})
export class NewEnfundeComponent implements OnInit {

  @Input() id: number;

  //enfunde
  enfundeForm: FormGroup;
  modelo: enfunde;
  listFincas: [{id: number, finca: string}];
  listTipoEnfunde:IdNombre
  datosEnfunde: any//{ anio: number, semana: number, cinta_id: number, cita_color: string};
  /* var Date */
  maxDate: string; // format yyyy-mm-dd 2020-12-25
  minDate: string;
  validDay: Date; 
  //

  constructor(
    private modalController: ModalController,
    private formBuilder: FormBuilder,
    private router:Router,
    private commonService: CommonService,
    private enfundeService: EnfundeService,
    //private commonService: CommonService,
  ) {
    this.enfundeForm = this.formBuilder.group({
      finca: ['', Validators.required],
      fecha: ['', Validators.required],
      ano: ['', Validators.required],
      semana: ['', Validators.required],
      cinta: ['', Validators.required],
      tipo: ['', Validators.required],
      cantida: ['', Validators.required],
//2020-01-20T14:54:51.099-05:00
    });
   }

  ngOnInit() {
    this.listaTipoEnfunde()
    this.listaFincas()
    this.dateValid()
    this.resetForm();
  }
  listaFincas(){
    /* this.enfundeService.listaFincas().subscribe(
      res=>{
        this.listFincas = res;
        console.log(res)
      }
    ); */
  }

  listaTipoEnfunde(){
    /* this.enfundeService.listaTipoEnfunde().subscribe(
      res=>{
        this.listTipoEnfunde = res;
        console.log(res)
      }
    ); */
  }

  salir(){
      
    this.modalController.dismiss(this.id);
  } 
  onSubmit(){
    let data = {
      fecha_enfunde : this.enfundeForm.value.fecha,
      finca_id :this.enfundeForm.value.finca,
      enfunde :this.enfundeForm.value.cantida,
      tipo_enfunde_id: this.enfundeForm.value.tipo,
      cinta_id:this.datosEnfunde.id
    }
    console.log(data)
    /* this.enfundeService.enfundeEnviar(data).subscribe(
      res =>{
        console.log(res);
        if (res.status == 'success') {
          this.resetForm();
          this.commonService.msjConfirm('Registro Exitoso','Se envío correctamente el enfunde');
          this.modalController.dismiss(this.id);
        } else {
          console.log("Some error occured");
        }
      }
    ) */
  }
  dateValid(){
    let hoy: Date = new Date();
    let tem_minDate: Date = new Date();

    this.validDay = this.sumarDias(tem_minDate,-30);

    this.maxDate = this.datetoEstring(hoy);
    this.minDate = this.datetoEstring(this.validDay);

    console.log('max: ', this.maxDate);
    console.log('min: ', this.minDate);

  }
  sumarDias(fecha: Date, dias: number){
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }
  datetoEstring(fecha:Date){
    let newDate: string;
    let anio:number = fecha.getFullYear();
    let mes:number = fecha.getMonth();
    mes = mes +1;
    let dias:number = fecha.getDate();
    //Para Seguir el formato agrego el '0' a mes
    if (mes < 10 || dias <10) {
      if (mes < 10 && dias <10) {
        newDate = anio +'-0'+mes+'-0'+dias;
      } else {
        if (mes < 10) {
          newDate = anio +'-0'+mes+'-'+dias;
        }
        if (dias <10) {
          newDate = anio +'-'+mes+'-0'+dias;
        }
      }
    } else {
      newDate = anio +'-'+mes+'-'+dias;
    }
    return newDate;
  }

  buscarEnfunde(){
    /* console.log('hola buscar', this.enfundeForm.value.fecha);
    this.enfundeService.buscarEnfunde(this.enfundeForm.value.fecha).subscribe(
      res=>{
        this.datosEnfunde = res;
        this.enfundeForm.setValue(
          {
            'finca': this.enfundeForm.value.finca==null?"":this.enfundeForm.value.finca,
            'fecha': this.enfundeForm.value.fecha,
            'ano':    res.anio,
            'semana': res.semana,
            'cinta':  res.color,
            'tipo':   this.enfundeForm.value.tipo==null?"":this.enfundeForm.value.tipo,
            'cantida':this.enfundeForm.value.cantida==null?"":this.enfundeForm.value.cantida,
          }); 
        console.log(this.datosEnfunde)
      }
    ) */
  }
  resetForm(){
    console.log(' form:', this.enfundeForm);
    this.enfundeForm.controls['fecha'].setValue('');
    this.enfundeForm.reset();
    console.log('222 form:', this.enfundeForm);
  }

}
