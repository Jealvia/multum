import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LaboresPage } from './labores.page';

describe('LaboresPage', () => {
  let component: LaboresPage;
  let fixture: ComponentFixture<LaboresPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaboresPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LaboresPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
