import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LaboresPage } from './labores.page';

const routes: Routes = [
  {
    path: '',
    component: LaboresPage
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LaboresPageRoutingModule {}
