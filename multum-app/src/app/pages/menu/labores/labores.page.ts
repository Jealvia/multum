import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-labores',
  templateUrl: './labores.page.html',
  styleUrls: ['./labores.page.scss'],
})
export class LaboresPage implements OnInit {

  nombre=''
  
  constructor(
    private router:Router,
    private cookieService: CookieService,
    private storage: Storage
  ) { }

  ngOnInit() {
    this.storage.get('nombres').then(res=>{
      this.nombre=res;
    })
  }

  seleccion(ruta:string){
    this.router.navigate([ruta]);
  }
  
}
