import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LaboresPageRoutingModule } from './labores-routing.module';

import { LaboresPage } from './labores.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { ModulesModule } from 'src/app/modules/modules.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LaboresPageRoutingModule,
    ComponentsModule, ModulesModule
  ],
  declarations: [LaboresPage]
})
export class LaboresPageModule {}
