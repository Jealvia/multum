import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  nombre=''

  constructor(
    private router:Router,
    private cookieService: CookieService,
    private storage: Storage
  ) { }

  ngOnInit() {
    this.storage.get('nombres').then(res=>{
      this.nombre=res;
    })
  }

  seleccion(ruta:string){
    this.router.navigate([ruta]);
  }

}
