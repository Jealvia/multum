import { Component, OnInit } from '@angular/core';
import { FiltroEnfundePage } from '../../modales/filtro-enfunde/filtro-enfunde.page';
import { ModalController } from '@ionic/angular';
import { TabmainPage } from 'src/app/tab/tabmain/tabmain.page';
import { ProduccionService } from '@services/produccion.service';
import { Storage } from '@ionic/storage';
import { Datum } from '../racimo/structs/estructuras.service';
import { parametros } from '@services/estructura.modelo';
@Component({
  selector: 'app-cajas',
  templateUrl: './cajas.page.html',
  styleUrls: ['./cajas.page.scss'],
})
export class CajasPage implements OnInit {
  b_lista = true; b_detalles = false;
  b_carga = false;
  listado: Array < Datum >;
  crear=false;
  params: parametros = {
    finca: 1,
    anio: 2020,
    semDesde: 1,
    semHasta: 52,
    id: 0,
    todos: false,
    showSelectGraf: false,
    grafica: 'none'
  }
  itemRacimo: Datum; // para pasar el item al hijo

  constructor(
    private modalController: ModalController,
    private tabPage: TabmainPage,
    private storage: Storage,
    private produccionService: ProduccionService,
  ) { }

  ngOnInit() {
    this.b_carga = true;
    this.tabPage.titulo = "Control Cajas";
    this.getlista()
  }
  getlista() {
    this.storage.get('token').then(res=>{
      this.produccionService.listaProduccion(this.params.anio,this.params.semDesde, this.params.semHasta, this.params.finca, this.params.todos, res)
      .subscribe(
        res => {
          console.log('respuesta listado', res)
          this.listado = res.data;
          this.b_carga = false;

        }
      );
    })
    
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: FiltroEnfundePage,
      cssClass: 'custom-modal-finca',
      componentProps: {
        'nombre': 'calibraje',
        'parametros': this.params,
      }
    });
    await modal.present();
    const {
      data
    } = await modal.onDidDismiss();

    if (data) {
      if (!data.dismissed) {
        console.log(data)
        this.params = {
          anio: data.anio,
          finca: data.finca == "" ? 0 : data.finca,
          id: 0,
          semDesde: data.sem_desde,
          semHasta: data.sem_hasta,
          todos: data.todos,
          showSelectGraf: false,
          grafica: 'none'
        }
        this.b_carga = true;
        this.getlista()
      }
    }
  }

  addCaja(){
    this.crear=true
    let racimo:Datum
    this.itemRacimo = racimo;
    this.b_lista = false;
    this.b_detalles = true;
  }

  ir( racimo: Datum){
    //this.router.navigate([ruta]);
    this.crear=false
    this.itemRacimo = racimo;
    this.b_lista = false;
    this.b_detalles = true;
  }
  closeDetalles(){
    //this.getlista()
    this.b_lista = true;
    this.b_detalles = false;
  }


}

