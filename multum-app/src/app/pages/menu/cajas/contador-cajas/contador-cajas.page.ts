import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TotalesPage } from 'src/app/pages/modales/totales/totales.page';
import { ModalController } from '@ionic/angular';
import { Datum, Racimos, DataRacimo, Cajas, TipoCajas } from '../../racimo/structs/estructuras.service';
import { ProduccionService } from '@services/produccion.service';
import { Storage } from '@ionic/storage';
import { OrdenCorte } from '../../ordendecorte/structs/estructuras.service';
@Component({
  selector: 'app-contador-cajas',
  templateUrl: './contador-cajas.page.html',
  styleUrls: ['./contador-cajas.page.scss'],
})
export class ContadorCajasPage implements OnInit {
  showResta: boolean = false; showRechazo = false;
  @Input() produccion: Datum;
  @Output() close: EventEmitter<any> = new EventEmitter();
  cajas: Cajas;
  racimoColores: {tipo: string, cantidad: number}[] = [{ tipo: '', cantidad: 0}];
  tipoCajas:Array<TipoCajas>;
  @Input() crear=true;
  orden_corte:number=0;
  carga=false;
  seleccion=true;
  listado:Array<OrdenCorte>


  constructor(
    private modalController: ModalController,
    private produccionService: ProduccionService,
    private storage: Storage,
  ) { }

  ngOnInit() {
    if(this.crear){
      this.cargarDatos()
      this.cajas={
        total_cajas:0,
        porcentaje_segunda:0,
        listado_cajas:{
          "41.5":0,
          "37.48":0,
          "43.0":0,
          "31":0,
          "40.5":0,
          "42":0
        },
        caja_segunda:{
          "50":0
        }
      }
      /* this.storage.get('token').then(res=>{
        this.produccionService.listaTipoCaja(res)
        .subscribe(
          res => {
            this.tipoCajas=res;
            console.log(this.tipoCajas)
          }
        );
      }) */
    }else{
      this.cajas = this.produccion.cajas;
      console.log('cargando Detalles');
      console.log('racimo val: ', this.cajas)
    }
    
    //Para eliminar el ultimo item color repetido, sumando su valor al primer elemente
    /*
    if (this.cajas.listado_cajas == 7) {
      this.cajas.data_racimos[0].cantidad = this.racimo.data_racimos[0].cantidad + this.racimo.data_racimos[6].cantidad;
      this.cajas.data_racimos.pop();
    }
    */
    
    
  }

  seleccionOrdenCorte(id:number){
    this.orden_corte=id;
    console.log(id)
    this.seleccion=false
  }

  cargarDatos(){
    this.storage.get('token').then(res=>{
      this.produccionService.listaOrdenCorte(res).
        subscribe(
          res=>{
            console.log('respuesta listado', res)
            this.listado = res;
            console.log('respuesta listado', res)
            this.carga=false;
            
          }
        )
    })
  }

  enviarResultados(){
    console.log(this.cajas)
    let resultado={
      'listado_cajas':this.cajas.listado_cajas,
      'orden_corte_id':this.orden_corte,
      'segunda':this.cajas.caja_segunda
    }
    this.storage.get('token').then(res=>{
      this.produccionService.guardarCajaControl(res,resultado)
      .subscribe(
        res => {
          console.log(res)
        }
      );
    })
  }

  back(){
    this.seleccion=true
    this.orden_corte=0
  }

  onClick(){
    console.log('texto');
  }

  itemOperador(restar: boolean, itemcaja: string,  b_rechazo?: boolean, ){
    //let cajaValue = this.cajas.listado_cajas[itemcaja];
    if (b_rechazo) {
      if (this.cajas.caja_segunda[50] < 1 && restar) {
        return
      }
      restar? this.cajas.caja_segunda[50] =  this.cajas.caja_segunda[50]-1: this.cajas.caja_segunda[50] = this.cajas.caja_segunda[50]+1
      return;
    }
    if (restar) {
      if (this.cajas.listado_cajas[itemcaja] < 1) {
        return
      }
      this.cajas.listado_cajas[itemcaja] = this.cajas.listado_cajas[itemcaja]-1;
      this.cajas.total_cajas = this.cajas.total_cajas-1
    } else {
      this.cajas.listado_cajas[itemcaja] = this.cajas.listado_cajas[itemcaja]+1;
      this.cajas.total_cajas = this.cajas.total_cajas+1
    }
    
  }

  sendData(){
    //this.commonService.msjConfirm('Por Favor Confirme',"Acepta estos valores de racimos cosechados y rechazados como válidos para la semana.")
    
    //this.presentModal() 
  }

  regresar(){
    this.close.emit(null);
  }
 
  async presentModal() {
    const modal = await this.modalController.create({
      component: TotalesPage,
      cssClass: 'custom-modal-finca',
      componentProps: {
        'tipo': 'Control Cajas',
        'data': this.produccion,
        'confirm': true,
      }
    });
    await modal.present();
    const {
      data
    } = await modal.onDidDismiss();

    if (data) {
      if (!data.dismissed) {
        console.log('no guardo')
        }
        console.log('guardo')
    }
    
  } 


}