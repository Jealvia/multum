import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContadorCajasPage } from './contador-cajas.page';

const routes: Routes = [
  {
    path: '',
    component: ContadorCajasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContadorCajasPageRoutingModule {}
