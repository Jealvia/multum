import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ContadorCajasPageRoutingModule } from './contador-cajas-routing.module';

import { ContadorCajasPage } from './contador-cajas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContadorCajasPageRoutingModule
  ],
  declarations: [ContadorCajasPage],
  exports: [ContadorCajasPage ]
})
export class ContadorCajasPageModule {}
