import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ContadorCajasPage } from './contador-cajas.page';

describe('ContadorCajasPage', () => {
  let component: ContadorCajasPage;
  let fixture: ComponentFixture<ContadorCajasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContadorCajasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ContadorCajasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
