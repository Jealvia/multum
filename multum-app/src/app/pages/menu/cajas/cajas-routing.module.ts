import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CajasPage } from './cajas.page';

const routes: Routes = [
  {
    path: '',
    component: CajasPage
  },
  {
    path: 'contador-cajas',
    loadChildren: () => import('./contador-cajas/contador-cajas.module').then( m => m.ContadorCajasPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CajasPageRoutingModule {}
