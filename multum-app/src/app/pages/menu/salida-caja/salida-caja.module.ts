import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SalidaCajaPageRoutingModule } from './salida-caja-routing.module';

import { SalidaCajaPage } from './salida-caja.page';
import { FiltroEnfundePage } from '../../modales/filtro-enfunde/filtro-enfunde.page';
import { FiltroEnfundePageModule } from '../../modales/filtro-enfunde/filtro-enfunde.module';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  entryComponents:[
    FiltroEnfundePage,
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SalidaCajaPageRoutingModule,
    FiltroEnfundePageModule,
    ComponentsModule,
    ReactiveFormsModule,
  ],
  declarations: [SalidaCajaPage]
})
export class SalidaCajaPageModule {}
