import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SalidaCajaPage } from './salida-caja.page';

describe('SalidaCajaPage', () => {
  let component: SalidaCajaPage;
  let fixture: ComponentFixture<SalidaCajaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalidaCajaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SalidaCajaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
