import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IdNombre } from '@services/estructura.modelo';
import { Router } from '@angular/router';
import { EstimadoService } from '@services/estimado.service';
import { EnfundeService } from '@services/enfunde.service';
import { CommonService } from '@services/common.service';
import { ModalController } from '@ionic/angular';
import { FiltroEnfundePage } from '../../modales/filtro-enfunde/filtro-enfunde.page';
import { TabmainPage } from 'src/app/tab/tabmain/tabmain.page';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-salida-caja',
  templateUrl: './salida-caja.page.html',
  styleUrls: ['./salida-caja.page.scss'],
})
export class SalidaCajaPage implements OnInit {
  
  calibrajeForm : FormGroup;
  lista=true;
  carga=false;
  listado:[{finca_id:{nombre:string},semana:number, }]
  titulo="Registro Salida";
  listFincas:Array<IdNombre>
  datosEnfunde:any

  constructor(
    private formBuilder: FormBuilder,
    private router:Router,
    private estimadoService: EstimadoService,
    private enfundeService: EnfundeService,
    private commonService: CommonService,
    private modalController: ModalController,
    private tabPage:TabmainPage,
    private storage: Storage,
  ) {
    this.calibrajeForm = this.formBuilder.group({
      finca: ['', Validators.required],
      orden: ['', Validators.required],
      fecha: ['', Validators.required],
      ano: ['', Validators.required],
      semana: ['', Validators.required],
      cinta: ['', Validators.required],
      tipo: ['', Validators.required],
      cantida: ['', Validators.required],
    }); 
   }

  ngOnInit() {
    this.tabPage.titulo="Salida Cajas"
  }

  onSubmit(){
    
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: FiltroEnfundePage,
      cssClass:'custom-modal-finca',
      componentProps:{
        'nombre':'salida de caja'
      }
    });
    await modal.present();
    const {data}=await modal.onDidDismiss();
    
    if(data){
      if(!data.dismissed){
        console.log(data)
        this.carga=true;
          let datos={
            'tipo':data.tipo,
            'fecha_desde':data.fechaDesde,
            'fecha_hasta':data.fechaHasta
          }
          /* this.fincaService.listFincasFiltro(datos).subscribe(
            res=>{
              this.fincas = res;
              console.log(res)
              this.carga=false;
            }
          ); */
      }
    }else{
      console.log("salida click")
    }
  }
  
  editar(id:number){
    this.titulo="Editar Salida"
    console.log(id)
    /* const estimadotmp=this.listado.find(z=>z.id==id)
    console.log(estimadotmp) */
  }

  delete(id:number){
    console.log(id)
  }

  addEstimado(){
    this.lista=false
  }

  listaFincas(){
    this.storage.get('token').then(res=>{
      this.enfundeService.listaFincas(res).subscribe(
        res=>{
          this.listFincas = res;
          console.log(res)
        }
      );
    })
  }

  buscarEnfunde(){
    if(!this.lista){
      this.storage.get('token').then(res=>{
        this.enfundeService.buscarEnfunde(this.calibrajeForm.value.fecha,res).subscribe(
          res=>{
            this.datosEnfunde = res;
            this.calibrajeForm.setValue(
              {
                'finca': this.calibrajeForm.value.finca==null?"":this.calibrajeForm.value.finca,
                'fecha': this.calibrajeForm.value.fecha,
                'ano':    res.anio,
                'semana': res.semana,
                'cinta':  res.color,
                'orden':this.calibrajeForm.value.orden==null?"":this.calibrajeForm.value.orden,
                //'tipo':   this.tipo,
                'cantida':this.calibrajeForm.value.cantida==null?"":this.calibrajeForm.value.cantida,
              }); 
            console.log(this.datosEnfunde)
          }
        )
      })
      
    }
    
  }

  back(){
    this.titulo="Registro Salida"
    this.lista=true;
    this.calibrajeForm.reset();
  }


}
