import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  AlertController,
  PopoverController,
  IonList,
  ModalController
} from '@ionic/angular';
import {
  Router
} from '@angular/router';
import {
  PopNewProdComponent
} from '../../../components/popover/pop-new-prod/pop-new-prod.component';
import {
  facturaDetalles
} from "../../../services/estructura.modelo";
import {
  FacturarComponent
} from 'src/app/components/modal/facturar/facturar.component';
import {
  CommonService
} from 'src/app/services/common.service';
import { TabmainPage } from 'src/app/tab/tabmain/tabmain.page';
@Component({
  selector: 'app-facturacion',
  templateUrl: './facturacion.page.html',
  styleUrls: ['./facturacion.page.scss'],
})
export class FacturacionPage implements OnInit {

  @ViewChild('listaFactura', {
    static: false
  }) lista: IonList;

  factura: Array < facturaDetalles > ;
  facturaReset: Array < facturaDetalles > ;
  facturaItem: facturaDetalles;

  subTotal: number = 0;

  constructor(
    private alertControl: AlertController,
    private router: Router,
    private popoverCtrl: PopoverController,
    private modalController: ModalController,
    private commonService: CommonService,
    private tabPage:TabmainPage
  ) {

  }

  ngOnInit() {
    this.tabPage.titulo="Facturación"
    this.validarUsuario()
  }

  validarUsuario() {
    /* this.commonService.validarUsuario().subscribe(
      res => {
        console.log(res);
        if (res.status == 'success') {
          console.log("Guardado exitosamente");
          //this.commonService.msjConfirm('Registro Exitoso', 'Muchas Gracias por su Registro');
        } else {
          console.log("Some error occured");
        }
      },
      error => {
        this.router.navigate(['/'])
        console.log(error)
      }
    ) */
  }

  nuevaFactura() {
    this.confirmarAlert();
  }
  enviarFactura() {
    this.modalFacturar(this.factura);
  }

  async modalFacturar(detalleFactura: Array < facturaDetalles > ) {
    const modal = await this.modalController.create({
      component: FacturarComponent,
      backdropDismiss: true,
      componentProps: {
        facCuerpo: detalleFactura
      },
      animated: true,
    });
    return await modal.present();
  }

  async confirmarAlert() {
    const alert = await this.alertControl.create({
      header: '¿Factura Actual?',
      //subHeader: 'Subtitle',
      message: '¿Desea descartar la factura actual en proceso?.',
      buttons: [{
        text: 'Descartar',
        cssClass: 'secondary',
        handler: (blah) => {
          console.log('Descartar factura: ', this.factura);
          this.factura = this.facturaReset;
          console.log('Descartar factura: ', this.factura);
        }
      }, {
        text: 'Facturar!!',
        handler: () => {
          console.log('Confirm Okay');
          this.router.navigate(["/home"]);
        }
      }]
    });
    await alert.present();
  }

  async popNew() {
    const popover = await this.popoverCtrl.create({
      component: PopNewProdComponent,
      //event: evento,
      mode: 'ios',
      backdropDismiss: true,
      componentProps: {
        id: this.fn_valId()
      }
    });

    await popover.present();
    const {
      data
    } = await popover.onDidDismiss();

    console.log('Salio del pop');
    if (data != null) {
      console.log("padre: ", data);
      this.newProd(data);
      console.warn("padre: ", this.factura);
      this.valorTotal();
    } else {
      console.log('no guardo');
    }

  }

  editDel_Prod(modelo: facturaDetalles, caso: string) {
    console.warn('array', this.factura);
    let indice = this.factura.indexOf(modelo);
    if (caso == 'edit') {
      this.factura[indice] = modelo;
    } else {
      this.factura.splice(indice, 1);

      console.log('indice: ', indice);
    }
  }

  fn_valId() {
    if (this.factura != null) {
      return this.factura.length;
    } else {
      return 0;
    }
  }
  newProd(modelo: facturaDetalles) {
    if (this.factura != null) {
      this.factura.push(modelo);
    } else {
      this.factura = [{
        'id': 1,
        'linea': modelo.linea,
        'cantidad': modelo.cantidad,
        'precio': modelo.precio,
        'valor': modelo.valor
      }];
    }
  }
  async modProd(item: facturaDetalles) {
    const popover = await this.popoverCtrl.create({
      component: PopNewProdComponent,
      //event: evento,
      mode: 'ios',
      backdropDismiss: true,
      componentProps: {
        b_mod: true,
        prod: item
      } // prod: {linea: item.linea, precio: item.precio, cantidad: item.cantidad, valor: item.valor}

    });
    await popover.present();
    const {
      data
    } = await popover.onDidDismiss();
    console.log("Salio del Pop");
    if (data != null) {
      console.log("padre: ", data);
      this.editDel_Prod(data, 'edit');
      this.valorTotal();
    } else {
      console.log("no guardo");
    }
    this.lista.closeSlidingItems();

  }

  valorTotal() {
    this.subTotal = 0;
    for (let index = 0; index < this.factura.length; index++) {
      const monto = this.factura[index].valor;
      this.subTotal += monto;
    }
  }

  onClick() {
    console.warn('En Construccion');
  }

}