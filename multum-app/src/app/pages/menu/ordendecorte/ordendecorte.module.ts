import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrdendecortePageRoutingModule } from './ordendecorte-routing.module';

import { OrdendecortePage } from './ordendecorte.page';
import { FiltroEnfundePage } from '../../modales/filtro-enfunde/filtro-enfunde.page';
import { FiltroEnfundePageModule } from '../../modales/filtro-enfunde/filtro-enfunde.module';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  entryComponents:[
    FiltroEnfundePage,
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrdendecortePageRoutingModule,
    FiltroEnfundePageModule,
    ComponentsModule, 
    ReactiveFormsModule,
  ],
  declarations: [OrdendecortePage]
})
export class OrdendecortePageModule {}
