import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrdendecortePage } from './ordendecorte.page';

const routes: Routes = [
  {
    path: '',
    component: OrdendecortePage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
})
export class OrdendecortePageRoutingModule {}
