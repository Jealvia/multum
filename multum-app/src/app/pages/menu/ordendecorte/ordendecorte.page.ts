import { Component, OnInit } from '@angular/core';
import { IdNombre, parametros } from '@services/estructura.modelo';
import { ModalController } from '@ionic/angular';
import { FiltroEnfundePage } from '../../modales/filtro-enfunde/filtro-enfunde.page';
import { OrdenCorte } from './structs/estructuras.service';
import { OrdenCorteService } from "@services/ordencorte.service";
import { TabmainPage } from 'src/app/tab/tabmain/tabmain.page';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-ordendecorte',
  templateUrl: './ordendecorte.page.html',
  styleUrls: ['./ordendecorte.page.scss'],
})
export class OrdendecortePage implements OnInit {

  lista=true;
  carga=false;

  listado:Array<OrdenCorte>
  titulo="Registro Orden de Corte";
  listFincas:Array<IdNombre>
  datosEnfunde:any;
  params: parametros  = {
    finca: 1,
    anio: 2020,
    semDesde: 1,
    semHasta: 52,
    id: 0,
    todos: null,
    grafica: 'none',
    showSelectGraf: false
  }

  constructor(
    private modalController: ModalController,
    private ordenCorteService:OrdenCorteService,
    private tabPage:TabmainPage,
    private storage: Storage,
  ) {
   }

  ngOnInit() {
    this.tabPage.titulo="Orden de Corte"
    this.carga=true;
    this.cargarDatos();
  }

  cargarDatos(){
    this.storage.get('token').then(res=>{
      this.ordenCorteService.listaOrdenCorte(this.params.anio,this.params.semDesde,
        this.params.semHasta,this.params.finca,this.params.todos,res).
        subscribe(
          res=>{
            console.log('respuesta listado', res)
            this.listado = res;
            console.log('respuesta listado', res)
            this.carga=false;
            
          }
        )
    })
    
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: FiltroEnfundePage,
      cssClass:'custom-modal-finca',
      componentProps:{
        'nombre':'orden corte',
        'parametros': this.params,
      },
    });
    await modal.present();
    const {data}=await modal.onDidDismiss();
    
    if(data){
      if(!data.dismissed){
        console.log(data)
        this.carga=true;
        this.params={
          id:0,
          anio:data.anio,
          finca:data.finca==""?0:data.finca,
          semDesde:data.sem_desde,
          semHasta:data.sem_hasta,
          todos:data.todos,
          grafica: 'none',
          showSelectGraf: false
        }
        this.cargarDatos();
      }
    }else{
      console.log("salida click")
    }
  }


}
