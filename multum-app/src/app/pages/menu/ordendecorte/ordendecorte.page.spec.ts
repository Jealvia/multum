import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OrdendecortePage } from './ordendecorte.page';

describe('OrdendecortePage', () => {
  let component: OrdendecortePage;
  let fixture: ComponentFixture<OrdendecortePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdendecortePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OrdendecortePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
