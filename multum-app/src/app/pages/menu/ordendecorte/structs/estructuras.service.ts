export interface EmbarqueMarcaCaja{
    marca_caja_id:string
}

export interface OrdenCorte{
    id :number,
    fecha_corte:Date ,
    semana: number,
    finca_id: string,
    embarque_id: EmbarqueMarcaCaja,
    cantidad:number ,
    calificador_id:string 
}