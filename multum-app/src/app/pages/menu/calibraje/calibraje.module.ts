import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CalibrajePageRoutingModule } from './calibraje-routing.module';

import { CalibrajePage } from './calibraje.page';
import { FiltroEnfundePage } from '../../modales/filtro-enfunde/filtro-enfunde.page';
import { FiltroEnfundePageModule } from '../../modales/filtro-enfunde/filtro-enfunde.module';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  entryComponents:[
    FiltroEnfundePage,
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CalibrajePageRoutingModule,
    FiltroEnfundePageModule,
    ComponentsModule, 
    ReactiveFormsModule,
  ],
  declarations: [CalibrajePage]
})
export class CalibrajePageModule {}
