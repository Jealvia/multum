import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CalibrajePage } from './calibraje.page';

const routes: Routes = [
  {
    path: '',
    component: CalibrajePage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
})
export class CalibrajePageRoutingModule {}
