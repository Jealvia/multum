export interface cintabyFecha{
    id: number;
    anio: number;
    semana: number;
    color: string;
    colorhex: string;
  }
export interface Calibraje{
    id:number ,
    tipo_calibraje_id:{id:number,nombre:string} ,
    cinta_id:cintabyFecha ,
    semana:number ,
    anio:number,
    cantidad :number,
    finca_id: {id:number,nombre:string},
    fecha_calibraje:Date
}