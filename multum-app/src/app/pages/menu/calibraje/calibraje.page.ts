import {
  Component,
  OnInit
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import {
  Router
} from '@angular/router';
import {
  EstimadoService
} from '@services/estimado.service';
import {
  EnfundeService
} from '@services/enfunde.service';
import {
  CommonService
} from '@services/common.service';
import {
  ModalController
} from '@ionic/angular';
import {
  FiltroEnfundePage
} from '../../modales/filtro-enfunde/filtro-enfunde.page';
import {
  IdNombre, parametros, requeridos
} from '@services/estructura.modelo';
import {
  Calibraje
} from "./structs/estructura-calibraje.service";
import {
  TabmainPage
} from 'src/app/tab/tabmain/tabmain.page';
import { Storage } from '@ionic/storage';
import { EstBasicaPage } from '../../modales/estadisticas/basica/Est-basica.page';
import { EstadisticasService } from '@services/estadisticas.service';
@Component({
  selector: 'app-calibraje',
  templateUrl: './calibraje.page.html',
  styleUrls: ['./calibraje.page.scss'],
})
export class CalibrajePage implements OnInit {

  calibrajeForm: FormGroup;
  lista = true;
  carga = false;
  listado: Array < Calibraje >
  titulo = "Registro Calibración";
  listFincas: Array < IdNombre >
  listCalibrajes: Array < IdNombre >
  datosEnfunde: any
  //estaditicasInventarios:any
  params: parametros = {
    finca: 1,
    anio: 2020,
    semDesde: 1,
    semHasta: 52,
    id: 0,
    todos: null,
    showSelectGraf: false,
    grafica: 'none'
  }

  constructor(
    private formBuilder: FormBuilder,
    private enfundeService: EnfundeService,
    private commonService: CommonService,
    private modalController: ModalController,
    private tabPage: TabmainPage,
    private storage: Storage,
    private estadisticaService: EstadisticasService,
  ) {
    this.calibrajeForm = this.formBuilder.group({
      finca: ['', Validators.required],
      fecha: ['', Validators.required],
      ano: ['', Validators.required],
      semana: ['', Validators.required],
      cinta: ['', Validators.required],
      tipo: ['', Validators.required],
      cantida: ['',[Validators.required, Validators.min(1)]],
    });
  }

  ngOnInit() {
    this.carga = true;
    this.tabPage.titulo = "Calibración"
    this.listaFincas()
    this.listaCalibrajes()
    this.listaTipoCalibrajes()
    //this.dataTemporal();
  }

  onSubmit() {
    console.log(this.datosEnfunde)
    let fecha = new Date(this.calibrajeForm.value.fecha)
    let data = {
      fecha_calibraje: fecha,
      finca_id: this.calibrajeForm.value.finca,
      tipo_calibraje_id: this.calibrajeForm.value.tipo,
      cinta_id: this.datosEnfunde.id,
      id: this.params.id,
      anio: fecha.getFullYear(),
      semana: this.calibrajeForm.value.semana,
      cantidad: this.calibrajeForm.value.cantida,
    }
    //console.log(data)
    this.storage.get('token').then(res=>{
      this.enfundeService.calibrajeEnviar(data,res).subscribe(
        res => {
          //console.log(res);
          if (res.status == 'success') {
            this.params = {
              finca: 1,
              anio: 2020,
              semDesde: 1,
              semHasta: 52,
              id: 0,
              todos: true,
              showSelectGraf: false,
              grafica: 'none'
            }
            this.listaCalibrajes()
            this.commonService.msjConfirm('Registro Exitoso', 'Se envío correctamente la Calibración');
            this.back();
          } else {
            console.log("Some error occured");
          }
        }
      )
    })
    
  }

  async presentModal(esEstadisticas?: boolean) {
    const modal = await this.modalController.create({
      component: FiltroEnfundePage,
      cssClass: 'custom-modal-finca',
      componentProps: {
        'nombre': 'calibraje',
        'parametros': this.params,
        //'b_estadisticas': esEstadisticas,
      }
    });
    
    await modal.present();
    const {
      data
    } = await modal.onDidDismiss();

    if (data) {
      if (!data.dismissed) {
          this.carga = true;
          this.params = {
            anio: data.anio,
            finca: data.finca == null ? 0 : data.finca,
            id: 0,
            semDesde: data.sem_desde,
            semHasta: data.sem_hasta,
            todos: data.todos,
            showSelectGraf: false,
            grafica: 'none'
          }
          this.listaCalibrajes()
      }
    }
  }

  editar(id: number) {
    this.params.id = id;
    console.log(this.listado.find(z => z.id == id))
    this.titulo = "Editar Calibración"
    const estimadotmp = this.listado.find(z => z.id == id)
    this.datosEnfunde = {
      id: estimadotmp.cinta_id.id
    }
    this.calibrajeForm.setValue({
      finca: estimadotmp.finca_id.id,
      fecha: estimadotmp.fecha_calibraje,
      ano: estimadotmp.anio,
      semana: estimadotmp.semana,
      cinta: estimadotmp.cinta_id.color,
      tipo: estimadotmp.tipo_calibraje_id.id,
      cantida: estimadotmp.cantidad,
    });
    this.lista = false;
  }

  delete(id: number) {
    console.log(id)
  }

  addEstimado() {
    this.params.id = 0;
    this.lista = false;
  }

  listaFincas() {
    this.storage.get('token').then(res=>{
      this.enfundeService.listaFincas(res).subscribe(
        res => {
          this.listFincas = res;
        }
      );
    })
    
  }

  listaCalibrajes() {
    this.storage.get('token').then(res=>{
      this.enfundeService.listaCalibraje(this.params.anio, this.params.semDesde, 
        this.params.semHasta, this.params.finca, this.params.todos,res)
      .subscribe(
        res => {
          console.log('respuesta listado', res)
          this.listado = res.data;
          this.carga = false;
        }
      );
    })
    
  }

  listaTipoCalibrajes() {
    this.storage.get('token').then(res=>{
      this.enfundeService.listaTipoCalibraje(res).subscribe(
        res => {
          this.listCalibrajes = res;
        }
      );
    })
    
  }

  buscarEnfunde() {
    if (!this.lista) {
      this.storage.get('token').then(res=>{
        this.enfundeService.buscarEnfunde(this.calibrajeForm.value.fecha,res).subscribe(
          res => {
            this.datosEnfunde = res;
            this.calibrajeForm.setValue({
              'finca': this.calibrajeForm.value.finca == null ? "" : this.calibrajeForm.value.finca,
              'fecha': this.calibrajeForm.value.fecha,
              'ano': res.anio,
              'semana': res.semana,
              'cinta': res.color,
              'tipo': this.calibrajeForm.value.tipo == null ? "" : this.calibrajeForm.value.tipo,
              'cantida': this.calibrajeForm.value.cantida == null ? "" : this.calibrajeForm.value.cantida,
            });
          }
        )
      })
      
    }

  }

  back() {
    this.titulo = "Registro Calibración"
    this.lista = true;
    this.calibrajeForm.reset();
  }

 
  get_requisitos(){ 
    let requisitos: requeridos = {
      finca: true
    };
     this.storage.get('token').then(res=>{ 
      this.commonService.requerimientos(requisitos, res).subscribe(
        res=>{
          console.log('esta es la respuesta: ',res);
          
          requisitos = res.data;
          if (requisitos.finca) {
            this.addEstimado();
          } else {
            this.commonService.msjConfirm('Fincas no Registradas', 'Por favor registre primero una finca');
          }
        },
        error=>{
          console.log('erroorr: ',error)
          this.commonService.msjErrorServ();
        }
      )
      
    })
  }
  


}

/*
dataTemporal(){
    this.storage.get('token').then(res=>{
      this.estadisticaService.estadisticaInventarioFinal(res, this.params.anio, this.params.semDesde, this.params.semHasta).subscribe(
        res => {
          this.estaditicasInventarios = res;
        }
      );
    })
  }
 async mostrarEstadisticas(){
    console.log('listado de estadisticas: ', this.estaditicasInventarios)
    const modal = await this.modalController.create({
      component: EstBasicaPage,
      cssClass: 'custom-modal-finca',
      componentProps: {
        'listado': this.listado,
        'listado2': this.estaditicasInventarios,
      }
    });
    await modal.present();
    const {
      data
    } = await modal.onDidDismiss();

    if (data) {
      if (!data.dismissed) {
        
      }
    } else {

    }
  }

*/