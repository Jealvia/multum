import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { FincaPageRoutingModule } from './finca-routing.module';
import { FincaPage } from './finca.page';

import { ComponentsModule } from 'src/app/components/components.module';
import { ModulesModule } from 'src/app/modules/modules.module';
import { FiltroFincaPage } from 'src/app/pages/modales/filtro-finca/filtro-finca.page';
import { FiltroFincaPageModule } from 'src/app/pages/modales/filtro-finca/filtro-finca.module';

@NgModule({
  entryComponents:[
    FiltroFincaPage,
    
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FincaPageRoutingModule,
    FiltroFincaPageModule,
    ReactiveFormsModule,
    ComponentsModule
  ],
  declarations: [FincaPage]
})
export class FincaPageModule {}
