import {
  Component,
  OnInit
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  finca,
  fincalist
} from 'src/app/services/estructura.modelo';
import {
  FincaService
} from 'src/app/services/finca.service';
import {
  CommonService
} from 'src/app/services/common.service';
import {
  Router, ActivatedRoute
} from '@angular/router';
import {
  ModalController,
  LoadingController
} from '@ionic/angular';
import {
  FiltroFincaPage
} from 'src/app/pages/modales/filtro-finca/filtro-finca.page';
import {
  TabmainPage
} from 'src/app/tab/tabmain/tabmain.page';

import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-finca',
  templateUrl: './finca.page.html',
  styleUrls: ['./finca.page.scss'],
})
export class FincaPage implements OnInit {

  fincaForm: FormGroup;
  modelo: finca;
  tipoFincas: [{
    id: number,
    nombre: string
  }];
  tipoSector: [{
    id: number,
    nombre: string
  }];
  fincas: Array < fincalist >
    lista = true;
  titulo = "Registro Finca";
  loaderToShow: any;
  carga = false;
  edit = 0;

  constructor(
    private formBuilder: FormBuilder,
    private fincaService: FincaService,
    private commonService: CommonService,
    private router: Router,
    public modalController: ModalController,
    public loadingController: LoadingController,
    private tabPage: TabmainPage,
    private storage: Storage,
    private rutaActiva: ActivatedRoute
  ) {
    this.fincaForm = this.formBuilder.group({
      nombre: ['', Validators.required],
      area_total: ['',[Validators.required,  Validators.min(1)]],
      area_cultivada: ['',[Validators.required,  Validators.min(1)]],
      cajas_por_semana: ['',[Validators.required,  Validators.min(1)]],
      codigo_magap: ['', Validators.required],
      tipo_finca_id: [0, Validators.required],
      sector_id: [0, Validators.required],

    });
  }

  ngOnInit() {
    this.tabPage.titulo = "Finca"
    this.carga = true;
    this.validarUsuario()
    this.leerTipos();
    this.listaFincas()
    if (this.rutaActiva.snapshot.params.nuevo == 'nuevo' ) {
      console.log('es nuevo por parametro')
      this.addFinca() 
    } else {
      console.log('************* Noooooo es nuevo por parametro')
    }
  }

  onSubmit() {
    this.parseForm();
    let data: finca = this.modelo;
    this.storage.get('token').then(res=>{
      this.fincaService.registrar(data,res).subscribe(
        res => {
          this.edit = 0;
          if (res.status == 'success') {
            this.commonService.msjConfirm('Registro Exitoso', 'Se envío correctamente la finca');
            this.listaFincas()
            this.back()
            /* this.commonService.msjConfirm('Registro Exitoso','Se envío correctamente la información de la finca');
            this.router.navigate(['/main/home']); */
          } else {
          }
        }
      )
    })
    

  }

  leerTipos() {
    this.storage.get('token').then(res=>{
      this.fincaService.listTipoFinca(res).subscribe(
        res => {
          this.tipoFincas = res;
        }
      );
      this.fincaService.listSector(res).subscribe(
        res => {
          this.tipoSector = res;
  
        }
      );
    })
    

  }

  parseForm() {
    this.modelo = {
      id: this.edit,
      nombre: this.fincaForm.value.nombre,
      area_total: this.fincaForm.value.area_total,
      area_cultivada: this.fincaForm.value.area_cultivada,
      cajas_por_semana: this.fincaForm.value.cajas_por_semana,
      codigo_magap: this.fincaForm.value.codigo_magap,
      tipo_finca_id: this.fincaForm.value.tipo_finca_id,
      sector_id: this.fincaForm.value.sector_id,
    }
  }

  validarUsuario() {
    this.storage.get('token').then(res=>{
      this.commonService.validarUsuario(res).subscribe(
        res => {
          if (res.status == 'success') {
            //this.commonService.msjConfirm('Registro Exitoso', 'Muchas Gracias por su Registro');
          } else {
          }
        },
        error => {
          this.router.navigate(['/'])
        }
      )
    })
    
  }

  listaFincas() {
    this.storage.get('token').then(res=>{
      this.fincaService.listFincas(res).subscribe(
        res => {
          console.log('la resp de finca es: ', res)
          this.fincas = res;
          this.carga = false;
        }
      );
    })
    
  }

  addFinca() {
    this.lista = !this.lista;
  }

  filter() {
  }

  back() {
    this.titulo = "Registro Finca"
    this.lista = true;
    this.edit = 0;
    this.fincaForm.reset();
  }

  editar(id: number) {
    this.titulo = "Editar Finca"
    const fincatmp = this.fincas.find(z => z.id == id)
    this.fincaForm.setValue({
      'nombre': fincatmp.nombre,
      'area_total': fincatmp.area_total,
      'area_cultivada': fincatmp.area_cultivada,
      'cajas_por_semana': fincatmp.cajas_por_semana,
      'codigo_magap': fincatmp.codigo_magap,
      'tipo_finca_id': fincatmp.tipo_finca_id.id,
      'sector_id': fincatmp.sector_id,
    });
    this.lista = false;
    this.edit = fincatmp.id;
  }

  delete(id: number) {
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: FiltroFincaPage,
      cssClass: 'custom-modal-finca',
      componentProps: {
        'nombre': 'finca'
      }
    });
    await modal.present();
    const {
      data
    } = await modal.onDidDismiss();

    if (data) {
      if (!data.dismissed) {
        this.carga = true;
        let datos = {
          'tipo': data.tipo,
          'fecha_desde': data.fechaDesde,
          'fecha_hasta': data.fechaHasta
        }
        this.storage.get('token').then(res=>{
          this.fincaService.listFincasFiltro(datos,res).subscribe(
            res => {
              this.fincas = res;
              this.carga = false;
            }
          );
        })
        
      }
    } else {
    }
  }

  areaTotal(){
    console.log('cambio valor area total')
    let area_total = this.fincaForm.value.area_total
    let area_cultivada = this.fincaForm.value.area_cultivada
    if (area_cultivada > area_total ) {
      this.fincaForm.controls['area_cultivada'].setValue(0)
      this.commonService.msjNotificacion('El área cultivada no puede ser mayor a área total',3000)
    } else {
      console.log('no es mayor')
    }
  }

}