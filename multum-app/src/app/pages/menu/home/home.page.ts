import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TabmainPage } from 'src/app/tab/tabmain/tabmain.page';
import { Storage } from '@ionic/storage';
import { RegistroService } from '@services/registro.service';
import { resumenSemana, requeridos } from '@services/estructura.modelo';
import { CommonService } from '@services/common.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  nombre=''
  datosResumen: resumenSemana;
  b_resumenSem: boolean = false;
  requisitos: requeridos = {
    finca: true,
  };
  
  constructor(
    private router:Router,
    private _app: TabmainPage,
    private storage: Storage,
    private commonService: CommonService,
    private registroService: RegistroService,
  ) {
    
   }

  ngOnInit() {
    //this.onClick()
    this.nombre = '';
    console.log('Antes de >>>>>>>>> inicio funcion consulta')
    this.consultaReatardo();
    
  }

  ionViewDidEnter() {
    this.get_requisitos()
    this.consultaReatardo();
  }

  seleccion(ruta:string){
    this.router.navigate([ruta]);
  }
  onClick(){
    console.log("Home",this._app.notificaciones)
  }

  resumenSemana(){
    this.storage.get('token').then(res=>{
      this.registroService.miResumen(res).subscribe(
        res => {
          console.log('respuesta de resumen semana: ', res)
          this.datosResumen = res;
          this.b_resumenSem = true;
        }
      );
    })
  }

  async consultaReatardo(){
    console.log('inicio funcion consulta')
    await this.storage.get('nombres').then(res=>{
      console.log('el nombre es: ', res)
      this.nombre=res;
    })
    await this.storage.get('token').then(res=>{
      this.registroService.miResumen(res).subscribe(
        res => {
          console.log('*respuesta de resumen semana: ', res)
          this.datosResumen = res;
          this.b_resumenSem = true;
        }
      );
    })
  }

  get_requisitos(){ //requerimientos: requeridos
    let requisitos = this.requisitos;
     this.storage.get('token').then(res=>{ 
      this.commonService.requerimientos(requisitos, res).subscribe(
        res=>{
          requisitos = res.data;
          if (!requisitos.finca) { // false
            
            this.commonService.msjRequerid('Fincas no Registradas', 'Por favor registre una finca', 'Finca', 'main/tabs/tab3/finca/nuevo');
          }
        },
        error=>{
          console.log('erroorr: ',error)
          this.commonService.msjErrorServ();
        }
      )
      
    })
  }
  
}
