import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ProduccionService } from "@services/produccion.service";
import { CommonService } from '@services/common.service';
import { ModalController } from '@ionic/angular';
import { TabmainPage } from 'src/app/tab/tabmain/tabmain.page';
import { FiltroEnfundePage } from '../../modales/filtro-enfunde/filtro-enfunde.page';
import { Router } from '@angular/router';
import { Datum } from '../racimo/structs/estructuras.service';
import { parametros } from '@services/estructura.modelo';
import { EstBasicaPage } from '../../modales/estadisticas/basica/Est-basica.page';
@Component({
  selector: 'app-produccion',
  templateUrl: './produccion.page.html',
  styleUrls: ['./produccion.page.scss'],
})
export class ProduccionPage implements OnInit {

  b_lista = true;
  b_carga = false; b_detalles = false;
  listado: Array < Datum >; itemProduccion: Datum;
  datosEnfunde: any
  params: parametros = {
    finca: 1,
    anio: 2020,
    semDesde: 1,
    semHasta: 52,
    id: 0,
    todos: false,
    showSelectGraf: false,
    grafica: 'racimo'
  }

  constructor(
    private commonService: CommonService,
    private produccionService: ProduccionService,
    private modalController: ModalController,
    private tabPage: TabmainPage,
    private storage: Storage,
    
    private router:Router,
  ) {
  }

  ngOnInit() {
    this.b_carga = true;
    this.tabPage.titulo = "Producción"
    this.getlista()
  }

  

  getlista() {
    this.storage.get('token').then(res=>{
      this.produccionService.listaProduccion(this.params.anio,this.params.semDesde, this.params.semHasta, this.params.finca, this.params.todos, res)
      .subscribe(
        res => {
          console.log('Consulta con finca: ', this.params.todos)
          console.log('respuesta listado', res)
          this.listado = res.data;
          this.b_carga = false;

        }
      );
    })
    
  }

  ir( produccion: Datum){
    this.itemProduccion = produccion;
    this.b_lista = false;
    this.b_detalles = true;
  }
  closeDetalles(){
    this.b_lista = true;
    this.b_detalles = false;
  }

  async presentModal(esEstadisticas?: boolean) {
    esEstadisticas == true? this.params.showSelectGraf = true: this.params.showSelectGraf = false;
    const modal = await this.modalController.create({
      component: FiltroEnfundePage,
      cssClass: 'custom-modal-finca',
      componentProps: {
        'nombre': 'calibraje',
        'parametros': this.params,
        'b_estadisticas': esEstadisticas,
      }
    });
    await modal.present();
    const {
      data
    } = await modal.onDidDismiss();

    if (data) {
      if (data.b_estadisticas) {
        this.mostrarEstadisticas()
      } else {
        if (!data.dismissed) {
          this.params = {
            anio: data.anio,
            finca: data.finca == "" ? 0 : data.finca,
            id: 0,
            semDesde: data.sem_desde,
            semHasta: data.sem_hasta,
            todos: data.todos,
            showSelectGraf: false,
            grafica: data.grafica
          }
          console.log(data)
          this.b_carga = true;
          this.getlista()
        }
      }
      
    }
  }

  async mostrarEstadisticas(){
    const modal = await this.modalController.create({
      component: EstBasicaPage,
      cssClass: 'custom-modal-finca',
      componentProps: {
        'params': this.params,
      }
    });
    await modal.present();
    const {
      data
    } = await modal.onDidDismiss();
  }

}
