import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProduccionPage } from './produccion.page';

const routes: Routes = [
  {
    path: '',
    component: ProduccionPage
  },  {
    path: 'resumen',
    loadChildren: () => import('./resumen/resumen.module').then( m => m.ResumenPageModule)
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProduccionPageRoutingModule {}
