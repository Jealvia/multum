import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TotalesPage } from 'src/app/pages/modales/totales/totales.page';
import { Datum } from '../../racimo/structs/estructuras.service';

@Component({
  selector: 'app-resumen',
  templateUrl: './resumen.page.html',
  styleUrls: ['./resumen.page.scss'],
})
export class ResumenPage implements OnInit {

  @Input() produccion: Datum;
  @Output() close: EventEmitter<any> = new EventEmitter();

  constructor(
    private modalController: ModalController,
  ) { }

  ngOnInit() {

  }

  async presentModal(tipo: string) {
    const modal = await this.modalController.create({
      component: TotalesPage,
      cssClass: 'custom-modal-finca',
      componentProps: {
        'tipo': tipo,
        'data': this.produccion
      }
    });
    await modal.present();
    const {
      data
    } = await modal.onDidDismiss();

    if (data) {
      if (!data.dismissed) {
        console.log('no guardo')
        }
        console.log('guardo')
    }
    
  }
  regresar(){
    this.close.emit(null);
  }

}
