import { Component, OnInit } from '@angular/core';
import { calificacion } from 'src/app/services/estructura.modelo';

@Component({
  selector: 'app-calificacion',
  templateUrl: './calificacion.page.html',
  styleUrls: ['./calificacion.page.scss'],
})
export class CalificacionPage implements OnInit {

  starFunc: number = 0; starDis: number = 0;
  modelo: calificacion = {ptsFuncion: 0,
    ptsDiseno: 0,
    comentario: '',};

  constructor() { }

  ngOnInit() {
  }
  fnPts(valor: number, caso: string){
    if (caso == 'fn') {
      this.modelo.ptsFuncion = valor;
    } else {
      this.modelo.ptsDiseno = valor;
    }
    
  }
  sendRanking(){

    console.log('los valores son: ', this.modelo)
  }
}
