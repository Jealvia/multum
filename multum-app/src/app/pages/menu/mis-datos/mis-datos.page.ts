import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { registro } from '@services/estructura.modelo';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { RegistroService } from '@services/registro.service';
import { CommonService } from '@services/common.service';
import { TabmainPage } from 'src/app/tab/tabmain/tabmain.page';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-mis-datos',
  templateUrl: './mis-datos.page.html',
  styleUrls: ['./mis-datos.page.scss'],
})
export class MisDatosPage implements OnInit {

  registroForm: FormGroup;
  mensaje: string = ''; b_mensaje: boolean = false;
  modelo: registro;
  carga=false;
  personaData: registro;

  constructor(
    public alertControl: AlertController,
    private formBuilder: FormBuilder,
    private router:Router,
    private registroService:RegistroService,
    private commonService: CommonService,
    private tabPage:TabmainPage,
    private storage: Storage,
  ) {
    this.prepareForm();
   }

   ngOnInit(): void {
    this.tabPage.titulo="Mis Datos"
    this.getDatos()
  }

  onBlur(){
    let confirmar: string;
    if (this.registroForm.value.clave == this.registroForm.value.confClave) {
      console.log('confirmada clave');
    } else {
      //this.registroForm.setValue({'confClave': ''},{onlySelf: false});
      //this.registroForm.setValue.confClave 
      this.commonService.msjConfirm('Clave no coincide','Por Favor Ingrese la clave que coincida');
      
    }
  }

  prepareForm(){
    let regexPatterns = {
      // this can be improved
      numeros: "[0-2]?[0-9]?",
      minutes: "[0-5]?[0-9]?",
      ruc: "[0-9]{11,14}",
   };

    this.registroForm = this.formBuilder.group({
    pNombre: ['', Validators.required],
    sNombre: [''],
    identificacion: ['',[Validators.required,Validators.pattern(regexPatterns.ruc)]],
    pApellido: ['', Validators.required],
    sApellido: ['', Validators.required],

    telefono: ['', Validators.required],
    direccion: ['', Validators.required],
    correo: ['',  [Validators.required, Validators.email]],

    fechaNac: ['', Validators.required],
    usuario: ['',[Validators.required, Validators.minLength(4),Validators.maxLength(18)]],
    clave: ['',[Validators.required, Validators.minLength(6)]],
    confClave: ['', Validators.required],
    
    });
  }

  getDatos() {
    this.storage.get('token').then(res=>{
      this.registroService.misDatos(res).subscribe(
        res => {
          console.log('la resp de mis datos  es: ', res)
          this.personaData = res;
          this.registroForm.setValue ({
            pNombre: this.personaData.primer_nombre,
            sNombre: this.personaData.segundo_nombre,
            identificacion: this.personaData.identificacion,
            pApellido: this.personaData.primer_apellido,
            sApellido: this.personaData.segundo_apellido,
        
            telefono: this.personaData.telefono,
            direccion: this.personaData.direccion,
            correo: this.personaData.correo,
        
            fechaNac: this.personaData.fecha_nacimiento== null? '': this.personaData.fecha_nacimiento,
            usuario: this.personaData.usuario,
            clave: '*****',
            confClave: '*****',
            
          });

        }
      );
    })
    
  }

  onSubmit(){
    this.carga=true;
    //this.parseForm();
    //let data: registro =  this.modelo;
    
    let data=
    {
      //'tipo_identificacion_id':this.registroForm.value.usuario,
      'primer_apellido': this.registroForm.value.pApellido,
      'identificacion': this.registroForm.value.identificacion,
      'segundo_apellido': this.registroForm.value.sApellido,
      'primer_nombre': this.registroForm.value.pNombre,
      'segundo_nombre': this.registroForm.value.sNombre,

      'telefono': this.registroForm.value.telefono,
      'direccion': this.registroForm.value.direccion,
      'correo':this.registroForm.value.correo,
      //'nombre': '',
      'fecha_nacimiento': this.registroForm.value.fechaNac,
      'usuario': this.registroForm.value.usuario,
      'contrasena': this.registroForm.value.clave,
      
    }
    this.storage.get('token').then(res=>{ 
      this.registroService.updateData(data, res).subscribe(
        res=>{
          console.log('esta es la respuesta: ',res);
          if (res.status == 'success') {
            this.commonService.msjConfirm('Registro Exitoso', 'Se actualizo correctamente sus datos');
            //this.prepareForm();
            this.carga=false;
            
          } 
          /*
          else {
            
            this.carga=false;
            if (res.mensaje == "Usuario existente") {
              this.commonService.msjConfirm('Usaario Existente','Por Favor intente con otro usuario');
            }
          }
          */
        },
        error=>{
          //this.router.navigate(['/'])
          console.log('erroorr: ',error)
          this.carga=false;
          this.commonService.msjErrorServ();
        }
      )
    })
    

  }


  parseForm(){
    //this.modelo.tipo_identificacion_id = this.registroForm.value.usuario;
    
    console.log('form pApellido', this.registroForm.value.pApellido);
    console.log('form pApellido', this.registroForm);
    console.log('form pApellido', this.modelo);
    this.modelo.primer_apellido = this.registroForm.value.pApellido;
    this.modelo.identificacion = this.registroForm.value.identificacion;
    this.modelo.segundo_apellido = this.registroForm.value.sApellido;
    this.modelo.primer_nombre = this.registroForm.value.pNombre;
    this.modelo.segundo_nombre = this.registroForm.value.sNombre;
    this.modelo.nombre = '';
    this.modelo.fecha_nacimiento = this.registroForm.value.fechaNac;
    this.modelo.usuario = this.registroForm.value.usuario;
    this.modelo.contrasena = this.registroForm.value.clave;
  }

  back() {
    this.router.navigate(['/main/tabs/tab3']);
  }
  
  


}
