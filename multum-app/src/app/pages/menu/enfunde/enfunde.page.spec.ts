import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EnfundePage } from './enfunde.page';

describe('EnfundePage', () => {
  let component: EnfundePage;
  let fixture: ComponentFixture<EnfundePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnfundePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EnfundePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
