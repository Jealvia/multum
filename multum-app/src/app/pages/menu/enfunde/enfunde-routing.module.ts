import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EnfundePage } from './enfunde.page';

const routes: Routes = [
  {
    path: '',
    component: EnfundePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EnfundePageRoutingModule {}
