import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router
} from '@angular/router';
import {
  EnfundeService
} from 'src/app/services/enfunde.service';
import {
  listaEnfunde,
  IdNombre,
  parametros,
  requeridos
} from 'src/app/services/estructura.modelo';
import {
  CommonService
} from 'src/app/services/common.service';
import {
  anioS
} from "@services/constants";
import {
  NewEnfundeComponent
} from 'src/app/components/modal/new-enfunde/new-enfunde.component';
import {
  ModalController
} from '@ionic/angular';
import {
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import {
  FiltroEnfundePage
} from '../../modales/filtro-enfunde/filtro-enfunde.page';
import {
  TabmainPage
} from 'src/app/tab/tabmain/tabmain.page';
import { Storage } from '@ionic/storage';
import { EstBasicaPage } from '../../modales/estadisticas/basica/Est-basica.page';
@Component({
  selector: 'app-enfunde',
  templateUrl: './enfunde.page.html',
  styleUrls: ['./enfunde.page.scss'],
})
export class EnfundePage implements OnInit {
  //radio: any
  public radio: string
  listFincas: [{
    id: number,
    finca: string
  }];
  listado: Array < listaEnfunde >
  params: parametros  = {
    finca: 1,
    anio: 2020,
    semDesde: 1,
    semHasta: 52,
    id: 0,
    todos: null,
    grafica: 'enfunde',
    showSelectGraf: false
  }
  listAnio = anioS;
  titulo = "Registro Enfunde";
  enfundeForm: FormGroup;
  lista = true;
  carga = false;
  maxDate: string; // format yyyy-mm-dd 2020-12-25
  minDate: string;
  validDay: Date;
  datosEnfunde: any //{ anio: number, semana: number, cinta_id: number, cita_color: string};
  listTipoEnfunde: IdNombre;
  tipo = 0
  estaditicasInventarios:any
  requisitos: requeridos = {
    finca: true,
  };


  constructor(
    private router: Router,
    private enfundeService: EnfundeService,
    private commonService: CommonService,
    private modalController: ModalController,
    private formBuilder: FormBuilder,
    private tabPage: TabmainPage,
    private storage: Storage,
  ) {
    this.enfundeForm = this.formBuilder.group({
      finca: ['', Validators.required],
      fecha: ['', Validators.required],
      ano: ['', Validators.required],
      semana: ['', Validators.required],
      cinta: ['', Validators.required],
      cantida: ['',[Validators.required, Validators.min(1)]],
      tipo: ['', Validators.required],
      //2020-01-20T14:54:51.099-05:00
    });
  }

  ngOnInit() {
    this.carga = true;
    this.tabPage.titulo = "Enfunde";
    this.listaTipoEnfunde()
    this.listaEnfunde()
    this.validarUsuario()
    this.listaFincas()
    this.dateValid()
    this.resetForm();
    //this.dataTemporal()
  }

  listaFincas() {
    this.storage.get('token').then(res=>{
      this.enfundeService.listaFincas(res).subscribe(
        res => {
          this.listFincas = res;
        }
      );
    })
    
  }

  validarUsuario() {
    this.storage.get('token').then(res=>{
      this.commonService.validarUsuario(res).subscribe(
        res => {
          if (res.status == 'success') {
          } else {
          }
        },
        error => {
          this.router.navigate(['/'])
        }
      )
    })
    
  }

  async listaEnfunde() {
    await this.storage.get('token').then(res=>{
      this.enfundeService.listaEnfunde(this.params.anio, this.params.semDesde, 
        this.params.semHasta, this.params.finca, this.params.todos,res)
        .subscribe(
          res => {
            console.log('111 listado: ', res.data)
            this.listado = res.data;
            this.carga = false;
  
          }
        );
    })
    
  }

  editar(id: number) {
    this.params.id = id;
    this.titulo = "Editar Enfunde"
    const estimadotmp = this.listado.find(z => z.id == id)
    this.tipo = estimadotmp.tipo_enfunde_id.id
    this.datosEnfunde = {
      id: estimadotmp.cinta_id.id
    }
    this.enfundeForm.setValue({
      finca: estimadotmp.finca_id.id,
      fecha: estimadotmp.fecha_enfunde,
      ano: estimadotmp.anio,
      semana: estimadotmp.semana,
      cinta: estimadotmp.cinta_id.color,
      cantida: estimadotmp.cantidad,
      tipo: estimadotmp.tipo_enfunde_id.id,
    });
    this.lista = false;
  }

  delete(id: number) {
  }

  

  async mostrarEstadisticas(){
    const modal = await this.modalController.create({
      component: EstBasicaPage,
      cssClass: 'custom-modal-finca',
      componentProps: {
        'params': this.params,
      }
    });
    await modal.present();
    const {
      data
    } = await modal.onDidDismiss();
  }

  onSubmit() {
    let fecha = new Date(this.enfundeForm.value.fecha)
    let data = {
      fecha_enfunde: fecha,
      finca_id: this.enfundeForm.value.finca,
      enfunde: this.enfundeForm.value.cantida,
      tipo_enfunde_id: this.enfundeForm.value.tipo,
      cinta_id: this.datosEnfunde.id,
      id: this.params.id
    }
    this.storage.get('token').then(res=>{
      this.enfundeService.enfundeEnviar(data,res).subscribe(
        res => {
          if (res.status == 'success') {
            this.params = {
              finca: 1,
              anio: 2020,
              semDesde: 1,
              semHasta: 52,
              id: 0,
              todos: true,
            }
            this.listaEnfunde();
            this.commonService.msjConfirm('Registro Exitoso', 'Se envío correctamente el enfunde');
            this.back();
          } else {
          }
        }
      )
    })
    

  }

  dateValid() {
    let hoy: Date = new Date();
    let tem_minDate: Date = new Date();

    this.validDay = this.sumarDias(tem_minDate, -30);

    this.maxDate = this.datetoString(hoy);
    this.minDate = this.datetoString(this.validDay);
  }

  sumarDias(fecha: Date, dias: number) {
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }

  datetoString(fecha: Date) {
    let newDate: string;
    let anio: number = fecha.getFullYear();
    let mes: number = fecha.getMonth();
    mes = mes + 1;
    let dias: number = fecha.getDate();
    //Para Seguir el formato agrego el '0' a mes
    if (mes < 10 || dias < 10) {
      if (mes < 10 && dias < 10) {
        newDate = anio + '-0' + mes + '-0' + dias;
      } else {
        if (mes < 10) {
          newDate = anio + '-0' + mes + '-' + dias;
        }
        if (dias < 10) {
          newDate = anio + '-' + mes + '-0' + dias;
        }
      }
    } else {
      newDate = anio + '-' + mes + '-' + dias;
    }
    return newDate;
  }

  buscarEnfunde() {
    if (!this.lista) {
      this.storage.get('token').then(res=>{
        this.enfundeService.buscarEnfunde(this.enfundeForm.value.fecha,res).subscribe(
          res => {
            console.log('la respuesta de cinta es: ', res)
            this.datosEnfunde = res;
            this.enfundeForm.setValue({
              'finca': this.enfundeForm.value.finca == null ? "" : this.enfundeForm.value.finca,
              'fecha': this.enfundeForm.value.fecha,
              'ano': res.anio,
              'semana': res.semana,
              'cinta': res.color,
              'cantida': this.enfundeForm.value.cantida == null ? "" : this.enfundeForm.value.cantida,
              'tipo': this.enfundeForm.value.tipo == null? 1: this.enfundeForm.value.tipo,
            });
          }
        )  
      })
      
    }

  }

  resetForm() {
    this.enfundeForm.controls['fecha'].setValue('');
    this.enfundeForm.reset();
  }

  back() {
    this.titulo = "Registro Enfunde"
    this.lista = true;
    this.enfundeForm.reset();
  }

  listaTipoEnfunde() {
    this.storage.get('token').then(res=>{
      this.enfundeService.listaTipoEnfunde(res).subscribe(
        res => {
          this.listTipoEnfunde = res;
          if (this.listTipoEnfunde[0]) {
            this.tipo = this.listTipoEnfunde[0].id;
          }
        }
      );
    })
    
  }

  async presentModal(esEstadisticas?: boolean) {
    console.log('presentar modal parametros: ', this.params)
    const modal = await this.modalController.create({
      component: FiltroEnfundePage,
      cssClass: 'custom-modal-finca',
      componentProps: {
        'nombre': 'enfunde',
        'parametros': this.params,
        'b_estadisticas': esEstadisticas,
      }
    });
    await modal.present();
    const {
      data
    } = await modal.onDidDismiss();

    if (data) {
      if (!data.dismissed) { // sino cancelo la modal
        this.params = {
          anio: data.anio,
          finca: data.finca == "" ? 0 : data.finca,
          id: 0,
          semDesde: data.sem_desde,
          semHasta: data.sem_hasta,
          todos: data.todos,
          grafica: 'enfunde',
          showSelectGraf: false
        }
        if (data.b_estadisticas) { //mostar estadicticas 
          this.mostrarEstadisticas()
        } else {
          this.carga = true;
          this.listaEnfunde()
        }
      }
    } 
  }

   get_requisitos(funcion: string){ //requerimientos: requeridos
    let actFuncion = funcion
    let requisitos = this.requisitos;
     this.storage.get('token').then(res=>{ 
      this.commonService.requerimientos(requisitos, res).subscribe(
        res=>{
          console.log('esta es la respuesta: ',res);
          requisitos = res.data;
          if (requisitos.finca) {
            //this.addEnfunde();
            if (funcion == 'add') {
              this.addEnfunde();
            }
            if (funcion == 'filtro') {
              this.presentModal()
            }
            if (funcion == 'Estadis') {
              this.presentModal(true) 
            }
          } else {
            this.commonService.msjConfirm('Fincas no Registradas', 'Por favor registre primero una finca');
          }
        },
        error=>{
          console.log('erroorr: ',error)
          this.commonService.msjErrorServ();
        }
      )
      
    })
  }

  addEnfunde() {
    this.params.id = 0;
    this.titulo = "Registro Enfunde";
    this.lista = false
  }

  

}