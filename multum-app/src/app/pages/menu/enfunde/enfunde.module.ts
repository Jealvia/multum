import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { IonicModule } from '@ionic/angular';

import { EnfundePageRoutingModule } from './enfunde-routing.module';

import { EnfundePage } from './enfunde.page';
import { ComponentsModule } from 'src/app/components/components.module';


import { ModulesModule } from "src/app/modules/modules.module";
import { FiltroEnfundePage } from '../../modales/filtro-enfunde/filtro-enfunde.page';
import { FiltroEnfundePageModule } from '../../modales/filtro-enfunde/filtro-enfunde.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


@NgModule({
  entryComponents:[
    FiltroEnfundePage,
    
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EnfundePageRoutingModule,
    FiltroEnfundePageModule,
    ReactiveFormsModule,
    ComponentsModule,
  ],
  declarations: [EnfundePage]
})
export class EnfundePageModule {}
