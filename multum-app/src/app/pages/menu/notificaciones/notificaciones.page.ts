import { Component, OnInit } from '@angular/core';
import { notificacion } from 'src/app/services/estructura.modelo';
import { CommonService } from 'src/app/services/common.service';
import { Router } from '@angular/router';
import { TabmainPage } from 'src/app/tab/tabmain/tabmain.page';


@Component({
  selector: 'app-notificaciones',
  templateUrl: './notificaciones.page.html',
  styleUrls: ['./notificaciones.page.scss'],
})
export class NotificacionesPage implements OnInit {

  notificaciones: Array<notificacion>= [{
    origen: 'Exportadora Equapak',
    titulo: 'Embarque Semana 49.',
    mensaje: 'Vapor Sunrise recibira cajas desde martes 5',
    estado: true,
    img: './assets/img/fruta/banana.jpg',
  }];
  constructor(
    private commonService: CommonService,
    private router: Router,
    private tabPage:TabmainPage
    ) { }

  ngOnInit() {
    this.tabPage.titulo="Notificaciones"
    this.validarUsuario()
  }

  salir(){
    this.router.navigate(['/main/tabs/tab3']);
  }

  validarUsuario() {
    /* this.commonService.validarUsuario().subscribe(
      res => {
        console.log(res);
        if (res.status == 'success') {
          console.log("Guardado exitosamente");
          //this.commonService.msjConfirm('Registro Exitoso', 'Muchas Gracias por su Registro');
        } else {
          console.log("Some error occured");
        }
      },
      error=>{
        this.router.navigate(['/'])
        console.log(error)
      }
    ) */
  }

}
