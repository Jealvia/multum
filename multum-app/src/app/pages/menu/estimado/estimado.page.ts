import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router
} from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import {
  estimado,
  cintabyFecha,
  EstimadoList,
  parametros,
  requeridos
} from 'src/app/services/estructura.modelo';
import {
  EstimadoService
} from 'src/app/services/estimado.service';
import {
  EnfundeService
} from 'src/app/services/enfunde.service';
import {
  CommonService
} from 'src/app/services/common.service';
import {
  ModalController
} from '@ionic/angular';
import {
  FiltroEnfundePage
} from '../../modales/filtro-enfunde/filtro-enfunde.page';
import {
  TabmainPage
} from 'src/app/tab/tabmain/tabmain.page';

import { Storage } from '@ionic/storage';
import { EstBasicaPage } from '../../modales/estadisticas/basica/Est-basica.page';

@Component({
  selector: 'app-estimado',
  templateUrl: './estimado.page.html',
  styleUrls: ['./estimado.page.scss'],
})
export class EstimadoPage implements OnInit {
  estimadoForm: FormGroup;
  params: parametros  = {
    finca: 1,
    anio: 2020,
    semDesde: 1,
    semHasta: 52,
    id: 0,
    todos: null,
    grafica: 'none',
    showSelectGraf: false
  }
  modelo: estimado = {
    fecha_estimado: '',
    cinta_id: null,
    finca_id: null,
    estimado: null,
    semana: null,
    anio: null,
    id: 0
  };
  dataCinta: cintabyFecha;
  listFincas: [{
    id: number,
    nombre: string
  }];
  cinta_id: number;
  datosEstimado: cintabyFecha; //any // { anio: number, semana: number, cinta_id: number, cita_color: string};

  //valid date
  maxDate: string; // format yyyy-mm-dd 2020-12-25
  minDate: string;
  validDay: Date;
  lista = true;
  carga = false;
  listado: Array < EstimadoList >
    titulo = "Registro Estimado";
  
  

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private estimadoService: EstimadoService,
    private enfundeService: EnfundeService,
    private commonService: CommonService,
    private modalController: ModalController,
    private tabPage: TabmainPage,
    private storage: Storage
  ) {
    this.estimadoForm = this.formBuilder.group({
      finca: ['', Validators.required],
      anio: ['', Validators.required],
      fecha: ['', Validators.required],
      sem_ingresada: [{
        value: '',
        disabled: true
      }],
      sem_estimada: ['',[Validators.required, Validators.max(53), Validators.min(1)]],
      //sem_estimada: ['', Validators.required],
      estimado_cajas: ['',[Validators.required, Validators.min(1)]],
    });
  }

  ngOnInit() {
    this.tabPage.titulo = "Estimado"
    //this.validarUsuario()
    this.listaFincas();
    this.dateValid();
    this.cargarListado();
  }

  validarUsuario() {
    this.storage.get('token').then(res=>{
      this.commonService.validarUsuario(res).subscribe(
        res => {
          if (res.status == 'success') {
            //this.commonService.msjConfirm('Registro Exitoso', 'Muchas Gracias por su Registro');
          } else {
          }
        },
        error => {
          this.router.navigate(['/'])
        }
      )
    })
    
  }

  buscarEnfunde() {
    console.log('fecha es: ',this.estimadoForm.value.fecha)
    if (!this.lista) {
      this.storage.get('token').then(res=>{
        this.enfundeService.buscarEnfunde(this.estimadoForm.value.fecha,res).subscribe(
          res => {
            this.datosEstimado = res;
            this.estimadoForm.setValue({
              'finca': this.estimadoForm.value.finca == null ? "" : this.estimadoForm.value.finca,
              'fecha': this.estimadoForm.value.fecha,
              'anio': res.anio,
              'sem_ingresada': res.semana,
              'sem_estimada': res.semana,
              'estimado_cajas': this.estimadoForm.value.estimado_cajas == null ? "" : this.estimadoForm.value.estimado_cajas,
            });
          }
        )
      })
    }
    
  }

  buscarEnfundeEdicion() {
    this.storage.get('token').then(res=>{
      this.enfundeService.buscarEnfunde(this.estimadoForm.value.fecha,res).subscribe(
        res => {
          this.datosEstimado = res;
          this.estimadoForm.setValue({
            'finca': this.estimadoForm.value.finca == null ? "" : this.estimadoForm.value.finca,
            'fecha': this.estimadoForm.value.fecha,
            'anio': this.estimadoForm.value.anio,
            'sem_ingresada': res.semana,
            'sem_estimada': this.estimadoForm.value.sem_estimada,
            'estimado_cajas': this.estimadoForm.value.estimado_cajas == null ? "" : this.estimadoForm.value.estimado_cajas,
          });
        }
      )
    })
    
  }

  onSubmit() {
    this.parseFrom();
    console.log('modelo: ', this.modelo)
    this.storage.get('token').then(res=>{
      this.estimadoService.estimadoEnviar(this.modelo,res).subscribe(
        res => {
          if (res.status == 'success') {
            
            this.commonService.msjConfirm('Registro Exitoso', 'Se envío correctamente el estimado');
            this.cargarListado()
            this.back();
          } else {
          }
        }
      )
    })
  }

  parseFrom() {
    this.modelo.fecha_estimado = this.estimadoForm.value.fecha;
    this.modelo.cinta_id = this.datosEstimado.id;
    this.modelo.finca_id = this.estimadoForm.value.finca;
    this.modelo.estimado = this.estimadoForm.value.estimado_cajas;
    this.modelo.semana = this.estimadoForm.value.sem_estimada;
    this.modelo.anio = this.estimadoForm.value.anio;
  }

  listaFincas() {
    this.storage.get('token').then(res=>{
      this.enfundeService.listaFincas(res).subscribe(
        res => {
          this.listFincas = res;
        }
      );
    })
    
  }

  dateValid() {
    let hoy: Date = new Date();
    let tem_minDate: Date = new Date();

    this.validDay = this.sumarDias(tem_minDate, -15);

    this.maxDate = this.datetoEstring(hoy);
    this.minDate = this.datetoEstring(this.validDay);
  }

  sumarDias(fecha: Date, dias: number) {
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }

  datetoEstring(fecha: Date) {
    let newDate: string;
    let anio: number = fecha.getFullYear();
    let mes: number = fecha.getMonth();
    mes = mes + 1;
    let dias: number = fecha.getDate();
    //Para Seguir el formato agrego el '0' a mes
    if (mes < 10 || dias < 10) {
      if (mes < 10 && dias < 10) {
        newDate = anio + '-0' + mes + '-0' + dias;
      } else {
        if (mes < 10) {
          newDate = anio + '-0' + mes + '-' + dias;
        }
        if (dias < 10) {
          newDate = anio + '-' + mes + '-0' + dias;
        }
      }
    } else {
      newDate = anio + '-' + mes + '-' + dias;
    }
    return newDate;
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: FiltroEnfundePage,
      cssClass: 'custom-modal-finca',
      componentProps: {
        'nombre': 'estimado',
        'parametros': this.params,
      }
    });
    await modal.present();
    const {
      data
    } = await modal.onDidDismiss(); //finca,anio,sem_desde,sem_hasta,todos

    if (data) {
      console.log('la data del modal filtro es: ', data)
     if (!data.dismissed) { // si no fue cancelado 
      this.params = {
        anio: data.anio,
        finca: data.finca == "" ? 0 : data.finca,
        id: 0,
        semDesde: data.sem_desde,
        semHasta: data.sem_hasta,
        todos: data.todos,
        grafica: 'none',
        showSelectGraf: false
      }
      this.cargarListado()
      
      }
    }
      
  }

  cargarListado() {
    this.carga = true;
    this.storage.get('token').then(res=>{
      this.estimadoService.listaEstimados(this.params.anio, this.params.semDesde, 
        this.params.semHasta, this.params.finca, this.params.todos,res).subscribe(
        res => {
          this.listado = res.data;
          console.log('listado Esrimado: ', res)
          this.carga = false;
        }
      );
    })
    
  }

  editar(id: number, bloqueado: boolean) {
    if (bloqueado) {
      this.commonService.msjNotificacion('El estimado esta bloqueado', 2000);
    } else {
      this.estimadoForm.reset();
      this.modelo.id = id;
      this.titulo = "Editar Estimado"
      const fincatmp = this.listado.find(z => z.id == id)

      this.estimadoForm.setValue({
        'finca': fincatmp.finca_id.id,
        'fecha': fincatmp.fecha_estimado,
        'sem_ingresada': '',
        'anio': fincatmp.anio,
        'sem_estimada': fincatmp.semana,
        'estimado_cajas': fincatmp.cantidad,
      });
      this.buscarEnfundeEdicion()
      this.lista = false;
    }
    
  }

  back() {
    this.titulo = "Registro Estimado"
    this.lista = true;
    this.estimadoForm.reset();
  }

  delete(id: number) {
  }

  addEstimado() {
    this.modelo.id = 0;
    this.estimadoForm.reset();
    this.lista = false
  }
  get_requisitos(){ 
    let requisitos: requeridos = {
      finca: true
    };
     this.storage.get('token').then(res=>{ 
      this.commonService.requerimientos(requisitos, res).subscribe(
        res=>{
          console.log('esta es la respuesta: ',res);
          
          requisitos = res.data;
          if (requisitos.finca) {
            this.addEstimado();
          } else {
            this.commonService.msjConfirm('Fincas no Registradas', 'Por favor registre primero una finca');
          }
        },
        error=>{
          console.log('erroorr: ',error)
          this.commonService.msjErrorServ();
        }
      )
      
    })
  }

}