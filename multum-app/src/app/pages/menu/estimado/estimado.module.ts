import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { EstimadoPageRoutingModule } from './estimado-routing.module';

import { EstimadoPage } from './estimado.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { ModulesModule } from "src/app/modules/modules.module";
import { ReactiveFormsModule } from '@angular/forms';
import { FiltroEnfundePage } from '../../modales/filtro-enfunde/filtro-enfunde.page';
import { FiltroEnfundePageModule } from '../../modales/filtro-enfunde/filtro-enfunde.module';
@NgModule({
  entryComponents:[
    FiltroEnfundePage,
    
  ],
  imports: [
    CommonModule,
    IonicModule,
    EstimadoPageRoutingModule,
    FiltroEnfundePageModule,
    ComponentsModule,
    ReactiveFormsModule,
  ],
  declarations: [EstimadoPage]
})
export class EstimadoPageModule {}
