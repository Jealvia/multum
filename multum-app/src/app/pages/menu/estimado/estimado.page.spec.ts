import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EstimadoPage } from './estimado.page';

describe('EstimadoPage', () => {
  let component: EstimadoPage;
  let fixture: ComponentFixture<EstimadoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstimadoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EstimadoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
