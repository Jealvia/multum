import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RacimoPage } from './racimo.page';

describe('RacimoPage', () => {
  let component: RacimoPage;
  let fixture: ComponentFixture<RacimoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RacimoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RacimoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
