import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RacimoPageRoutingModule } from './racimo-routing.module';

import { RacimoPage } from './racimo.page';
import { ContadorRacimoPageModule } from "./contador-racimo/contador-racimo.module";
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RacimoPageRoutingModule,
    ContadorRacimoPageModule
  ],
  declarations: [RacimoPage]
})
export class RacimoPageModule {}
