
    export interface ListadoCajas {
        '31'?:number,
        '40.5'?:number,
        '42'?:number,
        '41.5': number;
        '37.48'?: number;
        '43.0'?: number;
    }

    export interface TipoCajas{
        'id':number,
        'nombre':string,
        'peso':number
    }

    export interface CajaSegunda {
        50: number;
    }

    export interface Cajas {
        total_cajas: number;
        porcentaje_segunda: number;
        listado_cajas: ListadoCajas;
        caja_segunda: CajaSegunda;
    }

    export interface DataRacimo {
        color: string;
        cantidad: number;
        edad: number;
    }

    export interface Racimos {
        total_racimos: number;
        total_racimos_rechazado: number;
        ratio: number;
        data_racimos: DataRacimo[];
        recobro?: number;
    }

    export interface Inventario2 {
        cinta: string;
        edad: number;
        inicial: number;
        final: number;
    }

    export interface Inventario {
        inventarios: Inventario2[];
        total_inicial: number;
        total_final: number;
    }

    export interface Tendencia2 {
        cinta: string;
        edad: number;
        cantidad: number;
    }

    export interface Tendencia {
        tendencias: Tendencia2[];
        total_racimos: number;
        total_cajas: number;
    }

    export interface Datum {
        anio: number;
        semana: number;
        finca: string;
        orden: number;
        cajas: Cajas;
        racimos: Racimos;
        inventario: Inventario;
        tendencia: Tendencia;
    }

    export interface RootObject {
        mensaje: string;
        status: string;
        data: Datum[];
    }


/*



*/
export interface RacimoColor{
  orden_corte_id: number,
  color: string,
  nombre: string,
  cantidad: number,
  rechazado: number
}
export interface Racimo{
  finca: string,
  color: string, 
  cantidad: number,
  semanaCorte: number,
  orden: number,
}

export interface ControlRacimo{
  racimoColor: RacimoColor[],
  totalRacimos: number,
  totalRechazados: number
}


