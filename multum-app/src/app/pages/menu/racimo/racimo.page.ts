import { Component, OnInit } from '@angular/core';
import { RootObject, Datum, Racimos } from './structs/estructuras.service';
import { FiltroEnfundePage } from '../../modales/filtro-enfunde/filtro-enfunde.page';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { TabmainPage } from 'src/app/tab/tabmain/tabmain.page';
import { ProduccionService } from '@services/produccion.service';
import { Storage } from '@ionic/storage';
import { parametros } from '@services/estructura.modelo';

@Component({
  selector: 'app-racimo',
  templateUrl: './racimo.page.html',
  styleUrls: ['./racimo.page.scss'],
})
export class RacimoPage implements OnInit {
  
  b_lista = true; b_detalles = false;
  b_carga = false;
  listado: Array < Datum >;
  params: parametros = {
    finca: 1,
    anio: 2020,
    semDesde: 1,
    semHasta: 52,
    id: 0,
    todos: false,
    showSelectGraf: false,
    grafica: 'none'
  }
  itemRacimo: Datum; // para pasar el item al hijo
  crear=false;


  constructor(
    private modalController: ModalController,
    private router:Router,
    private tabPage: TabmainPage,
    private storage: Storage,
    private produccionService: ProduccionService,
  ) { }

  ngOnInit() {
    this.b_carga = true;
    this.tabPage.titulo = "Control Racimos";
    this.getlista()
  }
  getlista() {
    this.storage.get('token').then(res=>{
      this.produccionService.listaProduccion(this.params.anio,this.params.semDesde, this.params.semHasta, this.params.finca, this.params.todos, res)
      .subscribe(
        res => {
          console.log('Consulta con finca: ', this.params.todos)
          console.log('respuesta listado', res)
          this.listado = res.data;
          this.b_carga = false;

        }
      );
    })
    
  }

  addCaja(){
    this.crear=true
    let racimo:Datum
    this.itemRacimo = racimo;
    this.b_lista = false;
    this.b_detalles = true;
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: FiltroEnfundePage,
      cssClass: 'custom-modal-finca',
      componentProps: {
        'nombre': 'calibraje',
        'parametros': this.params,
      }
    });
    await modal.present();
    const {
      data
    } = await modal.onDidDismiss();

    if (data) {
      if (!data.dismissed) {
        this.params = {
          anio: data.anio,
          finca: data.finca == "" ? 0 : data.finca,
          id: 0,
          semDesde: data.sem_desde,
          semHasta: data.sem_hasta,
          todos: data.todos,
          showSelectGraf: false,
          grafica: 'none'
        }
        console.log(data)
        this.b_carga = true;
        this.getlista()
      }
    }
  }
  ir(ruta:string, racimo: Datum){
    //this.router.navigate([ruta]);
    this.crear=false
    this.itemRacimo = racimo;
    this.b_lista = false;
    this.b_detalles = true;
  }
  closeDetalles(){
    //this.getlista()
    this.b_lista = true;
    this.b_detalles = false;
  }


}
