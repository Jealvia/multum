import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RacimoPage } from './racimo.page';

const routes: Routes = [
  {
    path: '',
    component: RacimoPage
  },
  {
    path: 'contador-racimo',
    loadChildren: () => import('./contador-racimo/contador-racimo.module').then( m => m.ContadorRacimoPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RacimoPageRoutingModule {}
