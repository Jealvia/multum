import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ContadorRacimoPage } from './contador-racimo.page';

describe('ContadorRacimoPage', () => {
  let component: ContadorRacimoPage;
  let fixture: ComponentFixture<ContadorRacimoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContadorRacimoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ContadorRacimoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
