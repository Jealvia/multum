import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ContadorRacimoPageRoutingModule } from './contador-racimo-routing.module';

import { ContadorRacimoPage } from './contador-racimo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContadorRacimoPageRoutingModule
  ],
  declarations: [ContadorRacimoPage],
  exports: [ContadorRacimoPage]
})
export class ContadorRacimoPageModule {}
