import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {  RootObject, ControlRacimo, RacimoColor, Datum, Racimos, DataRacimo } from '../structs/estructuras.service';
import { ActivatedRoute } from '@angular/router';
import { TabmainPage } from 'src/app/tab/tabmain/tabmain.page';
import { TotalesPage } from 'src/app/pages/modales/totales/totales.page';
import { ModalController } from '@ionic/angular';
import { OrdenCorte } from '../../ordendecorte/structs/estructuras.service';
import { Storage } from '@ionic/storage';
import { ProduccionService } from '@services/produccion.service';
@Component({
  selector: 'app-contador-racimo',
  templateUrl: './contador-racimo.page.html',
  styleUrls: ['./contador-racimo.page.scss'],
})
export class ContadorRacimoPage implements OnInit {

  
  showResta: boolean = false; showRechazo = false;
  @Input() produccion: Datum;
  @Output() close: EventEmitter<any> = new EventEmitter();
  racimo: Racimos;
  racimoColores: {tipo: string, cantidad: number}[] = [{ tipo: '', cantidad: 0}];

  
  boxColor: ControlRacimo =  { racimoColor : [
    {color: 'light', nombre: 'Blanco',cantidad: 0,rechazado:0, orden_corte_id: 0 },
    {color: 'primary', nombre: 'verde',cantidad: 0,rechazado:0, orden_corte_id: 0 },
    {color: 'warning', nombre: 'amarillo',cantidad: 0,rechazado:0, orden_corte_id: 0 },
    {color: 'danger', nombre: 'rojo',cantidad: 0,rechazado:0, orden_corte_id: 0 },
    {color: 'tertiary', nombre: 'azul',cantidad: 0,rechazado:0 , orden_corte_id: 0},
    {color: 'dark', nombre: 'negro',cantidad: 0,rechazado:0, orden_corte_id: 0 },
    ],
  totalRacimos: 0,
  totalRechazados: 0
  }
  
  @Input() crear=true;
  orden_corte:number=0;
  carga=false;
  seleccion=true;
  listado:Array<OrdenCorte>


  constructor(
    private modalController: ModalController,
    private produccionService: ProduccionService,
    private storage: Storage,
  ) { }

  ngOnInit() {
    if(this.crear){
      this.cargarDatos()
      this.racimo={
        //data_racimos
        ratio:0,
        total_racimos:0,
        recobro:0,
        total_racimos_rechazado:0,
        data_racimos:[{
          cantidad:0,
          color:"verde",
          edad:0
        },{
          cantidad:0,
          color:"blanco",
          edad:0
        },{
          cantidad:0,
          color:"amarillo",
          edad:0
        },{
          cantidad:0,
          color:"negro",
          edad:0
        },{
          cantidad:0,
          color:"rojo",
          edad:0
        },{
          cantidad:0,
          color:"azul",
          edad:0
        },
      ]
      }
    }else{
      this.racimo = this.produccion.racimos;
      console.log('cargando Detalles');
      console.log('racimo val: ', this.racimo)
      //Para eliminar el ultimo item color repetido, sumando su valor al primer elemente
      if (this.racimo.data_racimos.length == 7) {
        this.racimo.data_racimos[0].cantidad = this.racimo.data_racimos[0].cantidad + this.racimo.data_racimos[6].cantidad;
        this.racimo.data_racimos.pop();
      }
    }

    
  }

  seleccionOrdenCorte(id:number){
    this.orden_corte=id;
    console.log(id)
    this.seleccion=false
  }

  cargarDatos(){
    this.storage.get('token').then(res=>{
      this.produccionService.listaOrdenCorteRacimo(res).
        subscribe(
          res=>{
            console.log('respuesta listado', res)
            this.listado = res;
            console.log('respuesta listado', res)
            this.carga=false;
            
          }
        )
    })
  }

  enviarResultados(){
    console.log(this.racimo)
    console.log(this.orden_corte)
    let resultado={
      'listado':this.racimo.data_racimos,
      'orden_corte_id':this.orden_corte,
      'total_racimos_rechazado':this.racimo.total_racimos_rechazado
    }
    this.storage.get('token').then(res=>{
      this.produccionService.guardarRacimoControl(res,resultado)
      .subscribe(
        res => {
          console.log(res)
        }
      );
    })
  }

  back(){
    this.seleccion=true
    this.orden_corte=0
  }

  onClick(){
    console.log('texto');
  }

  itemOperador(restar: boolean, racimo?: DataRacimo, b_rechazo?: boolean){
    if (b_rechazo) {
      if (this.racimo.total_racimos_rechazado < 1 && restar) {
        return
      }
      restar? this.racimo.total_racimos_rechazado =  this.racimo.total_racimos_rechazado-1: this.racimo.total_racimos_rechazado = this.racimo.total_racimos_rechazado+1
      return;
    }
    if (restar) {
      if (racimo.cantidad < 1) {
        return
      }
      racimo.cantidad = racimo.cantidad-1;
      this.racimo.total_racimos = this.racimo.total_racimos-1
    } else {
      racimo.cantidad = racimo.cantidad+1;
      this.racimo.total_racimos = this.racimo.total_racimos+1
    }
    
  }

  sendData(){
    //this.commonService.msjConfirm('Por Favor Confirme',"Acepta estos valores de racimos cosechados y rechazados como válidos para la semana.")
    console.log('Data Racimo: ', this.boxColor)
    //this.presentModal() 
  }

  regresar(){
    this.close.emit(null);
  }
 
  async presentModal() {
    const modal = await this.modalController.create({
      component: TotalesPage,
      cssClass: 'custom-modal-finca',
      componentProps: {
        'tipo': 'Control Racimos',
        'data': this.produccion,
        'confirm': true,
      }
    });
    await modal.present();
    const {
      data
    } = await modal.onDidDismiss();

    if (data) {
      if (!data.dismissed) {
        console.log('no guardo')
        }
        console.log('guardo')
    }
    
  } 


}
/*
  itemSumar(racimo: RacimoColor, esRechazo: boolean){
    console.log('Sumo','rezhazo: ', esRechazo);
    if (esRechazo) {
      racimo.rechazado=racimo.rechazado+1
      this.boxColor.totalRechazados = this.boxColor.totalRechazados + 1;
    } else {
      racimo.cantidad=racimo.cantidad+1
      this.boxColor.totalRacimos = this.boxColor.totalRacimos + 1;
    }
  }
  itemRestar(racimo: RacimoColor, esRechazo: boolean){
    console.log('Resto','rezhazo: ', esRechazo);
    if ( !esRechazo &&  racimo.cantidad==0) {
      return
    }
    if ( esRechazo && racimo.rechazado==0) {
      return
    }
    if (esRechazo) {
      racimo.rechazado=racimo.rechazado-1
      this.boxColor.totalRechazados = this.boxColor.totalRechazados - 1;
    } else {
      racimo.cantidad=racimo.cantidad-1
      this.boxColor.totalRacimos = this.boxColor.totalRacimos - 1;
    }
  }
*/

//this.parsetArrayRacimoColor()
 /* 
    parsetArrayRacimoColor(){
    let i =0; //contador = 0
    let temRacimoColor: {tipo: string, cantidad: number};
    for (const key in this.racimo.data_racimos) {
      temRacimoColor = {tipo: key, cantidad: this.racimo.data_racimos[key]};
      console.log('claves: ',temRacimoColor, ' i: ', i)
      if (i >0) {
        this.racimoColores.push(temRacimoColor)
      } else {
        this.racimoColores[i] = temRacimoColor;
      }
      i++
    }
    console.log('Resultado: ', this.racimoColores);
  }
  */