import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FiltroEnfundePage } from './filtro-enfunde.page';

describe('FiltroEnfundePage', () => {
  let component: FiltroEnfundePage;
  let fixture: ComponentFixture<FiltroEnfundePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltroEnfundePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FiltroEnfundePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
