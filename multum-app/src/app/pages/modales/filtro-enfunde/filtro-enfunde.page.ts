import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { EnfundeService } from '@services/enfunde.service';
import { IdNombre, parametros, ShowGrafico } from '@services/estructura.modelo';
import { Storage } from '@ionic/storage';
import { anioS } from '@services/constants';

@Component({
  selector: 'app-filtro-enfunde',
  templateUrl: './filtro-enfunde.page.html',
  styleUrls: ['./filtro-enfunde.page.scss'],
})
export class FiltroEnfundePage implements OnInit {

  @Input() nombre:string='';
  @Input() b_estadisticas: boolean = false; 
  @Input() parametros: parametros;
  
  
  
  filtroForm: FormGroup;
  listFincas:Array<IdNombre>;
  todos=false;
  ano_general:any;
  anios = anioS;

  constructor(
    private modalController: ModalController,
    private formBuilder: FormBuilder,
    private enfundeService: EnfundeService,
    private storage: Storage
  ) { 
    this.filtroForm = this.formBuilder.group({
      grafico: ['', Validators.required],
      finca: ['', ],
      ano: ['', Validators.required],
      fechaDesde: ['', Validators.required],
      fechaHasta : ['', Validators.required],
       
    });
    
  }

  ngOnInit() {
    console.log('ngOnInit parametros: ', this.parametros)
    this.filtroForm.controls['grafico'].setValue(this.parametros.grafica);
    this.filtroForm.setValue({
      grafico: this.parametros.grafica,
      finca: this.parametros.finca,
      ano: this.parametros.anio, //sumo 1 porque por algun motivo el picker lo resta 
      fechaDesde: this.parametros.semDesde,
      fechaHasta: this.parametros.semHasta,
    })
    let hoy: Date = new Date();
    this.ano_general=hoy.getFullYear();
    console.log(this.ano_general)
    this.listaFincas();
    console.log('filtros: ', this.filtroForm.value.ano)
  }

  listaFincas(){
    this.storage.get('token').then(res=>{
      this.enfundeService.listaFincas(res).subscribe(
        res=>{
          this.listFincas = res;
          console.log(res)
        }
      );
    })
    
  }

  onSubmit(){
    this.parametros.todos = true;
    console.log('filtros: ', this.filtroForm.value.finca)
    this.modalController.dismiss(
      { 
        'dismissed': false,
        'finca':this.filtroForm.value.finca,
        'anio': this.filtroForm.value.ano,
        'sem_desde':this.filtroForm.value.fechaDesde,
        'sem_hasta':this.filtroForm.value.fechaHasta,
        'todos': this.todos,
        'b_estadisticas': this.b_estadisticas,
        'grafica': this.parametros.grafica 
      }
    );
  }

  salir(){
    this.modalController.dismiss(
      {'dismissed': true}
    );
  } 
  selectGraf(){
    this.parametros.grafica = this.filtroForm.value.grafico; 
    console.log('elemento: ', this.parametros.grafica)
    
  }



}
