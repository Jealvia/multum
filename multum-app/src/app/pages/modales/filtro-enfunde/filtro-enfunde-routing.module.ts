import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FiltroEnfundePage } from './filtro-enfunde.page';

const routes: Routes = [
  {
    path: '',
    component: FiltroEnfundePage
  }
];

@NgModule({
  imports: [
    /* RouterModule.forChild(routes) */
  ],
  exports: [RouterModule],
})
export class FiltroEnfundePageRoutingModule {}
