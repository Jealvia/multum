import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FiltroEnfundePageRoutingModule } from './filtro-enfunde-routing.module';

import { FiltroEnfundePage } from './filtro-enfunde.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FiltroEnfundePageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [FiltroEnfundePage]
})
export class FiltroEnfundePageModule {}
