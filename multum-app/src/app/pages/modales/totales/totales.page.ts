import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Datum } from '../../menu/racimo/structs/estructuras.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-totales',
  templateUrl: './totales.page.html',
  styleUrls: ['./totales.page.scss'],
})
export class TotalesPage implements OnInit {

  // Data del Padre
  @Input() tipo: string;
  @Input() data: Datum; 
  @Input() confirm: boolean = false;

  
  totalCajasFisicas: number =0; totalCajasReferencia: number = 0; pesoreferencia: number = 43.00;
  pesoReferencia: number = 0;

  constructor(
    private modalController: ModalController,
    private router:Router,
  ) { }

  ngOnInit() {
    this.valorPesoReferencia()
  }

  valorPesoReferencia(){
    let i=0;
    if (this.tipo == 'Control Cajas') {
      let temPesoCaja: string[];
      //this.totalCajasReferencia
      temPesoCaja = Object.keys(this.data.cajas.listado_cajas);
      for (const key in this.data.cajas.listado_cajas) {
        this.totalCajasReferencia = this.totalCajasReferencia + (parseFloat(temPesoCaja[i])*this.data.cajas.listado_cajas[key])
        this.totalCajasFisicas = this.totalCajasFisicas + this.data.cajas.listado_cajas[key];
        console.log('key: ', key,' referencia: ', this.totalCajasReferencia, ' indice: ',i, ' cajasFisicas: ', this.totalCajasFisicas)
        i=i+1;
      }
      this.totalCajasReferencia = this.totalCajasReferencia/this.pesoreferencia;
      this.pesoReferencia = this.totalCajasReferencia/this.totalCajasFisicas;
    }
  }

  salir(){
    this.modalController.dismiss( );
  
  } 

  seleccion(ruta:string){
    this.router.navigate([ruta]);
    this.modalController.dismiss( );
    /*
    let page: string;
    if (this.tipo == 'Control Cajas') {
      page = 'cajas'
    }
    if (this.tipo == 'Control Racimos') {
      page = 'racimo'
    }
    
    this.router.navigate([ruta + '/'+ page]);
    console.log(ruta + '/'+ page)
    */
  }

  
}

/* 
controlRacimos: {
    cintaLista: {
      color: string,
      nombre: string,
      cantidad: number,
      semana: number,
    }[],
    totalCosechados: number,
    totalRechazados: number
  } = { cintaLista : [
    {color: 'light', nombre: 'blanco',cantidad: 400, semana: 8 },
    {color: 'primary', nombre: 'verde',cantidad: 500, semana: 9},
    {color: 'warning', nombre: 'amarillo',cantidad: 250, semana: 10},
    {color: 'danger', nombre: 'rojo',cantidad: 350, semana: 11 },
    {color: 'tertiary', nombre: 'azul',cantidad: 450, semana: 12},
    {color: 'dark', nombre: 'negro',cantidad: 650, semana: 13 },
    {color: 'light', nombre: 'blanco',cantidad: 400, semana: 8 },
    ],
    totalCosechados: 10,
    totalRechazados: 20
  } 
  //totalCajas: number; totalSegunda:number;

   controlCajas:{
    cajasLista: {
      tipo: number, cajas: number
    }[], 
    cajasSegunda: number,
    totalCajas: number,
    totalCajasSegunda: number
  } = {
    cajasLista: [
      {tipo: 37.48, cajas: 0 },
      {tipo: 40.50, cajas: 0 },
      {tipo: 41.50, cajas: 2160 },
      {tipo: 43.00, cajas: 240 },
    ],
    cajasSegunda: 50,
    totalCajas: 2301,
    totalCajasSegunda: 0
  }
*/
