import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as Constants from '../../../services/constants';
import {  Chart } from 'chart.js';
import { listaEnfunde } from '@services/estructura.modelo';
@Injectable({
  providedIn: 'root'
})
export class EstListadosService {

  constructor(
    private http: HttpClient,
  ) { }

  getEstEnfunde(listado: Array < listaEnfunde >, grafica: any): any{
    let labels: Array < any >= [], data: Array < any >= [], data1: Array < any >= [];
    let bars: any;
    if (listado) {
      //console.log('en getEstEnfunde Listado: ',listado)
      for (let index = listado.length-1; index >=0; index--) {
        const element = listado[index];
        let tmpLabel=element.anio+" - "+ element.semana
        labels.push(tmpLabel)
        data.push(element.cantidad)
        data1.push(element.cantidad+100)

      }
    }
    bars = new Chart(grafica.nativeElement, {
      type: 'line',
      data: {
        labels: labels,
        datasets: [{
          label: 'Enfunde por semana',
          data: data,
          backgroundColor: "transparent",
          pointBorderColor: "rgb(38, 194, 129)",
          pointBackgroundColor: "rgb(38, 194, 129)",
          borderColor: 'rgb(38, 194, 129)', // array should have same number of elements as number of dataset
          borderWidth: 1
        }
      ]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              //beginAtZero: true
            }
          }]
        }
      }
    });
    return bars
  }

  graficaMultilinea(listado: any, grafica: any): any{
    let labels: Array < any >= [], data: Array < any >= [], data1: Array < any >= [];
    let bars: any; let dataElementos: any;
    //console.log('entro en funcion multilinea: ', listado)

    bars = new Chart(grafica.nativeElement, {
      type: 'line',
      data: {
        labels: listado.labels,
        datasets: [
        {
          label: '8SEM',
          lineTension: 0,
          data: listado.datos["8"],
          backgroundColor: "transparent",
          pointBorderColor: "red",
          pointBackgroundColor: "red",
          borderColor: 'red', // array should have same number of elements as number of dataset
          borderWidth: 1
        },
        {
          label: '9SEM',
          lineTension: 0,
          data: listado.datos["9"],
          backgroundColor: "transparent",
          pointBorderColor: "rgb(38, 194, 129)",
          pointBackgroundColor: "rgb(38, 194, 129)",
          borderColor: 'rgb(38, 194, 129)', // array should have same number of elements as number of dataset
          borderWidth: 1
        },
        {
          label: '10SEM',
          lineTension: 0,
          data: listado.datos["10"],
          backgroundColor: "transparent",
          pointBorderColor: "blue",
          pointBackgroundColor: "blue",
          borderColor: 'blue', // array should have same number of elements as number of dataset
          borderWidth: 1
        },
        {
          label: '11SEM',
          lineTension: 0,
          data: listado.datos["11"],
          backgroundColor: "transparent",
          pointBorderColor: "yellow",
          pointBackgroundColor: "yellow",
          borderColor: 'yellow', // array should have same number of elements as number of dataset
          borderWidth: 1
        },
        {
          label: '12SEM',
          lineTension: 0,
          data: listado.datos["12"],
          backgroundColor: "transparent",
          pointBorderColor: "black",
          pointBackgroundColor: "black",
          borderColor: 'black', // array should have same number of elements as number of dataset
          borderWidth: 1
        },
        {
          label: '13SEM',
          lineTension: 0,
          data: listado.datos["13"],
          backgroundColor: "transparent",
          pointBorderColor: "orange",
          pointBackgroundColor: "orange",
          borderColor: 'orange', // array should have same number of elements as number of dataset
          borderWidth: 1
        },
        {
          label: '14SEM',
          lineTension: 0,
          data: listado.datos["14"],
          backgroundColor: "transparent",
          pointBorderColor: "rgb(38, 194, 129)",
          pointBackgroundColor: "rgb(38, 194, 129)",
          borderColor: 'rgb(38, 194, 129)', // array should have same number of elements as number of dataset
          borderWidth: 1
        }
      ]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              //beginAtZero: true
            }
          }]
        }
      }
    });
    return bars
  }

  graficaCajas(listado: any, grafica: any){
    let labels: Array < any >= [], data: Array < any >= [], data1: Array < any >= [];
    let bars: any;
    let dataElementos = [
      {
        label: 'Cajas', 
        data: listado.datos,
        backgroundColor: "transparent",
        pointBorderColor: "orange",
        pointBackgroundColor: "orange",
        borderColor: 'orange', // array should have same number of elements as number of dataset
        borderWidth: 1
      }
    ]
    bars = new Chart(grafica.nativeElement, {
      type: 'line',
      data: {
        labels: listado.labels,
        datasets: dataElementos
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              //beginAtZero: true
            }
          }]
        }
      }
    });
    return bars
  }

  graficaRatio(listado: any, grafica: any){
    let bars: any;
    let dataElementos = [
      {
        label: 'Ratio', 
        data: listado.datos,
        backgroundColor: "transparent",
        pointBorderColor: "rgb(38, 194, 129)",
        pointBackgroundColor: "rgb(38, 194, 129)",
        borderColor: 'rgb(38, 194, 129)', // array should have same number of elements as number of dataset
        borderWidth: 1
      }
    ]
    bars = new Chart(grafica.nativeElement, {
      type: 'line',
      data: {
        labels: listado.labels,
        datasets: dataElementos
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              //beginAtZero: true
            }
          }]
        }
      }
    });
    return bars
  }

  graficaGeneralNombre(listado: any, grafica: any, nombre: string){
    let bars: any;
    let dataElementos = [
      {
        label: nombre, 
        data: listado.datos,
        backgroundColor: "transparent",
        pointBorderColor: "rgb(38, 194, 129)",
        pointBackgroundColor: "rgb(38, 194, 129)",
        borderColor: 'rgb(38, 194, 129)', // array should have same number of elements as number of dataset
        borderWidth: 1
      }
    ]
    bars = new Chart(grafica.nativeElement, {
      type: 'line',
      data: {
        labels: listado.labels,
        datasets: dataElementos
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              //beginAtZero: true
            }
          }]
        }
      }
    });
    return bars
  }


  // para prosimo desarrollo 
  graficaDinamica(listado: any, grafica: any): any{
    let labels: Array < any >= [], data: Array < any >= [], data1: Array < any >= [];
    let bars: any;
    if (listado) {
      //console.log('en getEstEnfunde Listado: ',listado)
      for (let index = listado.length-1; index >=0; index--) {
        const element = listado[index];
        let tmpLabel=element.anio+" - "+ element.semana
        labels.push(tmpLabel)
        data.push(element.cantidad)
        data1.push(element.cantidad+100)

      }
    }
    let dataElementos = [
      {
        label: '8SEM', //'Enfunde por semana'
        data: [0],
        backgroundColor: "transparent",
        pointBorderColor: "rgb(38, 194, 129)",
        pointBackgroundColor: "rgb(38, 194, 129)",
        borderColor: 'rgb(38, 194, 129)', // array should have same number of elements as number of dataset
        borderWidth: 1
      },
      {
        label: '9SEM', //'Enfunde por semana'
        data: [0],
        backgroundColor: "transparent",
        pointBorderColor: "rgb(38, 194, 129)",
        pointBackgroundColor: "rgb(38, 194, 129)",
        borderColor: 'rgb(38, 194, 129)', // array should have same number of elements as number of dataset
        borderWidth: 1
      },
      {
        label: '10SEM', //'Enfunde por semana'
        data: [0,3,8],
        backgroundColor: "transparent",
        pointBorderColor: "rgb(38, 194, 129)",
        pointBackgroundColor: "rgb(38, 194, 129)",
        borderColor: 'rgb(38, 194, 129)', // array should have same number of elements as number of dataset
        borderWidth: 1
      }
    ]
    bars = new Chart(grafica.nativeElement, {
      type: 'line',
      data: {
        labels: listado.labels,
        datasets: dataElementos
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              //beginAtZero: true
            }
          }]
        }
      }
    });
    
    /*
    console.log('el retorno es: ', bars)
    let otroItem = {
      label: '9SEM', //'Enfunde por semana'
          data: data,
          backgroundColor: "transparent",
          pointBorderColor: "rgb(38, 194, 129)",
          pointBackgroundColor: "rgb(38, 194, 129)",
          borderColor: 'rgb(38, 194, 129)', // array should have same number of elements as number of dataset
          borderWidth: 1
    }
    bars.data.datasets.push(otroItem)
    */
    //console.log('**el retorno es: ', bars)
    return bars
  }

}