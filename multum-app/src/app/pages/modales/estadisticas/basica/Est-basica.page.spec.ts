import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EstBasicaPage } from './Est-basica.page';

describe('EstBasicaPage', () => {
  let component: EstBasicaPage;
  let fixture: ComponentFixture<EstBasicaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstBasicaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EstBasicaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
