import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EstBasicaPage } from './Est-basica.page';

const routes: Routes = [
  {
    path: '',
    component: EstBasicaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BasicaPageRoutingModule {}
