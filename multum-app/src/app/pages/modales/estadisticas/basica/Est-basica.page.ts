import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { listaEnfunde, parametros, ShowGrafico } from '@services/estructura.modelo';
import {  Chart } from 'chart.js';
import { ModalController, IonContent } from '@ionic/angular';
import { EstListadosService } from '../est-Listados.service';
import { Storage } from '@ionic/storage';
import { EstadisticasService } from '@services/estadisticas.service';
import { EnfundeService } from '@services/enfunde.service';

import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
@Component({
  selector: 'app-basica',
  templateUrl: './Est-basica.page.html',
  styleUrls: ['./Est-basica.page.scss'],
})
export class EstBasicaPage implements OnInit {

  @ViewChild('EnfundeChart') EnfundeChart;
  @ViewChild('inventarioChart') inventarioChart;
  @ViewChild('cosechaChart') cosechaChart;
  @ViewChild('cajasChart') cajasChart;
  @ViewChild('ratioChart') ratioChart; 
  @ViewChild('tendenciaChart') tendenciaChart;
  @ViewChild('content') contenedor;
  
  @Input() params: parametros;
  showGraficos: ShowGrafico = {
    inventarioFinal: false,
    cosecha: false,
    caja: false,
    ratio: false,
    tendencia: false,
    enfunde: false
  } 
  mensajeOrientacion:string; 
  orientacionTipo: string = ''; orientacion: string;
  orientacionSelec: {
    PORTRAIT_PRIMARY: string;
    PORTRAIT_SECONDARY: string;
    LANDSCAPE_PRIMARY: string;
    LANDSCAPE_SECONDARY: string;
    PORTRAIT: string;
    LANDSCAPE: string;
    ANY: string;
  }

  listadoCosecha: Array<any> =  []

  bars: any;
  colorArray: any;
  constructor(
    private modalController: ModalController,
    private EstListados: EstListadosService,
    private Estadisticas: EstadisticasService,
    private storage: Storage,
    private enfundeService: EnfundeService,
    private screenOrientation: ScreenOrientation
  ) {
    this.mensajeOrientacion = "la orientacion es: " + this.screenOrientation.type;
    this.orientacionSelec = this.screenOrientation.ORIENTATIONS;
    console.log('ori ',this.orientacionSelec)
    
    this.screenOrientation.onChange().subscribe(
      () => {
          console.log("la orientacion cambio: " , this.screenOrientation.type);
          this.mensajeOrientacion = "la orientacion cambio: " + this.screenOrientation.type;
          
      }
    );

    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE).then(function success() {
      console.log("Successfully locked the orientation");
      let mensaje = 'logrado giro de pantala'
      //return mensaje;
      
    }, function error(errMsg) {
          console.log("Error locking the orientation :: " + errMsg);
          let mensaje = 'No se logro el giro de pantalla'
          let alto: number = screen.height, ancho: number = screen.width;
          console.log('ancho: ', screen.width, 'alto: ', screen.height, 'orientacion: ', screen.orientation.type , 'orientacion: ');
          alto = alto - (alto * 0.2); ancho = ancho - (ancho * 0.2);
          document.getElementById('contenido').style.width = alto+'px';
          document.getElementById('contenido').style.height = ancho+'px';
          document.getElementById('contenido').style.transform = "rotate(270deg)";
          document.getElementById('marco').style.marginTop = (alto * 0.12) + 'px';
          
          //return mensaje;
    });

  }
  ionViewDidEnter() {
    this.getlistadoCosecha();
  }
  ngOnInit() {
  }
  onSelect(event) {
    console.log(event);
  }

  salir(){
    this.modalController.dismiss(
      {'dismissed': true}
    );
  } 

  pantalla(){
    console.log('la orientacion select es: ', this.orientacion, 'girando pantalla: ', this.screenOrientation.type)
    
      this.orientacionTipo = this.screenOrientation.type;
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE).then(function success() {
        console.log("Successfully locked the orientation");
        let mensaje = 'logrado giro de pantala'
        return mensaje;
        
        }, function error(errMsg) {
            console.log("Error locking the orientation :: " + errMsg);
            let mensaje = 'No se logro el giro de pantalla'
            return mensaje;
        });
  }

  getlistadoCosecha(){
    
    let estaditicasEnfunde: any, estaditicasInventarios: any, listCajas: any, listRatio: any;
    console.log('listado consola')

    this.selectGraf()

     this.storage.get('token').then(res=>{

      //Inventarios Enfunde
      if (this.showGraficos.enfunde == true) {
        this.enfundeService.listaEnfunde(this.params.anio, this.params.semDesde, this.params.semHasta, this.params.finca, this.params.todos,res)
        .subscribe(
          res => {
            console.log('enfunde res : ', res.data)
            estaditicasEnfunde = res.data;
            this.bars = this.EstListados.getEstEnfunde(estaditicasEnfunde, this.EnfundeChart)
          }
        );
      }

      //inventario Final
      if (this.showGraficos.inventarioFinal == true) {
        this.Estadisticas.estNombre(res,'inventario', this.params.anio, this.params.semDesde, this.params.semHasta).subscribe(
          res => {
            console.log('inventario res : ', res)
            estaditicasInventarios = res;
            this.bars = this.EstListados.graficaMultilinea(estaditicasInventarios, this.inventarioChart)
          }
        );
      }
      

      //Cocecha
      if (this.showGraficos.cosecha == true) {
        this.Estadisticas.estNombre(res,'cosecha', this.params.anio, this.params.semDesde, this.params.semHasta).subscribe(
          res => {
            console.log('cosecha res : ', res)
            this.listadoCosecha = res;
            this.bars = this.EstListados.graficaMultilinea(this.listadoCosecha, this.cosechaChart)
          }
        );
      }
      
      //caja
      if (this.showGraficos.caja == true) {
        this.Estadisticas.estNombre(res, 'caja', this.params.anio, this.params.semDesde, this.params.semHasta).subscribe(
          res => {
            console.log('cajas res : ', res)
            listCajas= res;
            this.bars = this.EstListados.graficaCajas(listCajas, this.cajasChart)
          }
        );
      }
      
      
      //listRatio
      if (this.showGraficos.ratio == true) {
        this.Estadisticas.estNombre(res, 'ratio', this.params.anio, this.params.semDesde, this.params.semHasta).subscribe(
          res => {
            console.log('ratio res : ', res)
            listRatio= res;
            this.bars = this.EstListados.graficaRatio(listRatio, this.ratioChart)
          }
        );
      }

      //Tendencia
      if (this.showGraficos.tendencia == true) {
        this.Estadisticas.estNombre(res, 'tendencia', this.params.anio, this.params.semDesde, this.params.semHasta).subscribe(
          res => {
            console.log('ratio res : ', res)
            listRatio= res;
            this.bars = this.EstListados.graficaGeneralNombre(listRatio, this.tendenciaChart, 'Tendencias')
          }
        );
      }

    })
  }

  selectGraf(){
    this.params.grafica 
    console.log('elemento: ', this.params.grafica)
    //debugger
    for (const key in this.showGraficos) {
      if (this.params.grafica == key) {
         this.showGraficos[key] = true;
         break
      }
    }
    console.log('11',this.showGraficos )
  }


}


/*
 dataEnfunde(listado: Array < listaEnfunde >){
    let tmpListaDatos = []
    if (listado) {
      console.log('Listado: ',listado)
      for (let index = listado.length-1; index >=0; index--) {
        const element = listado[index];
        let tmpLabel=element.anio+" - "+ element.semana
        let tmp = {
          "x": element.cantidad,
          "y": tmpLabel
        }
        let tmp1 = {
          "x": element.cantidad+100,
          "y": tmpLabel
        }
        //tmpListaDatos.push(tmp)
        this.labels.push(tmpLabel)
        this.data.push(element.cantidad)
        this.data1.push(element.cantidad+100)
      } 
    }
    let datos = {
      "name": "Enfunde",
      "series": tmpListaDatos
    }
    this.listadoDatos.push(datos)
    this.bars = new Chart(this.barChart.nativeElement, {
      type: 'line',
      data: {
        labels: this.labels,
        datasets: [{
          label: 'Enfunde por semana',
          data: this.data,
          backgroundColor: "transparent",
          pointBorderColor: "rgb(38, 194, 129)",
          pointBackgroundColor: "rgb(38, 194, 129)",
          borderColor: 'rgb(38, 194, 129)', 
          borderWidth: 1
        }
      ]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
             
            }
          }]
        }
      }
    });
    console.log('listado2: ',this.listado2)
  }
*/