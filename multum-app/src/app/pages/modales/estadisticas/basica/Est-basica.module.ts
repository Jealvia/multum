import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BasicaPageRoutingModule } from './Est-basica-routing.module';

import { EstBasicaPage } from './Est-basica.page';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BasicaPageRoutingModule
  ],
  declarations: [EstBasicaPage],
  providers: [
    ScreenOrientation,
  ]
})
export class BasicaPageModule {}
