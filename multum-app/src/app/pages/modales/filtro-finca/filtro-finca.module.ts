import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FiltroFincaPageRoutingModule } from './filtro-finca-routing.module';

import { FiltroFincaPage } from './filtro-finca.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FiltroFincaPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [FiltroFincaPage]
})
export class FiltroFincaPageModule {}
