import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FiltroFincaPage } from './filtro-finca.page';

describe('FiltroFincaPage', () => {
  let component: FiltroFincaPage;
  let fixture: ComponentFixture<FiltroFincaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltroFincaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FiltroFincaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
