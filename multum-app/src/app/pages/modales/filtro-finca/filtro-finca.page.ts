import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FincaService } from '@services/finca.service';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-filtro-finca',
  templateUrl: './filtro-finca.page.html',
  styleUrls: ['./filtro-finca.page.scss'],
})
export class FiltroFincaPage implements OnInit {

  listTipoEnfunde:any;
  filtroForm: FormGroup;

  constructor(
    private modalController: ModalController,
    private formBuilder: FormBuilder,
    private fincaService:FincaService,
    private storage: Storage,
  ) {
    this.filtroForm = this.formBuilder.group({
      tipo: ['', Validators.required],
      fechaDesde: ['', Validators.required],
      fechaHasta : ['', Validators.required],
       
    });
   }

  ngOnInit() {
    this.cargarTipoFincas();
  }

  cargarTipoFincas(){
    this.storage.get('token').then(res=>{
      this.fincaService.listTipoFinca(res).subscribe(
        res=>{
          this.listTipoEnfunde = res;
          console.log(this.listTipoEnfunde)
        }
      )
    })
    
  }

  onSubmit(){
    this.modalController.dismiss(
      {
        'dismissed': false,
        'tipo':this.filtroForm.value.tipo,
        'fechaDesde':this.filtroForm.value.fechaDesde,
        'fechaHasta':this.filtroForm.value.fechaHasta
      }
    );
  }

  salir(){
    this.modalController.dismiss(
      {'dismissed': true}
    );
  } 

  

}
