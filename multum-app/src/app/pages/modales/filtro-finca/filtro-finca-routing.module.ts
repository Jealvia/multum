import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FiltroFincaPage } from './filtro-finca.page';

const routes: Routes = [
  {
    path: '',
    component: FiltroFincaPage
  }
];

@NgModule({
  imports: [
    /* RouterModule.forChild(routes) */
  ],
  exports: [RouterModule],
})
export class FiltroFincaPageRoutingModule {}
