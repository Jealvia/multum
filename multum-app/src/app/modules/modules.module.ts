import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
/* Formularios */
import { FormsModule, ReactiveFormsModule, FormGroup, FormBuilder } from '@angular/forms';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule, 

  ],
  exports: [
    FormsModule, ReactiveFormsModule, 
  ]
})
export class ModulesModule { }
