// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBin75iIJsYVsE1ZBUUbfBTT_pl5T0y-KE",
    authDomain: "multum-5dbee.firebaseapp.com",
    databaseURL: "https://multum-5dbee.firebaseio.com",
    projectId: "multum-5dbee",
    storageBucket: "multum-5dbee.appspot.com",
    messagingSenderId: "869628666553",
    appId: "1:869628666553:web:190b4f33e1ff8510887168"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
