import {
  Component
} from '@angular/core';
import {
  AngularFireMessaging
} from '@angular/fire/messaging';
import {
  mergeMapTo
} from 'rxjs/operators';
import {
  NotificacionesService
} from './services/notificaciones.service';

//import * from 'firebase';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  messaging;
  constructor(
    private afMessaging: AngularFireMessaging,
  ) {
    //this.requestPermission();
    /* this.messaging.onMessage((payload) => {
      console.log("Message received. ", payload);
    }); */
    this.listen()
    //this.escucha()

  }

  ngOnInit() {

  }

  escucha() {
    this.afMessaging.messaging.subscribe(
      (_messaging) => {
        this.messaging = _messaging;
        console.log(_messaging)
        _messaging.onMessage =_messaging.onMessage.bind(_messaging);
        //_messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
      }
    )
  }

  
  listen() {
    this.afMessaging.messaging
      .subscribe((message) => { 
        
        message.onMessage.bind(this.fun)
        console.log(message); 
        
      });
  }

  fun(data){
    console.log(data)

  }

}
//https://gitlab.com/kaptea-tutorials/angular/push-notifications/blob/master/package-lock.json
//https://firebase.google.com/docs/cloud-messaging/js/receive
//https://www.youtube.com/watch?v=z27IroVNFLI&feature=emb_logo
//https://www.youtube.com/watch?time_continue=431&v=BsCBCudx58g&feature=emb_logo
//https://github.com/angular/angularfire
/* https://fcm.googleapis.com/fcm/send

Content-Type
application/json
Authorization
key=AAAAynnoYrk:APA91bGc6URgZHb3JmM4mBZEfAeFpHSYDT6P7OwNoaoaeM-Z3uTBhi3wRc8rgXgne1JkzibGFb5IFlbcVdRutO1z2CN8T1Q70pcEc-gNqbaqsngB-9JCER9ClOZYy7BTjjJY3gh7Dy87
{
	"notification":{
		"title":"Titulo 1",
		"body":"Cuerpo prueba"
	},
	"to":"cwizOwKnC63EgJ13zzt981:APA91bF--8NwzyFrGBQaCLM1YzKG0Vb7-5Z_Ss7Qo8EgBHTEroQYkWZCd74cpmIYw4VOAr4ox8B06LlttGo2dgkg-LfjRfERKa7Ww3lHyZkZJXnArsi34obiU8gGpy0TTTsluG-2B9jH"
	
} */