export interface estadisticasDetalles {
    nombredet?: string;
    ordendecorte: number, 
    evaluacion: number, 
    longitudminima: number, 
    longitudmaxima: number, 
    dedoscaja: number, 
    clusterscaja: number, 
    tipopallet: string, 
    dimensionpallet: string, 
    tipoplastico: string, 
    sellado: string
  }
  
  export interface PeriodicElement {
    codigo: string;
    nombre: string;
    asignadas: number;
    declaradas: number;
    rechazadas: number;
    faltantes: number;  
    ingresadas: number;
    calidad: number;  
    expand: estadisticasDetalles[];
  }
  export var finca_DATA: PeriodicElement[] = [
  {
    codigo: '5',
    nombre: 'LA PATRICIA',
    asignadas: 892,
    declaradas: 892,
    rechazadas: 0,
    faltantes: 0,
    ingresadas: 892,
    calidad: 93.190000,
    expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
    {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
    {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
    ]
  }, {
    codigo: '3',
    nombre: 'SAUCES',
    asignadas: 122,
    declaradas: 122,
    rechazadas: 0,
    faltantes: 0,
    ingresadas: 122,
    calidad: 92.630000,
    expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
    {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
    ]
  }, {
    codigo: '4',
    nombre: 'LA GUARICO',
    asignadas: 270,
    declaradas: 270,
    rechazadas: 0,
    faltantes: 0,
    ingresadas: 270,
    calidad: 92.560000,
    expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
    {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
    {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
    ]
  }, {
    codigo: '190',
    nombre: 'VERGELES',
    asignadas: 398,
    declaradas: 398,
    rechazadas: 0,
    faltantes: 0,
    ingresadas: 398,
    calidad: 91.57500,
    expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
    {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
    {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
    ]
  }, {
    codigo: '186',
    nombre: 'LA SERAFINA',
    asignadas: 501,
    declaradas: 501,
    rechazadas: 0,
    faltantes: 0,
    ingresadas: 501,
    calidad: 91.486666,
    expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
    {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
    ]
  }, {
    codigo: '173',
    nombre: 'CHAGUANA',
    asignadas: 553,
    declaradas: 253,
    rechazadas: 0,
    faltantes: 0,
    ingresadas: 253,
    calidad: 91.275000,
    expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
    {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
    {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
    ]
  }, {
    codigo: '158',
    nombre: 'LA MINA',
    asignadas: 432,
    declaradas: 432,
    rechazadas: 0,
    faltantes: 0,
    ingresadas: 432,
    calidad: 90.396666,
    expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
    {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
    ]
  }, {
    codigo: '151',
    nombre: 'LAGO VERDE',
    asignadas: 50,
    declaradas: 50,
    rechazadas: 0,
    faltantes: 0,
    ingresadas: 50,
    calidad: 90.240000,
    expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
    {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
    {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
    ]
  },
];
export var productores_DATA: PeriodicElement[] = [
    {
      codigo: '00107',
      nombre: 'SANTOS NORBERTO SARMIENTO ARIAS',
      asignadas: 892,
      declaradas: 892,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 892,
      calidad: 93.190000,
      expand: [{ ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
      {ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '00116',
      nombre: 'CELIA EUFEMIA AVILA GRANDA',
      asignadas: 122,
      declaradas: 122,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 122,
      calidad: 92.630000,
      expand: [{ ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '00103',
      nombre: 'HUGO PATRICIO AVILA GRANDA',
      asignadas: 270,
      declaradas: 270,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 270,
      calidad: 92.560000,
      expand: [{ ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '00850',
      nombre: 'MARIO ALADINO SARMIENTO AVILA',
      asignadas: 398,
      declaradas: 398,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 398,
      calidad: 91.57500,
      expand: [{ordendecorte: 292437, evaluacion: 428202, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}]
    }, {
      codigo: '00298',
      nombre: 'AURORA DEL CISNE AGUILAR CASTILLO',
      asignadas: 501,
      declaradas: 501,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 501,
      calidad: 91.486666,
      expand: [{ ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '00731',
      nombre: 'JULIO CESAR RODRIGUEZ PAREDES',
      asignadas: 553,
      declaradas: 253,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 253,
      calidad: 91.275000,
      expand: [{ ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      { ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '00109',
      nombre: 'OLGUER AUGUSTO AVILA GRANDA',
      asignadas: 432,
      declaradas: 432,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 432,
      calidad: 90.396666,
      expand: [{ordendecorte: 292442, evaluacion: 428230, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}]    
    }, {
      codigo: '00713',
      nombre: 'ANGEL RIGOBERTO PAGUAY AVILA',
      asignadas: 50,
      declaradas: 50,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 50,
      calidad: 90.240000,
      expand: [{ ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    },
  ];
export var Marca_DATA: PeriodicElement[] = [
    {
      codigo: '5',
      nombre: 'NATURE PREMIUM',
      asignadas: 892,
      declaradas: 892,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 892,
      calidad: 93.190000,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '3',
      nombre: 'IREN PREMIUM',
      asignadas: 122,
      declaradas: 122,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 122,
      calidad: 92.630000,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '4',
      nombre: 'OKE FLO FAIR TRADE',
      asignadas: 270,
      declaradas: 270,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 270,
      calidad: 92.560000,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '190',
      nombre: 'BIO FT TAPED',
      asignadas: 398,
      declaradas: 398,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 398,
      calidad: 91.57500,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '186',
      nombre: 'FLO MH PRONTO',
      asignadas: 501,
      declaradas: 501,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 501,
      calidad: 91.486666,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '173',
      nombre: 'EQUIFRUIT ORGANICO',
      asignadas: 553,
      declaradas: 253,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 253,
      calidad: 91.275000,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '158',
      nombre: 'EQUAL EXCHANGE',
      asignadas: 432,
      declaradas: 432,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 432,
      calidad: 90.396666,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '151',
      nombre: 'BIO FLO CINTA',
      asignadas: 50,
      declaradas: 50,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 50,
      calidad: 90.240000,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    },
  ];
export var vapor_DATA: PeriodicElement[] = [
    {
      codigo: '5',
      nombre: 'LIMARI',
      asignadas: 892,
      declaradas: 892,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 892,
      calidad: 93.190000,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '3',
      nombre: 'NORTHERN DEFENDER',
      asignadas: 122,
      declaradas: 122,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 122,
      calidad: 92.630000,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '4',
      nombre: 'MSC NORA',
      asignadas: 270,
      declaradas: 270,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 270,
      calidad: 92.560000,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '190',
      nombre: 'SUNRISE',
      asignadas: 398,
      declaradas: 398,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 398,
      calidad: 91.57500,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '186',
      nombre: 'BALTIC',
      asignadas: 501,
      declaradas: 501,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 501,
      calidad: 91.486666,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '173',
      nombre: 'RAM VINCES',
      asignadas: 553,
      declaradas: 253,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 253,
      calidad: 91.275000,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '158',
      nombre: 'ANTILEN',
      asignadas: 432,
      declaradas: 432,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 432,
      calidad: 90.396666,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '151',
      nombre: 'AQUAVIT',
      asignadas: 50,
      declaradas: 50,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 50,
      calidad: 90.240000,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    },
  ];
export var puerto_DATA: PeriodicElement[] = [
    {
      codigo: '5',
      nombre: 'Puerto de Oakland',
      asignadas: 892,
      declaradas: 892,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 892,
      calidad: 93.190000,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '3',
      nombre: 'Puerto de Seattle-Tacoma',
      asignadas: 122,
      declaradas: 122,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 122,
      calidad: 92.630000,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '4',
      nombre: 'Puerto de Long Beach',
      asignadas: 270,
      declaradas: 270,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 270,
      calidad: 92.560000,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '190',
      nombre: 'Puerto de Valencia',
      asignadas: 398,
      declaradas: 398,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 398,
      calidad: 91.57500,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '186',
      nombre: 'Puerto de Los Ángeles',
      asignadas: 501,
      declaradas: 501,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 501,
      calidad: 91.486666,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '173',
      nombre: 'Puerto de Las Palmas',
      asignadas: 553,
      declaradas: 253,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 253,
      calidad: 91.275000,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '158',
      nombre: 'Puerto de El Havre',
      asignadas: 432,
      declaradas: 432,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 432,
      calidad: 90.396666,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '151',
      nombre: 'Puerto de Flensburg',
      asignadas: 50,
      declaradas: 50,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 50,
      calidad: 90.240000,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    },
  ];
export  var embarque_DATA: PeriodicElement[] = [
    {
      codigo: '5',
      nombre: 'EOB138464',
      asignadas: 892,
      declaradas: 892,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 892,
      calidad: 93.190000,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '3',
      nombre: 'OFP138468',
      asignadas: 122,
      declaradas: 122,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 122,
      calidad: 92.630000,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '4',
      nombre: 'OKCB38499',
      asignadas: 270,
      declaradas: 270,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 270,
      calidad: 92.560000,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '190',
      nombre: 'OFPR38479',
      asignadas: 398,
      declaradas: 398,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 398,
      calidad: 91.57500,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '186',
      nombre: 'EOAC38499',
      asignadas: 501,
      declaradas: 501,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 501,
      calidad: 91.486666,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '173',
      nombre: 'EBS138480',
      asignadas: 553,
      declaradas: 253,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 253,
      calidad: 91.275000,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '158',
      nombre: 'EBS138482',
      asignadas: 432,
      declaradas: 432,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 432,
      calidad: 90.396666,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    }, {
      codigo: '151',
      nombre: 'OQF42005G4',
      asignadas: 50,
      declaradas: 50,
      rechazadas: 0,
      faltantes: 0,
      ingresadas: 50,
      calidad: 90.240000,
      expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
      {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
      {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
      ]
    },
  ];
  