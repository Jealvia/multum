/* filter by Angular Material Base: https://stackblitz.com/angular/qkknxdnrvmbv?file=src%2Fapp%2Fautocomplete-display-example.ts 
                                    https://stackblitz.com/angular/ggkbnnjnepn?file=src%2Fapp%2Ftable-expandable-rows-example.ts
                  Modific            https://stackblitz.com/edit/angular-material2-expandable-rows-filter-pagination-sorting?file=app%2Ftable-example.ts */

import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource, MatSort, MatAutocompleteSelectedEvent} from '@angular/material';
import {PageEvent} from '@angular/material/paginator';
import { animate, state, style, transition, trigger } from '@angular/animations';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import * as data from './content';
import { RouterLinkWithHref } from '@angular/router';
@Component({
  selector: 'app-estadisticas',
  templateUrl: './estadisticas.component.html',
  styleUrls: ['./estadisticas.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('void', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('*', style({ height: '*', visibility: 'visible' })),
      transition('void <=> *', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class EstadisticasComponent implements OnInit {
  displayedColumns: string[] = ['codigo', 'nombre', 'asignadas', 'declaradas', 'rechazadas', 'faltantes', 'ingresadas', 'calidad'];
  datas = data;
  //dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  ELEMENT_DATA: data.PeriodicElement[];
  dataSource : MatTableDataSource<data.PeriodicElement>;
  dataFiltrada : data.PeriodicElement[];
  searchText: any; b_searchText: boolean = false; checkTodos: boolean = false;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  isExpansionDetailRow = (index, row) => row.hasOwnProperty('detailRow');

 
  searchFilter: elementSeach[] = [{id: 0, nombre: null}] // = searchFilter; 
  buscar:string = 'Buscar';
  opc: {id: number, nombre: string}[] = [{id: 1, nombre: 'Finca'}, {id: 2, nombre: 'Productor'},
  {id: 3, nombre: 'Marca Caja'}, {id: 4, 'nombre': 'Vapor'}, {id: 5, 'nombre': 'Destino'},   {id: 6, nombre: 'Embarques'} ]; //,
  opcSelect: string;

  anioDesde: any; anioHasta: any; semanaDesde: number; semanaHasta: number;

  myControl = new FormControl();
  val: boolean = true;
  filteredOptions: Observable<elementSeach[]>;

  // MatPaginator Inputs
  length = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  pageEvent: PageEvent;

  numRowSelect: number;

  constructor() { }

  ngOnInit() {

    this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.name),
      map(name => name ? this._filter(name) : this.searchFilter.slice())
      
    );
    
      this.length = 0;

    
  }
  /* Filter */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    //filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  
  selectSubMenu(id:number){
    console.log('id: ', id);
    this.ELEMENT_DATA = null;
    switch (id) {
      case 1:
        this.ELEMENT_DATA = this.datas.finca_DATA; 
        break;
      case 2:
        this.ELEMENT_DATA = this.datas.productores_DATA;
        break;
      case 3:
        this.ELEMENT_DATA = this.datas.Marca_DATA;
        break;
      case 4:
        this.ELEMENT_DATA = this.datas.vapor_DATA;
        break;
      case 5:
        this.ELEMENT_DATA = this.datas.puerto_DATA;
        break;
      case 6:
        this.ELEMENT_DATA = this.datas.embarque_DATA;
        break;
      default:
        break;
    }
    console.log('data: ', this.ELEMENT_DATA);
    this.filtrerNombre();

    this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.name),
      map(name => name ? this._filter(name) : this.searchFilter.slice())
      
    );

    
  }

 private _filter(name: string): elementSeach[] {
  const filterValue = name.toLowerCase();
  this.b_searchText = true;
  return this.searchFilter.filter(option => option.nombre.toLowerCase().includes(filterValue));
  }
  
  displayFn(user?: elementSeach): string | undefined {
    if (this.myControl) {
      console.log('>>>>>>>>>>>>> 33 form buscar ',this.myControl.value.name ); 
    }
    
    console.log('item funcion ', user)
    return user ? user.nombre : undefined;
  }

  itemSelected(event: MatAutocompleteSelectedEvent) {
    console.log("Selected item", event.option.value);
    }
    verValor(auto){
      console.log("ver valor 1",auto);
      console.log("ver valor 2", this.searchText);
    }
  eleminarSelect(){
    this.searchText = {id: null, name: null}
    this.b_searchText = false;
  }

  searchTodos(){
    if (this.myControl.disabled) {
      this.buscar = 'Nombre';
    } else {
      this.buscar = 'Todos';
    }
    this.eleminarSelect();
    this.myControl.disabled ? this.myControl.enable() : this.myControl.disable()
  }
  search(){
    console.log('search: ', this.searchText );
    if (this.searchText == undefined || this.searchText == null ) {
      this.searchText = {id: null, nombre: null}
    }
    if (!this.myControl.disabled) { // si es un nombre
      //console.log('bandera if',this.myControl.disabled);
      //console.log('data: ', this.ELEMENT_DATA);
      //console.log('nombre a buscar: ', this.searchText.nombre);
      this.dataFiltrada = this.ELEMENT_DATA.filter(item => item.nombre == this.searchText.nombre);
      this.dataSource = new MatTableDataSource<data.PeriodicElement>(this.dataFiltrada);
      this.length = this.ELEMENT_DATA.length;
    } else { // si es todo
      //console.log('bandera else',this.myControl.disabled);
      this.dataSource = new MatTableDataSource<data.PeriodicElement>(this.ELEMENT_DATA);
      this.length = this.ELEMENT_DATA.length;
    }

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  selectRow(num:number, row: any, tpl: any){
    this.numRowSelect = num;
   console.log('**row: ', row);
   console.log('**tpl: ', tpl);
  }
  
  filtrerNombre(){
    this.searchFilter = [];
    for (let i = 0; i < this.ELEMENT_DATA.length; i++) {
      const nombre = this.ELEMENT_DATA[i].nombre;
      this.searchFilter.push({id: i, nombre: nombre});
    }
    console.log('filtro',this.searchFilter);
  }

}

export interface elementSeach {
  id: number;
  nombre: string;
}




/*
const searchFilter: elementSeach[] = [
  {id: 1, nombre: 'NATURE PREMIUM'},
  {id: 2, nombre: 'IREN PREMIUM'},
  {id: 3, nombre: 'OKE FLO FAIR TRADE'},
  {id: 4, nombre: 'BIO FT TAPED'},
  {id: 5, nombre: 'FLO MH PRONTO'},
  {id: 6, nombre: 'EQUIFRUIT ORGANICO'},
  {id: 7, nombre: 'EQUAL EXCHANGE'},
  {id: 8, nombre: 'BIO FLO CINTA'},
]
export interface estadisticasDetalles {
  nombredet: string;
  ordendecorte: number, 
  evaluacion: number, 
  longitudminima: number, 
  longitudmaxima: number, 
  dedoscaja: number, 
  clusterscaja: number, 
  tipopallet: string, 
  dimensionpallet: string, 
  tipoplastico: string, 
  sellado: string
}

export interface PeriodicElement {
  codigo: string;
  nombre: string;
  asignadas: number;
  declaradas: number;
  rechazadas: number;
  faltantes: number;  
  ingresadas: number;
  calidad: number;  
  expand: estadisticasDetalles[];
}

var ELEMENT_DATA: PeriodicElement[] = [
  {
    codigo: '5',
    nombre: 'NATURE PREMIUM',
    asignadas: 892,
    declaradas: 892,
    rechazadas: 0,
    faltantes: 0,
    ingresadas: 892,
    calidad: 93.190000,
    expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
    {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
    {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
    ]
  }, {
    codigo: '3',
    nombre: 'IREN PREMIUM',
    asignadas: 122,
    declaradas: 122,
    rechazadas: 0,
    faltantes: 0,
    ingresadas: 122,
    calidad: 92.630000,
    expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
    {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
    ]
  }, {
    codigo: '4',
    nombre: 'OKE FLO FAIR TRADE',
    asignadas: 270,
    declaradas: 270,
    rechazadas: 0,
    faltantes: 0,
    ingresadas: 270,
    calidad: 92.560000,
    expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
    {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
    {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
    ]
  }, {
    codigo: '190',
    nombre: 'BIO FT TAPED',
    asignadas: 398,
    declaradas: 398,
    rechazadas: 0,
    faltantes: 0,
    ingresadas: 398,
    calidad: 91.57500,
    expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
    {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
    {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
    ]
  }, {
    codigo: '186',
    nombre: 'FLO MH PRONTO',
    asignadas: 501,
    declaradas: 501,
    rechazadas: 0,
    faltantes: 0,
    ingresadas: 501,
    calidad: 91.486666,
    expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
    {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
    ]
  }, {
    codigo: '173',
    nombre: 'EQUIFRUIT ORGANICO',
    asignadas: 553,
    declaradas: 253,
    rechazadas: 0,
    faltantes: 0,
    ingresadas: 253,
    calidad: 91.275000,
    expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
    {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
    {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
    ]
  }, {
    codigo: '158',
    nombre: 'EQUAL EXCHANGE',
    asignadas: 432,
    declaradas: 432,
    rechazadas: 0,
    faltantes: 0,
    ingresadas: 432,
    calidad: 90.396666,
    expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
    {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
    ]
  }, {
    codigo: '151',
    nombre: 'BIO FLO CINTA',
    asignadas: 50,
    declaradas: 50,
    rechazadas: 0,
    faltantes: 0,
    ingresadas: 50,
    calidad: 90.240000,
    expand: [{ nombredet: 'SANTOS NORBERTO SARMIENTO ARIAS', ordendecorte: 292436, evaluacion: 428103, longitudminima: 10, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'},
    {nombredet: 'CELIA EUFEMIA AVILA GRANDA', ordendecorte: 292436, evaluacion: 428104, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Europea', dimensionpallet: '1.02 x 1.22', tipoplastico: 'Tubopack', sellado: 'Normal'},
    {nombredet: 'AURORA DEL CISNE AGUILAR CASTILLO', ordendecorte: 292452, evaluacion: 428809, longitudminima: 20, longitudmaxima: 20, dedoscaja: 16, clusterscaja: 4, tipopallet: 'Americano', dimensionpallet: '1.02 x 1.22', tipoplastico: 'banavac', sellado: 'Con Plu'}
    ]
  },
];
*/
