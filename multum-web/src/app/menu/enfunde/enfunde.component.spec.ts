import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnfundeComponent } from './enfunde.component';

describe('EnfundeComponent', () => {
  let component: EnfundeComponent;
  let fixture: ComponentFixture<EnfundeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnfundeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnfundeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
