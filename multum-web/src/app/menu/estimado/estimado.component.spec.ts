import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstimadoComponent } from './estimado.component';

describe('EstimadoComponent', () => {
  let component: EstimadoComponent;
  let fixture: ComponentFixture<EstimadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstimadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstimadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
