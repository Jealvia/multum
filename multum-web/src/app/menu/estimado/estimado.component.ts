import { Component, OnInit, ViewChild } from '@angular/core';
import {PageEvent} from '@angular/material/paginator';
import { CommonService } from 'app/services/common.service';
import { Estimado, msjSnack } from 'app/services/estructuras.modelos';
//Mat Table
import {MatPaginator, MatTableDataSource, MatSort, MatSnackBar} from '@angular/material';
import { anioS } from 'app/services/constants';
// Pdf
import { TablePdfService } from '@services/pdf/table-pdf.service';
import { MsjSnackComponent } from 'app/components/barras/msj-snack/msj-snack.component';

@Component({
  selector: 'app-estimado',
  templateUrl: './estimado.component.html',
  styleUrls: ['./estimado.component.scss']
})
export class EstimadoComponent implements OnInit {

  //datos mat table
  displayedColumns: string[] = ['position', 'productor', 'nombreFinca', 'semana', 'cantidad'];
  dataSource: MatTableDataSource<Estimado>;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  listado : Array<Estimado>;

  //Parametros
  anio: number = null;
  semDesde: number = null;
  semHasta: number = null;
  anios = anioS;
  validSemHasta: string;
  validSemDesde:string;

  // MatPaginator Inputs
  pagineo: PageEvent = {
    pageIndex: 0,
    pageSize: 10,
    length: 0
  };
  pageSizeOptions: number[] = [10, 20, 50];

  todosDatos: boolean = true;
  isLoading: boolean = true;

  //badge
  hideNumBadge: boolean = true;
  numBadge: number = 0; temLengListado: number= 0;
  constructor(
    private commonService: CommonService,
    private pdfTableService: TablePdfService,
    private _snackBar: MatSnackBar,
  ) { }


  ngOnInit() {
    console.log('iniciando');
    this.getListado(1,10, this.todosDatos);
  }

  getListado(pagina: number, elementos: number, todos?: boolean, refresh?: boolean){
    let url: string = 'lista-estimados'; let temNuevRegistros: number;
    this.commonService.listado(url,pagina,elementos, this.anio, this.semDesde, this.semHasta, todos).subscribe(
      res =>{
        $('#mensaje').hide();
        console.log(res);
          this.listado = res.data;
          this.dataSource = new MatTableDataSource(this.listado);
          if (refresh == true) {
            temNuevRegistros = res.length - this.pagineo.length;
          }
          this.pagineo.length = res.length;
          /*caso badge this.temLengListado = this.pagineo.length; */
          this.dataSource.sort = this.sort;
          // Casos objetos anidados,  nested objects
          this.dataSource.sortingDataAccessor = (item, property) => {
            switch(property) {
              case 'nombreFinca': return item.finca_id.nombre;
              case 'productor': return item.finca_id.productor_id;
              default: return item[property];
            }
          };
          this.dataSource.filterPredicate = (data, filter) => {
            const dataStr = data.finca_id.productor_id.toLocaleLowerCase() + data.finca_id.nombre.toLocaleLowerCase() + data.semana +  data.cantidad;
            return dataStr.indexOf(filter) != -1; 
          }
          //Cierre de Casos objetos anidados,  nested objects
          if (todos) {
            this.dataSource.paginator = this.paginator;
          }
          this.isLoading = false;

          if (refresh == true) {
            let mensaje = temNuevRegistros > 1?temNuevRegistros+' registros nuevos':temNuevRegistros+' registro nuevo';
            this.openSnackBar(mensaje, 'primary');
          }
      },
      error=>{
        console.log('erroorr: ',error)
        $('#mensaje').show();
      }
    )
  }

  actualizarPage(pagineo:PageEvent){
    console.log('page: ',pagineo);
    this.getListado(pagineo.pageIndex +1,pagineo.pageSize);
    
  }

  filtrar(){
    this.getListado(this.pagineo.pageIndex +1,this.pagineo.pageSize);
  }


  filterSemana(){
    if (this.semDesde < 1 ||this.semDesde > 53) {
      this.semDesde = 1;
      this.validSemDesde = 'la Semana debe estar entre 1 y 53';
    }else{
      this.validSemDesde = '';
    }
    if (this.semHasta != null) {
      if (this.semHasta > 1 || this.semHasta < 53) {
        this.validSemHasta = '';
      }
      if ( this.semHasta > 53) {
        this.semHasta = this.semDesde
        this.validSemHasta = 'No puede ser mayor a 53';
      }
      if (this.semHasta < this.semDesde) {
        this.semHasta = this.semDesde
        this.validSemHasta = 'No puede ser menor a Semana Desde';
      }
      
    }
    
  }
  /**
   * 
   * @param filterValue tipo String, para filtros
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  
  @ViewChild('fileInput',{static:false}) fileInput;
  file: File | null = null;

  onClickFileInputButton(): void {
    this.fileInput.nativeElement.click();
  }

  onChangeFileInput(): void {
    const files: { [key: string]: File } = this.fileInput.nativeElement.files;
    this.file = files[0];
  }
  enviarFile(file:File){
    let formData = new FormData();
    formData.append("file", file);
    this.commonService.suibirArchivo(formData).subscribe(
      res => {
        console.log(res);
      },
      error=> {
        console.warn('error', error)
      }
      
    )
  }
  fnBadge(){
    this.hideNumBadge = false;
    this.numBadge = this.pagineo.length - this.temLengListado;
  }

  /**
   * 
   */
  
  generarPdf(){
    let titulo: string = 'Listado de Estimado';
    let usuario: string = 'EQUAPAK';
    let columns = ['#', 'Productor', 'Finca', 'Semana', 'Estimado'];
    // Parametros, formato del listado
    let list: object[];
    let compareObject = {'id': 'id', 'productor': 'finca_id.productor_id', 'finca': 'finca_id.nombre', 'semana':'semana', 'cantidad':'cantidad'}
    list = this.commonService.objectFormat(compareObject,this.listado);
    this.pdfTableService.generarPdf(titulo, usuario, columns, this.pdfTableService.parseColBody(list, compareObject));
  }

  //  this.openSnackBar('Mensaje', 'danger');
  openSnackBar(mensje: string, type?: string, ico?: string) {
    let datos: msjSnack = { mensaje: mensje, type: type, ico: ico} 
    this._snackBar.openFromComponent(MsjSnackComponent, {
      duration: 10* 1000,
      data: datos,
    });
  }
  
}
