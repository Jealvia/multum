import { Component, OnInit, Inject, ViewChild, EventEmitter } from '@angular/core';
import { Embarque, Estimado_Selected, Mantenimiento, msjSnack } from 'app/services/estructuras.modelos';
import {PageEvent} from '@angular/material/paginator';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmbarqueService } from 'app/services/embarque.service';
import { MantenimientoService } from 'app/services/mantenimiento.service';
//import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import {MatPaginator, MatTableDataSource, MatSort, MatSnackBar} from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { CommonService } from 'app/services/common.service';
import { ListaEstimadoComponent } from 'app/components/popover/lista-estimado/lista-estimado.component';
import { EstimadosService } from 'app/services/estimados.service';
import { anioS } from 'app/services/constants';
import { TablePdfService } from '@services/pdf/table-pdf.service';
import { MsjSnackComponent } from 'app/components/barras/msj-snack/msj-snack.component';

@Component({
  selector: 'app-embarques',
  templateUrl: './embarques.component.html',
  styleUrls: ['./embarques.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('void', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('*', style({ height: '*', visibility: 'visible' })),
      transition('void <=> *', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class EmbarquesComponent implements OnInit {

  listado: Array<Embarque>;
  dataSource : MatTableDataSource<Embarque>;

  embarqueData:  DialogData = {
      id : 0,
      codigo_alterno: '',
      fecha_embarque_desde: '',
      fecha_embarque_hasta:'',
      anio : null,
      semana: null,
      cantidad: '',
      peso: '',
      largo: '',
      calidad_minima: '',
      calidad_maxima: '',
      precio: 0,
      puerto_salida_id : 0,
      puerto_destino_id : {id: 0, puerto: ''},
      vapor_id : {id: 0, vapor: ''},
      marca_caja_id : 0,
  }
  nuevaData: DialogData = newData;  //Para Iniciar la modal con modelo vacio

   // Datos Parametros:
   anio: number = null;
   semDesde: number = null;
   semHasta: number = null;
   anios = anioS;
   validSemHasta: string;
   validSemDesde:string;


  // MatPaginator Inputs
  
  pagineo: PageEvent = {
    pageIndex: 0,
    pageSize: 10,
    length: 0
  };
  pageSizeOptions: number[] = [10, 20, 50];
  displayedColumns: string[] = ['codigo_alterno', 'fecha_embarque_desde', 'fecha_embarque_hasta', 'puerto_destino_id', 'vapor_id', 'acciones'];
  
  todosDatos: boolean = true;
  isLoading: boolean = true;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  isExpansionDetailRow = (index, row) => row.hasOwnProperty('detailRow');

  constructor(
    public dialog: MatDialog,
    private embarqueService: EmbarqueService,
    private estimadosService: EstimadosService,
    private commonService: CommonService,
    private pdfTableService: TablePdfService,
    private _snackBar: MatSnackBar,
    ) { }

  ngOnInit() {
    this.getListado(1,10, this.todosDatos);
  }

  getListado(pagina: number, elementos:number, todos?: boolean, refresh?: boolean){
    let temNuevRegistros: number;
    this.embarqueService.listado(pagina,elementos, this.anio, this.semDesde, this.semHasta, todos).subscribe(
      res =>{
          console.log('Listado respuesta: ', res );
          this.listado = res.data;
          this.dataSource = new MatTableDataSource(this.listado);
          if (refresh == true) {
            temNuevRegistros = res.length - this.pagineo.length;
          }
          this.pagineo.length = res.length;
          this.dataSource.sort = this.sort;
          // Casos objetos anidados,  nested objects
          this.dataSource.sortingDataAccessor = (item, property) => {
            switch(property) {
              case 'vapor_id': return item.vapor_id.nombre;
              case 'puerto_destino_id': return item.puerto_destino_id.nombre;
              default: return item[property];
            }
          };
          this.dataSource.filterPredicate = (data, filter) => {
            const dataStr = data.codigo_alterno.toLocaleLowerCase() + data.fecha_embarque_desde + data.fecha_embarque_hasta + data.puerto_destino_id.nombre.toLocaleLowerCase() + data.vapor_id.nombre.toLocaleLowerCase();
            return dataStr.indexOf(filter) != -1; 
          }
          //Cierre de Casos objetos anidados,  nested objects
          if (todos) {
            this.dataSource.paginator = this.paginator;
          }
          this.isLoading = false;

          if (refresh == true) {
            let mensaje = temNuevRegistros > 1?temNuevRegistros+' registros nuevos':temNuevRegistros+' registro nuevo';
            this.openSnackBar(mensaje, 'primary');
          }
      },
      error=>{
        console.log('erroorr: ',error)
      })
  }

  
  openDialog( modelo: DialogData ): void {
    if (modelo.id >0) {
      this.embarqueData = {
        id : modelo.id,  
        codigo_alterno: modelo.codigo_alterno,
        fecha_embarque_desde: modelo.fecha_embarque_desde,
        fecha_embarque_hasta: modelo.fecha_embarque_hasta,
        anio : modelo.anio,
        semana: modelo.semana,
        cantidad: modelo.cantidad,
        peso:  modelo.peso,
        largo: modelo.largo,
        calidad_minima: modelo.calidad_minima,
        calidad_maxima: modelo.calidad_maxima,
        precio: modelo.precio,
        puerto_salida_id : modelo.puerto_salida_id,
        puerto_destino_id :  {id: modelo.puerto_destino_id.id,puerto: '' },
        vapor_id : {id: modelo.vapor_id.id, vapor:''},
        marca_caja_id : modelo.marca_caja_id
      }
    }
    
    /* */
    const dialogRef = this.dialog.open(DialogEmbarque, {
      width: '850px',
      data: modelo.id>0?this.embarqueData:modelo,
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('resultado dialogo',result);
      if (result != null &&  result != 'undefined') {
          this.embarqueData = result;
          this.registar(result); 
      }
      console.log('-->decide no proceder', result);
    });

  }

  registar(modelo: DialogData) {
    this.embarqueService.registro(modelo).subscribe(
      res => {
        console.log('Registro Respuesta: ',res);
        this.getListado(this.pagineo.pageIndex +1,this.pagineo.pageSize,this.todosDatos);
      }
    )
  }

  listaEstimado: Estimado_Selected;
  openListEstimado(theEmbarque: Embarque): void {
    this.listaEstimado = { embarque_id: theEmbarque.id, estimados: [], anio: theEmbarque.anio, semana: theEmbarque.semana}
    const dialogRef = this.dialog.open(ListaEstimadoComponent, {
      width: '850px',
      data: this.listaEstimado,
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('Salio de la Modal');
      if (result != null) {
        this.listaEstimado = result;
        this.enviarSelected(result);
        console.log('respuesta de select: ', this.listaEstimado);
      } else {
        console.log('Decide Cancelar');
      }
      
    });
  }

  enviarSelected(listaEstimados: Estimado_Selected){

    this.estimadosService.enviarSelected(listaEstimados).subscribe(
      res=>{
        console.log(res);
      },
      error=>{
        console.log('erroorr: ',error)
      }
    ) 
  }


  filterSemana(){
    if (this.semDesde < 1 ||this.semDesde > 53) {
      this.semDesde = 1;
      this.validSemDesde = 'la Semana debe estar entre 1 y 53';
    }else{
      this.validSemDesde = '';
    }
    if (this.semHasta != null) {
      if (this.semHasta > 1 || this.semHasta < 53) {
        this.validSemHasta = '';
      }
      if ( this.semHasta > 53) {
        this.semHasta = this.semDesde
        this.validSemHasta = 'No puede ser mayor a 53';
      }
      if (this.semHasta < this.semDesde) {
        this.semHasta = this.semDesde
        this.validSemHasta = 'No puede ser menor a Semana Desde';
      }
      
    }
    
  }
  
  updateListado(pagineo:PageEvent){
    console.log('page: ',pagineo);
    this.getListado(pagineo.pageIndex +1,pagineo.pageSize,this.todosDatos);
    
  }
  filtrar(){
    this.filterSemana();
    this.getListado(this.pagineo.pageIndex +1,this.pagineo.pageSize,this.todosDatos);
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  generarPdf(){
    let titulo: string = 'Listado de Embarques';
    let usuario: string = 'EQUAPAK';
    let columns = ['#', 'Código', 'Desde', 'Hasta', 'Puerto Destino', 'Vapor'];
    // Parametros, formato del listado
    let list: object[];
    let compareObject = {'id': 'id', 'codigo_alterno': 'codigo_alterno', 'fecha_embarque_desde': 'fecha_embarque_desde', 'fecha_embarque_hasta': 'fecha_embarque_hasta', 'puerto':'puerto_destino_id.nombre', 'vapor':'vapor_id.nombre'}
    list = this.commonService.objectFormat(compareObject,this.listado);
    this.pdfTableService.generarPdf(titulo, usuario, columns, this.pdfTableService.parseColBody(list, compareObject));
  }

   //  this.openSnackBar('Mensaje', 'danger');
   openSnackBar(mensje: string, type?: string, ico?: string) {
    let datos: msjSnack = { mensaje: mensje, type: type, ico: ico} 
    this._snackBar.openFromComponent(MsjSnackComponent, {
      duration: 10* 1000,
      data: datos,
    });
  }

}
/* ******************************************************************************************************
  *******************************************************************************************************
  *******************************************************************************************************
  ******************************************************************************************************/  



@Component({
  selector: 'dialogEmbarque',
  templateUrl: 'dialogEmbarque.html',
  styleUrls: ['./embarques.component.scss'],
})
export class DialogEmbarque implements OnInit {
  listaPuerto: Mantenimiento;
  listaVapor: Mantenimiento;
  listaMarcaCaja: Mantenimiento;
  startDate = new Date();
  datosFecha: any;
  busquedaAnio: number; busquedaSemana: number;

  //rango Calidad
  element = {name: 'Title1', from: 0, to: 100, active: true};
  slidersRefresh: EventEmitter<void> = new EventEmitter<void>();

  embarqueForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private mantService: MantenimientoService,
    public dialogRef: MatDialogRef<DialogEmbarque>,
    private embarqueService: EmbarqueService,
    private commonService: CommonService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {
      this.embarqueForm = this.formBuilder.group({
        id : [0],  
        codigo_alterno: ['', Validators.required],  
        fecha_embarque_desde: [ this.startDate, Validators.required],  
        fecha_embarque_hasta: [this.startDate],  
        anio : [{value: '', disabled: true}, Validators.required],
        semana: [{value: '', disabled: true}, Validators.required],
        cantidad: ['', Validators.required],  
        peso: ['' ],  
        largo: ['' ],  
        calidad_minima: [''],  
        calidad_maxima: [''],  
        precio: ['', Validators.required],  
        puerto_salida_id : [0, Validators.required],  
        puerto_destino_id : [0, Validators.required],  
        vapor_id : [0, Validators.required],  
        marca_caja_id : [0, Validators.required],  

      });
    }
  
  ngOnInit(): void {
    this.leerLista();
    console.warn('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Data entrada Padre: ', this.data)
    if (this.data.id > 0) {
      this.parseForm(this.data);
    } 
    
  }

  onNoClick(): void {
    this.data = newData;
    this.dialogRef.close();
  }

  onSubmit(){
    this.parseModel();
    this.dialogRef.close(this.data)
   
    
    
  }

  parseModel(){
    if (this.data.id = 0) {
      this.data.fecha_embarque_desde = this.embarqueForm.value.fecha_embarque_desde.toISOString();
      this.data.fecha_embarque_hasta = this.embarqueForm.value.fecha_embarque_hasta.toISOString();
    } 
    
    else {
      this.data.fecha_embarque_desde = this.commonService.formatDate(new Date(this.embarqueForm.value.fecha_embarque_desde));
      this.data.fecha_embarque_hasta = this.commonService.formatDate(new Date(this.embarqueForm.value.fecha_embarque_hasta));
    }
    
    this.data = {
      id : this.embarqueForm.value.id,
      codigo_alterno: this.embarqueForm.value.codigo_alterno,
      fecha_embarque_desde: this.data.fecha_embarque_desde,
      fecha_embarque_hasta: this.data.fecha_embarque_hasta, 
      anio : this.busquedaAnio==null?this.data.anio:this.busquedaAnio, 
      semana: this.busquedaSemana==null?this.data.semana:this.busquedaSemana, 
      cantidad: this.embarqueForm.value.cantidad,
      peso: this.embarqueForm.value.peso,
      largo: this.embarqueForm.value.largo,
      calidad_minima: this.embarqueForm.value.calidad_minima,
      calidad_maxima: this.embarqueForm.value.calidad_maxima,
      precio: this.embarqueForm.value.precio,
      puerto_salida_id : this.embarqueForm.value.puerto_salida_id,
      puerto_destino_id : this.embarqueForm.value.puerto_destino_id,
      vapor_id : this.embarqueForm.value.vapor_id,
      marca_caja_id : this.embarqueForm.value.marca_caja_id
    }

    console.log('datos 222 parse: ',this.data);
  }
  leerLista() {
    let pagina: 0;
    let elementos: 99;
    this.mantService.listado('puerto',  1, 99, null).subscribe(
      res => {
        console.log(res);
        this.listaPuerto = res.data;
        console.log('listaPuerto: ', this.listaPuerto);
        
      }
    )
    this.mantService.listado('vapor',  1, 99, null).subscribe(
      res => {
        console.log(res);
        this.listaVapor = res.data;
        console.log('listaVapor: ', this.listaVapor);
        
      }
    )
    this.embarqueService.listaMarcaCaja().subscribe(
      res => {
        console.log(res);
        this.listaMarcaCaja = res;
        console.log('listaMarcaCajas: ', this.listaMarcaCaja);
        
      }
    )
  }

  buscarEnfunde(val: any){
    
    this.embarqueService.fechaBuscar(val.toISOString()).subscribe(
      res=>{
        this.datosFecha = res;
        console.warn('respuesta busqueda: ', res);
        /*
        this.embarqueForm.setValue(
          {
            id : this.embarqueForm.value.id,  
            codigo_alterno: this.embarqueForm.value.codigo_alterno==null?"":this.embarqueForm.value.codigo_alterno,
            fecha_embarque_desde: this.embarqueForm.value.fecha_embarque_desde,
            fecha_embarque_hasta: this.embarqueForm.value.fecha_embarque_hasta,
            anio : res.anio,
            semana: res.semana,
            cantidad: this.embarqueForm.value.cantidad==null?"":this.embarqueForm.value.cantidad,
            peso:  this.embarqueForm.value.peso==null?"":this.embarqueForm.value.peso,
            largo: this.embarqueForm.value.largo==null?"":this.embarqueForm.value.largo,  
            calidad_minima: this.embarqueForm.value.calidad_minima==null?"":this.embarqueForm.value.calidad_minima,
            calidad_maxima: this.embarqueForm.value.calidad_maxima==null?"":this.embarqueForm.value.calidad_maxima,
            precio: this.embarqueForm.value.precio==null?"":this.embarqueForm.value.precio, 
            puerto_salida_id : this.embarqueForm.value.puerto_salida_id==null?"":this.embarqueForm.value.puerto_salida_id,
            puerto_destino_id : this.embarqueForm.value.puerto_destino_id==null?"":this.embarqueForm.value.puerto_destino_id, 
            vapor_id : this.embarqueForm.value.vapor_id==null?"":this.embarqueForm.value.vapor_id ,
            marca_caja_id : this.embarqueForm.value.marca_caja_id==null?"":this.embarqueForm.value.marca_caja_id, 

          }); 
        */
          this.busquedaAnio = res.anio; this.busquedaSemana = res.semana;
        console.log(this.datosFecha)
        console.warn('formulario ---> ', this.embarqueForm);
      }
    )

    
  }

  parseForm(modelo: DialogData){
    this.embarqueForm.setValue(
      {
        id : modelo.id,  
        codigo_alterno: modelo.codigo_alterno,
        fecha_embarque_desde: modelo.fecha_embarque_desde,
        fecha_embarque_hasta: modelo.fecha_embarque_hasta,
        anio : modelo.anio,
        semana: modelo.semana,
        cantidad: modelo.cantidad,
        peso:  modelo.peso,
        largo: modelo.largo,
        calidad_minima: modelo.calidad_minima,
        calidad_maxima: modelo.calidad_maxima,
        precio: modelo.precio,
        puerto_salida_id : modelo.puerto_salida_id,
        puerto_destino_id : modelo.puerto_destino_id.id,
        vapor_id : modelo.vapor_id.id, 
        marca_caja_id : modelo.marca_caja_id

      }); 
  }
  fn(min: number, max?: number){
    this.embarqueForm.controls['calidad_minima'].setValue(min);
    this.embarqueForm.controls['calidad_maxima'].setValue(max);
  }

}

export interface DialogData {
  id :number;
  codigo_alterno:string;
  fecha_embarque_desde: string;
  fecha_embarque_hasta:string; 
  anio :number;
  semana: number;
  cantidad: string;
  peso: string;
  largo: string;
  calidad_minima: string;
  calidad_maxima: string;
  precio: number;
  puerto_salida_id : number;
  puerto_destino_id : {id: number, puerto: string},
  vapor_id : {id: number, vapor: string},
  marca_caja_id : number;
}
export const newData:  DialogData = {
  id : 0,
  codigo_alterno: '',
  fecha_embarque_desde: '',
  fecha_embarque_hasta:'',
  anio : null,
  semana: null,
  cantidad: '',
  peso: '',
  largo: '',
  calidad_minima: '',
  calidad_maxima: '',
  precio: 0,
  puerto_salida_id : 0,
  puerto_destino_id : {id: 0, puerto: ''},
  vapor_id : {id: 0, vapor: ''},
  marca_caja_id : 0,
}






