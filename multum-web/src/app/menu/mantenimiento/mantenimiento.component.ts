import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { MantenimientoService } from 'app/services/mantenimiento.service';
import { HttpClient } from '@angular/common/http';
import * as Constants from '../../services/constants';
import { ActivatedRoute } from '@angular/router';
import { Mantenimiento, confirmData, msjSnack } from 'app/services/estructuras.modelos';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ConfirmComponent } from 'app/components/popover/confirm/confirm.component';
import { MatTableDataSource } from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

//pdf
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { TablePdfService } from '@services/pdf/table-pdf.service';
import { MsjSnackComponent } from 'app/components/barras/msj-snack/msj-snack.component';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-mantenimiento',
  templateUrl: './mantenimiento.component.html',
  styleUrls: ['./mantenimiento.component.scss']
})
export class MantenimientoComponent implements OnInit {
  //Datos Mat Table
  displayedColumns: string[] = ['position', 'nombre', 'acciones'];
  dataSource: MatTableDataSource<Mantenimiento>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;


  confirmData: confirmData;
  listado: Array<Mantenimiento>;
  nomefiltro: string = null;
  nombreMant: string = (this._activatedRoute.url['value'][this._activatedRoute.url['value'].length - 1]['path']).toLowerCase();
  
  
  // MatPaginator Inputs
  pageSizeOptions: number[] = [10, 20, 50];

  // MatPaginator Output
  pagineo: PageEvent = {
    pageIndex: 0,
    pageSize: 10,
    length: 0
  };
  url = Constants.API_ENDPOINT;
  //Para nuevo mantenimiento
  datoDialogo: DialogData = {
    id:0, 
    nombre: ''
  }

  todosDatos: boolean = true;
  isLoading: boolean = true;

  constructor(
    private mantService: MantenimientoService,
    private pdfTableService: TablePdfService,
    private http: HttpClient,
    private _activatedRoute: ActivatedRoute,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,
  ) { }

  ngOnInit() {

    console.log('en mantenimiento de: ',this.nombreMant);
    this.getListado(1,10);
  }

  actualizar(paginador: PageEvent) {
    this.pagineo = paginador;
    console.log('page: ', this.pagineo);
    this.getListado(this.pagineo.pageIndex + 1, this.pagineo.pageSize);
  }

  getListado(pagina: number, elementos: number, todos?:boolean, refresh?: boolean) {
    let temNuevRegistros: number;
    this.mantService.listado(this.nombreMant,  pagina, elementos, this.nomefiltro, todos).subscribe(
      res => {
        console.log('Respuesta: ',res);
        this.listado = res.data;
        this.dataSource = new MatTableDataSource(this.listado);
        if (refresh == true) {
          temNuevRegistros = res.length - this.pagineo.length;
        }
        this.pagineo.length = res.length;
        this.dataSource.sort = this.sort;

        if (todos) {
          this.dataSource.paginator = this.paginator;
        }
        this.isLoading = false;

        if (refresh == true) {
          let mensaje = temNuevRegistros > 1?temNuevRegistros+' registros nuevos':temNuevRegistros+' registro nuevo';
          this.openSnackBar(mensaje, 'primary');
        }
      }
    )
  }

  new_Edit(id: number, nombre: string){
    this.mantService.registro(this.nombreMant,id, nombre).subscribe(
      res=> {
        console.log(res);
        if (res.status == 'success') {
          
          this.getListado(this.pagineo.pageIndex + 1, this.pagineo.pageSize);
          console.log("correcto!!!");
          
        } else {
          console.log("Some error occured");
        }
      }
    )
  }

  delete(id:number){
    this.mantService.eliminar(this.nombreMant,id).subscribe(
      res=> {
        $('#mensaje').hide();
        console.log(res);

        if (res.status == 'success') {

          this.getListado(this.pagineo.pageIndex + 1, this.pagineo.pageSize);
          console.log("correcto!!!");
          
        } else {
          console.log("Some error occured");
        }
      },
      error=>{
        console.log('erroorr: ',error)
        $('#mensaje').show();
      }
    )
  }

  filtrar(){
    this.getListado(this.pagineo.pageIndex +1,this.pagineo.pageSize);
  }

  openDialog(item:DialogData): void { // id:number, nombre: string
    console.log('Item Dialogo: ', item);
    item.nombre = item.nombre;
    item.mantNombre = this.nombreMant.charAt(0).toUpperCase() + this.nombreMant.slice(1);
    console.log('Item Dialogo 2: ', item);
    const dialogRef = this.dialog.open(DialogMantenimiento, {
      width: '250px',
      data: item
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      
      if (result == null || result == 'undefined') {
        console.log('null');
      } else {
        
        this.new_Edit(result.id, result.nombre);
      }
      
    });
  }

  openConfirm( numUser: number): void {
    this.confirmData = {
      titulo: 'Eliminar ' + this.nombreMant.charAt(0).toUpperCase() + this.nombreMant.slice(1),
      mensaje: 'Por Favor Confime.',
      confirm: null
    }
    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '250px',
      data: this.confirmData
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != null &&  result != 'undefined') {
        if (result.confirm == true) {
          this.delete(numUser); 
        } else {
          console.log('-->decide no proceder', result);
        }
      }
     
      
    });

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    //filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  generarPdf(){
    let titulo: string = 'Listado de '+ this.nombreMant.charAt(0).toUpperCase() + this.nombreMant.slice(1);
    let usuario: string = 'EQUAPAK';
    let columns = ["Id", "Nombre"];
    this.pdfTableService.generarPdf(titulo, usuario, columns, this.listado);
  }
  
 //  this.openSnackBar('Mensaje', 'danger');
 openSnackBar(mensje: string, type?: string, ico?: string) {
  let datos: msjSnack = { mensaje: mensje, type: type, ico: ico} 
  this._snackBar.openFromComponent(MsjSnackComponent, {
    duration: 10* 1000,
    data: datos,
  });
}

}

/* ******************************************************************************************************
  *******************************************************************************************************
  *******************************************************************************************************
  ******************************************************************************************************/  

@Component({
  selector: 'dialogMantenimiento',
  templateUrl: 'dialogMantenimiento.html',
  styleUrls: ['./mantenimiento.component.scss']
})
export class DialogMantenimiento {
  
  constructor(
    public dialogRef: MatDialogRef<DialogMantenimiento>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    //this.data =  {id: null, nombre: null};
    console.log('saliendo ', this.data);
    this.dialogRef.close();
  }

}

export interface DialogData {
  id: number;
  mantNombre?: string;
  nombre?: string;
}


