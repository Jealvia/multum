import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import {MatPaginator, MatTableDataSource, MatSort, MatSnackBar} from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { ProductoresService } from 'app/services/productores.service';
import { persona, confirmData, msjSnack } from 'app/services/estructuras.modelos';
import {PageEvent} from '@angular/material/paginator';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ConfirmComponent } from 'app/components/popover/confirm/confirm.component';
import * as Constants from '../../services/constants';
import { element } from 'protractor';
import { TablePdfService } from '@services/pdf/table-pdf.service';
import { stringify } from 'querystring';
import { MsjSnackComponent } from 'app/components/barras/msj-snack/msj-snack.component';

@Component({
  selector: 'app-productores',
  templateUrl: './productores.component.html',
  styleUrls: ['./productores.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('void', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('*', style({ height: '*', visibility: 'visible' })),
      transition('void <=> *', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ProductoresComponent implements OnInit {
  
  constantes = Constants;

  confirmData: confirmData;
  // MatPaginator Inputs
  pageSizeOptions: number[] = [10, 20, 50];

  // MatPaginator Output
  pagineo: PageEvent = {
    pageIndex: 0,
    pageSize: 10,
    length: 0
  };
  todosDatos: boolean = true;
  isLoading: boolean = true;
  
  listado: Array<persona>;
  dataSource : MatTableDataSource<persona>;

  displayedColumns: string[] = ['id', 'identificacion', 'nombre', 'telefono', 'user', 'fn_productor_id', 'persona_id']; // 
  
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  isExpansionDetailRow = (index, row) => row.hasOwnProperty('detailRow');

  constructor(
    private productoresService: ProductoresService,
    private pdfTableService: TablePdfService,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar,
  ) { 
    
   }

  ngOnInit() {
    console.log('iniciando');
    
    
    this.getListado(1,10, this.todosDatos)
    
    
  }
  getListado(pagina: number, elementos: number, todos?:boolean, refresh?: boolean){
    console.log('pagina ',pagina,' elementos', elementos); let temNuevRegistros: number;
    this.productoresService.listado(pagina,elementos, todos).subscribe(
      res =>{
        console.log(res);
          $('#mensaje').hide();
          this.listado = res.data;
          this.dataSource = new MatTableDataSource(this.listado);
          if (refresh == true) {
            temNuevRegistros = res.length - this.pagineo.length;
          }
          this.pagineo.length = res.length;
          this.dataSource.sort = this.sort;

          if (todos) {
            this.dataSource.paginator = this.paginator;
          }
          this.isLoading = false;

          if (refresh == true) {
            let mensaje = temNuevRegistros > 1?temNuevRegistros+' registros nuevos':temNuevRegistros+' registro nuevo';
            this.openSnackBar(mensaje, 'primary');
          }
      },
      error=>{
        console.log('erroorr: ',error)
        $('#mensaje').show();
      }
      
    )
  }
  actualizarPage(pagineo:PageEvent){
    console.log('page: ',pagineo);
    this.getListado(pagineo.pageIndex +1,pagineo.pageSize);
    
  }
  aprobar(numUser: number,aprobado:boolean){
    this.productoresService.regitrarUsuario(numUser, aprobado).subscribe(
      res=> {
        console.log('la respuesta de la aprobacion es: ',res);
        if (res.status == 'success') {
          this.getListado(this.paginator.pageIndex +1,this.paginator.pageSize, this.todosDatos);
        } else {
          console.log("Some error occured");
        }
      }
    )
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    //filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  openConfirm( numUser: number,aprobado:boolean): void {

    this.confirmData = {
      titulo: (aprobado? 'Aprobar':'Desaprobar')+' Productor',
      mensaje: 'Por Favor Confime.',
      confirm: null
    }
    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '250px',
      data: this.confirmData
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != null &&  result != 'undefined') {
        console.log('-->The dialog was closed', result);
        if (result.confirm == true) {
          this.aprobar(numUser,aprobado);
        } else {
          console.log('-->decide no proceder', result);
        }
      }
      //debugger
      this.getListado(this.paginator.pageIndex +1,this.paginator.pageSize, this.todosDatos);  
     /*Al final de la operacion, Refrescando el listado
      this.productoresService.listado(this.pagineo.pageIndex +1,this.pagineo.pageSize).subscribe( 
        res =>{
          console.log(res);
            this.listado = res.data;
            this.dataSource = new MatTableDataSource<persona>(this.listado);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            console.log('respuesta: ', res );
            console.log('--->>dataSource: ', this.dataSource );
            this.pagineo.length = res.length;
        }
      )
      */
    });

  }
  generarPdf(){
    let titulo: string = 'Listado de Productores';
    let usuario: string = 'EQUAPAK';
    let columns = ['#', 'Ruc.', 'Nombre', 'Teléfono', 'Correo', 'Fincas', '¿Aprobado?'];
    //Parametros 
    let infoTabla = {'#': 1 , 'identificacion': '05', 'nombre': 'juan', 'telefono': '0551', 'user': 'prpr','fn_productor_id': 1, 'persona_id': true};//{'#':number,'identificacion':string,'nombre':string, 'telefono':string,  'user':string,  'fn_productor_id':number, 'persona_id':boolean};
    this.pdfTableService.generarPdf(titulo, usuario, columns, this.pdfTableService.parseColBody(this.listado, infoTabla));
  }

  //  this.openSnackBar('Mensaje', 'danger');
  openSnackBar(mensje: string, type?: string, ico?: string) {
    let datos: msjSnack = { mensaje: mensje, type: type, ico: ico} 
    this._snackBar.openFromComponent(MsjSnackComponent, {
      duration: 10* 1000,
      data: datos,
    });
  }
  
  
}


/*
parseColBody(listado: any): any[]{
    let newList = new Array();
    let newList2 = new Array();
    let colHeader = ['#','identificacion', 'nombre', 'telefono',  'user',  'fn_productor_id', 'persona_id'];
    let colBody = ['#','identificacion', 'nombre', 'telefono',  'user',  'fn_productor_id', 'persona_id'];
    let objColHeader: {'#': number , 'identificacion': string, 'nombre': string, 'telefono': string, 'user': string,'fn_productor_id': number, 'persona_id': boolean};
    objColHeader = {'#': 1 , 'identificacion': '05', 'nombre': 'juan', 'telefono': '0551', 'user': 'prpr','fn_productor_id': 1, 'persona_id': true};
    for (let i = 0; i < listado.length; i++) {
      const element = listado[i];
      let item = new Array();
      
      item[0] =  i+1;
      item[1] =  element.identificacion;
      item[2] =  element.nombre;
      item[3] =  element.telefono;
      item[4] =  element.user;
      item[5] =  element.fn_productor_id.length;
      item[6] =  element.persona_id?'Verdadero':'Falso';
      newList[i] = item;
      // dinamico
      //debugger;
      objColHeader[colHeader[0]] = i+1;
      for (let j = 0; j < colBody.length; j++) {
        const item = colBody[j];
        
        if (j > 0) { // para que ignore '#'
          objColHeader[colHeader[j]] = element[colHeader[j]]
          //debugger;
        }
        
      }
      console.log('>>valores: ', Object.values(objColHeader))
      console.log('lista2: ', newList2[i])
      newList2[i] = Object.values(objColHeader);
      
    }
    console.log('prueba: ', objColHeader[colHeader[0]]);
    console.log('arrayN: ', newList);
    console.log('>>> arrayN2 : ', newList2);
    return newList;
  }
*/