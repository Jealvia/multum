export interface embarquesElement {
    id: number;
    codigo: string;
    embarquedesde: string;
    anio: number;
    semana: number;
    embarquehasta: string;
    puertosalida: string;
    puertodestino: string;  
    cantidad: number;
    peso: number;  
    largo: number;  
    calidadminima: number;  
    calidadmaxima: number;  
    vapor: string;  
    marcacaja: string;  
    precio: number;  
    estimadopendiente: string;  
    cupo?: string;  
    estado: string;  
    expand: OrdenCorteElement[];
  }
  export interface OrdenCorteElement {
      productor: string, 
      finca: string, 
      anio: number, 
      semana: number, 
      estimado: number, 
      primercalibraje: number, 
      segundocalibraje: number, 
      barrida: number, 
      adelantado: number, 
      ordendecorte: number,
      numeroordendecorte?: number,
  }

  export var ELEMENT_DATA: embarquesElement[] = [
    { 
      id: 1,
      codigo: 'EOB138464',
      embarquedesde: '17/01/2020',
      anio: 2020,
      semana: 7,
      embarquehasta: '17/01/2020',
      puertosalida: 'Puerto Bolivar',
      puertodestino: 'Puerto de Oakland',
      cantidad: 10000,
      peso: 18.14,  
      largo: 8,  
      calidadminima: 39,  
      calidadmaxima: 48,  
      vapor: 'LIMARI',  
      marcacaja: 'NATURE PREMIUM',  
      precio: 8.00,  
      estimadopendiente: '4/250',  
      cupo: '9.200/10.000',  
      estado: 'En Proceso',  
      expand: [
      { productor: 'SANTOS NORBERTO SARMIENTO ARIAS', finca: 'LA PATRICIA', anio: 2020, semana: 4, estimado: 450, primercalibraje: 450, segundocalibraje: 445, barrida: 5, adelantado: 0, numeroordendecorte: 292436, ordendecorte: 450},
      { productor: 'CELIA EUFEMIA AVILA GRANDA', finca: 'LA GUARICO', anio: 2020, semana: 4, estimado: 250, primercalibraje: 245, segundocalibraje: 240, barrida: 8, adelantado: 2, numeroordendecorte: 292452, ordendecorte: 250},
      { productor: 'AURORA DEL CISNE AGUILAR CASTILLO', finca: 'LA SERAFINA', anio: 2020, semana: 4, estimado: 310, primercalibraje: 324, segundocalibraje: 320, barrida: 6, adelantado: 0, numeroordendecorte: 292464, ordendecorte: 326}
      ]
    }, {
      id: 2,
      codigo: 'OFP138468',
      embarquedesde: '17/01/2020',
      anio: 2020,
      semana: 4,
      embarquehasta: '17/01/2020',
      puertosalida: 'Puerto Bolivar',
      puertodestino: 'Puerto de Seattle-Tacoma',
      cantidad: 12000,
      peso: 18.14,  
      largo: 8,  
      calidadminima: 39,  
      calidadmaxima: 48,  
      vapor: 'NORTHERN DEFENDER',  
      marcacaja: 'IREN PREMIUM',  
      precio: 8.00,  
      estimadopendiente: '4/250',  
      cupo: '9.200/10.000',  
      estado: 'En Proceso',  
      expand: [{ productor: 'SANTOS NORBERTO SARMIENTO ARIAS', finca: 'LA PATRICIA', anio: 2020, semana: 4, estimado: 450, primercalibraje: 450, segundocalibraje: 445, barrida: 5, adelantado: 0, numeroordendecorte: 292436, ordendecorte: 450},
      { productor: 'CELIA EUFEMIA AVILA GRANDA', finca: 'LA GUARICO', anio: 2020, semana: 4, estimado: 250, primercalibraje: 245, segundocalibraje: 240, barrida: 8, adelantado: 2, numeroordendecorte: 292452, ordendecorte: 250},
      { productor: 'AURORA DEL CISNE AGUILAR CASTILLO', finca: 'LA SERAFINA', anio: 2020, semana: 4, estimado: 310, primercalibraje: 324, segundocalibraje: 320, barrida: 6, adelantado: 0, numeroordendecorte: 292464, ordendecorte: 326}
      ]
    }, {
      id: 3,
      codigo: 'OKCB38499',
      embarquedesde: '17/01/2020',
      anio: 2020,
      semana: 4,
      embarquehasta: '17/01/2020',
      puertosalida: 'Puerto Bolivar',
      puertodestino: 'Puerto de Oakland',
      cantidad: 11000,
      peso: 18.14,  
      largo: 8,  
      calidadminima: 39,  
      calidadmaxima: 48,  
      vapor: 'LIMARI',  
      marcacaja: 'IREN PREMIUM',  
      precio: 8.00,  
      estimadopendiente: '4/250',  
      cupo: '9.200/10.000',  
      estado: 'En Proceso',  
      expand: [{ productor: 'SANTOS NORBERTO SARMIENTO ARIAS', finca: 'LA PATRICIA', anio: 2020, semana: 4, estimado: 450, primercalibraje: 450, segundocalibraje: 445, barrida: 5, adelantado: 0, numeroordendecorte: 292436, ordendecorte: 450},
      { productor: 'CELIA EUFEMIA AVILA GRANDA', finca: 'LA GUARICO', anio: 2020, semana: 4, estimado: 250, primercalibraje: 245, segundocalibraje: 240, barrida: 8, adelantado: 2, numeroordendecorte: 292452, ordendecorte: 250},
      { productor: 'AURORA DEL CISNE AGUILAR CASTILLO', finca: 'LA SERAFINA', anio: 2020, semana: 4, estimado: 310, primercalibraje: 324, segundocalibraje: 320, barrida: 6, adelantado: 0, numeroordendecorte: 292464, ordendecorte: 326}
      ]
    }, {
      id: 4,
      codigo: 'OFPR38479',
      embarquedesde: '17/01/2020',
      anio: 2020,
      semana: 4,
      embarquehasta: '17/01/2020',
      puertosalida: 'Puerto Bolivar',
      puertodestino: 'Puerto de Long Beach',
      cantidad: 10000,
      peso: 18.14,  
      largo: 8,  
      calidadminima: 39,  
      calidadmaxima: 48,  
      vapor: 'NORTHERN DEFENDER',  
      marcacaja: 'NATURE PREMIUM',  
      precio: 8.00,  
      estimadopendiente: '4/250',  
      cupo: '9.200/10.000',  
      estado: 'En Proceso',  
      expand: [{ productor: 'SANTOS NORBERTO SARMIENTO ARIAS', finca: 'LA PATRICIA', anio: 2020, semana: 4, estimado: 450, primercalibraje: 450, segundocalibraje: 445, barrida: 5, adelantado: 0, numeroordendecorte: 292436, ordendecorte: 450},
      { productor: 'CELIA EUFEMIA AVILA GRANDA', finca: 'LA GUARICO', anio: 2020, semana: 4, estimado: 250, primercalibraje: 245, segundocalibraje: 240, barrida: 8, adelantado: 2, numeroordendecorte: 292452, ordendecorte: 250},
      { productor: 'AURORA DEL CISNE AGUILAR CASTILLO', finca: 'LA SERAFINA', anio: 2020, semana: 4, estimado: 310, primercalibraje: 324, segundocalibraje: 320, barrida: 6, adelantado: 0, numeroordendecorte: 292464, ordendecorte: 326}
      ]
    }, {
      id: 5,
      codigo: 'EOAC38499',
      embarquedesde: '17/01/2020',
      anio: 2020,
      semana: 4,
      embarquehasta: '17/01/2020',
      puertosalida: 'Puerto Bolivar',
      puertodestino: 'Puerto de Seattle-Tacoma',
      cantidad: 11000,
      peso: 18.14,  
      largo: 8,  
      calidadminima: 39,  
      calidadmaxima: 48,  
      vapor: 'SUNRISE',  
      marcacaja: 'OKE FLO FAIR TRADE',  
      precio: 8.00,  
      estimadopendiente: '4/250',  
      cupo: '9.200/10.000',  
      estado: 'En Proceso',  
      expand: [{ productor: 'SANTOS NORBERTO SARMIENTO ARIAS', finca: 'LA PATRICIA', anio: 2020, semana: 4, estimado: 450, primercalibraje: 450, segundocalibraje: 445, barrida: 5, adelantado: 0, numeroordendecorte: 292436, ordendecorte: 450},
      { productor: 'CELIA EUFEMIA AVILA GRANDA', finca: 'LA GUARICO', anio: 2020, semana: 4, estimado: 250, primercalibraje: 245, segundocalibraje: 240, barrida: 8, adelantado: 2, numeroordendecorte: 292452, ordendecorte: 250},
      { productor: 'AURORA DEL CISNE AGUILAR CASTILLO', finca: 'LA SERAFINA', anio: 2020, semana: 4, estimado: 310, primercalibraje: 324, segundocalibraje: 320, barrida: 6, adelantado: 0, numeroordendecorte: 292464, ordendecorte: 326}
      ]
    }, {
      id: 6,
      codigo: 'EBS138480',
      embarquedesde: '17/01/2020',
      anio: 2020,
      semana: 4,
      embarquehasta: '17/01/2020',
      puertosalida: 'Puerto Bolivar',
      puertodestino: 'Puerto de El Havre',
      cantidad: 15000,
      peso: 18.14,  
      largo: 8,  
      calidadminima: 39,  
      calidadmaxima: 48, 
      vapor: 'BALTIC',  
      marcacaja: 'BIO FT TAPED',  
      precio: 8.00,  
      estimadopendiente: '4/250',  
      cupo: '9.200/10.000',  
      estado: 'En Proceso',  
      expand: [{ productor: 'SANTOS NORBERTO SARMIENTO ARIAS', finca: 'LA PATRICIA', anio: 2020, semana: 4, estimado: 450, primercalibraje: 450, segundocalibraje: 445, barrida: 5, adelantado: 0, numeroordendecorte: 292436, ordendecorte: 450},
      { productor: 'CELIA EUFEMIA AVILA GRANDA', finca: 'LA GUARICO', anio: 2020, semana: 4, estimado: 250, primercalibraje: 245, segundocalibraje: 240, barrida: 8, adelantado: 2, numeroordendecorte: 292452, ordendecorte: 250},
      { productor: 'AURORA DEL CISNE AGUILAR CASTILLO', finca: 'LA SERAFINA', anio: 2020, semana: 4, estimado: 310, primercalibraje: 324, segundocalibraje: 320, barrida: 6, adelantado: 0, numeroordendecorte: 292464, ordendecorte: 326}
      ]
    }, {
      id: 7,
      codigo: 'EBS138482',
      embarquedesde: '28/01/2020',
      anio: 2020,
      semana: 5,
      embarquehasta: '28/01/2020',
      puertosalida: 'Puerto Bolivar',
      puertodestino: 'Puerto de El Havre',
      cantidad: 14000,
      peso: 18.14,  
      largo: 8,  
      calidadminima: 39,  
      calidadmaxima: 48,  
      vapor: 'RAM VINCES',  
      marcacaja: 'BIO FT TAPED',  
      precio: 8.00,  
      estimadopendiente: '4/250',  
      cupo: '9.200/10.000',  
      estado: 'En Proceso',  
      expand: [{ productor: 'SANTOS NORBERTO SARMIENTO ARIAS', finca: 'LA PATRICIA', anio: 2020, semana: 5, estimado: 450, primercalibraje: 450, segundocalibraje: 445, barrida: 5, adelantado: 0, ordendecorte: 292466},
      { productor: 'CELIA EUFEMIA AVILA GRANDA', finca: 'LA GUARICO', anio: 2020, semana: 5, estimado: 450, primercalibraje: 450, segundocalibraje: 445, barrida: 5, adelantado: 0, ordendecorte: 292477},
      { productor: 'AURORA DEL CISNE AGUILAR CASTILLO', finca: 'LA SERAFINA', anio: 2020, semana: 5, estimado: 450, primercalibraje: 450, segundocalibraje: 445, barrida: 5, adelantado: 0, ordendecorte: 292564}
      ]
    }, {
      id: 8,
      codigo: 'OQF42005G4',
      embarquedesde: '30/01/2020',
      anio: 2020,
      semana: 5,
      embarquehasta: '30/01/2020',
      puertosalida: 'Puerto Bolivar',
      puertodestino: 'Puerto de El Havre',
      cantidad: 12000,
      peso: 18.14,  
      largo: 8,  
      calidadminima: 39,  
      calidadmaxima: 48,  
      vapor: 'AQUAVIT',  
      marcacaja: 'EQUIFRUIT ORGANICO',  
      precio: 8.00,  
      estimadopendiente: '4/250',  
      cupo: '9.200/10.000',  
      estado: 'En Proceso',  
      expand: [{ productor: 'SANTOS NORBERTO SARMIENTO ARIAS', finca: 'LA PATRICIA', anio: 2020, semana: 5, estimado: 450, primercalibraje: 450, segundocalibraje: 445, barrida: 5, adelantado: 0, ordendecorte: 292466},
      { productor: 'CELIA EUFEMIA AVILA GRANDA', finca: 'LA GUARICO', anio: 2020, semana: 5, estimado: 450, primercalibraje: 450, segundocalibraje: 445, barrida: 5, adelantado: 0, ordendecorte: 292477},
      { productor: 'AURORA DEL CISNE AGUILAR CASTILLO', finca: 'LA SERAFINA', anio: 2020, semana: 5, estimado: 450, primercalibraje: 450, segundocalibraje: 445, barrida: 5, adelantado: 0, ordendecorte: 292564}
      ]
    },
  ];