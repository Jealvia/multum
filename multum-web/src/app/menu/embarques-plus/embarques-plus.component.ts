
import { Component, OnInit, Inject, ViewChild, EventEmitter } from '@angular/core';
import { Embarque, Estimado_Selected } from 'app/services/estructuras.modelos';
import {PageEvent} from '@angular/material/paginator';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmbarqueService } from 'app/services/embarque.service';
import { MantenimientoService } from 'app/services/mantenimiento.service';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import {MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { CommonService } from 'app/services/common.service';
import { ListaEstimadoComponent } from 'app/components/popover/lista-estimado/lista-estimado.component';
import * as data from './content';
import * as constantsS from '../../services/constants';

import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

@Component({
  selector: 'app-embarques-plus',
  templateUrl: './embarques-plus.component.html',
  styleUrls: ['./embarques-plus.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('void', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('*', style({ height: '*', visibility: 'visible' })),
      transition('void <=> *', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class EmbarquesPlusComponent implements OnInit {

  
  datos = data;
  constantsS =constantsS;
  listado: Array<data.embarquesElement> = this.datos.ELEMENT_DATA;
  dataSource : MatTableDataSource<data.embarquesElement>;
  /*
  embarqueData:  DialogData = {
      id : 0,
      codigo_alterno: '',
      fecha_embarque_desde: '',
      fecha_embarque_hasta:'',
      anio : null,
      semana: null,
      cantidad: '',
      peso: '',
      largo: '',
      calidad_minima: '',
      calidad_maxima: '',
      precio: 0,
      puerto_salida_id : 0,
      puerto_destino_id : {id: 0, puerto: ''},
      vapor_id : {id: 0, vapor: ''},
      marca_caja_id : 0,
  }
  */



 embarqueData:  data.embarquesElement;


  nuevaData: data.embarquesElement = newData;

  anio: number = null;
  sem_desde: number = null;
  sem_hasta: number = null;


  // MatPaginator Inputs
  pagineo: PageEvent = {
    pageIndex: 0,
    pageSize: 10,
    length: 0
  };

  //displayedColumns: string[] = ['codigo_alterno', 'fecha_embarque_desde', 'fecha_embarque_hasta', 'puerto_destino_id', 'vapor_id', 'acciones'];
  //displayedColumns: string[] = ['codigo', 'embarquedesde', 'anio', 'semana', 'embarquehasta', 'puertosalida', 'puertodestino', 'cantidad', 'peso', 'largo', 'calidadminima', 'calidadmaxima', 'vapor', 'marcacaja', 'precio', 'estimadopendiente', 'cupo', 'estado'];
  /*displayedColumns: string[] =[
    'codigo', 'embarquedesde', 'embarquehasta', 'puertosalida',   
    'vapor', 'marcacaja', 'precio'] **/
  displayedColumns: string[] =  ['nro','codigo', 'anio', 'puertodestino', 'vapor', 'marcacaja', 'estimadopendiente', 'cupo', 'estado', 'acciones'];
  pageSizeOptions: number[] = [10, 20, 50];

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  isExpansionDetailRow = (index, row) => row.hasOwnProperty('detailRow');

  constructor(
    public dialog: MatDialog,
    private embarqueService: EmbarqueService
    ) { }

  ngOnInit() {
    this.dataSource =  new MatTableDataSource<data.embarquesElement>(this.listado);
    /*
    this.embarqueService.listado(1,100, null, null, null).subscribe(
      res =>{
        console.log(res);
          $('#mensaje').hide();
          this.listado = res.data;
          this.dataSource = new MatTableDataSource<Embarque>(this.listado);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          console.log('respuesta: ', res );
          console.log('--->>dataSource: ', this.dataSource );
          this.pagineo.length = res.length;
          //console.log('respuesta: ')
      },
      error=>{
        console.log('erroorr: ',error)
        $('#mensaje').show();
      })
    */
  }

  
  newEditEmb( modelo: data.embarquesElement ): void {
    if (modelo.id >0) {
      this.embarqueData = modelo;
      /*
      this.embarqueData = {
        id : modelo.id,  
        codigo_alterno: modelo.codigo_alterno,
        fecha_embarque_desde: modelo.fecha_embarque_desde,
        fecha_embarque_hasta: modelo.fecha_embarque_hasta,
        anio : modelo.anio,
        semana: modelo.semana,
        cantidad: modelo.cantidad,
        peso:  modelo.peso,
        largo: modelo.largo,
        calidad_minima: modelo.calidad_minima,
        calidad_maxima: modelo.calidad_maxima,
        precio: modelo.precio,
        puerto_salida_id : modelo.puerto_salida_id,
        puerto_destino_id :  {id: modelo.puerto_destino_id.id,puerto: '' },
        vapor_id : {id: modelo.vapor_id.id, vapor:''},
        marca_caja_id : modelo.marca_caja_id
      }
      */
    }
    
    /* */
    const dialogRef = this.dialog.open(DialogEmbarquePlus, {
      width: '860px',
      data: modelo.id>0?this.embarqueData:modelo,
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('resultado dialogo',result);
      if (result != null &&  result != 'undefined') {
        console.log('-->The dialog was closed', result);
        
          this.embarqueData = result;
          this.registar(result);
          
          
        
      }
      console.log('-->decide no proceder', result);
    });

  }

  validSemana(){
    console.log('semana cambio: ', this.sem_desde );
    if (this.sem_desde<0 || this.sem_desde >53) {
      this.sem_desde = 0;
    }
    if (this.sem_hasta < this.sem_desde || this.sem_hasta > 53) {
      this.sem_hasta = this.sem_desde;
    }
  }

  pasefrom(){

  }

  registar(modelo: DialogData) {
    this.embarqueService.registro(modelo).subscribe(
      res => {
        console.log(res);
        console.log('-->Inicio de Actualizacion<--');
          this.updateListado();
      }
    )
  }

  updateListado(){
    this.embarqueService.listado(1,100, null, null, null).subscribe(
      res =>{
        console.log(res);
          this.listado = res.data;
          //this.dataSource = new MatTableDataSource<Embarque>(this.listado);
          this.dataSource =  new MatTableDataSource<data.embarquesElement>(this.listado);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          console.log('respuesta: ', res );
          console.log('--->>dataSource: ', this.dataSource );
          this.pagineo.length = res.length;
      })
  }

  listaEstimado : Array<Estimado_Selected>;
  openListEstimado(): void {
    const dialogRef = this.dialog.open(ListaEstimadoComponent, {
      width: '960px',
      data: this.listaEstimado,
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.listaEstimado = result;
    });
  }
  

}
/* ******************************************************************************************************
  *******************************************************************************************************
  *******************************************************************************************************
  ******************************************************************************************************/  



@Component({
  selector: 'dialogEmbarqueplus',
  templateUrl: './dialogEmbarqueplus.html',
})
export class DialogEmbarquePlus implements OnInit {
  listaPuerto: [{id: number, puerto:string}];
  listaVapor: [{id: number, vapor:string}];
  listaMarcaCaja: [{id: number, marca_caja:string}];
  startDate = new Date();
  datosFecha: {id: number, anio: number, semana: number, color: string} = {id: null, anio: null, semana: null, color: null}
  busquedaAnio: number; busquedaSemana: number;


  element = {name: 'Title1', from: 0, to: 100, active: true};
  slidersRefresh: EventEmitter<void> = new EventEmitter<void>();


  embarqueForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private mantService: MantenimientoService,
    public dialogRef: MatDialogRef<DialogEmbarquePlus>,
    private embarqueService: EmbarqueService,
    private commonService: CommonService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {
      this.embarqueForm = this.formBuilder.group({
        id : [0],  
        codigo: ['', Validators.required],  
        fecha_embarque_desde: [this.startDate, Validators.required],  
        anio : [{value: '', disabled: true}, Validators.required],
        semana: [{value: '', disabled: true}, Validators.required],
        fecha_embarque_hasta: [this.startDate],  
        anioHasta : [{value: '', disabled: true}, Validators.required],
        semanaHasta: [{value: '', disabled: true}, Validators.required],
        cantidad: ['', Validators.required],  
        peso: ['' ],  
        largo: ['' ],  
        calidad_minima: [''],  
        calidad_maxima: [''],  
        precio: ['', Validators.required],  
        cupo: ['', Validators.required],  
        puerto_salida_id : [0, Validators.required],  
        puerto_destino_id : [0, Validators.required],  
        vapor_id : [0, Validators.required],  
        marca_caja_id : [0, Validators.required],  

      });
    }
  
  ngOnInit(): void {
    console.warn('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ngOnInit <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<')
    this.leerLista();
    console.warn('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Data entrada Padre: ', this.data)
    if (this.data.id > 0) {
      this.parseForm(this.data);
    } 
    
  }

  onNoClick(): void {
    //this.data = newData;
    this.dialogRef.close();
  }

  onSubmit(){
    this.parseModel();
    this.dialogRef.close(this.data)
   
    
    
  }

  parseModel(){
    if (this.data.id = 0) {
      this.data.fecha_embarque_desde = this.embarqueForm.value.fecha_embarque_desde.toISOString();
      this.data.fecha_embarque_hasta = this.embarqueForm.value.fecha_embarque_hasta.toISOString();
    } 
    
    else {
      this.data.fecha_embarque_desde = this.commonService.formatDate(new Date(this.embarqueForm.value.fecha_embarque_desde));
      this.data.fecha_embarque_hasta = this.commonService.formatDate(new Date(this.embarqueForm.value.fecha_embarque_hasta));
    }
    
    this.data = {
      id : this.embarqueForm.value.id,
      codigo_alterno: this.embarqueForm.value.codigo_alterno,
      fecha_embarque_desde: this.data.fecha_embarque_desde,
      anio : this.busquedaAnio==null?this.data.anio:this.busquedaAnio, 
      semana: this.busquedaSemana==null?this.data.semana:this.busquedaSemana, 
      fecha_embarque_hasta: this.data.fecha_embarque_hasta, 
      
      cantidad: this.embarqueForm.value.cantidad,
      peso: this.embarqueForm.value.peso,
      largo: this.embarqueForm.value.largo,
      calidad_minima: this.embarqueForm.value.calidad_minima,
      calidad_maxima: this.embarqueForm.value.calidad_maxima,
      precio: this.embarqueForm.value.precio,
      puerto_salida_id : this.embarqueForm.value.puerto_salida_id,
      puerto_destino_id : this.embarqueForm.value.puerto_destino_id,
      vapor_id : this.embarqueForm.value.vapor_id,
      marca_caja_id : this.embarqueForm.value.marca_caja_id
    }

    console.log('datos 222 parse: ',this.data);
  }
  leerLista() {
    let pagina: 0;
    let elementos: 99;
    this.mantService.listado('puerto',  1, 99, null).subscribe(
      res => {
        console.log(res);
        this.listaPuerto = res.data;
        console.log('listaPuerto: ', this.listaPuerto);
        
      }
    )
    this.mantService.listado('vapor',  1, 99, null).subscribe(
      res => {
        console.log(res);
        this.listaVapor = res.data;
        console.log('listaVapor: ', this.listaVapor);
        
      }
    )
    this.embarqueService.listaMarcaCaja().subscribe(
      res => {
        console.log(res);
        this.listaMarcaCaja = res;
        console.log('listaMarcaCajas: ', this.listaMarcaCaja);
        
      }
    )
  }

  buscarEnfunde(val: any){
    
    this.embarqueService.fechaBuscar(val.toISOString()).subscribe(
      res=>{
        this.datosFecha = res;
        console.warn('respuesta busqueda: ', res);

        /*
        this.embarqueForm.setValue(
          {
            id : this.embarqueForm.value.id,  
            codigo_alterno: this.embarqueForm.value.codigo_alterno==null?"":this.embarqueForm.value.codigo_alterno,
            fecha_embarque_desde: this.embarqueForm.value.fecha_embarque_desde,
            fecha_embarque_hasta: this.embarqueForm.value.fecha_embarque_hasta,
            anio : res.anio,
            semana: res.semana,
            cantidad: this.embarqueForm.value.cantidad==null?"":this.embarqueForm.value.cantidad,
            peso:  this.embarqueForm.value.peso==null?"":this.embarqueForm.value.peso,
            largo: this.embarqueForm.value.largo==null?"":this.embarqueForm.value.largo,  
            calidad_minima: this.embarqueForm.value.calidad_minima==null?"":this.embarqueForm.value.calidad_minima,
            calidad_maxima: this.embarqueForm.value.calidad_maxima==null?"":this.embarqueForm.value.calidad_maxima,
            precio: this.embarqueForm.value.precio==null?"":this.embarqueForm.value.precio, 
            puerto_salida_id : this.embarqueForm.value.puerto_salida_id==null?"":this.embarqueForm.value.puerto_salida_id,
            puerto_destino_id : this.embarqueForm.value.puerto_destino_id==null?"":this.embarqueForm.value.puerto_destino_id, 
            vapor_id : this.embarqueForm.value.vapor_id==null?"":this.embarqueForm.value.vapor_id ,
            marca_caja_id : this.embarqueForm.value.marca_caja_id==null?"":this.embarqueForm.value.marca_caja_id, 

          }); 
          */
          this.busquedaAnio = res.anio; this.busquedaSemana = res.semana;
        console.log(this.datosFecha)
        console.warn('formulario ---> ', this.embarqueForm);
      }
    )

    
  }

  parseForm(modelo: DialogData){
    this.embarqueForm.setValue(
      {
        id : modelo.id,  
        codigo_alterno: modelo.codigo_alterno,
        fecha_embarque_desde: modelo.fecha_embarque_desde,
        fecha_embarque_hasta: modelo.fecha_embarque_hasta,
        anio : modelo.anio,
        semana: modelo.semana,
        cantidad: modelo.cantidad,
        peso:  modelo.peso,
        largo: modelo.largo,
        calidad_minima: modelo.calidad_minima,
        calidad_maxima: modelo.calidad_maxima,
        precio: modelo.precio,
        puerto_salida_id : modelo.puerto_salida_id,
        puerto_destino_id : modelo.puerto_destino_id.id,
        vapor_id : modelo.vapor_id.id, 
        marca_caja_id : modelo.marca_caja_id

      }); 
  }

}

export interface DialogData {
  id :number;
  codigo_alterno:string;
  fecha_embarque_desde: string;
  fecha_embarque_hasta:string; 
  anio :number;
  semana: number;
  cantidad: string;
  peso: string;
  largo: string;
  calidad_minima: string;
  calidad_maxima: string;
  precio: number;
  puerto_salida_id : number;
  puerto_destino_id : {id: number, puerto: string},
  vapor_id : {id: number, vapor: string},
  marca_caja_id : number;
}
export const newData:  data.embarquesElement = {
    id: 0,
    codigo: '',
    embarquedesde: '',
    anio: null,
    semana: null,
    embarquehasta: '',
    puertosalida: '',
    puertodestino: '', 
    cantidad: 0,
    peso: 0, 
    largo: 0, 
    calidadminima: 0,  
    calidadmaxima: 0, 
    vapor: '',  
    marcacaja: '',  
    precio: null, 
    estimadopendiente: '',  
    estado: '', 
    expand: null,
}






