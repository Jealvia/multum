import { Component, OnInit, ViewChild } from '@angular/core';
import { OrdenCorte, fullOrdenByEmbarque, Embarque, Ordencortelist, msjSnack, Estimado_Selected } from 'app/services/estructuras.modelos';
import {PageEvent} from '@angular/material/paginator';
import { OrdenCorteService, saveOrden } from 'app/services/orden-corte.service';
import { CommonService } from 'app/services/common.service';
import { ActivatedRoute, Router } from '@angular/router';
//import * as data from '../embarques-plus/content';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import { EmbarqueService } from 'app/services/embarque.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MsjSnackComponent } from 'app/components/barras/msj-snack/msj-snack.component';
import { ConfirmComponent } from 'app/components/popover/confirm/confirm.component';
import { MatDialog } from '@angular/material/dialog';
import { ListaEstimadoComponent } from 'app/components/popover/lista-estimado/lista-estimado.component';
import { EstimadosService } from '@services/estimados.service';

@Component({
  selector: 'app-orden-corte',
  templateUrl: './orden-corte.component.html',
  styleUrls: ['./orden-corte.component.scss']
})
export class OrdenCorteComponent implements OnInit {

  //Datos Mat Table
  isLoaded: boolean = false;
  displayedColumns: string[] = ['position', 'productor_id', 'nombre', 'anio', 'estimado_id', 'cantidad',  'acciones' ]; //'codigo_bodega',
  dataSource: MatTableDataSource<Ordencortelist>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  //Codigo de Emnbarque 
  codEmbarque: string = this._activatedRoute.snapshot.params.codigo;

  
  // MatPaginator Inputs
  pagineo: PageEvent = {
    pageIndex: 0,
    pageSize: 10,
    length: 0
  };
  pageSizeOptions: number[] = [10, 20, 50];

  // MatPaginator Output
  pageEvent: PageEvent;

  //Datos 
  //datos = data;
  isLoading: boolean = true;
  
  //datoEmbarques: Array<data.embarquesElement>; 
  embarqueActual:Embarque;
  listado: Ordencortelist[];
  sendOrdenCorte: saveOrden = {
  "embarqueId": 0,
  "total": 0,
  "finalizado": false,
  "ordencortelist": [
    {"id": 0, "valor": 0}
  ]
  }

  //datasEmbarques: Array<data.embarquesElement>;
  dataOrdenbyEmbarque:fullOrdenByEmbarque;
  selecEmbarque: string; selecEmbarqueForm = new FormControl(); 
  filterEmbarque: Observable<Embarque[]>; 
  ListaEmbarque: Embarque[]; 

  anio: number = null;
  sem_desde: number = null;
  sem_hasta: number = null;


  //text filtre
  searchText: any; b_searchText: boolean = false;

  tempCantidad: number = 0;

  constructor(
    private ordenCorteService:OrdenCorteService,
    private commonService: CommonService,
    private _activatedRoute: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private servEmbarque: EmbarqueService,
    public dialog: MatDialog,
    private router:Router,
    private estimadosService: EstimadosService
  ) {
    
   }

  ngOnInit() {
    this.listEmbarque();
    console.log('codigo', this.codEmbarque);
    if (this.codEmbarque) {
      this.getListado()
    }
    if (this.ListaEmbarque != null && this.ListaEmbarque != undefined) {
      this.fnFiltreEmbarque();
    }
    
  }
  fnFiltreEmbarque(){
    this.filterEmbarque = this.selecEmbarqueForm.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filter(String(value)))
        );
  }
  listEmbarque(){
    this.commonService.listado('lista-embarque',1,99999,null,null, null).subscribe(
      res =>{
        console.log('listado embarque: ',res);
        this.ListaEmbarque = res.data;
        this.fnFiltreEmbarque();
      },
      error=>{
        console.log('erroorr: ',error)
      }
    )
  }

  getListado(){
    this.ordenCorteService.listOrdenByEmbarque(parseInt(this.codEmbarque)).subscribe(
      res =>{
        console.log('Listado: ',res);
        this.dataOrdenbyEmbarque = res;
        this.embarqueActual = res.embarque;
        this.listado = res.ordencortelist;
        this.dataSource = new MatTableDataSource(this.listado);
        this.pagineo.length = res.ordencortelist.length;
        this.dataSource.sort = this.sort;
        this.isLoaded = true;
        
        for (const item of this.listado) {
          this.tempCantidad = item.cantidad + this.tempCantidad;
        }
        if (this.embarqueActual.cantidad_actual == null) {
          this.embarqueActual.cantidad_actual = this.tempCantidad;
        }

        this.isLoading = false;
      },
      error=>{
        console.log('erroorr: ',error)
      }
    )
    
    console.log('iniciando', this.listado);
  }
  EmbarqueFilter(value: any){
    console.log('valor: ', value);
    
  }
  actualizarPage(pagineo:PageEvent){
    console.log('page: ',pagineo);
    //this.listado = this.datoEmbarques[0].expand;
    
  }
  search(){
    this.codEmbarque = this.searchText;
    this.getListado()
  }

  deshacerEmbarque(){
    this.eleminarSelect();
    this.codEmbarque = '';
    this.listado = [];
    
  }
  
  private _filter(value: string): Embarque[]{ // Embarque[] {
    const filterValue = value.toLowerCase();
    console.log('text: '+ this.searchText +'value:' +value )
    if (value != '') {
      this.b_searchText = true;
    }
    return this.ListaEmbarque.filter(option => option.codigo_alterno.toLowerCase().includes(filterValue) || option.anio == (parseInt(filterValue)) || option.semana == (parseInt(filterValue)));
  }

  eleminarSelect(){
    
    console.log('text:', this.searchText )
    this.searchText = ''; 
    this.b_searchText = false;
  }

  sumaTotal(indice: number, valor: number){
    let total = 0;let diferencia: number;
    if (valor < 0 || valor > this.listado[indice].estimado_id) {
      valor < 0? this.openSnackBar('Orden de Corte no puede ser menor a 0', 'warn'): this.openSnackBar('Orden de Corte no puede ser mayor al estimado', 'warn');
      this.listado[indice].cantidad = 0;
      for (const item of this.listado) {
        total = item.cantidad + total;
      }
      this.embarqueActual.cantidad_actual = total;
    }else{
      for (const item of this.listado) {
        total = item.cantidad + total;
      }
      if (total  >= this.embarqueActual.cantidad) {
        this.openSnackBar('Cantidad actual es superior a la catidad deseada', 'warn');
        diferencia = total - this.embarqueActual.cantidad;
        this.listado[indice].cantidad = this.listado[indice].cantidad - diferencia;
        total = total - diferencia;
      } 
      this.embarqueActual.cantidad_actual = total; 
    }
    
    
    
    console.log('total: ',this.embarqueActual.cantidad_actual, ' diferencia: ', diferencia);
  }

  save(finalizar:boolean){
    let total = 0;
    this.sendOrdenCorte.embarqueId = this.embarqueActual.id;
    this.sendOrdenCorte.finalizado = finalizar;
    for (let i = 0; i < this.listado.length; i++) {
      const element = this.listado[i];
      total = total + element.cantidad;
      this.sendOrdenCorte.ordencortelist[i] = { "id": element.id, "valor": element.cantidad};
    }
    this.sendOrdenCorte.total = total
    console.log("send enviar data: ", this.sendOrdenCorte)
    if (finalizar) {
      
      this.openConfirm('Embarque','Si finaliza este embarque, no podrá modificarlo.', finalizar);
      
    } else {
      this.sendData();
      this.openSnackBar('Registro Guardado Correctamente', 'primary');
    }
   
    
  }

  sendData(){
    this.ordenCorteService.guardarFinalizar(this.sendOrdenCorte).subscribe(
      res=>{
        console.log(res);
        if (this.sendOrdenCorte.finalizado == true) {
          this.openSnackBar('Registro Finalizado Correctamente', 'primary');
          this.router.navigate(['main/Embarque']);
        }
      },
      error=>{
        console.log('erroorr: ',error)
       
      }
    ) 
  }

  updateListado(pagineo:PageEvent){
    console.log('page: ',pagineo);
    this.getListado();
    
  }
  openConfirm(titulo: string, mensaje: string, confirm: boolean, id?:number ): void {

    let confirmData = {
      titulo: titulo,
      mensaje: mensaje,
      confirm: confirm
    }
    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '350px',
      data: confirmData
    });

    dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed', result);
        if (result == null || result == 'undefined' || result.confirm == false) {
            console.log('no Confirmo!!');
        } else {
          console.log('confirm: ', confirmData.confirm)
          titulo =='Embarque'?this.sendData():this.eliminarOrden(confirmData.confirm,id,true);
        }
    });

  }
  //  this.openSnackBar('Mensaje', 'danger');
  openSnackBar(mensje: string, type?: string, ico?: string) {
    let datos: msjSnack = { mensaje: mensje, type: type, ico: ico} 
    this._snackBar.openFromComponent(MsjSnackComponent, {
      duration: 10* 1000,
      data: datos,
    });
  }

  savedisabled():boolean{
    let bloqueado: boolean;
    
    if (this.embarqueActual.finalizado == true || this.embarqueActual.cantidad_actual > this.embarqueActual.cantidad || this.listado.length == 0 ) { //|| this.embarqueActual.cantidad_actual < 1 
      bloqueado = true;
    }else{
      bloqueado = false
    }
    return bloqueado;
    ;
  }
  eliminarOrden(eliminar: boolean, id: number, showConfirm: boolean){
    if (showConfirm == false) {
      console.log('entro en funcion eliminar: false')
      this.openConfirm('Eliminar orden de corte', 'Si elimina podra usarlo en otro embarque.',false, id)
    }else{
      console.log('entro en funcion eliminar: true', eliminar)
      if (eliminar == true) {
        this.ordenCorteService.eliminarOdenFromEmbarque(id).subscribe(
          res=>{
            console.log(res);
            this.getListado();
          },
          error=>{
            console.log('erroorr: ',error)
          }
        ) 
      }
    }

    
  }


  listaEstimado: Estimado_Selected;
  openListEstimado(theEmbarque: Embarque): void {
    this.listaEstimado = { embarque_id: theEmbarque.id, estimados: [], anio: theEmbarque.anio, semana: theEmbarque.semana}
    const dialogRef = this.dialog.open(ListaEstimadoComponent, {
      width: '850px',
      data: this.listaEstimado,
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('Salio de la Modal');
      if (result != null) {
        this.listaEstimado = result;
        this.enviarSelected(result);
        console.log('respuesta de select: ', this.listaEstimado);
        
      } else {
        console.log('Decide Cancelar');
      }
      
    });
  }

  enviarSelected(listaEstimados: Estimado_Selected){

    this.estimadosService.enviarSelected(listaEstimados).subscribe(
      res=>{
        console.log(res);
        this.getListado()
      },
      error=>{
        console.log('erroorr: ',error)
      }
    ) 
  }


}

  
