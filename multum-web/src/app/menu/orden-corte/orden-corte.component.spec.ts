import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenCorteComponent } from './orden-corte.component';

describe('OrdenCorteComponent', () => {
  let component: OrdenCorteComponent;
  let fixture: ComponentFixture<OrdenCorteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdenCorteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdenCorteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
