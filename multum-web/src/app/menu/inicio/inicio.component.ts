import { Component, OnInit } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { CommonService } from 'app/services/common.service';
import * as Constants from '../../services/constants';
  
@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {
  contantes = Constants; 
  constructor(
    private afMessaging: AngularFireMessaging,
    private commonService:CommonService,
  ) {
    this.requestPermission()
   }

  ngOnInit() {
  }

  requestPermission() {
    this.afMessaging.requestToken
      .subscribe(
        (token) => {
          $('#mensaje').hide();
          console.log('Permission granted! Save to the server!', token);
          this.commonService.almacenarToken(token).subscribe(
            res => {
              console.log(res)
            }
          )
        },
        (error) => {
          //$('#mensaje').show(); Revisar prox
          console.error(error);
        },
      );
  }

}
