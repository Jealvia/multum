import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import { CalibrajeService } from '@services/calibraje.service';
import { calibraje, msjSnack } from 'app/services/estructuras.modelos';
import {PageEvent} from '@angular/material/paginator';
import {anioS}  from '../../services/constants'
import {MatSnackBar} from '@angular/material/snack-bar';
import { MsjSnackComponent } from '../../components/barras/msj-snack/msj-snack.component';
import { TablePdfService } from '@services/pdf/table-pdf.service';
import { CommonService } from '@services/common.service';

@Component({
  selector: 'app-calibraje',
  templateUrl: './calibraje.component.html',
  styleUrls: ['./calibraje.component.scss']
})
export class CalibrajeComponent implements OnInit {
  //datos mat table
  displayedColumns: string[] = ['position', 'productor', 'nombreFinca', 'semana','tipo_calibraje_id', 'cinta_id', 'cantidad'];
  dataSource: MatTableDataSource<calibraje>;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  listado: Array<calibraje>;
  // Datos Parametros:
  anio: number = null;
  semDesde: number = null;
  semHasta: number = null;
  anios = anioS;
  validSemHasta: string;
  validSemDesde:string;

  // MatPaginator Inputs
  pagineo: PageEvent = {
    pageIndex: 0,
    pageSize: 10,
    length: 0
  };
  pageSizeOptions: number[] = [10, 20, 50];

  todosDatos: boolean = true;
  isLoading: boolean = true;

  // MatPaginator Output
  pageEvent: PageEvent;
  
  //isExpansionDetailRow = (index, row) => row.hasOwnProperty('detailRow');
  isExpansionDetailRow: any;
  constructor(
    private calibrajeService: CalibrajeService,
    private pdfTableService: TablePdfService,
    private _snackBar: MatSnackBar,
    private commonS: CommonService
  ) {

  }

  ngOnInit() {
    console.log('iniciando');
    
    this.getListado(1,10, this.todosDatos);
  }
  getListado(pagina: number, elementos: number, todos?:boolean, refresh?: boolean){
    if (todos == undefined) todos = false; let temNuevRegistros: number;
    this.calibrajeService.listado(pagina,elementos, this.anio, this.semDesde, this.semHasta, todos).subscribe(
      res =>{
        console.log(res);
          this.listado = res.data;
          this.dataSource = new MatTableDataSource(this.listado);
          if (refresh == true) {
            temNuevRegistros = res.length - this.pagineo.length;
          }
          this.pagineo.length = res.length;
          // Casos objetos anidados,  nested objects
          this.dataSource.sortingDataAccessor = (item, property) => {
            switch(property) {
              case 'nombreFinca': return item.finca_id.nombre;
              case 'productor': return item.finca_id.productor_id;
              default: return item[property];
            }
          };
          this.dataSource.filterPredicate = (data, filter) => {
            const dataStr = data.finca_id.productor_id.toLocaleLowerCase() + data.finca_id.nombre.toLocaleLowerCase() + data.semana + data.tipo_calibraje_id.toLocaleLowerCase() +data.cinta_id.toLocaleLowerCase() + data.cantidad;
            return dataStr.indexOf(filter) != -1; 
          }
          //Cierre de Casos objetos anidados,  nested objects
          if (todos) {
            this.dataSource.paginator = this.paginator;
          }
          this.dataSource.sort = this.sort;
          this.isLoading = false;

          if (refresh == true) {
            let mensaje = temNuevRegistros > 1?temNuevRegistros+' registros nuevos':temNuevRegistros+' registro nuevo';
            this.openSnackBar(mensaje, 'primary');
          }
      },
      error=>{
        console.log('erroorr: ',error)
        $('#mensaje').show();
      }
    )
  }

  updateListado(pagineo:PageEvent, todos?:boolean){
    console.log('page: ',pagineo);
    if (!todos) {
      this.getListado(pagineo.pageIndex +1,pagineo.pageSize);
    }
    
    
  }
  filtrar(){
    this.getListado(this.pagineo.pageIndex +1,this.pagineo.pageSize,  this.todosDatos);
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  filterSemana(){
    if (this.semDesde < 1 ||this.semDesde > 53) {
      this.semDesde = 1;
      this.validSemDesde = 'la Semana debe estar entre 1 y 53';
    }else{
      this.validSemDesde = '';
    }
    if (this.semHasta != null) {
      if (this.semHasta > 1 || this.semHasta < 53) {
        this.validSemHasta = '';
      }
      if ( this.semHasta > 53) {
        this.semHasta = this.semDesde
        this.validSemHasta = 'No puede ser mayor a 53';
      }
      if (this.semHasta < this.semDesde) {
        this.semHasta = this.semDesde
        this.validSemHasta = 'No puede ser menor a Semana Desde';
      }
      
    }
    
  }
  //  this.openSnackBar('la Semana debe estar entre 1 y 53', 'danger');
  openSnackBar(mensje: string, type?: string, ico?: string) {
    let datos: msjSnack = { mensaje: mensje, type: type, ico: ico} 
    this._snackBar.openFromComponent(MsjSnackComponent, {
      duration: 3* 1000,
      data: datos,
    });
  }

  generarPdf(){
    let titulo: string = 'Listado de Calibraje';
    let usuario: string = 'EQUAPAK';
    let columns = ['#', 'Productor', 'Finca', 'Semana', 'Tipo', 'Cinta', 'cantidad'];
    // Parametros, formato del listado
    let list: object[];
    let compareObject = {'id': 'finca_id.id', 'productor': 'finca_id.productor_id', 'finca': 'finca_id.nombre','semana':'semana','tipo_calibraje_id':'tipo_calibraje_id', 'cinta_id': 'cinta_id', 'cantidad':'cantidad'}
    list = this.commonS.objectFormat(compareObject,this.listado);
    this.pdfTableService.generarPdf(titulo, usuario, columns, this.pdfTableService.parseColBody(list, compareObject));
  }
}


