import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalibrajeComponent } from './calibraje.component';

describe('CalibrajeComponent', () => {
  let component: CalibrajeComponent;
  let fixture: ComponentFixture<CalibrajeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalibrajeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalibrajeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
