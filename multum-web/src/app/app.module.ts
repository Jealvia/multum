import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AngularFireModule } from '@angular/fire';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';


import {
  AgmCoreModule
} from '@agm/core';

import { AccountModule } from './account/account.module';
import { MainModule } from './main/main.module';
import { environment } from '../environments/environment';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EmbarquesPlusComponent } from './menu/embarques-plus/embarques-plus.component';



@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    AccountModule,
    MainModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireMessagingModule,
    AgmCoreModule.forRoot({
      apiKey: 'YOUR_GOOGLE_MAPS_API_KEY'
    }),
  ],
  declarations: [
    AppComponent,
    
    
    
    
    //mainComponent,
    //AccountComponent
    /*
    EnfundeComponent,
    EstadisticasComponent,
    ContactosComponent,
    InicioComponent,
    */

  ],
  providers: [
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: [] },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
