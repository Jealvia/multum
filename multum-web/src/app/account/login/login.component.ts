import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as Constants from '../../services/constants';
import { HttpClient } from '@angular/common/http';

import { AngularFireMessaging } from '@angular/fire/messaging';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  mensaje: string = ''; b_mensaje: boolean = false;
  constantes = Constants;

  
  constructor(
    private formBuilder: FormBuilder,
    private router:Router,
    private http: HttpClient,
    private afMessaging: AngularFireMessaging,
  ) { 
    if (localStorage.getItem('token') != null) {
      this.router.navigate(['/main/Inicio']);
    }
    this.loginForm = this.formBuilder.group({
      usuario: ['', Validators.required],
      clave: ['', Validators.required],
       
    });
    this.afMessaging.messaging.subscribe(
      (_messaging) => {
        _messaging.onMessage = _messaging.onMessage.bind(_messaging);
        _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
      }
    )
   }

  

  ngOnInit() {
    
    
  }
  fn_login(){
    console.log('Click Login');
  }

  serv_inicioSesion(): any {
    let datos = {
      usuario: String(this.loginForm.value.usuario),
      contasena: String(this.loginForm.value.clave),
    }
    console.log('datos send login web: ', datos);
    let response;
    try {
      response = this.http.post < any > (`${Constants.API_ENDPOINT}` + "iniciar-sesion-web/",
      datos, {
          headers: {
            'Content-Type': "application/x-www-form-urlencoded"
          }
        }
      );
    } catch (error) {
      console.log("error:" + error)
    }

    return response;
  }

  onSubmit(){
    console.log('login');
    this.serv_inicioSesion().subscribe(
      res=>{
        console.log(res);
        //debugger;
        if (res.status == 'success') {
          /*
          this.cookieService.set('token', res.token, new Date(2999,12,30));
          this.cookieService.set('session', res.session, new Date(2999,12,30));
          this.cookieService.set('user', res.user, new Date(2999,12,30));
          */
          this.saveLocal(res.token, res.session, res.user);

          this.router.navigate(['/main/Inicio']);
        } else {
          this.b_mensaje = true;
          this.mensaje = 'Datos Incorrectos!!';
          console.log("Some error occured")
        }
      },
      error=>{
        console.log('erroorr: ',error)
        this.b_mensaje = true;
        this.mensaje = this.constantes.error_servidor;
      }
    ) 
  }

  saveLocal(token: any, seccion, user){
    localStorage.setItem('token', token);
    localStorage.setItem('session', seccion);
    localStorage.setItem('user', user);
  }


}

/*
  
          this.storage.set('token', res.token)
            .then(
              () => console.log('Stored item!'),
              error => console.error('Error storing item', error)
          );
          this.storage.set('session', res.session)
            .then(
              () => console.log('Stored item!'),
              error => console.error('Error storing item', error)
          );
          this.storage.set('user',res.user)
            .then(
              () => console.log('Stored item!'),
              error => console.error('Error storing item', error)
          );
*/