import { NgModule } from '@angular/core';
//import { RouterModule, ROUTES } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { mainRoutingModule } from './main.routing';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { UserProfileComponent } from '../user-profile/user-profile.component';
import { TableListComponent } from '../table-list/table-list.component';
import { TypographyComponent } from '../typography/typography.component';
import { IconsComponent } from '../icons/icons.component';
import { NotificationsComponent } from '../notifications/notifications.component';
import { UpgradeComponent } from '../upgrade/upgrade.component';

import { HttpClientModule } from '@angular/common/http';


import { InicioComponent } from '../menu/inicio/inicio.component';
import { EnfundeComponent } from '../menu/enfunde/enfunde.component';
import { CalibrajeComponent } from '../menu/calibraje/calibraje.component';
import { EstadisticasComponent } from '../menu/estadisticas/estadisticas.component';
import { ContactosComponent } from '../menu/contactos/contactos.component';
import { OrdenCorteComponent } from '../menu/orden-corte/orden-corte.component';
import { ProductoresComponent } from '../menu/productores/productores.component';

import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSelectModule
} from '@angular/material';
import { EnfundeService } from 'app/services/enfunde.service';
import { apiMaterialModule } from 'app/apis/material-module';
import { OrdenCorteService } from 'app/services/orden-corte.service';
import { ProductoresService } from 'app/services/productores.service';
import { mainComponent } from './main.component';
import { ComponentsModule } from 'app/components/components.module';
import { MantenimientoComponent, DialogMantenimiento } from 'app/menu/mantenimiento/mantenimiento.component';
import { EstimadoComponent } from 'app/menu/estimado/estimado.component';
import { CdkDetailRowDirective } from 'app/apis/cdk-detail-row.directive';
import { FincaComponent } from '../menu/finca/finca.component';
import { EmbarquesComponent, DialogEmbarque } from '../menu/embarques/embarques.component';
import { EmbarqueService } from 'app/services/embarque.service';
import { ListaEstimadoComponent } from 'app/components/popover/lista-estimado/lista-estimado.component';
import { EmbarquesPlusComponent, DialogEmbarquePlus } from 'app/menu/embarques-plus/embarques-plus.component';
import { EstimadosService } from 'app/services/estimados.service';
import { Ng5SliderModule } from 'ng5-slider';


@NgModule({
  imports: [
    CommonModule,
    //RouterModule.forChild(ROUTES),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    HttpClientModule,
    apiMaterialModule,
    mainRoutingModule,
    ComponentsModule,
    Ng5SliderModule //Rango input
  ],
  declarations: [
    mainComponent,
    DashboardComponent,
    UserProfileComponent,
    TableListComponent,
    TypographyComponent,
    IconsComponent,
    NotificationsComponent,
    UpgradeComponent,
    ProductoresComponent,
    MantenimientoComponent, DialogMantenimiento,
    EstimadoComponent,

    InicioComponent,
    EnfundeComponent, 
    CalibrajeComponent,
    EstadisticasComponent,
    ContactosComponent,
    OrdenCorteComponent,
    FincaComponent,
    EmbarquesComponent, DialogEmbarque,
    ListaEstimadoComponent,

    CdkDetailRowDirective, //tables expand and filter 
    EmbarquesPlusComponent, DialogEmbarquePlus
  ],
  providers: [
    EnfundeService,
    OrdenCorteService,
    ProductoresService,
    EmbarqueService,
    EstimadosService,
  ],
  entryComponents: [
    DialogMantenimiento, DialogEmbarque, 
    ListaEstimadoComponent, DialogEmbarquePlus
  ],


})

export class MainModule {}
