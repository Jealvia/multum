
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from '../dashboard/dashboard.component';
import { UserProfileComponent } from '../user-profile/user-profile.component';
import { TableListComponent } from '../table-list/table-list.component';
import { TypographyComponent } from '../typography/typography.component';
import { IconsComponent } from '../icons/icons.component';
import { NotificationsComponent } from '../notifications/notifications.component';
import { UpgradeComponent } from '../upgrade/upgrade.component';

import { mainComponent } from "./main.component";
import { InicioComponent } from '../menu/inicio/inicio.component';
import { EnfundeComponent } from '../menu/enfunde/enfunde.component';
import { CalibrajeComponent } from '../menu/calibraje/calibraje.component';
import { EstadisticasComponent } from '../menu/estadisticas/estadisticas.component';
import { ContactosComponent } from '../menu/contactos/contactos.component';
import { OrdenCorteComponent } from '../menu/orden-corte/orden-corte.component';
import { ProductoresComponent } from '../menu/productores/productores.component';
import { MantenimientoComponent } from 'app/menu/mantenimiento/mantenimiento.component';
import { EstimadoComponent } from 'app/menu/estimado/estimado.component';
import { EmbarquesComponent } from '../menu/embarques/embarques.component';
import { EmbarquesPlusComponent } from 'app/menu/embarques-plus/embarques-plus.component';

const routes: Routes = [
  {
    path: '',
    component: mainComponent,
    children: [
      
      { path: '', redirectTo: 'Inicio', pathMatch: 'full' },

    { path: 'dashboard',      component: DashboardComponent },
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'table-list',     component: TableListComponent },
    { path: 'typography',     component: TypographyComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'notifications',  component: NotificationsComponent },
    { path: 'upgrade',        component: UpgradeComponent },

    { path: 'Inicio',         component: InicioComponent },
    { path: 'Enfunde',   component: EnfundeComponent },
    { path: 'Calibraje',  component: CalibrajeComponent },
    { path: 'Estadisticas',   component: EstadisticasComponent },
    { path: 'Contactos',   component: ContactosComponent },
    { path: 'Embarque',         component: EmbarquesComponent },
    { path: 'OrdenCorte',   component: OrdenCorteComponent },
    { path: 'OrdenCorte/:codigo',   component: OrdenCorteComponent },
    { path: 'Productores',   component: ProductoresComponent },
    { path: 'Vapor',   component: MantenimientoComponent },
    { path: 'Puerto',   component: MantenimientoComponent },
    { path: 'Estimado',   component: EstimadoComponent }, 
    { path: 'EmbarquePlus',  component: EmbarquesPlusComponent },
    ]
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class mainRoutingModule { }











    // {
    //   path: '',
    //   children: [ {
    //     path: 'dashboard',
    //     component: DashboardComponent
    // }]}, {
    // path: '',
    // children: [ {
    //   path: 'userprofile',
    //   component: UserProfileComponent
    // }]
    // }, {
    //   path: '',
    //   children: [ {
    //     path: 'icons',
    //     component: IconsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'notifications',
    //         component: NotificationsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'typography',
    //         component: TypographyComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'upgrade',
    //         component: UpgradeComponent
    //     }]
    // }