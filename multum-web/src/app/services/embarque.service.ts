import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as Constants from './constants';

@Injectable({
  providedIn: 'root'
})
export class EmbarqueService {
  token= localStorage.getItem('token');

  constructor(
    private http: HttpClient,
  ) { }
  //lista-orden-corte/pagina=/elementos=/anio=/sem_desde=/sem_hasta=/
  //token=this.cookieService.get('token');
  url = Constants.API_ENDPOINT;
  //token=this.cookieService.get('token');
  registro(modelo: any): any {
    /*
    let data = {
      id: "0",
      codigo_alterno: "wefwfwf",
      fecha_embarque_desde: (modelo.fecha_embarque_desde).toISOString(),
      fecha_embarque_hasta: "Mon Jan 20",
      anio: "2020",
      semana: "1",
      cantidad: "50",
      peso: "22",
      largo: "22",
      calidad_minima: "22",
      calidad_maxima: "22",
      precio: "15",
      puerto_salida_id: "12",
      puerto_destino_id: "12",
      vapor_id: "45",
      marca_caja_id: "1",
    } 
    */
   let data = modelo;
   console.log("------------------------------------------------------------------------------")
   console.log(data)
     let response;
     try {
       response = this.http.post < any > (`${this.url}` + "crear-embarque/",
       data, {
           headers: {
             'Content-Type': "application/x-www-form-urlencoded",
             'Authorization': "Token " + this.token,
           }
         }
       );
     } catch (error) {
       console.log("error:" + error)
     }
 
     return response;
   }

   fechaBuscar(fecha:any):any{
    console.log('++++++++token: ',this.token)
    let response;
    console.log('envio fecha Back:::::::::::::::::::::::::: ', fecha);
    let datos={
      'fecha_enfunde':fecha
    }
    console.log('entro en buscar: ', datos)
    try {
      response = this.http.post < any > (`${Constants.API_ENDPOINT}` + "buscar-enfunde/",
      datos,{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + this.token,
        }
      });
    } catch (error) {
      console.log("error:" + error)
    }
    return response;
  }

  listaMarcaCaja():any{
    let response;
    
    try {
      response = this.http.get < any > (`${Constants.API_ENDPOINT}` + "marca-caja-list/",{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + this.token
        }
      }
     
      
      );
     
    } catch (error) {
      console.log("error:" + error)
    }
    
    return response;
  }

  listado(pagina:number, elementos:number, anio: number, sem_desde: number, sem_hasta: number, todos?: boolean ):any {
    let pag:string = pagina.toString();
    let elemen:string = elementos.toString();
    if (todos == undefined) todos = false
    console.log('listado fn: '+pag+', '+elemen);
    let response;
    try {
                 
      response =this.http.get<any>(`${this.url}` + "lista-embarque/pagina="+pag+'/elementos='+elemen+'/anio='+anio+'/sem_desde='+sem_desde+'/sem_hasta='+sem_hasta+'/todos='+todos+'/', {
        headers: {
          'Content-Type': "application/x-www-form-urlencoded",
          'Authorization': "Token " + this.token
        },
      }
      );
    } catch (error) {
      console.log("error:" + error)
    }
    return response;
  }

}

export interface DialogData {
  id :number;
  codigo_alterno:string;
  fecha_embarque_desde: Date;
  fecha_embarque_hasta: Date; 
  anio :string;
  semana: string;
  cantidad: string;
  peso: string;
  largo: string;
  calidad_minima: string;
  calidad_maxima: string;
  precio: number;
  puerto_salida_id : number;
  puerto_destino_id : number;
  vapor_id : number;
  marca_caja_id : number;
}
