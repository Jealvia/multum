import { Injectable } from '@angular/core';
import * as Constants from './constants';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MantenimientoService {

  constructor(
    private http: HttpClient,
  ) { }

  token= localStorage.getItem('token');
  url = Constants.API_ENDPOINT;

  listado(tipoMant: string,pagina:number, elementos:number, nombre:string, todos?:boolean ):any {
    if (nombre == '') {
      nombre = null;
    }
    let pag = pagina; //.toString();
    let elemen  = elementos; //.toString();
    if (todos == undefined) todos = false
    console.log('listado fn: '+pag+', '+elemen);
    let response;
    console.log(`${this.url}` + "lista-"+tipoMant+"/pagina="+pag+'/elementos='+elemen+'/'+tipoMant+'='+nombre+'/');
    try {
      response = this.http.get<any>(`${this.url}` + "lista-"+tipoMant+"/pagina="+pag+'/elementos='+elemen+'/'+tipoMant+'='+nombre+'/todos='+todos+'/', {
        headers: {
          'Content-Type': "application/x-www-form-urlencoded",
          'Authorization': "Token " + this.token
        },
      }
      );

    } catch (error) {
      console.log("error:" + error)
    }

    return response;
  }

  registro(tipoMant: string, id: number, nombre:string): any {
    let data = {
      id: id,
      vapor: nombre,
      puerto: nombre
    }
     let response;
     try {
       response = this.http.post < any > (`${this.url}` + 'registro-'+tipoMant +"/",
       data, {
           headers: {
             'Content-Type': "application/x-www-form-urlencoded"
           }
         }
       );
     } catch (error) {
       console.log("error:" + error)
     }
 
     return response;
   }

  eliminar(tipoMant: string, id: number): any {
    let data = {
      id: id
    }
     let response;
     try {
       response = this.http.post < any > (`${this.url}` + 'eliminar-'+tipoMant +"/",
       data, {
           headers: {
             'Content-Type': "application/x-www-form-urlencoded"
           }
         }
       );
     } catch (error) {
       console.log("error:" + error)
     }
 
     return response;
   }
}

