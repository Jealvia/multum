import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as Constants from './constants';
@Injectable({
  providedIn: 'root'
})
export class ProductoresService {

  constructor(
    private http: HttpClient,
  ) { }

  token= localStorage.getItem('token');
  url = Constants.API_ENDPOINT;

  listado(pagina:number, elementos:number, todos?: boolean):any {
    let pag:string = pagina.toString();
    let elemen:string = elementos.toString();
    if (todos == undefined) todos = false;
    console.log('listado fn: '+pag+', '+elemen);
    let response;
    try {
      response = this.http.get<any>(`${this.url}` + "lista-solicitudes/pagina="+pag+'/elementos='+elemen+'/todos='+todos+'/', {
        headers: {
          'Content-Type': "application/x-www-form-urlencoded",
          'Authorization': "Token " + this.token
        },
      }
      );
    } catch (error) {
      console.log("error:" + error)
    }
    return response;
  }

  regitrarUsuario(dato: number, bandera:boolean): any {
    let data = {
      persona_id: dato,
      aprobado: bandera
    }
    console.log('data: ',data);
     let response;
     try {
       response = this.http.post < any > (`${this.url}` + "aprobar-usuario/",
       data, {
           headers: {
             'Content-Type': "application/x-www-form-urlencoded"
           }
         }
       );
     } catch (error) {
       console.log("error:" + error)
     }
 
     return response;
   }

   aprobado(dato: number): any {
    let data = {
      persona_id: dato
    }
     let response;
     try {
       response = this.http.post < any > (`${this.url}` + "aprobar-usuario/",
       data, {
           headers: {
             'Content-Type': "application/x-www-form-urlencoded"
           }
         }
       );
     } catch (error) {
       console.log("error:" + error)
     }
 
     return response;
   }

}

