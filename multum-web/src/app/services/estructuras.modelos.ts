export interface calibraje {
    finca_id : {
        id: number;
        nombre: string;
        productor_id: string;
    }
    semana: number;
    tipo_calibraje_id: string;
    cinta_id: string;
    cantidad: number; 
}
export interface enfunde {
    finca_id : {
        id: number;
        nombre: string;
        productor_id: string;
    }
    cinta_id: string;
    semana: number;
    cantidad: number; 
    tipo_enfunde_id: string;
}
export interface OrdenCorte {
    finca_id : {
        id: number;
        nombre: string;
        productor_id: string;
    }
    embarque_id: {
        id:number;
        vapor_id: number;
    }
    semana: number;
    cantidad: string;
     
}
export interface persona {
    id: number;
    user: string; //Correo
    nombre: string;
    identificacion: string;
    telefono: string;
    direccion: string;
    persona_id: [boolean];
    fn_productor_id: fincas[];
}
export interface fincas {
    area_total: number;
    area_cultivada: number;
    cajas_por_semana: number;
    codigo_magap: string;
    tipo_finca_id: number;
    sector_id: number;
    productor_id: number;
    nombre: string;
}
export interface Estimado{
    id: number;
    finca_id: {id: number, 
            nombre: string, 
            productor_id: string}; 
    anio: number; 
    semana: number;
    cantidad: number;
    check?: boolean;
}
export interface Estimado_Selected{
    embarque_id: number;
    estimados: number[];
    anio: number;
    semana: number;
}


export interface confirmData {
    titulo: string;
    mensaje: string;
    confirm: boolean;
}


export interface Mantenimiento {
    id: number;
    nombre: string;
}


export interface Embarque {
    id: number;
    codigo_alterno: string;
    fecha_embarque_desde: Date;
    fecha_embarque_hasta: Date;
    anio: number;
    semana: number;
    cantidad: number;
    cantidad_actual?: number;
    peso: number;
    largo: number;
    calidad_minima: number;
    calidad_maxima: number;
    precio: number;
    puerto_salida_id: Mantenimiento;
    puerto_destino_id: Mantenimiento;
    vapor_id: Mantenimiento;
    marca_caja_id: string;
    finalizado: boolean;
}

export interface FincaId {
    id: number;
    nombre: string;
    productor_id: string;
}

export interface Ordencortelist {
    id?: number;
    codigo_bodega: string;
    fecha_corte?: any;
    anio: number;
    semana: number;
    finca_id: FincaId;
    embarque_id: number;
    cantidad: number;
    facturado: boolean;
    estimado_id: number;
}

export interface fullOrdenByEmbarque {
    embarque: Embarque;
    ordencortelist: Ordencortelist[];
}

// interface o modelos para popover y barras
export interface msjSnack {
    mensaje: string;
    type?: string;
    ico?: string;
}