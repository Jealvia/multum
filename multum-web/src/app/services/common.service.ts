import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import * as Constants from './constants';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  token= localStorage.getItem('token');
  constructor(
    private http: HttpClient,
    
  ) { }

  url = Constants.API_ENDPOINT;
  
  listado(url: string, pagina:number, elementos:number, anio:number, sem_desde:number,sem_hasta:number, todos?: boolean):any {
    let pag:string = pagina.toString();
    let elemen:string = elementos.toString();
    if (todos == undefined) todos = false
    //console.log('listado fn: '+pag+', '+elemen);
    let response;
    console.log('***: ',`${this.url}` + url +"/pagina="+pag+'/elementos='+elemen+'/anio='+anio+'/sem_desde='+sem_desde+'/sem_hasta='+sem_hasta+'/');
    try {
      response = this.http.get<any>(`${this.url}` + url+"/pagina="+pag+'/elementos='+elemen+'/anio='+anio+'/sem_desde='+sem_desde+'/sem_hasta='+sem_hasta+'/todos='+todos+'/', {
        headers: {
          'Content-Type': "application/x-www-form-urlencoded",
          'Authorization': "Token " + this.token
        },
      }
      );

    } catch (error) {
      console.log("error:" + error)
    }

    return response;
  }

  almacenarToken(token):any{
    console.log('+++++++token: ', this.token)
    let response;
    let datos={
      'token':String(token)
    }
    try {
      response = this.http.post < any > (`${Constants.API_ENDPOINT}` + "registrar-token-web/",
      datos,{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + this.token,
        }
      });
    } catch (error) {
      console.log("error:" + error)
    }
    return response;
  }
  //2020-01-20T14:54:51.099-05:00
  formatDate(fecha:Date){
    console.warn("Format//////////////////////:", fecha);
    //let result = fecha.getFullYear() + '-'+(fecha.getMonth() + 1) +'-' + fecha.getDate() + 'T00:00:00.099-05:00' 
    let result = fecha.toISOString();
    return result;
  }

  suibirArchivo(file):any{
    let response;
    let datos=file
    console.log('file 2: ', file)
    try {
      response = this.http.post < any > (`${Constants.API_ENDPOINT}` + "subir_archivo/",
      datos,{
         headers: {
         // 'Content-Type': "amultipart/form-data",
        //'Content-Type': "application/x-www-form-urlencoded", 
        //'Authorization': "Token " + this.cookieService.get('token'),
        }
      });
    } catch (error) {
      console.log("error:" + error)
    }
    return response;
  }

  /**
   * 
   * @param compareObject formato{'nombre columna Nuevo':''Nombre columna vieja o en listado'}
   * @param listado Listado de valores, objeto viejo
   */
   objectFormat(compareObject: object, listado: object[]): object[]{
    let lista = listado; 
    let newObject = [];
    let nombrePropiedad: string[];
    
    for (let i = 0; i < lista.length; i++){ // para cada item u objeto de la lista
      let tempObject = {}; //almacena temporalmente el valor del nuevo listado[object]
      // recorre item: {clave:valor} = {claveNueva: claveVieja}
      for (const col  in compareObject) {  //columna, col=clave del nuevo objeto
        const value = compareObject[col]; //valor,  col=clave del viejo objeto
        if (compareObject.hasOwnProperty(value)) { //si ambas col son iguales
          tempObject[col] = lista[i][value]
        }else{
          if (value.indexOf('.') > 0) { 
            nombrePropiedad = value.split('.') 
            let valueObject = nombrePropiedad[0] //clave objecto 
            nombrePropiedad = nombrePropiedad.slice(1); //1er valor de la clave objecto
            for (const key in lista[i][valueObject]) {
              const valores = lista[i][valueObject];
              if ( nombrePropiedad[0]  == key) { 
                tempObject[col] = valores[key]
              }
            }
          }
        }
      }
      newObject[i] = tempObject;
    }
    
    console.log(newObject);
    return newObject;
  }
  
  


  

}
