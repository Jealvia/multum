import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as Constants from './constants';
import { Estimado_Selected } from './estructuras.modelos';


@Injectable({
  providedIn: 'root'
})
export class EstimadosService {

  constructor(
    private http: HttpClient,
  ) { }

  url = Constants.API_ENDPOINT;
  token= localStorage.getItem('token');

  listadoSelect(pagina:number, elementos:number, anio:number, sem_desde:number,sem_hasta:number):any {
    let pag:string = pagina.toString();
    let elemen:string = elementos.toString();
    console.log('listado fn: '+pag+', '+elemen);
    let response;
    console.log(`${this.url}` + "lista-enfunde/pagina="+pag+'/elementos='+elemen+'/anio='+anio+'/sem_desde='+sem_desde+'/sem_hasta='+sem_hasta+'/');
    try {
      response = this.http.get<any>(`${this.url}` + "lista-seleccion-estimados/pagina="+pag+'/elementos='+elemen+'/anio='+anio+'/sem_desde='+sem_desde+'/sem_hasta='+sem_hasta+'/', {
        headers: {
          'Content-Type': "application/x-www-form-urlencoded",
          'Authorization': "Token " + this.token
        },
      }
      );

    } catch (error) {
      console.log("error:" + error)
    }

    return response;
  }

  enviarSelected(modelo:Estimado_Selected):any{
    let response;
    let datos=modelo
    try {
      response = this.http.post < any > (`${Constants.API_ENDPOINT}` + "crear-orden-seleccion/",
      datos,{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        //'Authorization': "Token " + this.cookieService.get('token'),
        }
      });
    } catch (error) {
      console.log("error:" + error)
    }
    return response;
  }


}
