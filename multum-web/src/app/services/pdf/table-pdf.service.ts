import { Injectable } from '@angular/core';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
@Injectable({
  providedIn: 'root'
})
export class TablePdfService {

  constructor(
   
  ) { }

  pdfHeader(titulo: string, usuario: string ): jsPDF{
    let pdf = new jsPDF();
    //fecha
    let fecha = new Date();
    let hoy: string = fecha.getDate()+ "/"+ (fecha.getMonth() + 1) + "/"+ fecha.getFullYear();
    //imagen 
    let logo = new Image();
    logo.src = "/assets/img/multum-logo.png";
    pdf.addImage(logo, 'PNG', 20, 10,40,20);
    //textos:
    pdf.setTextColor('#3C4858');
    pdf.setFontSize(10);
    pdf.text(160,20,"Fecha:"+hoy);
    pdf.text(usuario, 160, 25 );
    pdf.setFontSize(22);
    pdf.setTextColor('#000000');
    pdf.text(70,35,titulo);
    //fin retorn
    return pdf;
  }
  /**
   * 
   * @param titulo 
   * @param usuario 
   * @param columns titulos de la cabecera
   * @param listado lista de valores
   */
  
  generarPdf(titulo: string, usuario: string, columns: string[], listado: any){
  let pdf = new jsPDF();
  pdf = this.pdfHeader(titulo, usuario);
  let data = []; let totalPagesExp = '{total_pages_count_string}';
  for (let i = 0; i < listado.length; i++) {
    const element = listado[i];
    data.push(Object.values(element))
  }
  //definir el ancho de las columnas
  let columnWidth: column[];
  columnWidth = this.columnWidth(columns, titulo)

  const autoTable = 'autoTable'; 
  pdf[autoTable]({
    //head: headRows,
     columns,
    body: data,
    startY: 40,
    showHead: 'firstPage',
    headStyles: { fillColor: "#768190"},
    didDrawPage: function (data) {
       // Footer
      var str = 'Page ' + pdf.internal.getNumberOfPages()
      // Total page number plugin only available in jspdf v1.0+
      if (typeof pdf.putTotalPages === 'function') {
        str = str + ' de ' + totalPagesExp
      }
      pdf.setFontSize(10)
      
      // jsPDF 1.4+ uses getWidth, <1.4 uses .width
      var pageSize = pdf.internal.pageSize
      var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
      pdf.text('© 2020, Derechos Reservados a Sosnegsa, Multum Master.', data.settings.margin.left, pageHeight - 10)
      pdf.text(str, 180, pageHeight - 10)
    },
    columnStyles: columnWidth
    /*
    columnStyles: { 
      0: {columnWidth: 10},
      1: {columnWidth: 50},
      2: {columnWidth: 30},
      3: {columnWidth: 20},
      4: {columnWidth: 30},
      5: {columnWidth: 20},
      6: {columnWidth: 20},
    }
    */
  })

 
  // Total page number plugin only available in jspdf v1.0+
  if (typeof pdf.putTotalPages === 'function') {
    pdf.putTotalPages(totalPagesExp)
  }
  pdf.setFontSize(10);
  pdf.text('Total Registros: '+ data.length, 14, pdf[autoTable].previous.finalY + 10)
  pdf.save(titulo+'.pdf');
  }

  /**
   * 
   * @param listado, lista de datos a presentar
   * @param infoTabla, info de la cabezera y tipos de datos 
   */
  parseColBody(listado: any, infoTabla:any): any[]{ 
    let newList = new Array(); // retorno
    let colHeader = Object.keys(infoTabla); // para obtener las claves del objeto
    let objColHeader = infoTabla;
    
    console.log('tipe: ', typeof(infoTabla.persona_id))
    for (let i = 0; i < listado.length; i++) {
      const element = listado[i];
      objColHeader[colHeader[0]] = i+1;
      for (let j = 0; j < colHeader.length; j++) {
        const item = colHeader[j];
        if (j > 0) { // para que ignore '#'
        
          objColHeader[item] = typeof(element[item]) === null? '': this.dataTableType(element[item]);
          
        }
      }
      newList[i] = Object.values(objColHeader);
    }
    console.log('arrayN: ', newList);
    return newList;
  }
  /**
   * preparando elementos para el (listado Array), que construye la table pdf
   * @param element, a operar recibe number, boolean y object.
   */
  dataTableType(element: any){
    if (typeof(element) === 'string') { // 
      if (element.includes('00Z')) { // alternativa de tipo Date
        let fecha = element.split('-') 
        element = fecha[2].slice(0,2)+ "/" + fecha[1]+ "/" + fecha[0];
        return element;
      }
      return element;
    }
    if (typeof(element) === 'number') {
      return element;
    }
    if (typeof(element) === 'boolean') {
      return element?'Si':'No ';
    }
    if (typeof(element) === 'object') {
      if (element.length == 1 && typeof(element[0]) === 'boolean') { //Para el caso de productor que tiene [boolean]
        return element[0]?'Si':'No';
      } else {
        return element.length
      }
    }
    
  }
  columnWidth(columns: string[], titulo: string):column[]{
    let columnWidths: column[];

    //la suma del ancho de todas las columnas de sumar 190
    if (titulo == 'Listado de Productores') {
      columnWidths = [
        {columnWidth: 10},
        {columnWidth: 30},
        {columnWidth: 40},
        {columnWidth: 30},
        {columnWidth: 40},
        {columnWidth: 15},
        {columnWidth: 25},
      ]
    }
    if (titulo == 'Listado de Enfunde' || titulo == 'Listado de Calibraje') {
      columnWidths = [
        {columnWidth: 10},
        {columnWidth: 60},
        {columnWidth: 35},
        {columnWidth: 20},
        {columnWidth: 30},
        {columnWidth: 15},
        {columnWidth: 20},
      ]
    }
    if (titulo == 'Listado de Estimado') {
      columnWidths = [
        {columnWidth: 10},
        {columnWidth: 70},
        {columnWidth: 50},
        {columnWidth: 30},
        {columnWidth: 30},
      ]
    }
    if (titulo == 'Listado de Embarque') {
      columnWidths = [
        {columnWidth: 10},
        {columnWidth: 40},
        {columnWidth: 30},
        {columnWidth: 30},
        {columnWidth: 40},
        {columnWidth: 40},
      ]
    }
    return columnWidths
  }

  
}

export interface column{
    columnWidth: number;
  
  
}
/*
generarPdf2(titulo: string, usuario: string, columns: string[], listado: any){
    let pdf = new jsPDF();
    pdf = this.pdfHeader(titulo, usuario);
    let data = [];
    for (let i = 0; i < listado.length; i++) {
      const element = listado[i];
      data.push(Object.values(element))
    }
    //console.log('listado: ', data);
    let numMarginTop: 40;
    const autoTable = 'autoTable'; 
    // sintaxis correcta es pdf.autoTable mas da error por eso esta opcion:
    // info: C:\proyectos\Multum\multum-web\node_modules\jspdf-autotable\dist\index.d.ts o node_modules\jspdf-autotable\dist\index.d.ts
    pdf[autoTable]( columns,data,
      { margin:{ top: numMarginTop  },
      headStyles: { fillColor: "#768190"},
     
      }
    );
    pdf.save(titulo+'.pdf');

  }

 pdf[autoTable]( columns,data,
      { margin:{ top: 40  },
      //styles: { fillColor: "#768190" },
      headStyles: { fillColor: "#768190"},
      didParseCell: function (table) {
        if (cont == columns.length) {
          return;
        }
        cont = cont +1;
        console.log('>>>>>>tabla row head: ',table.section, ' : ',cont)
        
        if (cont <= columns.length) {
          table.cell.styles.fillColor = "#768190";
        }
        
        if (table.section === 'head') {
          table.cell.styles.textColor = '#000000';
          console.log('tabla row: ',table)
          //debugger
          table.cell.styles.fillColor = "#768190";
        }
        
      }
      //theme: 'grid',
      //styles:{fillColor:'#999999', lineColor: '#ffffff'}
      //columnStyles: {0: {halign: 'center', fillColor: [0, 255, 0]}}
      }
    );
*/
