import {
    Injectable
} from '@angular/core';
import {
    HttpClient
} from '@angular/common/http';
import * as Constants from './constants';
import * as firebase from 'firebase';

import 'rxjs/add/operator/take';
import {
    BehaviorSubject
} from 'rxjs/BehaviorSubject'
@Injectable({
    providedIn: 'root'
})
@Injectable()
export class NotificacionesService {
    messaging = firebase.messaging()
    currentMessage = new BehaviorSubject(null)
    token = localStorage.getItem('token');

    constructor(
        private http: HttpClient,

    ) {}

    updateToken(token) {

    }

    getPermission() {
        this.messaging.requestPermission()
            .then(() => {
                console.log('Notification permission granted.');
                return this.messaging.getToken()
            })
            .then(token => {
                console.log(token)
                this.updateToken(token)
            })
            .catch((err) => {
                console.log('Unable to get permission to notify.', err);
            });
    }

    receiveMessage() {
        this.messaging.onMessage((payload) => {
            console.log("Message received. ", payload);
            this.currentMessage.next(payload)
        });
    }

}