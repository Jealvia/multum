import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as Constants from './constants';
@Injectable({
  providedIn: 'root'
})
export class OrdenCorteService {

  constructor(
    private http: HttpClient,
  ) { }
  //lista-orden-corte/pagina=/elementos=/anio=/sem_desde=/sem_hasta=/
  token= localStorage.getItem('token');
  url = Constants.API_ENDPOINT;

  listado(pagina:number, elementos:number,anio:number, sem_desde:number,sem_hasta:number, embarqueId: number):any {
    let pag:string = pagina.toString();
    let elemen:string = elementos.toString();
    console.log('listado fn: '+pag+', '+elemen);
    let response;

    try {
      response = this.http.get<any>(`${this.url}` + "lista-orden-corte/pagina="+pag+'/elementos='+elemen+'/anio='+anio+'/sem_desde='+sem_desde+'/sem_hasta='+sem_hasta+'/'+'embarque_id='+embarqueId+'/', {
        headers: {
          'Content-Type': "application/x-www-form-urlencoded",
          'Authorization': "Token " + this.token
        },
      }
      );

    } catch (error) {
      console.log("error:" + error)
    }

    return response;
  }

  listOrdenByEmbarque(id:number){
    let response;
    let datos =  {id: id}; 
    console.log('envio id: ',id)
    try {
      response = this.http.post < any > (`${Constants.API_ENDPOINT}` + "vista-embarque-orden/",
      datos,{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + this.token,
        }
      });
    } catch (error) {
      console.log("error:" + error)
    }
    return response;
  }

  guardarFinalizar(data: saveOrden){
    let response;
    let datos =   data;
    try {
      response = this.http.post < any > (`${Constants.API_ENDPOINT}` + "finalizar-orden-corte/",
      datos,{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + this.token,
        }
      });
    } catch (error) {
      console.log("error:" + error)
    }
    return response;
  }

  eliminarOdenFromEmbarque(id: number){
    let response;
    
    let datos =   {'id':id};
    console.log('id: ', id)
    try {
      response = this.http.post < any > (`${Constants.API_ENDPOINT}` + "eliminar-orden-corte/",
      datos,{
         headers: {
        'Content-Type': "application/x-www-form-urlencoded",
        'Authorization': "Token " + this.token,
        }
      });
    } catch (error) {
      console.log("error:" + error)
    }
    return response;
  }

}

export interface saveOrden{
  "embarqueId": number,
  "total": number,
  "finalizado": boolean,
  "ordencortelist": 
    {"id": number, "valor": number }[]
  
}
