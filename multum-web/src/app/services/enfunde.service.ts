import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as Constants from './constants';
@Injectable({
  providedIn: 'root'
})
export class EnfundeService {

  constructor(
    private http: HttpClient,
  ) {

  }

  token= localStorage.getItem('token');
  url = Constants.API_ENDPOINT;

  listado(pagina:number, elementos:number, anio:number, sem_desde:number,sem_hasta:number, todos?: boolean):any {
    let pag:string = pagina.toString();
    let elemen:string = elementos.toString();
    console.log('listado fn: '+pag+', '+elemen);
    let response;
    console.log(`${this.url}` + "lista-enfunde/pagina="+pag+'/elementos='+elemen+'/anio='+anio+'/sem_desde='+sem_desde+'/sem_hasta='+sem_hasta+'/todos='+todos+'/');
    try {
      response = this.http.get<any>(`${this.url}` + "lista-enfunde/pagina="+pag+'/elementos='+elemen+'/anio='+anio+'/sem_desde='+sem_desde+'/sem_hasta='+sem_hasta+'/todos='+todos+'/', {
        headers: {
          'Content-Type': "application/x-www-form-urlencoded",
          'Authorization': "Token " + this.token
        },
      }
      );

    } catch (error) {
      console.log("error:" + error)
    }

    return response;
  }

}
