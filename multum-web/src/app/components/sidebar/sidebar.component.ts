import { Component, OnInit } from '@angular/core';
import * as Constants from '../../services/constants';
import { Router } from '@angular/router';
import { ConfirmComponent } from '../popover/confirm/confirm.component';
import { MatDialog } from '@angular/material/dialog';
  
declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
  /*
    { path: '/dashboard', title: 'Dashboard',  icon: 'dashboard', class: '' },
    { path: '/user-profile', title: 'User Profile',  icon:'person', class: '' },
    { path: '/table-list', title: 'Table List',  icon:'content_paste', class: '' },
    { path: '/typography', title: 'Typography',  icon:'library_books', class: '' },
    { path: '/icons', title: 'Icons',  icon:'bubble_chart', class: '' },
    { path: '/maps', title: 'Maps',  icon:'location_on', class: '' },
    { path: '/notifications', title: 'Notifications',  icon:'notifications', class: '' },
  */
    { path: '/main/Inicio', title: 'inicio',  icon:'home', class: '' },
    { path: '/main/Productores', title: 'Productores',  icon:'group', class: '' },
    //{ path: '/main/Finca', title: 'Finca',  icon:'unarchive', class: '' },
    { path: '/main/Enfunde', title: 'Enfunde',  icon:'unarchive', class: '' },
    { path: '/main/Calibraje', title: 'Calibraje',  icon:'timeline', class: '' }, 
    { path: '/main/Estimado', title: 'Estimado',  icon:'post_add', class: '' }, 
    { path: '/main/Embarque', title: 'Embarque',  icon:'layers', class: '' },
    //{ path: '/main/OrdenCorte', title: 'Orden de Corte',  icon:'crop', class: '' },

    { path: '/main/Vapor', title: 'Vapor',  icon:'directions_boat', class: '' },
    { path: '/main/Puerto', title: 'Puerto',  icon:'where_to_vote', class: '' },
    //{ path: '/main/Estadisticas', title: 'Estadísticas',  icon:'unarchive', class: '' },
    //{ path: '/main/EmbarquePlus', title: 'EmbarquePlus',  icon:'unarchive', class: '' },
    /*
    
    { path: '/main/Contactos', title: 'Contactos',  icon:'unarchive', class: '' },
    { path: '/main/ControlCajas', title: 'Control Cajas',  icon:'unarchive', class: '' },
    { path: '/main/upgrade', title: 'Upgrade to PRO',  icon:'unarchive', class: '' },
    */
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  contantes = Constants; 
  menuItems: any[];

  constructor(
    private router: Router,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };

  closeSeccion(){
    //this.cookieService.delete('test');
    localStorage.clear();
    this.router.navigate(['/account/login']);
}
openConfirm( ): void {

    let confirmData = {
      titulo: 'Desea Salir de Multum',
      mensaje: 'Por Favor Confime.',
      confirm: null
    }
    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '250px',
      data: confirmData
    });

    dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed', result);
        if (result == null || result == 'undefined' || result.confirm == false) {
            console.log('no Confirmo!!');
        } else {
            this.closeSeccion();
        }
    });

}
}
