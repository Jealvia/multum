import { Component, Inject} from '@angular/core';
import {MAT_SNACK_BAR_DATA} from '@angular/material/snack-bar';
import {msjSnack} from '../../../services/estructuras.modelos'
@Component({
  selector: 'app-msj-snack',
  templateUrl: './msj-snack.component.html',
  styleUrls: ['./msj-snack.component.scss']
})
export class MsjSnackComponent {

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: msjSnack) { }

}
