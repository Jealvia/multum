import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'subheader-bar',
  templateUrl: './subheader-bar.component.html',
  styleUrls: ['./subheader-bar.component.scss']
})
export class SubheaderBarComponent implements OnInit {

  nombre: string = this._activatedRoute.url['value'][this._activatedRoute.url['value'].length - 1]['path'];
  constructor(
    private _activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
  }

  fnNombre(){
    if (this.nombre == 'Estadisticas') {
      this.nombre = 'Estadísticas'
    }
    if (parseInt(this.nombre)) {
      this.nombre = this._activatedRoute.url['value'][this._activatedRoute.url['value'].length - 2]['path'];
      
    }
    if (this.nombre == 'OrdenCorte') {
      this.nombre = 'Orden de Corte';
    }
    return this.nombre;
  }
}
