import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Estimado, Estimado_Selected } from 'app/services/estructuras.modelos';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { CommonService } from 'app/services/common.service';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
@Component({
  selector: 'app-lista-estimado',
  templateUrl: './lista-estimado.component.html',
  styleUrls: ['./lista-estimado.component.scss']
})
export class ListaEstimadoComponent implements OnInit {
  listado : Array<Estimado>;
  listaSelectEstimados: number[]= [0];


  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  // MatPaginator Inputs
  pagineo: PageEvent = {
    pageIndex: 0,
    pageSize: 10,
    length: 0
  };
  pageSizeOptions: number[] = [10, 20, 50];

   // MatPaginator Output
   pageEvent: PageEvent;
   
  constructor(
    private commonService: CommonService,
    public dialogRef: MatDialogRef<ListaEstimadoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Estimado_Selected,
    ) {}
  
  
  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    console.log('iniciando');
    this.getListado(1,10);
  }

  getListado(pagina: number, elementos: number){
    let url: string = 'lista-seleccion-estimados';
    this.commonService.listado(url,pagina,elementos, this.data.anio, this.data.semana, this.data.semana).subscribe(
      res =>{
        console.log('respuesta listado: ',res);
          this.listado = res.data;
          console.log('respuesta: ', res );
          this.pagineo.length = res.length;
          //console.log('respuesta: ')
      }
    )
  }

  actualizarPage(pagineo:PageEvent){
    console.log('page: ',pagineo);
    this.getListado(pagineo.pageIndex +1,pagineo.pageSize);
    
  }

  filtrar(){
    this.getListado(this.pagineo.pageIndex +1,this.pagineo.pageSize);
  }

  tomarEstimado(indice:number, estimado: Estimado, check: boolean){
    //debugger;
    if (check) {
      if ( this.listaSelectEstimados[0] == 0) {
        this.listaSelectEstimados[0] = estimado.id;
      }else{
        this.listaSelectEstimados.push(estimado.id);
      }
    }else{
      this.listaSelectEstimados.splice(indice,1);
    }
    console.log('check:' + check+' listaselect: ' +this.listaSelectEstimados);
    this.data.estimados = this.listaSelectEstimados;
  }
}


