import { Component,  Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent  {

  constructor(
    public dialogRef: MatDialogRef<ConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: confirmData) {}

    onNoClick(): void {
      this.data.confirm = false;
      this.dialogRef.close(this.data);
    }

}
export interface confirmData {
  titulo: string;
  mensaje: string;
  confirm: boolean;
}