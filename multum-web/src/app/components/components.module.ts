import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ConfirmComponent } from './popover/confirm/confirm.component';
import { apiMaterialModule } from 'app/apis/material-module';
import { SubheaderBarComponent } from './barras/subheader-bar/subheader-bar.component';
import { MsjSnackComponent } from './barras/msj-snack/msj-snack.component';
//import { ListaEstimadoPopover } from './popover/lista-estimado/lista-estimado.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    apiMaterialModule
  ],
  declarations: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    ConfirmComponent,
    SubheaderBarComponent,
    MsjSnackComponent,
    //ListaEstimadoPopover
  ],
  exports: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    ConfirmComponent,
    SubheaderBarComponent,
    MsjSnackComponent,
    //ListaEstimadoPopover
  ],
  entryComponents: [
    ConfirmComponent,
    MsjSnackComponent,
    //ListaEstimadoPopover
  ]
})
export class ComponentsModule { }
