// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
/* importScripts('https://www.gstatic.com/firebasejs/6.3.4/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/6.3.4/firebase-messaging.js'); */
importScripts('https://www.gstatic.com/firebasejs/7.7.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.7.0/firebase-messaging.js');

/* importScripts('https://www.gstatic.com/firebasejs/5.5.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.5.0/firebase-messaging.js'); */


// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.


firebase.initializeApp({
  'apiKey': "AIzaSyBin75iIJsYVsE1ZBUUbfBTT_pl5T0y-KE",
  'authDomain': "multum-5dbee.firebaseapp.com",
  'databaseURL': "https://multum-5dbee.firebaseio.com",
  'projectId': "multum-5dbee",
  'storageBucket': "multum-5dbee.appspot.com",
  'messagingSenderId': "869628666553",
  'appId': "1:869628666553:web:190b4f33e1ff8510887168"
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();
messaging.usePublicVapidKey("BPKDcVXskCbHumvEK6g-3at96ftmY5YdXfQ0xQH5Yn5F1IX3XrUE3Llrmht3dMwCtHuNj6gWs-V2kwX0czvQzOY");
messaging.setBackgroundMessageHandler(function (payload) {
  const title="Hello world";
  const options={
    body:payload.data.status
  }
  return self.registration.showNotification(title,options);
})

/* messaging.onMessage((payload) => {
  console.log("Message received. ", payload);
}); */