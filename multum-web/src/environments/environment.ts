// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyBin75iIJsYVsE1ZBUUbfBTT_pl5T0y-KE",
  authDomain: "multum-5dbee.firebaseapp.com",
  databaseURL: "https://multum-5dbee.firebaseio.com",
  projectId: "multum-5dbee",
  storageBucket: "multum-5dbee.appspot.com",
  messagingSenderId: "869628666553",
  appId: "1:869628666553:web:190b4f33e1ff8510887168"
  }
};
